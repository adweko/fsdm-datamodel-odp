PROCEDURE "sap.fsdm.procedures::SectionCoverageDelete" (IN ROW "sap.fsdm.tabletypes::SectionCoverageTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CoverageID=' || TO_VARCHAR("CoverageID") || ' ' ||
                'SectionID=' || TO_VARCHAR("SectionID") || ' ' ||
                '_ReinsuranceContract.FinancialContractID=' || TO_VARCHAR("_ReinsuranceContract.FinancialContractID") || ' ' ||
                '_ReinsuranceContract.IDSystem=' || TO_VARCHAR("_ReinsuranceContract.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CoverageID",
                        "IN"."SectionID",
                        "IN"."_ReinsuranceContract.FinancialContractID",
                        "IN"."_ReinsuranceContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CoverageID",
                        "IN"."SectionID",
                        "IN"."_ReinsuranceContract.FinancialContractID",
                        "IN"."_ReinsuranceContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CoverageID",
                        "SectionID",
                        "_ReinsuranceContract.FinancialContractID",
                        "_ReinsuranceContract.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CoverageID",
                                    "IN"."SectionID",
                                    "IN"."_ReinsuranceContract.FinancialContractID",
                                    "IN"."_ReinsuranceContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CoverageID",
                                    "IN"."SectionID",
                                    "IN"."_ReinsuranceContract.FinancialContractID",
                                    "IN"."_ReinsuranceContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "CoverageID" is null and
            "SectionID" is null and
            "_ReinsuranceContract.FinancialContractID" is null and
            "_ReinsuranceContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::SectionCoverage" (
        "CoverageID",
        "SectionID",
        "_ReinsuranceContract.FinancialContractID",
        "_ReinsuranceContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AccountingBasis",
        "AnnualAggregateDeductibleAmount",
        "AnnualAggregateLimitAmount",
        "BoundDate",
        "BusinessType",
        "CapacityAmount",
        "CapacityLossRatioPercent",
        "CedentClassOfInsuranceIndicator",
        "CessionBasis",
        "ClaimMadeType",
        "CleanCutIndicator",
        "CoverageBoundDate",
        "CoverageDescription",
        "CoverageEndDate",
        "CoverageStartDate",
        "Currency",
        "DeductibleAmount",
        "DeductibleLossRatioPercent",
        "Description",
        "DiscountingType",
        "EndDate",
        "ExtraContractualObligationAmount",
        "GroupInsurancePolicyIndicator",
        "InsurancePolicyDistributionChannel",
        "IsNonProportionalSection",
        "IsProportionalSection",
        "LifecycleStatus",
        "LimitAmount",
        "LimitCurrency",
        "MaximumAmount",
        "MaximumCurrency",
        "MaximumLiabilityAmount",
        "NumberOfMaxima",
        "PaymentCurrency",
        "QuotaSharePercent",
        "RankOrdinalNumber",
        "ReferenceBasis",
        "ReinsuranceCalculationMethod",
        "ReinsuranceTechnicalType",
        "StartDate",
        "Sunrise",
        "Sunset",
        "UnderwritingConfederation",
        "UnderwritingCountry",
        "UnderwritingEventLimitAmount",
        "UnderwritingLimitAmount",
        "WithProfitIndicator",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CoverageID" as "CoverageID" ,
            "OLD_SectionID" as "SectionID" ,
            "OLD__ReinsuranceContract.FinancialContractID" as "_ReinsuranceContract.FinancialContractID" ,
            "OLD__ReinsuranceContract.IDSystem" as "_ReinsuranceContract.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AccountingBasis" as "AccountingBasis" ,
            "OLD_AnnualAggregateDeductibleAmount" as "AnnualAggregateDeductibleAmount" ,
            "OLD_AnnualAggregateLimitAmount" as "AnnualAggregateLimitAmount" ,
            "OLD_BoundDate" as "BoundDate" ,
            "OLD_BusinessType" as "BusinessType" ,
            "OLD_CapacityAmount" as "CapacityAmount" ,
            "OLD_CapacityLossRatioPercent" as "CapacityLossRatioPercent" ,
            "OLD_CedentClassOfInsuranceIndicator" as "CedentClassOfInsuranceIndicator" ,
            "OLD_CessionBasis" as "CessionBasis" ,
            "OLD_ClaimMadeType" as "ClaimMadeType" ,
            "OLD_CleanCutIndicator" as "CleanCutIndicator" ,
            "OLD_CoverageBoundDate" as "CoverageBoundDate" ,
            "OLD_CoverageDescription" as "CoverageDescription" ,
            "OLD_CoverageEndDate" as "CoverageEndDate" ,
            "OLD_CoverageStartDate" as "CoverageStartDate" ,
            "OLD_Currency" as "Currency" ,
            "OLD_DeductibleAmount" as "DeductibleAmount" ,
            "OLD_DeductibleLossRatioPercent" as "DeductibleLossRatioPercent" ,
            "OLD_Description" as "Description" ,
            "OLD_DiscountingType" as "DiscountingType" ,
            "OLD_EndDate" as "EndDate" ,
            "OLD_ExtraContractualObligationAmount" as "ExtraContractualObligationAmount" ,
            "OLD_GroupInsurancePolicyIndicator" as "GroupInsurancePolicyIndicator" ,
            "OLD_InsurancePolicyDistributionChannel" as "InsurancePolicyDistributionChannel" ,
            "OLD_IsNonProportionalSection" as "IsNonProportionalSection" ,
            "OLD_IsProportionalSection" as "IsProportionalSection" ,
            "OLD_LifecycleStatus" as "LifecycleStatus" ,
            "OLD_LimitAmount" as "LimitAmount" ,
            "OLD_LimitCurrency" as "LimitCurrency" ,
            "OLD_MaximumAmount" as "MaximumAmount" ,
            "OLD_MaximumCurrency" as "MaximumCurrency" ,
            "OLD_MaximumLiabilityAmount" as "MaximumLiabilityAmount" ,
            "OLD_NumberOfMaxima" as "NumberOfMaxima" ,
            "OLD_PaymentCurrency" as "PaymentCurrency" ,
            "OLD_QuotaSharePercent" as "QuotaSharePercent" ,
            "OLD_RankOrdinalNumber" as "RankOrdinalNumber" ,
            "OLD_ReferenceBasis" as "ReferenceBasis" ,
            "OLD_ReinsuranceCalculationMethod" as "ReinsuranceCalculationMethod" ,
            "OLD_ReinsuranceTechnicalType" as "ReinsuranceTechnicalType" ,
            "OLD_StartDate" as "StartDate" ,
            "OLD_Sunrise" as "Sunrise" ,
            "OLD_Sunset" as "Sunset" ,
            "OLD_UnderwritingConfederation" as "UnderwritingConfederation" ,
            "OLD_UnderwritingCountry" as "UnderwritingCountry" ,
            "OLD_UnderwritingEventLimitAmount" as "UnderwritingEventLimitAmount" ,
            "OLD_UnderwritingLimitAmount" as "UnderwritingLimitAmount" ,
            "OLD_WithProfitIndicator" as "WithProfitIndicator" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."CoverageID",
                        "OLD"."SectionID",
                        "OLD"."_ReinsuranceContract.FinancialContractID",
                        "OLD"."_ReinsuranceContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."CoverageID" AS "OLD_CoverageID" ,
                "OLD"."SectionID" AS "OLD_SectionID" ,
                "OLD"."_ReinsuranceContract.FinancialContractID" AS "OLD__ReinsuranceContract.FinancialContractID" ,
                "OLD"."_ReinsuranceContract.IDSystem" AS "OLD__ReinsuranceContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AccountingBasis" AS "OLD_AccountingBasis" ,
                "OLD"."AnnualAggregateDeductibleAmount" AS "OLD_AnnualAggregateDeductibleAmount" ,
                "OLD"."AnnualAggregateLimitAmount" AS "OLD_AnnualAggregateLimitAmount" ,
                "OLD"."BoundDate" AS "OLD_BoundDate" ,
                "OLD"."BusinessType" AS "OLD_BusinessType" ,
                "OLD"."CapacityAmount" AS "OLD_CapacityAmount" ,
                "OLD"."CapacityLossRatioPercent" AS "OLD_CapacityLossRatioPercent" ,
                "OLD"."CedentClassOfInsuranceIndicator" AS "OLD_CedentClassOfInsuranceIndicator" ,
                "OLD"."CessionBasis" AS "OLD_CessionBasis" ,
                "OLD"."ClaimMadeType" AS "OLD_ClaimMadeType" ,
                "OLD"."CleanCutIndicator" AS "OLD_CleanCutIndicator" ,
                "OLD"."CoverageBoundDate" AS "OLD_CoverageBoundDate" ,
                "OLD"."CoverageDescription" AS "OLD_CoverageDescription" ,
                "OLD"."CoverageEndDate" AS "OLD_CoverageEndDate" ,
                "OLD"."CoverageStartDate" AS "OLD_CoverageStartDate" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."DeductibleAmount" AS "OLD_DeductibleAmount" ,
                "OLD"."DeductibleLossRatioPercent" AS "OLD_DeductibleLossRatioPercent" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."DiscountingType" AS "OLD_DiscountingType" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."ExtraContractualObligationAmount" AS "OLD_ExtraContractualObligationAmount" ,
                "OLD"."GroupInsurancePolicyIndicator" AS "OLD_GroupInsurancePolicyIndicator" ,
                "OLD"."InsurancePolicyDistributionChannel" AS "OLD_InsurancePolicyDistributionChannel" ,
                "OLD"."IsNonProportionalSection" AS "OLD_IsNonProportionalSection" ,
                "OLD"."IsProportionalSection" AS "OLD_IsProportionalSection" ,
                "OLD"."LifecycleStatus" AS "OLD_LifecycleStatus" ,
                "OLD"."LimitAmount" AS "OLD_LimitAmount" ,
                "OLD"."LimitCurrency" AS "OLD_LimitCurrency" ,
                "OLD"."MaximumAmount" AS "OLD_MaximumAmount" ,
                "OLD"."MaximumCurrency" AS "OLD_MaximumCurrency" ,
                "OLD"."MaximumLiabilityAmount" AS "OLD_MaximumLiabilityAmount" ,
                "OLD"."NumberOfMaxima" AS "OLD_NumberOfMaxima" ,
                "OLD"."PaymentCurrency" AS "OLD_PaymentCurrency" ,
                "OLD"."QuotaSharePercent" AS "OLD_QuotaSharePercent" ,
                "OLD"."RankOrdinalNumber" AS "OLD_RankOrdinalNumber" ,
                "OLD"."ReferenceBasis" AS "OLD_ReferenceBasis" ,
                "OLD"."ReinsuranceCalculationMethod" AS "OLD_ReinsuranceCalculationMethod" ,
                "OLD"."ReinsuranceTechnicalType" AS "OLD_ReinsuranceTechnicalType" ,
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."Sunrise" AS "OLD_Sunrise" ,
                "OLD"."Sunset" AS "OLD_Sunset" ,
                "OLD"."UnderwritingConfederation" AS "OLD_UnderwritingConfederation" ,
                "OLD"."UnderwritingCountry" AS "OLD_UnderwritingCountry" ,
                "OLD"."UnderwritingEventLimitAmount" AS "OLD_UnderwritingEventLimitAmount" ,
                "OLD"."UnderwritingLimitAmount" AS "OLD_UnderwritingLimitAmount" ,
                "OLD"."WithProfitIndicator" AS "OLD_WithProfitIndicator" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SectionCoverage" as "OLD"
            on
                      "IN"."CoverageID" = "OLD"."CoverageID" and
                      "IN"."SectionID" = "OLD"."SectionID" and
                      "IN"."_ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceContract.FinancialContractID" and
                      "IN"."_ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::SectionCoverage" (
        "CoverageID",
        "SectionID",
        "_ReinsuranceContract.FinancialContractID",
        "_ReinsuranceContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AccountingBasis",
        "AnnualAggregateDeductibleAmount",
        "AnnualAggregateLimitAmount",
        "BoundDate",
        "BusinessType",
        "CapacityAmount",
        "CapacityLossRatioPercent",
        "CedentClassOfInsuranceIndicator",
        "CessionBasis",
        "ClaimMadeType",
        "CleanCutIndicator",
        "CoverageBoundDate",
        "CoverageDescription",
        "CoverageEndDate",
        "CoverageStartDate",
        "Currency",
        "DeductibleAmount",
        "DeductibleLossRatioPercent",
        "Description",
        "DiscountingType",
        "EndDate",
        "ExtraContractualObligationAmount",
        "GroupInsurancePolicyIndicator",
        "InsurancePolicyDistributionChannel",
        "IsNonProportionalSection",
        "IsProportionalSection",
        "LifecycleStatus",
        "LimitAmount",
        "LimitCurrency",
        "MaximumAmount",
        "MaximumCurrency",
        "MaximumLiabilityAmount",
        "NumberOfMaxima",
        "PaymentCurrency",
        "QuotaSharePercent",
        "RankOrdinalNumber",
        "ReferenceBasis",
        "ReinsuranceCalculationMethod",
        "ReinsuranceTechnicalType",
        "StartDate",
        "Sunrise",
        "Sunset",
        "UnderwritingConfederation",
        "UnderwritingCountry",
        "UnderwritingEventLimitAmount",
        "UnderwritingLimitAmount",
        "WithProfitIndicator",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CoverageID" as "CoverageID",
            "OLD_SectionID" as "SectionID",
            "OLD__ReinsuranceContract.FinancialContractID" as "_ReinsuranceContract.FinancialContractID",
            "OLD__ReinsuranceContract.IDSystem" as "_ReinsuranceContract.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AccountingBasis" as "AccountingBasis",
            "OLD_AnnualAggregateDeductibleAmount" as "AnnualAggregateDeductibleAmount",
            "OLD_AnnualAggregateLimitAmount" as "AnnualAggregateLimitAmount",
            "OLD_BoundDate" as "BoundDate",
            "OLD_BusinessType" as "BusinessType",
            "OLD_CapacityAmount" as "CapacityAmount",
            "OLD_CapacityLossRatioPercent" as "CapacityLossRatioPercent",
            "OLD_CedentClassOfInsuranceIndicator" as "CedentClassOfInsuranceIndicator",
            "OLD_CessionBasis" as "CessionBasis",
            "OLD_ClaimMadeType" as "ClaimMadeType",
            "OLD_CleanCutIndicator" as "CleanCutIndicator",
            "OLD_CoverageBoundDate" as "CoverageBoundDate",
            "OLD_CoverageDescription" as "CoverageDescription",
            "OLD_CoverageEndDate" as "CoverageEndDate",
            "OLD_CoverageStartDate" as "CoverageStartDate",
            "OLD_Currency" as "Currency",
            "OLD_DeductibleAmount" as "DeductibleAmount",
            "OLD_DeductibleLossRatioPercent" as "DeductibleLossRatioPercent",
            "OLD_Description" as "Description",
            "OLD_DiscountingType" as "DiscountingType",
            "OLD_EndDate" as "EndDate",
            "OLD_ExtraContractualObligationAmount" as "ExtraContractualObligationAmount",
            "OLD_GroupInsurancePolicyIndicator" as "GroupInsurancePolicyIndicator",
            "OLD_InsurancePolicyDistributionChannel" as "InsurancePolicyDistributionChannel",
            "OLD_IsNonProportionalSection" as "IsNonProportionalSection",
            "OLD_IsProportionalSection" as "IsProportionalSection",
            "OLD_LifecycleStatus" as "LifecycleStatus",
            "OLD_LimitAmount" as "LimitAmount",
            "OLD_LimitCurrency" as "LimitCurrency",
            "OLD_MaximumAmount" as "MaximumAmount",
            "OLD_MaximumCurrency" as "MaximumCurrency",
            "OLD_MaximumLiabilityAmount" as "MaximumLiabilityAmount",
            "OLD_NumberOfMaxima" as "NumberOfMaxima",
            "OLD_PaymentCurrency" as "PaymentCurrency",
            "OLD_QuotaSharePercent" as "QuotaSharePercent",
            "OLD_RankOrdinalNumber" as "RankOrdinalNumber",
            "OLD_ReferenceBasis" as "ReferenceBasis",
            "OLD_ReinsuranceCalculationMethod" as "ReinsuranceCalculationMethod",
            "OLD_ReinsuranceTechnicalType" as "ReinsuranceTechnicalType",
            "OLD_StartDate" as "StartDate",
            "OLD_Sunrise" as "Sunrise",
            "OLD_Sunset" as "Sunset",
            "OLD_UnderwritingConfederation" as "UnderwritingConfederation",
            "OLD_UnderwritingCountry" as "UnderwritingCountry",
            "OLD_UnderwritingEventLimitAmount" as "UnderwritingEventLimitAmount",
            "OLD_UnderwritingLimitAmount" as "UnderwritingLimitAmount",
            "OLD_WithProfitIndicator" as "WithProfitIndicator",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."CoverageID",
                        "OLD"."SectionID",
                        "OLD"."_ReinsuranceContract.FinancialContractID",
                        "OLD"."_ReinsuranceContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."CoverageID" AS "OLD_CoverageID" ,
                "OLD"."SectionID" AS "OLD_SectionID" ,
                "OLD"."_ReinsuranceContract.FinancialContractID" AS "OLD__ReinsuranceContract.FinancialContractID" ,
                "OLD"."_ReinsuranceContract.IDSystem" AS "OLD__ReinsuranceContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AccountingBasis" AS "OLD_AccountingBasis" ,
                "OLD"."AnnualAggregateDeductibleAmount" AS "OLD_AnnualAggregateDeductibleAmount" ,
                "OLD"."AnnualAggregateLimitAmount" AS "OLD_AnnualAggregateLimitAmount" ,
                "OLD"."BoundDate" AS "OLD_BoundDate" ,
                "OLD"."BusinessType" AS "OLD_BusinessType" ,
                "OLD"."CapacityAmount" AS "OLD_CapacityAmount" ,
                "OLD"."CapacityLossRatioPercent" AS "OLD_CapacityLossRatioPercent" ,
                "OLD"."CedentClassOfInsuranceIndicator" AS "OLD_CedentClassOfInsuranceIndicator" ,
                "OLD"."CessionBasis" AS "OLD_CessionBasis" ,
                "OLD"."ClaimMadeType" AS "OLD_ClaimMadeType" ,
                "OLD"."CleanCutIndicator" AS "OLD_CleanCutIndicator" ,
                "OLD"."CoverageBoundDate" AS "OLD_CoverageBoundDate" ,
                "OLD"."CoverageDescription" AS "OLD_CoverageDescription" ,
                "OLD"."CoverageEndDate" AS "OLD_CoverageEndDate" ,
                "OLD"."CoverageStartDate" AS "OLD_CoverageStartDate" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."DeductibleAmount" AS "OLD_DeductibleAmount" ,
                "OLD"."DeductibleLossRatioPercent" AS "OLD_DeductibleLossRatioPercent" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."DiscountingType" AS "OLD_DiscountingType" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."ExtraContractualObligationAmount" AS "OLD_ExtraContractualObligationAmount" ,
                "OLD"."GroupInsurancePolicyIndicator" AS "OLD_GroupInsurancePolicyIndicator" ,
                "OLD"."InsurancePolicyDistributionChannel" AS "OLD_InsurancePolicyDistributionChannel" ,
                "OLD"."IsNonProportionalSection" AS "OLD_IsNonProportionalSection" ,
                "OLD"."IsProportionalSection" AS "OLD_IsProportionalSection" ,
                "OLD"."LifecycleStatus" AS "OLD_LifecycleStatus" ,
                "OLD"."LimitAmount" AS "OLD_LimitAmount" ,
                "OLD"."LimitCurrency" AS "OLD_LimitCurrency" ,
                "OLD"."MaximumAmount" AS "OLD_MaximumAmount" ,
                "OLD"."MaximumCurrency" AS "OLD_MaximumCurrency" ,
                "OLD"."MaximumLiabilityAmount" AS "OLD_MaximumLiabilityAmount" ,
                "OLD"."NumberOfMaxima" AS "OLD_NumberOfMaxima" ,
                "OLD"."PaymentCurrency" AS "OLD_PaymentCurrency" ,
                "OLD"."QuotaSharePercent" AS "OLD_QuotaSharePercent" ,
                "OLD"."RankOrdinalNumber" AS "OLD_RankOrdinalNumber" ,
                "OLD"."ReferenceBasis" AS "OLD_ReferenceBasis" ,
                "OLD"."ReinsuranceCalculationMethod" AS "OLD_ReinsuranceCalculationMethod" ,
                "OLD"."ReinsuranceTechnicalType" AS "OLD_ReinsuranceTechnicalType" ,
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."Sunrise" AS "OLD_Sunrise" ,
                "OLD"."Sunset" AS "OLD_Sunset" ,
                "OLD"."UnderwritingConfederation" AS "OLD_UnderwritingConfederation" ,
                "OLD"."UnderwritingCountry" AS "OLD_UnderwritingCountry" ,
                "OLD"."UnderwritingEventLimitAmount" AS "OLD_UnderwritingEventLimitAmount" ,
                "OLD"."UnderwritingLimitAmount" AS "OLD_UnderwritingLimitAmount" ,
                "OLD"."WithProfitIndicator" AS "OLD_WithProfitIndicator" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SectionCoverage" as "OLD"
            on
                                                "IN"."CoverageID" = "OLD"."CoverageID" and
                                                "IN"."SectionID" = "OLD"."SectionID" and
                                                "IN"."_ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceContract.FinancialContractID" and
                                                "IN"."_ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::SectionCoverage"
    where (
        "CoverageID",
        "SectionID",
        "_ReinsuranceContract.FinancialContractID",
        "_ReinsuranceContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."CoverageID",
            "OLD"."SectionID",
            "OLD"."_ReinsuranceContract.FinancialContractID",
            "OLD"."_ReinsuranceContract.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::SectionCoverage" as "OLD"
        on
                                       "IN"."CoverageID" = "OLD"."CoverageID" and
                                       "IN"."SectionID" = "OLD"."SectionID" and
                                       "IN"."_ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceContract.FinancialContractID" and
                                       "IN"."_ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceContract.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
