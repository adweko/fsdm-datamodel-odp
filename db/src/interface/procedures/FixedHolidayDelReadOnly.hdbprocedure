PROCEDURE "sap.fsdm.procedures::FixedHolidayDelReadOnly" (IN ROW "sap.fsdm.tabletypes::FixedHolidayTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::FixedHolidayTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::FixedHolidayTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'DayOfMonth=' || TO_VARCHAR("DayOfMonth") || ' ' ||
                'MonthOfYear=' || TO_VARCHAR("MonthOfYear") || ' ' ||
                '_BusinessCalendar.BusinessCalendar=' || TO_VARCHAR("_BusinessCalendar.BusinessCalendar") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."DayOfMonth",
                        "IN"."MonthOfYear",
                        "IN"."_BusinessCalendar.BusinessCalendar"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."DayOfMonth",
                        "IN"."MonthOfYear",
                        "IN"."_BusinessCalendar.BusinessCalendar"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "DayOfMonth",
                        "MonthOfYear",
                        "_BusinessCalendar.BusinessCalendar"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."DayOfMonth",
                                    "IN"."MonthOfYear",
                                    "IN"."_BusinessCalendar.BusinessCalendar"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."DayOfMonth",
                                    "IN"."MonthOfYear",
                                    "IN"."_BusinessCalendar.BusinessCalendar"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "DayOfMonth" is null and
            "MonthOfYear" is null and
            "_BusinessCalendar.BusinessCalendar" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "DayOfMonth",
            "MonthOfYear",
            "_BusinessCalendar.BusinessCalendar",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::FixedHoliday" WHERE
            (
            "DayOfMonth" ,
            "MonthOfYear" ,
            "_BusinessCalendar.BusinessCalendar" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."DayOfMonth",
            "OLD"."MonthOfYear",
            "OLD"."_BusinessCalendar.BusinessCalendar",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::FixedHoliday" as "OLD"
        on
                              "IN"."DayOfMonth" = "OLD"."DayOfMonth" and
                              "IN"."MonthOfYear" = "OLD"."MonthOfYear" and
                              "IN"."_BusinessCalendar.BusinessCalendar" = "OLD"."_BusinessCalendar.BusinessCalendar" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "DayOfMonth",
        "MonthOfYear",
        "_BusinessCalendar.BusinessCalendar",
        "BusinessValidFrom",
        "BusinessValidTo",
        "HolidayShiftRule",
        "NameOfHoliday",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_DayOfMonth" as "DayOfMonth" ,
            "OLD_MonthOfYear" as "MonthOfYear" ,
            "OLD__BusinessCalendar.BusinessCalendar" as "_BusinessCalendar.BusinessCalendar" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_HolidayShiftRule" as "HolidayShiftRule" ,
            "OLD_NameOfHoliday" as "NameOfHoliday" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."DayOfMonth",
                        "OLD"."MonthOfYear",
                        "OLD"."_BusinessCalendar.BusinessCalendar",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."DayOfMonth" AS "OLD_DayOfMonth" ,
                "OLD"."MonthOfYear" AS "OLD_MonthOfYear" ,
                "OLD"."_BusinessCalendar.BusinessCalendar" AS "OLD__BusinessCalendar.BusinessCalendar" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."HolidayShiftRule" AS "OLD_HolidayShiftRule" ,
                "OLD"."NameOfHoliday" AS "OLD_NameOfHoliday" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::FixedHoliday" as "OLD"
            on
                                      "IN"."DayOfMonth" = "OLD"."DayOfMonth" and
                                      "IN"."MonthOfYear" = "OLD"."MonthOfYear" and
                                      "IN"."_BusinessCalendar.BusinessCalendar" = "OLD"."_BusinessCalendar.BusinessCalendar" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_DayOfMonth" as "DayOfMonth",
            "OLD_MonthOfYear" as "MonthOfYear",
            "OLD__BusinessCalendar.BusinessCalendar" as "_BusinessCalendar.BusinessCalendar",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_HolidayShiftRule" as "HolidayShiftRule",
            "OLD_NameOfHoliday" as "NameOfHoliday",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."DayOfMonth",
                        "OLD"."MonthOfYear",
                        "OLD"."_BusinessCalendar.BusinessCalendar",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."DayOfMonth" AS "OLD_DayOfMonth" ,
                "OLD"."MonthOfYear" AS "OLD_MonthOfYear" ,
                "OLD"."_BusinessCalendar.BusinessCalendar" AS "OLD__BusinessCalendar.BusinessCalendar" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."HolidayShiftRule" AS "OLD_HolidayShiftRule" ,
                "OLD"."NameOfHoliday" AS "OLD_NameOfHoliday" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::FixedHoliday" as "OLD"
            on
               "IN"."DayOfMonth" = "OLD"."DayOfMonth" and
               "IN"."MonthOfYear" = "OLD"."MonthOfYear" and
               "IN"."_BusinessCalendar.BusinessCalendar" = "OLD"."_BusinessCalendar.BusinessCalendar" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
