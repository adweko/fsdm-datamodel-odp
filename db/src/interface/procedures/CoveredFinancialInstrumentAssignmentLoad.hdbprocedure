PROCEDURE "sap.fsdm.procedures::CoveredFinancialInstrumentAssignmentLoad" (IN ROW "sap.fsdm.tabletypes::CoveredFinancialInstrumentAssignmentTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'LotID=' || TO_VARCHAR("LotID") || ' ' ||
                '_AccountingSystem.AccountingSystemID=' || TO_VARCHAR("_AccountingSystem.AccountingSystemID") || ' ' ||
                '_CollateralPool.CollectionID=' || TO_VARCHAR("_CollateralPool.CollectionID") || ' ' ||
                '_CollateralPool.IDSystem=' || TO_VARCHAR("_CollateralPool.IDSystem") || ' ' ||
                '_CollateralPool._Client.BusinessPartnerID=' || TO_VARCHAR("_CollateralPool._Client.BusinessPartnerID") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_SecuritiesAccount.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccount.FinancialContractID") || ' ' ||
                '_SecuritiesAccount.IDSystem=' || TO_VARCHAR("_SecuritiesAccount.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."LotID",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_CollateralPool._Client.BusinessPartnerID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."LotID",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_CollateralPool._Client.BusinessPartnerID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "LotID",
                        "_AccountingSystem.AccountingSystemID",
                        "_CollateralPool.CollectionID",
                        "_CollateralPool.IDSystem",
                        "_CollateralPool._Client.BusinessPartnerID",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_SecuritiesAccount.FinancialContractID",
                        "_SecuritiesAccount.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."LotID",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_CollateralPool.CollectionID",
                                    "IN"."_CollateralPool.IDSystem",
                                    "IN"."_CollateralPool._Client.BusinessPartnerID",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."LotID",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_CollateralPool.CollectionID",
                                    "IN"."_CollateralPool.IDSystem",
                                    "IN"."_CollateralPool._Client.BusinessPartnerID",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::CoveredFinancialInstrumentAssignment" (
        "LotID",
        "_AccountingSystem.AccountingSystemID",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_CollateralPool._Client.BusinessPartnerID",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CoverageAmount",
        "CoverageAmountCurrency",
        "CoverageShare",
        "CoveredFinancialInstrumentAssignmentCategory",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_LotID" as "LotID" ,
            "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID" ,
            "OLD__CollateralPool.CollectionID" as "_CollateralPool.CollectionID" ,
            "OLD__CollateralPool.IDSystem" as "_CollateralPool.IDSystem" ,
            "OLD__CollateralPool._Client.BusinessPartnerID" as "_CollateralPool._Client.BusinessPartnerID" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID" ,
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CoverageAmount" as "CoverageAmount" ,
            "OLD_CoverageAmountCurrency" as "CoverageAmountCurrency" ,
            "OLD_CoverageShare" as "CoverageShare" ,
            "OLD_CoveredFinancialInstrumentAssignmentCategory" as "CoveredFinancialInstrumentAssignmentCategory" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."LotID",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_CollateralPool._Client.BusinessPartnerID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."LotID" as "OLD_LotID",
                                "OLD"."_AccountingSystem.AccountingSystemID" as "OLD__AccountingSystem.AccountingSystemID",
                                "OLD"."_CollateralPool.CollectionID" as "OLD__CollateralPool.CollectionID",
                                "OLD"."_CollateralPool.IDSystem" as "OLD__CollateralPool.IDSystem",
                                "OLD"."_CollateralPool._Client.BusinessPartnerID" as "OLD__CollateralPool._Client.BusinessPartnerID",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                                "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."CoverageAmount" as "OLD_CoverageAmount",
                                "OLD"."CoverageAmountCurrency" as "OLD_CoverageAmountCurrency",
                                "OLD"."CoverageShare" as "OLD_CoverageShare",
                                "OLD"."CoveredFinancialInstrumentAssignmentCategory" as "OLD_CoveredFinancialInstrumentAssignmentCategory",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CoveredFinancialInstrumentAssignment" as "OLD"
            on
                ifnull( "IN"."LotID", '') = "OLD"."LotID" and
                ifnull( "IN"."_AccountingSystem.AccountingSystemID", '') = "OLD"."_AccountingSystem.AccountingSystemID" and
                ifnull( "IN"."_CollateralPool.CollectionID", '') = "OLD"."_CollateralPool.CollectionID" and
                ifnull( "IN"."_CollateralPool.IDSystem", '') = "OLD"."_CollateralPool.IDSystem" and
                ifnull( "IN"."_CollateralPool._Client.BusinessPartnerID", '') = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CoveredFinancialInstrumentAssignment" (
        "LotID",
        "_AccountingSystem.AccountingSystemID",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_CollateralPool._Client.BusinessPartnerID",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CoverageAmount",
        "CoverageAmountCurrency",
        "CoverageShare",
        "CoveredFinancialInstrumentAssignmentCategory",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_LotID"  as "LotID",
            "OLD__AccountingSystem.AccountingSystemID"  as "_AccountingSystem.AccountingSystemID",
            "OLD__CollateralPool.CollectionID"  as "_CollateralPool.CollectionID",
            "OLD__CollateralPool.IDSystem"  as "_CollateralPool.IDSystem",
            "OLD__CollateralPool._Client.BusinessPartnerID"  as "_CollateralPool._Client.BusinessPartnerID",
            "OLD__FinancialInstrument.FinancialInstrumentID"  as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__SecuritiesAccount.FinancialContractID"  as "_SecuritiesAccount.FinancialContractID",
            "OLD__SecuritiesAccount.IDSystem"  as "_SecuritiesAccount.IDSystem",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_CoverageAmount"  as "CoverageAmount",
            "OLD_CoverageAmountCurrency"  as "CoverageAmountCurrency",
            "OLD_CoverageShare"  as "CoverageShare",
            "OLD_CoveredFinancialInstrumentAssignmentCategory"  as "CoveredFinancialInstrumentAssignmentCategory",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."LotID",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_CollateralPool._Client.BusinessPartnerID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."LotID" as "OLD_LotID",
                        "OLD"."_AccountingSystem.AccountingSystemID" as "OLD__AccountingSystem.AccountingSystemID",
                        "OLD"."_CollateralPool.CollectionID" as "OLD__CollateralPool.CollectionID",
                        "OLD"."_CollateralPool.IDSystem" as "OLD__CollateralPool.IDSystem",
                        "OLD"."_CollateralPool._Client.BusinessPartnerID" as "OLD__CollateralPool._Client.BusinessPartnerID",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."CoverageAmount" as "OLD_CoverageAmount",
                        "OLD"."CoverageAmountCurrency" as "OLD_CoverageAmountCurrency",
                        "OLD"."CoverageShare" as "OLD_CoverageShare",
                        "OLD"."CoveredFinancialInstrumentAssignmentCategory" as "OLD_CoveredFinancialInstrumentAssignmentCategory",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CoveredFinancialInstrumentAssignment" as "OLD"
            on
                ifnull( "IN"."LotID", '' ) = "OLD"."LotID" and
                ifnull( "IN"."_AccountingSystem.AccountingSystemID", '' ) = "OLD"."_AccountingSystem.AccountingSystemID" and
                ifnull( "IN"."_CollateralPool.CollectionID", '' ) = "OLD"."_CollateralPool.CollectionID" and
                ifnull( "IN"."_CollateralPool.IDSystem", '' ) = "OLD"."_CollateralPool.IDSystem" and
                ifnull( "IN"."_CollateralPool._Client.BusinessPartnerID", '' ) = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '' ) = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccount.IDSystem", '' ) = "OLD"."_SecuritiesAccount.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::CoveredFinancialInstrumentAssignment"
    where (
        "LotID",
        "_AccountingSystem.AccountingSystemID",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_CollateralPool._Client.BusinessPartnerID",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."LotID",
            "OLD"."_AccountingSystem.AccountingSystemID",
            "OLD"."_CollateralPool.CollectionID",
            "OLD"."_CollateralPool.IDSystem",
            "OLD"."_CollateralPool._Client.BusinessPartnerID",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_SecuritiesAccount.FinancialContractID",
            "OLD"."_SecuritiesAccount.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CoveredFinancialInstrumentAssignment" as "OLD"
        on
           ifnull( "IN"."LotID", '' ) = "OLD"."LotID" and
           ifnull( "IN"."_AccountingSystem.AccountingSystemID", '' ) = "OLD"."_AccountingSystem.AccountingSystemID" and
           ifnull( "IN"."_CollateralPool.CollectionID", '' ) = "OLD"."_CollateralPool.CollectionID" and
           ifnull( "IN"."_CollateralPool.IDSystem", '' ) = "OLD"."_CollateralPool.IDSystem" and
           ifnull( "IN"."_CollateralPool._Client.BusinessPartnerID", '' ) = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
           ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
           ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '' ) = "OLD"."_SecuritiesAccount.FinancialContractID" and
           ifnull( "IN"."_SecuritiesAccount.IDSystem", '' ) = "OLD"."_SecuritiesAccount.IDSystem" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::CoveredFinancialInstrumentAssignment" (
        "LotID",
        "_AccountingSystem.AccountingSystemID",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_CollateralPool._Client.BusinessPartnerID",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CoverageAmount",
        "CoverageAmountCurrency",
        "CoverageShare",
        "CoveredFinancialInstrumentAssignmentCategory",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "LotID", '' ) as "LotID",
            ifnull( "_AccountingSystem.AccountingSystemID", '' ) as "_AccountingSystem.AccountingSystemID",
            ifnull( "_CollateralPool.CollectionID", '' ) as "_CollateralPool.CollectionID",
            ifnull( "_CollateralPool.IDSystem", '' ) as "_CollateralPool.IDSystem",
            ifnull( "_CollateralPool._Client.BusinessPartnerID", '' ) as "_CollateralPool._Client.BusinessPartnerID",
            ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
            ifnull( "_SecuritiesAccount.FinancialContractID", '' ) as "_SecuritiesAccount.FinancialContractID",
            ifnull( "_SecuritiesAccount.IDSystem", '' ) as "_SecuritiesAccount.IDSystem",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "CoverageAmount"  ,
            "CoverageAmountCurrency"  ,
            "CoverageShare"  ,
            "CoveredFinancialInstrumentAssignmentCategory"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END