PROCEDURE "sap.fsdm.procedures::InflationSwapCalculationPeriodEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::InflationSwapCalculationPeriodTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::InflationSwapCalculationPeriodTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::InflationSwapCalculationPeriodTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "IntervalEndDate" is null and
            "IntervalStartDate" is null and
            "RoleOfPayer" is null and
            "_Swap.FinancialContractID" is null and
            "_Swap.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "IntervalEndDate" ,
                "IntervalStartDate" ,
                "RoleOfPayer" ,
                "_Swap.FinancialContractID" ,
                "_Swap.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."IntervalEndDate" ,
                "OLD"."IntervalStartDate" ,
                "OLD"."RoleOfPayer" ,
                "OLD"."_Swap.FinancialContractID" ,
                "OLD"."_Swap.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InflationSwapCalculationPeriod" "OLD"
            on
                "IN"."IntervalEndDate" = "OLD"."IntervalEndDate" and
                "IN"."IntervalStartDate" = "OLD"."IntervalStartDate" and
                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                "IN"."_Swap.FinancialContractID" = "OLD"."_Swap.FinancialContractID" and
                "IN"."_Swap.IDSystem" = "OLD"."_Swap.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "IntervalEndDate" ,
            "IntervalStartDate" ,
            "RoleOfPayer" ,
            "_Swap.FinancialContractID" ,
            "_Swap.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."IntervalEndDate" ,
                "OLD"."IntervalStartDate" ,
                "OLD"."RoleOfPayer" ,
                "OLD"."_Swap.FinancialContractID" ,
                "OLD"."_Swap.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InflationSwapCalculationPeriod_Historical" "OLD"
            on
                "IN"."IntervalEndDate" = "OLD"."IntervalEndDate" and
                "IN"."IntervalStartDate" = "OLD"."IntervalStartDate" and
                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                "IN"."_Swap.FinancialContractID" = "OLD"."_Swap.FinancialContractID" and
                "IN"."_Swap.IDSystem" = "OLD"."_Swap.IDSystem" 
        );

END
