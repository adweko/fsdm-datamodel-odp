PROCEDURE "sap.fsdm.procedures::EndOfDayPriceObservationDelReadOnly" (IN ROW "sap.fsdm.tabletypes::EndOfDayPriceObservationTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::EndOfDayPriceObservationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::EndOfDayPriceObservationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CleanDirtyIndicator=' || TO_VARCHAR("CleanDirtyIndicator") || ' ' ||
                'PriceDataProvider=' || TO_VARCHAR("PriceDataProvider") || ' ' ||
                'PriceSeriesType=' || TO_VARCHAR("PriceSeriesType") || ' ' ||
                '_Exchange.MarketIdentifierCode=' || TO_VARCHAR("_Exchange.MarketIdentifierCode") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CleanDirtyIndicator",
                        "IN"."PriceDataProvider",
                        "IN"."PriceSeriesType",
                        "IN"."_Exchange.MarketIdentifierCode",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CleanDirtyIndicator",
                        "IN"."PriceDataProvider",
                        "IN"."PriceSeriesType",
                        "IN"."_Exchange.MarketIdentifierCode",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CleanDirtyIndicator",
                        "PriceDataProvider",
                        "PriceSeriesType",
                        "_Exchange.MarketIdentifierCode",
                        "_FinancialInstrument.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CleanDirtyIndicator",
                                    "IN"."PriceDataProvider",
                                    "IN"."PriceSeriesType",
                                    "IN"."_Exchange.MarketIdentifierCode",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CleanDirtyIndicator",
                                    "IN"."PriceDataProvider",
                                    "IN"."PriceSeriesType",
                                    "IN"."_Exchange.MarketIdentifierCode",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "CleanDirtyIndicator" is null and
            "PriceDataProvider" is null and
            "PriceSeriesType" is null and
            "_Exchange.MarketIdentifierCode" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "CleanDirtyIndicator",
            "PriceDataProvider",
            "PriceSeriesType",
            "_Exchange.MarketIdentifierCode",
            "_FinancialInstrument.FinancialInstrumentID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::EndOfDayPriceObservation" WHERE
            (
            "CleanDirtyIndicator" ,
            "PriceDataProvider" ,
            "PriceSeriesType" ,
            "_Exchange.MarketIdentifierCode" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."CleanDirtyIndicator",
            "OLD"."PriceDataProvider",
            "OLD"."PriceSeriesType",
            "OLD"."_Exchange.MarketIdentifierCode",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::EndOfDayPriceObservation" as "OLD"
        on
                              "IN"."CleanDirtyIndicator" = "OLD"."CleanDirtyIndicator" and
                              "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
                              "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
                              "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
                              "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "CleanDirtyIndicator",
        "PriceDataProvider",
        "PriceSeriesType",
        "_Exchange.MarketIdentifierCode",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AdjustedClosePrice",
        "AdjustmentFactor",
        "Ask",
        "Bid",
        "Close",
        "Currency",
        "EndOfDayPriceObservationCategory",
        "High",
        "Low",
        "NAVperShare",
        "NumberOfTrades",
        "Open",
        "PriceNotationForm",
        "PriceType",
        "TradedNominalAmount",
        "TradedNominalAmountCurrency",
        "Unit",
        "Volume",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_CleanDirtyIndicator" as "CleanDirtyIndicator" ,
            "OLD_PriceDataProvider" as "PriceDataProvider" ,
            "OLD_PriceSeriesType" as "PriceSeriesType" ,
            "OLD__Exchange.MarketIdentifierCode" as "_Exchange.MarketIdentifierCode" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AdjustedClosePrice" as "AdjustedClosePrice" ,
            "OLD_AdjustmentFactor" as "AdjustmentFactor" ,
            "OLD_Ask" as "Ask" ,
            "OLD_Bid" as "Bid" ,
            "OLD_Close" as "Close" ,
            "OLD_Currency" as "Currency" ,
            "OLD_EndOfDayPriceObservationCategory" as "EndOfDayPriceObservationCategory" ,
            "OLD_High" as "High" ,
            "OLD_Low" as "Low" ,
            "OLD_NAVperShare" as "NAVperShare" ,
            "OLD_NumberOfTrades" as "NumberOfTrades" ,
            "OLD_Open" as "Open" ,
            "OLD_PriceNotationForm" as "PriceNotationForm" ,
            "OLD_PriceType" as "PriceType" ,
            "OLD_TradedNominalAmount" as "TradedNominalAmount" ,
            "OLD_TradedNominalAmountCurrency" as "TradedNominalAmountCurrency" ,
            "OLD_Unit" as "Unit" ,
            "OLD_Volume" as "Volume" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."CleanDirtyIndicator",
                        "OLD"."PriceDataProvider",
                        "OLD"."PriceSeriesType",
                        "OLD"."_Exchange.MarketIdentifierCode",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."CleanDirtyIndicator" AS "OLD_CleanDirtyIndicator" ,
                "OLD"."PriceDataProvider" AS "OLD_PriceDataProvider" ,
                "OLD"."PriceSeriesType" AS "OLD_PriceSeriesType" ,
                "OLD"."_Exchange.MarketIdentifierCode" AS "OLD__Exchange.MarketIdentifierCode" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AdjustedClosePrice" AS "OLD_AdjustedClosePrice" ,
                "OLD"."AdjustmentFactor" AS "OLD_AdjustmentFactor" ,
                "OLD"."Ask" AS "OLD_Ask" ,
                "OLD"."Bid" AS "OLD_Bid" ,
                "OLD"."Close" AS "OLD_Close" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."EndOfDayPriceObservationCategory" AS "OLD_EndOfDayPriceObservationCategory" ,
                "OLD"."High" AS "OLD_High" ,
                "OLD"."Low" AS "OLD_Low" ,
                "OLD"."NAVperShare" AS "OLD_NAVperShare" ,
                "OLD"."NumberOfTrades" AS "OLD_NumberOfTrades" ,
                "OLD"."Open" AS "OLD_Open" ,
                "OLD"."PriceNotationForm" AS "OLD_PriceNotationForm" ,
                "OLD"."PriceType" AS "OLD_PriceType" ,
                "OLD"."TradedNominalAmount" AS "OLD_TradedNominalAmount" ,
                "OLD"."TradedNominalAmountCurrency" AS "OLD_TradedNominalAmountCurrency" ,
                "OLD"."Unit" AS "OLD_Unit" ,
                "OLD"."Volume" AS "OLD_Volume" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EndOfDayPriceObservation" as "OLD"
            on
                                      "IN"."CleanDirtyIndicator" = "OLD"."CleanDirtyIndicator" and
                                      "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
                                      "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
                                      "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
                                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_CleanDirtyIndicator" as "CleanDirtyIndicator",
            "OLD_PriceDataProvider" as "PriceDataProvider",
            "OLD_PriceSeriesType" as "PriceSeriesType",
            "OLD__Exchange.MarketIdentifierCode" as "_Exchange.MarketIdentifierCode",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AdjustedClosePrice" as "AdjustedClosePrice",
            "OLD_AdjustmentFactor" as "AdjustmentFactor",
            "OLD_Ask" as "Ask",
            "OLD_Bid" as "Bid",
            "OLD_Close" as "Close",
            "OLD_Currency" as "Currency",
            "OLD_EndOfDayPriceObservationCategory" as "EndOfDayPriceObservationCategory",
            "OLD_High" as "High",
            "OLD_Low" as "Low",
            "OLD_NAVperShare" as "NAVperShare",
            "OLD_NumberOfTrades" as "NumberOfTrades",
            "OLD_Open" as "Open",
            "OLD_PriceNotationForm" as "PriceNotationForm",
            "OLD_PriceType" as "PriceType",
            "OLD_TradedNominalAmount" as "TradedNominalAmount",
            "OLD_TradedNominalAmountCurrency" as "TradedNominalAmountCurrency",
            "OLD_Unit" as "Unit",
            "OLD_Volume" as "Volume",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."CleanDirtyIndicator",
                        "OLD"."PriceDataProvider",
                        "OLD"."PriceSeriesType",
                        "OLD"."_Exchange.MarketIdentifierCode",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."CleanDirtyIndicator" AS "OLD_CleanDirtyIndicator" ,
                "OLD"."PriceDataProvider" AS "OLD_PriceDataProvider" ,
                "OLD"."PriceSeriesType" AS "OLD_PriceSeriesType" ,
                "OLD"."_Exchange.MarketIdentifierCode" AS "OLD__Exchange.MarketIdentifierCode" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AdjustedClosePrice" AS "OLD_AdjustedClosePrice" ,
                "OLD"."AdjustmentFactor" AS "OLD_AdjustmentFactor" ,
                "OLD"."Ask" AS "OLD_Ask" ,
                "OLD"."Bid" AS "OLD_Bid" ,
                "OLD"."Close" AS "OLD_Close" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."EndOfDayPriceObservationCategory" AS "OLD_EndOfDayPriceObservationCategory" ,
                "OLD"."High" AS "OLD_High" ,
                "OLD"."Low" AS "OLD_Low" ,
                "OLD"."NAVperShare" AS "OLD_NAVperShare" ,
                "OLD"."NumberOfTrades" AS "OLD_NumberOfTrades" ,
                "OLD"."Open" AS "OLD_Open" ,
                "OLD"."PriceNotationForm" AS "OLD_PriceNotationForm" ,
                "OLD"."PriceType" AS "OLD_PriceType" ,
                "OLD"."TradedNominalAmount" AS "OLD_TradedNominalAmount" ,
                "OLD"."TradedNominalAmountCurrency" AS "OLD_TradedNominalAmountCurrency" ,
                "OLD"."Unit" AS "OLD_Unit" ,
                "OLD"."Volume" AS "OLD_Volume" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EndOfDayPriceObservation" as "OLD"
            on
               "IN"."CleanDirtyIndicator" = "OLD"."CleanDirtyIndicator" and
               "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
               "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
               "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
               "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
