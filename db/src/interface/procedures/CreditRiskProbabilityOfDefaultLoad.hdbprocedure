PROCEDURE "sap.fsdm.procedures::CreditRiskProbabilityOfDefaultLoad" (IN ROW "sap.fsdm.tabletypes::CreditRiskProbabilityOfDefaultTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PDEstimationMethod=' || TO_VARCHAR("PDEstimationMethod") || ' ' ||
                'PDTimeHorizon=' || TO_VARCHAR("PDTimeHorizon") || ' ' ||
                'PDTimeHorizonUnit=' || TO_VARCHAR("PDTimeHorizonUnit") || ' ' ||
                'RiskProvisionScenario=' || TO_VARCHAR("RiskProvisionScenario") || ' ' ||
                'ASSOC_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("ASSOC_BusinessPartner.BusinessPartnerID") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PDEstimationMethod",
                        "IN"."PDTimeHorizon",
                        "IN"."PDTimeHorizonUnit",
                        "IN"."RiskProvisionScenario",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PDEstimationMethod",
                        "IN"."PDTimeHorizon",
                        "IN"."PDTimeHorizonUnit",
                        "IN"."RiskProvisionScenario",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PDEstimationMethod",
                        "PDTimeHorizon",
                        "PDTimeHorizonUnit",
                        "RiskProvisionScenario",
                        "ASSOC_BusinessPartner.BusinessPartnerID",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PDEstimationMethod",
                                    "IN"."PDTimeHorizon",
                                    "IN"."PDTimeHorizonUnit",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PDEstimationMethod",
                                    "IN"."PDTimeHorizon",
                                    "IN"."PDTimeHorizonUnit",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::CreditRiskProbabilityOfDefault" (
        "PDEstimationMethod",
        "PDTimeHorizon",
        "PDTimeHorizonUnit",
        "RiskProvisionScenario",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "MarginOfConservatismCategoryA",
        "MarginOfConservatismCategoryB",
        "MarginOfConservatismCategoryC",
        "PDBasedOnMarginsOfConservatism",
        "PDBasedOnMarginsOfConservatismAndSupervisoryMeasures",
        "ProbabilityOfDefault",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PDEstimationMethod" as "PDEstimationMethod" ,
            "OLD_PDTimeHorizon" as "PDTimeHorizon" ,
            "OLD_PDTimeHorizonUnit" as "PDTimeHorizonUnit" ,
            "OLD_RiskProvisionScenario" as "RiskProvisionScenario" ,
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_MarginOfConservatismCategoryA" as "MarginOfConservatismCategoryA" ,
            "OLD_MarginOfConservatismCategoryB" as "MarginOfConservatismCategoryB" ,
            "OLD_MarginOfConservatismCategoryC" as "MarginOfConservatismCategoryC" ,
            "OLD_PDBasedOnMarginsOfConservatism" as "PDBasedOnMarginsOfConservatism" ,
            "OLD_PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" as "PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" ,
            "OLD_ProbabilityOfDefault" as "ProbabilityOfDefault" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."PDEstimationMethod",
                        "IN"."PDTimeHorizon",
                        "IN"."PDTimeHorizonUnit",
                        "IN"."RiskProvisionScenario",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."PDEstimationMethod" as "OLD_PDEstimationMethod",
                                "OLD"."PDTimeHorizon" as "OLD_PDTimeHorizon",
                                "OLD"."PDTimeHorizonUnit" as "OLD_PDTimeHorizonUnit",
                                "OLD"."RiskProvisionScenario" as "OLD_RiskProvisionScenario",
                                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" as "OLD_ASSOC_BusinessPartner.BusinessPartnerID",
                                "OLD"."ASSOC_FinancialContract.FinancialContractID" as "OLD_ASSOC_FinancialContract.FinancialContractID",
                                "OLD"."ASSOC_FinancialContract.IDSystem" as "OLD_ASSOC_FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."MarginOfConservatismCategoryA" as "OLD_MarginOfConservatismCategoryA",
                                "OLD"."MarginOfConservatismCategoryB" as "OLD_MarginOfConservatismCategoryB",
                                "OLD"."MarginOfConservatismCategoryC" as "OLD_MarginOfConservatismCategoryC",
                                "OLD"."PDBasedOnMarginsOfConservatism" as "OLD_PDBasedOnMarginsOfConservatism",
                                "OLD"."PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" as "OLD_PDBasedOnMarginsOfConservatismAndSupervisoryMeasures",
                                "OLD"."ProbabilityOfDefault" as "OLD_ProbabilityOfDefault",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CreditRiskProbabilityOfDefault" as "OLD"
            on
                ifnull( "IN"."PDEstimationMethod", '') = "OLD"."PDEstimationMethod" and
                ifnull( "IN"."PDTimeHorizon", 0) = "OLD"."PDTimeHorizon" and
                ifnull( "IN"."PDTimeHorizonUnit", '') = "OLD"."PDTimeHorizonUnit" and
                ifnull( "IN"."RiskProvisionScenario", '') = "OLD"."RiskProvisionScenario" and
                ifnull( "IN"."ASSOC_BusinessPartner.BusinessPartnerID", '') = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                ifnull( "IN"."ASSOC_FinancialContract.FinancialContractID", '') = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                ifnull( "IN"."ASSOC_FinancialContract.IDSystem", '') = "OLD"."ASSOC_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CreditRiskProbabilityOfDefault" (
        "PDEstimationMethod",
        "PDTimeHorizon",
        "PDTimeHorizonUnit",
        "RiskProvisionScenario",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "MarginOfConservatismCategoryA",
        "MarginOfConservatismCategoryB",
        "MarginOfConservatismCategoryC",
        "PDBasedOnMarginsOfConservatism",
        "PDBasedOnMarginsOfConservatismAndSupervisoryMeasures",
        "ProbabilityOfDefault",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PDEstimationMethod"  as "PDEstimationMethod",
            "OLD_PDTimeHorizon"  as "PDTimeHorizon",
            "OLD_PDTimeHorizonUnit"  as "PDTimeHorizonUnit",
            "OLD_RiskProvisionScenario"  as "RiskProvisionScenario",
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID"  as "ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD_ASSOC_FinancialContract.FinancialContractID"  as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem"  as "ASSOC_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID"  as "_FinancialInstrument.FinancialInstrumentID",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_MarginOfConservatismCategoryA"  as "MarginOfConservatismCategoryA",
            "OLD_MarginOfConservatismCategoryB"  as "MarginOfConservatismCategoryB",
            "OLD_MarginOfConservatismCategoryC"  as "MarginOfConservatismCategoryC",
            "OLD_PDBasedOnMarginsOfConservatism"  as "PDBasedOnMarginsOfConservatism",
            "OLD_PDBasedOnMarginsOfConservatismAndSupervisoryMeasures"  as "PDBasedOnMarginsOfConservatismAndSupervisoryMeasures",
            "OLD_ProbabilityOfDefault"  as "ProbabilityOfDefault",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."PDEstimationMethod",
                        "IN"."PDTimeHorizon",
                        "IN"."PDTimeHorizonUnit",
                        "IN"."RiskProvisionScenario",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."PDEstimationMethod" as "OLD_PDEstimationMethod",
                        "OLD"."PDTimeHorizon" as "OLD_PDTimeHorizon",
                        "OLD"."PDTimeHorizonUnit" as "OLD_PDTimeHorizonUnit",
                        "OLD"."RiskProvisionScenario" as "OLD_RiskProvisionScenario",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" as "OLD_ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID" as "OLD_ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem" as "OLD_ASSOC_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."MarginOfConservatismCategoryA" as "OLD_MarginOfConservatismCategoryA",
                        "OLD"."MarginOfConservatismCategoryB" as "OLD_MarginOfConservatismCategoryB",
                        "OLD"."MarginOfConservatismCategoryC" as "OLD_MarginOfConservatismCategoryC",
                        "OLD"."PDBasedOnMarginsOfConservatism" as "OLD_PDBasedOnMarginsOfConservatism",
                        "OLD"."PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" as "OLD_PDBasedOnMarginsOfConservatismAndSupervisoryMeasures",
                        "OLD"."ProbabilityOfDefault" as "OLD_ProbabilityOfDefault",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CreditRiskProbabilityOfDefault" as "OLD"
            on
                ifnull( "IN"."PDEstimationMethod", '' ) = "OLD"."PDEstimationMethod" and
                ifnull( "IN"."PDTimeHorizon", 0 ) = "OLD"."PDTimeHorizon" and
                ifnull( "IN"."PDTimeHorizonUnit", '' ) = "OLD"."PDTimeHorizonUnit" and
                ifnull( "IN"."RiskProvisionScenario", '' ) = "OLD"."RiskProvisionScenario" and
                ifnull( "IN"."ASSOC_BusinessPartner.BusinessPartnerID", '' ) = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                ifnull( "IN"."ASSOC_FinancialContract.FinancialContractID", '' ) = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                ifnull( "IN"."ASSOC_FinancialContract.IDSystem", '' ) = "OLD"."ASSOC_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::CreditRiskProbabilityOfDefault"
    where (
        "PDEstimationMethod",
        "PDTimeHorizon",
        "PDTimeHorizonUnit",
        "RiskProvisionScenario",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."PDEstimationMethod",
            "OLD"."PDTimeHorizon",
            "OLD"."PDTimeHorizonUnit",
            "OLD"."RiskProvisionScenario",
            "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CreditRiskProbabilityOfDefault" as "OLD"
        on
           ifnull( "IN"."PDEstimationMethod", '' ) = "OLD"."PDEstimationMethod" and
           ifnull( "IN"."PDTimeHorizon", 0 ) = "OLD"."PDTimeHorizon" and
           ifnull( "IN"."PDTimeHorizonUnit", '' ) = "OLD"."PDTimeHorizonUnit" and
           ifnull( "IN"."RiskProvisionScenario", '' ) = "OLD"."RiskProvisionScenario" and
           ifnull( "IN"."ASSOC_BusinessPartner.BusinessPartnerID", '' ) = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
           ifnull( "IN"."ASSOC_FinancialContract.FinancialContractID", '' ) = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
           ifnull( "IN"."ASSOC_FinancialContract.IDSystem", '' ) = "OLD"."ASSOC_FinancialContract.IDSystem" and
           ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::CreditRiskProbabilityOfDefault" (
        "PDEstimationMethod",
        "PDTimeHorizon",
        "PDTimeHorizonUnit",
        "RiskProvisionScenario",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "MarginOfConservatismCategoryA",
        "MarginOfConservatismCategoryB",
        "MarginOfConservatismCategoryC",
        "PDBasedOnMarginsOfConservatism",
        "PDBasedOnMarginsOfConservatismAndSupervisoryMeasures",
        "ProbabilityOfDefault",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "PDEstimationMethod", '' ) as "PDEstimationMethod",
            ifnull( "PDTimeHorizon", 0 ) as "PDTimeHorizon",
            ifnull( "PDTimeHorizonUnit", '' ) as "PDTimeHorizonUnit",
            ifnull( "RiskProvisionScenario", '' ) as "RiskProvisionScenario",
            ifnull( "ASSOC_BusinessPartner.BusinessPartnerID", '' ) as "ASSOC_BusinessPartner.BusinessPartnerID",
            ifnull( "ASSOC_FinancialContract.FinancialContractID", '' ) as "ASSOC_FinancialContract.FinancialContractID",
            ifnull( "ASSOC_FinancialContract.IDSystem", '' ) as "ASSOC_FinancialContract.IDSystem",
            ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "MarginOfConservatismCategoryA"  ,
            "MarginOfConservatismCategoryB"  ,
            "MarginOfConservatismCategoryC"  ,
            "PDBasedOnMarginsOfConservatism"  ,
            "PDBasedOnMarginsOfConservatismAndSupervisoryMeasures"  ,
            "ProbabilityOfDefault"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END