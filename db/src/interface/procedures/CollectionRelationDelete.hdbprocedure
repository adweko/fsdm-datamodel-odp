PROCEDURE "sap.fsdm.procedures::CollectionRelationDelete" (IN ROW "sap.fsdm.tabletypes::CollectionRelationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CollectionRelationType=' || TO_VARCHAR("CollectionRelationType") || ' ' ||
                '_SourceInCollectionRelation.CollectionID=' || TO_VARCHAR("_SourceInCollectionRelation.CollectionID") || ' ' ||
                '_SourceInCollectionRelation.IDSystem=' || TO_VARCHAR("_SourceInCollectionRelation.IDSystem") || ' ' ||
                '_SourceInCollectionRelation._Client.BusinessPartnerID=' || TO_VARCHAR("_SourceInCollectionRelation._Client.BusinessPartnerID") || ' ' ||
                '_TargetInCollectionRelation.CollectionID=' || TO_VARCHAR("_TargetInCollectionRelation.CollectionID") || ' ' ||
                '_TargetInCollectionRelation.IDSystem=' || TO_VARCHAR("_TargetInCollectionRelation.IDSystem") || ' ' ||
                '_TargetInCollectionRelation._Client.BusinessPartnerID=' || TO_VARCHAR("_TargetInCollectionRelation._Client.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CollectionRelationType",
                        "IN"."_SourceInCollectionRelation.CollectionID",
                        "IN"."_SourceInCollectionRelation.IDSystem",
                        "IN"."_SourceInCollectionRelation._Client.BusinessPartnerID",
                        "IN"."_TargetInCollectionRelation.CollectionID",
                        "IN"."_TargetInCollectionRelation.IDSystem",
                        "IN"."_TargetInCollectionRelation._Client.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CollectionRelationType",
                        "IN"."_SourceInCollectionRelation.CollectionID",
                        "IN"."_SourceInCollectionRelation.IDSystem",
                        "IN"."_SourceInCollectionRelation._Client.BusinessPartnerID",
                        "IN"."_TargetInCollectionRelation.CollectionID",
                        "IN"."_TargetInCollectionRelation.IDSystem",
                        "IN"."_TargetInCollectionRelation._Client.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CollectionRelationType",
                        "_SourceInCollectionRelation.CollectionID",
                        "_SourceInCollectionRelation.IDSystem",
                        "_SourceInCollectionRelation._Client.BusinessPartnerID",
                        "_TargetInCollectionRelation.CollectionID",
                        "_TargetInCollectionRelation.IDSystem",
                        "_TargetInCollectionRelation._Client.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CollectionRelationType",
                                    "IN"."_SourceInCollectionRelation.CollectionID",
                                    "IN"."_SourceInCollectionRelation.IDSystem",
                                    "IN"."_SourceInCollectionRelation._Client.BusinessPartnerID",
                                    "IN"."_TargetInCollectionRelation.CollectionID",
                                    "IN"."_TargetInCollectionRelation.IDSystem",
                                    "IN"."_TargetInCollectionRelation._Client.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CollectionRelationType",
                                    "IN"."_SourceInCollectionRelation.CollectionID",
                                    "IN"."_SourceInCollectionRelation.IDSystem",
                                    "IN"."_SourceInCollectionRelation._Client.BusinessPartnerID",
                                    "IN"."_TargetInCollectionRelation.CollectionID",
                                    "IN"."_TargetInCollectionRelation.IDSystem",
                                    "IN"."_TargetInCollectionRelation._Client.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "CollectionRelationType" is null and
            "_SourceInCollectionRelation.CollectionID" is null and
            "_SourceInCollectionRelation.IDSystem" is null and
            "_SourceInCollectionRelation._Client.BusinessPartnerID" is null and
            "_TargetInCollectionRelation.CollectionID" is null and
            "_TargetInCollectionRelation.IDSystem" is null and
            "_TargetInCollectionRelation._Client.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::CollectionRelation" (
        "CollectionRelationType",
        "_SourceInCollectionRelation.CollectionID",
        "_SourceInCollectionRelation.IDSystem",
        "_SourceInCollectionRelation._Client.BusinessPartnerID",
        "_TargetInCollectionRelation.CollectionID",
        "_TargetInCollectionRelation.IDSystem",
        "_TargetInCollectionRelation._Client.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CollectionRelationType" as "CollectionRelationType" ,
            "OLD__SourceInCollectionRelation.CollectionID" as "_SourceInCollectionRelation.CollectionID" ,
            "OLD__SourceInCollectionRelation.IDSystem" as "_SourceInCollectionRelation.IDSystem" ,
            "OLD__SourceInCollectionRelation._Client.BusinessPartnerID" as "_SourceInCollectionRelation._Client.BusinessPartnerID" ,
            "OLD__TargetInCollectionRelation.CollectionID" as "_TargetInCollectionRelation.CollectionID" ,
            "OLD__TargetInCollectionRelation.IDSystem" as "_TargetInCollectionRelation.IDSystem" ,
            "OLD__TargetInCollectionRelation._Client.BusinessPartnerID" as "_TargetInCollectionRelation._Client.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."CollectionRelationType",
                        "OLD"."_SourceInCollectionRelation.CollectionID",
                        "OLD"."_SourceInCollectionRelation.IDSystem",
                        "OLD"."_SourceInCollectionRelation._Client.BusinessPartnerID",
                        "OLD"."_TargetInCollectionRelation.CollectionID",
                        "OLD"."_TargetInCollectionRelation.IDSystem",
                        "OLD"."_TargetInCollectionRelation._Client.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."CollectionRelationType" AS "OLD_CollectionRelationType" ,
                "OLD"."_SourceInCollectionRelation.CollectionID" AS "OLD__SourceInCollectionRelation.CollectionID" ,
                "OLD"."_SourceInCollectionRelation.IDSystem" AS "OLD__SourceInCollectionRelation.IDSystem" ,
                "OLD"."_SourceInCollectionRelation._Client.BusinessPartnerID" AS "OLD__SourceInCollectionRelation._Client.BusinessPartnerID" ,
                "OLD"."_TargetInCollectionRelation.CollectionID" AS "OLD__TargetInCollectionRelation.CollectionID" ,
                "OLD"."_TargetInCollectionRelation.IDSystem" AS "OLD__TargetInCollectionRelation.IDSystem" ,
                "OLD"."_TargetInCollectionRelation._Client.BusinessPartnerID" AS "OLD__TargetInCollectionRelation._Client.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CollectionRelation" as "OLD"
            on
                      "IN"."CollectionRelationType" = "OLD"."CollectionRelationType" and
                      "IN"."_SourceInCollectionRelation.CollectionID" = "OLD"."_SourceInCollectionRelation.CollectionID" and
                      "IN"."_SourceInCollectionRelation.IDSystem" = "OLD"."_SourceInCollectionRelation.IDSystem" and
                      "IN"."_SourceInCollectionRelation._Client.BusinessPartnerID" = "OLD"."_SourceInCollectionRelation._Client.BusinessPartnerID" and
                      "IN"."_TargetInCollectionRelation.CollectionID" = "OLD"."_TargetInCollectionRelation.CollectionID" and
                      "IN"."_TargetInCollectionRelation.IDSystem" = "OLD"."_TargetInCollectionRelation.IDSystem" and
                      "IN"."_TargetInCollectionRelation._Client.BusinessPartnerID" = "OLD"."_TargetInCollectionRelation._Client.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CollectionRelation" (
        "CollectionRelationType",
        "_SourceInCollectionRelation.CollectionID",
        "_SourceInCollectionRelation.IDSystem",
        "_SourceInCollectionRelation._Client.BusinessPartnerID",
        "_TargetInCollectionRelation.CollectionID",
        "_TargetInCollectionRelation.IDSystem",
        "_TargetInCollectionRelation._Client.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CollectionRelationType" as "CollectionRelationType",
            "OLD__SourceInCollectionRelation.CollectionID" as "_SourceInCollectionRelation.CollectionID",
            "OLD__SourceInCollectionRelation.IDSystem" as "_SourceInCollectionRelation.IDSystem",
            "OLD__SourceInCollectionRelation._Client.BusinessPartnerID" as "_SourceInCollectionRelation._Client.BusinessPartnerID",
            "OLD__TargetInCollectionRelation.CollectionID" as "_TargetInCollectionRelation.CollectionID",
            "OLD__TargetInCollectionRelation.IDSystem" as "_TargetInCollectionRelation.IDSystem",
            "OLD__TargetInCollectionRelation._Client.BusinessPartnerID" as "_TargetInCollectionRelation._Client.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."CollectionRelationType",
                        "OLD"."_SourceInCollectionRelation.CollectionID",
                        "OLD"."_SourceInCollectionRelation.IDSystem",
                        "OLD"."_SourceInCollectionRelation._Client.BusinessPartnerID",
                        "OLD"."_TargetInCollectionRelation.CollectionID",
                        "OLD"."_TargetInCollectionRelation.IDSystem",
                        "OLD"."_TargetInCollectionRelation._Client.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."CollectionRelationType" AS "OLD_CollectionRelationType" ,
                "OLD"."_SourceInCollectionRelation.CollectionID" AS "OLD__SourceInCollectionRelation.CollectionID" ,
                "OLD"."_SourceInCollectionRelation.IDSystem" AS "OLD__SourceInCollectionRelation.IDSystem" ,
                "OLD"."_SourceInCollectionRelation._Client.BusinessPartnerID" AS "OLD__SourceInCollectionRelation._Client.BusinessPartnerID" ,
                "OLD"."_TargetInCollectionRelation.CollectionID" AS "OLD__TargetInCollectionRelation.CollectionID" ,
                "OLD"."_TargetInCollectionRelation.IDSystem" AS "OLD__TargetInCollectionRelation.IDSystem" ,
                "OLD"."_TargetInCollectionRelation._Client.BusinessPartnerID" AS "OLD__TargetInCollectionRelation._Client.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CollectionRelation" as "OLD"
            on
                                                "IN"."CollectionRelationType" = "OLD"."CollectionRelationType" and
                                                "IN"."_SourceInCollectionRelation.CollectionID" = "OLD"."_SourceInCollectionRelation.CollectionID" and
                                                "IN"."_SourceInCollectionRelation.IDSystem" = "OLD"."_SourceInCollectionRelation.IDSystem" and
                                                "IN"."_SourceInCollectionRelation._Client.BusinessPartnerID" = "OLD"."_SourceInCollectionRelation._Client.BusinessPartnerID" and
                                                "IN"."_TargetInCollectionRelation.CollectionID" = "OLD"."_TargetInCollectionRelation.CollectionID" and
                                                "IN"."_TargetInCollectionRelation.IDSystem" = "OLD"."_TargetInCollectionRelation.IDSystem" and
                                                "IN"."_TargetInCollectionRelation._Client.BusinessPartnerID" = "OLD"."_TargetInCollectionRelation._Client.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::CollectionRelation"
    where (
        "CollectionRelationType",
        "_SourceInCollectionRelation.CollectionID",
        "_SourceInCollectionRelation.IDSystem",
        "_SourceInCollectionRelation._Client.BusinessPartnerID",
        "_TargetInCollectionRelation.CollectionID",
        "_TargetInCollectionRelation.IDSystem",
        "_TargetInCollectionRelation._Client.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."CollectionRelationType",
            "OLD"."_SourceInCollectionRelation.CollectionID",
            "OLD"."_SourceInCollectionRelation.IDSystem",
            "OLD"."_SourceInCollectionRelation._Client.BusinessPartnerID",
            "OLD"."_TargetInCollectionRelation.CollectionID",
            "OLD"."_TargetInCollectionRelation.IDSystem",
            "OLD"."_TargetInCollectionRelation._Client.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CollectionRelation" as "OLD"
        on
                                       "IN"."CollectionRelationType" = "OLD"."CollectionRelationType" and
                                       "IN"."_SourceInCollectionRelation.CollectionID" = "OLD"."_SourceInCollectionRelation.CollectionID" and
                                       "IN"."_SourceInCollectionRelation.IDSystem" = "OLD"."_SourceInCollectionRelation.IDSystem" and
                                       "IN"."_SourceInCollectionRelation._Client.BusinessPartnerID" = "OLD"."_SourceInCollectionRelation._Client.BusinessPartnerID" and
                                       "IN"."_TargetInCollectionRelation.CollectionID" = "OLD"."_TargetInCollectionRelation.CollectionID" and
                                       "IN"."_TargetInCollectionRelation.IDSystem" = "OLD"."_TargetInCollectionRelation.IDSystem" and
                                       "IN"."_TargetInCollectionRelation._Client.BusinessPartnerID" = "OLD"."_TargetInCollectionRelation._Client.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
