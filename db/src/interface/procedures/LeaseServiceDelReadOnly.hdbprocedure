PROCEDURE "sap.fsdm.procedures::LeaseServiceDelReadOnly" (IN ROW "sap.fsdm.tabletypes::LeaseServiceTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::LeaseServiceTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::LeaseServiceTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ID=' || TO_VARCHAR("ID") || ' ' ||
                '_LeaseServiceAgreement.FinancialContractID=' || TO_VARCHAR("_LeaseServiceAgreement.FinancialContractID") || ' ' ||
                '_LeaseServiceAgreement.IDSystem=' || TO_VARCHAR("_LeaseServiceAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."_LeaseServiceAgreement.FinancialContractID",
                        "IN"."_LeaseServiceAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."_LeaseServiceAgreement.FinancialContractID",
                        "IN"."_LeaseServiceAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ID",
                        "_LeaseServiceAgreement.FinancialContractID",
                        "_LeaseServiceAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."_LeaseServiceAgreement.FinancialContractID",
                                    "IN"."_LeaseServiceAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."_LeaseServiceAgreement.FinancialContractID",
                                    "IN"."_LeaseServiceAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ID" is null and
            "_LeaseServiceAgreement.FinancialContractID" is null and
            "_LeaseServiceAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "ID",
            "_LeaseServiceAgreement.FinancialContractID",
            "_LeaseServiceAgreement.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::LeaseService" WHERE
            (
            "ID" ,
            "_LeaseServiceAgreement.FinancialContractID" ,
            "_LeaseServiceAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."ID",
            "OLD"."_LeaseServiceAgreement.FinancialContractID",
            "OLD"."_LeaseServiceAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::LeaseService" as "OLD"
        on
                              "IN"."ID" = "OLD"."ID" and
                              "IN"."_LeaseServiceAgreement.FinancialContractID" = "OLD"."_LeaseServiceAgreement.FinancialContractID" and
                              "IN"."_LeaseServiceAgreement.IDSystem" = "OLD"."_LeaseServiceAgreement.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "ID",
        "_LeaseServiceAgreement.FinancialContractID",
        "_LeaseServiceAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Description",
        "EndDate",
        "StartDate",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_ID" as "ID" ,
            "OLD__LeaseServiceAgreement.FinancialContractID" as "_LeaseServiceAgreement.FinancialContractID" ,
            "OLD__LeaseServiceAgreement.IDSystem" as "_LeaseServiceAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Description" as "Description" ,
            "OLD_EndDate" as "EndDate" ,
            "OLD_StartDate" as "StartDate" ,
            "OLD_Type" as "Type" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ID",
                        "OLD"."_LeaseServiceAgreement.FinancialContractID",
                        "OLD"."_LeaseServiceAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."ID" AS "OLD_ID" ,
                "OLD"."_LeaseServiceAgreement.FinancialContractID" AS "OLD__LeaseServiceAgreement.FinancialContractID" ,
                "OLD"."_LeaseServiceAgreement.IDSystem" AS "OLD__LeaseServiceAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."Type" AS "OLD_Type" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::LeaseService" as "OLD"
            on
                                      "IN"."ID" = "OLD"."ID" and
                                      "IN"."_LeaseServiceAgreement.FinancialContractID" = "OLD"."_LeaseServiceAgreement.FinancialContractID" and
                                      "IN"."_LeaseServiceAgreement.IDSystem" = "OLD"."_LeaseServiceAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_ID" as "ID",
            "OLD__LeaseServiceAgreement.FinancialContractID" as "_LeaseServiceAgreement.FinancialContractID",
            "OLD__LeaseServiceAgreement.IDSystem" as "_LeaseServiceAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Description" as "Description",
            "OLD_EndDate" as "EndDate",
            "OLD_StartDate" as "StartDate",
            "OLD_Type" as "Type",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ID",
                        "OLD"."_LeaseServiceAgreement.FinancialContractID",
                        "OLD"."_LeaseServiceAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ID" AS "OLD_ID" ,
                "OLD"."_LeaseServiceAgreement.FinancialContractID" AS "OLD__LeaseServiceAgreement.FinancialContractID" ,
                "OLD"."_LeaseServiceAgreement.IDSystem" AS "OLD__LeaseServiceAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."Type" AS "OLD_Type" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::LeaseService" as "OLD"
            on
               "IN"."ID" = "OLD"."ID" and
               "IN"."_LeaseServiceAgreement.FinancialContractID" = "OLD"."_LeaseServiceAgreement.FinancialContractID" and
               "IN"."_LeaseServiceAgreement.IDSystem" = "OLD"."_LeaseServiceAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
