PROCEDURE "sap.fsdm.procedures::TradingLimitAssignmentLoad" (IN ROW "sap.fsdm.tabletypes::TradingLimitAssignmentTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ContributionAmountType=' || TO_VARCHAR("ContributionAmountType") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_SecuritiesAccount.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccount.FinancialContractID") || ' ' ||
                '_SecuritiesAccount.IDSystem=' || TO_VARCHAR("_SecuritiesAccount.IDSystem") || ' ' ||
                '_TradingLimit.LimitID=' || TO_VARCHAR("_TradingLimit.LimitID") || ' ' ||
                '_TradingLimit._TimeBucket.MaturityBandID=' || TO_VARCHAR("_TradingLimit._TimeBucket.MaturityBandID") || ' ' ||
                '_TradingLimit._TimeBucket.TimeBucketID=' || TO_VARCHAR("_TradingLimit._TimeBucket.TimeBucketID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ContributionAmountType",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TradingLimit.LimitID",
                        "IN"."_TradingLimit._TimeBucket.MaturityBandID",
                        "IN"."_TradingLimit._TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ContributionAmountType",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TradingLimit.LimitID",
                        "IN"."_TradingLimit._TimeBucket.MaturityBandID",
                        "IN"."_TradingLimit._TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ContributionAmountType",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_SecuritiesAccount.FinancialContractID",
                        "_SecuritiesAccount.IDSystem",
                        "_TradingLimit.LimitID",
                        "_TradingLimit._TimeBucket.MaturityBandID",
                        "_TradingLimit._TimeBucket.TimeBucketID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ContributionAmountType",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem",
                                    "IN"."_TradingLimit.LimitID",
                                    "IN"."_TradingLimit._TimeBucket.MaturityBandID",
                                    "IN"."_TradingLimit._TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ContributionAmountType",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem",
                                    "IN"."_TradingLimit.LimitID",
                                    "IN"."_TradingLimit._TimeBucket.MaturityBandID",
                                    "IN"."_TradingLimit._TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::TradingLimitAssignment" (
        "ContributionAmountType",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_TradingLimit.LimitID",
        "_TradingLimit._TimeBucket.MaturityBandID",
        "_TradingLimit._TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ContributionAmount",
        "ContributionAmountCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ContributionAmountType" as "ContributionAmountType" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID" ,
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem" ,
            "OLD__TradingLimit.LimitID" as "_TradingLimit.LimitID" ,
            "OLD__TradingLimit._TimeBucket.MaturityBandID" as "_TradingLimit._TimeBucket.MaturityBandID" ,
            "OLD__TradingLimit._TimeBucket.TimeBucketID" as "_TradingLimit._TimeBucket.TimeBucketID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ContributionAmount" as "ContributionAmount" ,
            "OLD_ContributionAmountCurrency" as "ContributionAmountCurrency" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."ContributionAmountType",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TradingLimit.LimitID",
                        "IN"."_TradingLimit._TimeBucket.MaturityBandID",
                        "IN"."_TradingLimit._TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."ContributionAmountType" as "OLD_ContributionAmountType",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                                "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                                "OLD"."_TradingLimit.LimitID" as "OLD__TradingLimit.LimitID",
                                "OLD"."_TradingLimit._TimeBucket.MaturityBandID" as "OLD__TradingLimit._TimeBucket.MaturityBandID",
                                "OLD"."_TradingLimit._TimeBucket.TimeBucketID" as "OLD__TradingLimit._TimeBucket.TimeBucketID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."ContributionAmount" as "OLD_ContributionAmount",
                                "OLD"."ContributionAmountCurrency" as "OLD_ContributionAmountCurrency",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::TradingLimitAssignment" as "OLD"
            on
                ifnull( "IN"."ContributionAmountType", '') = "OLD"."ContributionAmountType" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" and
                ifnull( "IN"."_TradingLimit.LimitID", '') = "OLD"."_TradingLimit.LimitID" and
                ifnull( "IN"."_TradingLimit._TimeBucket.MaturityBandID", '') = "OLD"."_TradingLimit._TimeBucket.MaturityBandID" and
                ifnull( "IN"."_TradingLimit._TimeBucket.TimeBucketID", '') = "OLD"."_TradingLimit._TimeBucket.TimeBucketID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::TradingLimitAssignment" (
        "ContributionAmountType",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_TradingLimit.LimitID",
        "_TradingLimit._TimeBucket.MaturityBandID",
        "_TradingLimit._TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ContributionAmount",
        "ContributionAmountCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ContributionAmountType"  as "ContributionAmountType",
            "OLD__FinancialContract.FinancialContractID"  as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem"  as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID"  as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__SecuritiesAccount.FinancialContractID"  as "_SecuritiesAccount.FinancialContractID",
            "OLD__SecuritiesAccount.IDSystem"  as "_SecuritiesAccount.IDSystem",
            "OLD__TradingLimit.LimitID"  as "_TradingLimit.LimitID",
            "OLD__TradingLimit._TimeBucket.MaturityBandID"  as "_TradingLimit._TimeBucket.MaturityBandID",
            "OLD__TradingLimit._TimeBucket.TimeBucketID"  as "_TradingLimit._TimeBucket.TimeBucketID",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_ContributionAmount"  as "ContributionAmount",
            "OLD_ContributionAmountCurrency"  as "ContributionAmountCurrency",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."ContributionAmountType",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TradingLimit.LimitID",
                        "IN"."_TradingLimit._TimeBucket.MaturityBandID",
                        "IN"."_TradingLimit._TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."ContributionAmountType" as "OLD_ContributionAmountType",
                        "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                        "OLD"."_TradingLimit.LimitID" as "OLD__TradingLimit.LimitID",
                        "OLD"."_TradingLimit._TimeBucket.MaturityBandID" as "OLD__TradingLimit._TimeBucket.MaturityBandID",
                        "OLD"."_TradingLimit._TimeBucket.TimeBucketID" as "OLD__TradingLimit._TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."ContributionAmount" as "OLD_ContributionAmount",
                        "OLD"."ContributionAmountCurrency" as "OLD_ContributionAmountCurrency",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::TradingLimitAssignment" as "OLD"
            on
                ifnull( "IN"."ContributionAmountType", '' ) = "OLD"."ContributionAmountType" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '' ) = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccount.IDSystem", '' ) = "OLD"."_SecuritiesAccount.IDSystem" and
                ifnull( "IN"."_TradingLimit.LimitID", '' ) = "OLD"."_TradingLimit.LimitID" and
                ifnull( "IN"."_TradingLimit._TimeBucket.MaturityBandID", '' ) = "OLD"."_TradingLimit._TimeBucket.MaturityBandID" and
                ifnull( "IN"."_TradingLimit._TimeBucket.TimeBucketID", '' ) = "OLD"."_TradingLimit._TimeBucket.TimeBucketID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::TradingLimitAssignment"
    where (
        "ContributionAmountType",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_TradingLimit.LimitID",
        "_TradingLimit._TimeBucket.MaturityBandID",
        "_TradingLimit._TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ContributionAmountType",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_SecuritiesAccount.FinancialContractID",
            "OLD"."_SecuritiesAccount.IDSystem",
            "OLD"."_TradingLimit.LimitID",
            "OLD"."_TradingLimit._TimeBucket.MaturityBandID",
            "OLD"."_TradingLimit._TimeBucket.TimeBucketID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TradingLimitAssignment" as "OLD"
        on
           ifnull( "IN"."ContributionAmountType", '' ) = "OLD"."ContributionAmountType" and
           ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
           ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" and
           ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
           ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '' ) = "OLD"."_SecuritiesAccount.FinancialContractID" and
           ifnull( "IN"."_SecuritiesAccount.IDSystem", '' ) = "OLD"."_SecuritiesAccount.IDSystem" and
           ifnull( "IN"."_TradingLimit.LimitID", '' ) = "OLD"."_TradingLimit.LimitID" and
           ifnull( "IN"."_TradingLimit._TimeBucket.MaturityBandID", '' ) = "OLD"."_TradingLimit._TimeBucket.MaturityBandID" and
           ifnull( "IN"."_TradingLimit._TimeBucket.TimeBucketID", '' ) = "OLD"."_TradingLimit._TimeBucket.TimeBucketID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::TradingLimitAssignment" (
        "ContributionAmountType",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_TradingLimit.LimitID",
        "_TradingLimit._TimeBucket.MaturityBandID",
        "_TradingLimit._TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ContributionAmount",
        "ContributionAmountCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "ContributionAmountType", '' ) as "ContributionAmountType",
            ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
            ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
            ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
            ifnull( "_SecuritiesAccount.FinancialContractID", '' ) as "_SecuritiesAccount.FinancialContractID",
            ifnull( "_SecuritiesAccount.IDSystem", '' ) as "_SecuritiesAccount.IDSystem",
            ifnull( "_TradingLimit.LimitID", '' ) as "_TradingLimit.LimitID",
            ifnull( "_TradingLimit._TimeBucket.MaturityBandID", '' ) as "_TradingLimit._TimeBucket.MaturityBandID",
            ifnull( "_TradingLimit._TimeBucket.TimeBucketID", '' ) as "_TradingLimit._TimeBucket.TimeBucketID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "ContributionAmount"  ,
            "ContributionAmountCurrency"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END