PROCEDURE "sap.fsdm.procedures::PlanBudgetForecastDelete" (IN ROW "sap.fsdm.tabletypes::PlanBudgetForecastTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ID=' || TO_VARCHAR("ID") || ' ' ||
                'PlanBudgetForecastScenario=' || TO_VARCHAR("PlanBudgetForecastScenario") || ' ' ||
                'VersionID=' || TO_VARCHAR("VersionID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."PlanBudgetForecastScenario",
                        "IN"."VersionID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."PlanBudgetForecastScenario",
                        "IN"."VersionID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ID",
                        "PlanBudgetForecastScenario",
                        "VersionID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."PlanBudgetForecastScenario",
                                    "IN"."VersionID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."PlanBudgetForecastScenario",
                                    "IN"."VersionID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ID" is null and
            "PlanBudgetForecastScenario" is null and
            "VersionID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::PlanBudgetForecast" (
        "ID",
        "PlanBudgetForecastScenario",
        "VersionID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Plan.ID",
        "_Plan.PlanBudgetForecastScenario",
        "_Plan.VersionID",
        "Category",
        "EndDate",
        "StartDate",
        "Status",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ID" as "ID" ,
            "OLD_PlanBudgetForecastScenario" as "PlanBudgetForecastScenario" ,
            "OLD_VersionID" as "VersionID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__Plan.ID" as "_Plan.ID" ,
            "OLD__Plan.PlanBudgetForecastScenario" as "_Plan.PlanBudgetForecastScenario" ,
            "OLD__Plan.VersionID" as "_Plan.VersionID" ,
            "OLD_Category" as "Category" ,
            "OLD_EndDate" as "EndDate" ,
            "OLD_StartDate" as "StartDate" ,
            "OLD_Status" as "Status" ,
            "OLD_Type" as "Type" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ID",
                        "OLD"."PlanBudgetForecastScenario",
                        "OLD"."VersionID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."ID" AS "OLD_ID" ,
                "OLD"."PlanBudgetForecastScenario" AS "OLD_PlanBudgetForecastScenario" ,
                "OLD"."VersionID" AS "OLD_VersionID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_Plan.ID" AS "OLD__Plan.ID" ,
                "OLD"."_Plan.PlanBudgetForecastScenario" AS "OLD__Plan.PlanBudgetForecastScenario" ,
                "OLD"."_Plan.VersionID" AS "OLD__Plan.VersionID" ,
                "OLD"."Category" AS "OLD_Category" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."Type" AS "OLD_Type" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PlanBudgetForecast" as "OLD"
            on
                      "IN"."ID" = "OLD"."ID" and
                      "IN"."PlanBudgetForecastScenario" = "OLD"."PlanBudgetForecastScenario" and
                      "IN"."VersionID" = "OLD"."VersionID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::PlanBudgetForecast" (
        "ID",
        "PlanBudgetForecastScenario",
        "VersionID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Plan.ID",
        "_Plan.PlanBudgetForecastScenario",
        "_Plan.VersionID",
        "Category",
        "EndDate",
        "StartDate",
        "Status",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ID" as "ID",
            "OLD_PlanBudgetForecastScenario" as "PlanBudgetForecastScenario",
            "OLD_VersionID" as "VersionID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__Plan.ID" as "_Plan.ID",
            "OLD__Plan.PlanBudgetForecastScenario" as "_Plan.PlanBudgetForecastScenario",
            "OLD__Plan.VersionID" as "_Plan.VersionID",
            "OLD_Category" as "Category",
            "OLD_EndDate" as "EndDate",
            "OLD_StartDate" as "StartDate",
            "OLD_Status" as "Status",
            "OLD_Type" as "Type",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ID",
                        "OLD"."PlanBudgetForecastScenario",
                        "OLD"."VersionID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ID" AS "OLD_ID" ,
                "OLD"."PlanBudgetForecastScenario" AS "OLD_PlanBudgetForecastScenario" ,
                "OLD"."VersionID" AS "OLD_VersionID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_Plan.ID" AS "OLD__Plan.ID" ,
                "OLD"."_Plan.PlanBudgetForecastScenario" AS "OLD__Plan.PlanBudgetForecastScenario" ,
                "OLD"."_Plan.VersionID" AS "OLD__Plan.VersionID" ,
                "OLD"."Category" AS "OLD_Category" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."Type" AS "OLD_Type" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PlanBudgetForecast" as "OLD"
            on
                                                "IN"."ID" = "OLD"."ID" and
                                                "IN"."PlanBudgetForecastScenario" = "OLD"."PlanBudgetForecastScenario" and
                                                "IN"."VersionID" = "OLD"."VersionID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::PlanBudgetForecast"
    where (
        "ID",
        "PlanBudgetForecastScenario",
        "VersionID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ID",
            "OLD"."PlanBudgetForecastScenario",
            "OLD"."VersionID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PlanBudgetForecast" as "OLD"
        on
                                       "IN"."ID" = "OLD"."ID" and
                                       "IN"."PlanBudgetForecastScenario" = "OLD"."PlanBudgetForecastScenario" and
                                       "IN"."VersionID" = "OLD"."VersionID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
