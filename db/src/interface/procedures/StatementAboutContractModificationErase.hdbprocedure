PROCEDURE "sap.fsdm.procedures::StatementAboutContractModificationErase" (IN ROW "sap.fsdm.tabletypes::StatementAboutContractModificationTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "BookValueType" is null and
            "ModificationCategory" is null and
            "ModificationDate" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::StatementAboutContractModification"
        WHERE
        (            "BookValueType" ,
            "ModificationCategory" ,
            "ModificationDate" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" 
        ) in
        (
            select                 "OLD"."BookValueType" ,
                "OLD"."ModificationCategory" ,
                "OLD"."ModificationDate" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::StatementAboutContractModification" "OLD"
            on
            "IN"."BookValueType" = "OLD"."BookValueType" and
            "IN"."ModificationCategory" = "OLD"."ModificationCategory" and
            "IN"."ModificationDate" = "OLD"."ModificationDate" and
            "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::StatementAboutContractModification_Historical"
        WHERE
        (
            "BookValueType" ,
            "ModificationCategory" ,
            "ModificationDate" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" 
        ) in
        (
            select
                "OLD"."BookValueType" ,
                "OLD"."ModificationCategory" ,
                "OLD"."ModificationDate" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::StatementAboutContractModification_Historical" "OLD"
            on
                "IN"."BookValueType" = "OLD"."BookValueType" and
                "IN"."ModificationCategory" = "OLD"."ModificationCategory" and
                "IN"."ModificationDate" = "OLD"."ModificationDate" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        );

END
