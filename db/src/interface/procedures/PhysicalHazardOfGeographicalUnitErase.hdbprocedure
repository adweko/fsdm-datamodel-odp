PROCEDURE "sap.fsdm.procedures::PhysicalHazardOfGeographicalUnitErase" (IN ROW "sap.fsdm.tabletypes::PhysicalHazardOfGeographicalUnitTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "DataProvider" is null and
            "PhysicalHazardType" is null and
            "_GeographicalUnit.GeographicalStructureID" is null and
            "_GeographicalUnit.GeographicalUnitID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::PhysicalHazardOfGeographicalUnit"
        WHERE
        (            "DataProvider" ,
            "PhysicalHazardType" ,
            "_GeographicalUnit.GeographicalStructureID" ,
            "_GeographicalUnit.GeographicalUnitID" 
        ) in
        (
            select                 "OLD"."DataProvider" ,
                "OLD"."PhysicalHazardType" ,
                "OLD"."_GeographicalUnit.GeographicalStructureID" ,
                "OLD"."_GeographicalUnit.GeographicalUnitID" 
            from :ROW "IN"
            inner join "sap.fsdm::PhysicalHazardOfGeographicalUnit" "OLD"
            on
            "IN"."DataProvider" = "OLD"."DataProvider" and
            "IN"."PhysicalHazardType" = "OLD"."PhysicalHazardType" and
            "IN"."_GeographicalUnit.GeographicalStructureID" = "OLD"."_GeographicalUnit.GeographicalStructureID" and
            "IN"."_GeographicalUnit.GeographicalUnitID" = "OLD"."_GeographicalUnit.GeographicalUnitID" 
        );

        --delete data from history table
        delete from "sap.fsdm::PhysicalHazardOfGeographicalUnit_Historical"
        WHERE
        (
            "DataProvider" ,
            "PhysicalHazardType" ,
            "_GeographicalUnit.GeographicalStructureID" ,
            "_GeographicalUnit.GeographicalUnitID" 
        ) in
        (
            select
                "OLD"."DataProvider" ,
                "OLD"."PhysicalHazardType" ,
                "OLD"."_GeographicalUnit.GeographicalStructureID" ,
                "OLD"."_GeographicalUnit.GeographicalUnitID" 
            from :ROW "IN"
            inner join "sap.fsdm::PhysicalHazardOfGeographicalUnit_Historical" "OLD"
            on
                "IN"."DataProvider" = "OLD"."DataProvider" and
                "IN"."PhysicalHazardType" = "OLD"."PhysicalHazardType" and
                "IN"."_GeographicalUnit.GeographicalStructureID" = "OLD"."_GeographicalUnit.GeographicalStructureID" and
                "IN"."_GeographicalUnit.GeographicalUnitID" = "OLD"."_GeographicalUnit.GeographicalUnitID" 
        );

END
