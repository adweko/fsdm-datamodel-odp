PROCEDURE "sap.fsdm.procedures::TradingPeriodDelReadOnly" (IN ROW "sap.fsdm.tabletypes::TradingPeriodTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::TradingPeriodTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::TradingPeriodTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'StartDate=' || TO_VARCHAR("StartDate") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."StartDate",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."StartDate",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "StartDate",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."StartDate",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."StartDate",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "StartDate" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "StartDate",
            "_Collection.CollectionID",
            "_Collection.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::TradingPeriod" WHERE
            (
            "StartDate" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."StartDate",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TradingPeriod" as "OLD"
        on
                              "IN"."StartDate" = "OLD"."StartDate" and
                              "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                              "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "StartDate",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "EndDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_StartDate" as "StartDate" ,
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_EndDate" as "EndDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."StartDate",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TradingPeriod" as "OLD"
            on
                                      "IN"."StartDate" = "OLD"."StartDate" and
                                      "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                                      "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_StartDate" as "StartDate",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_EndDate" as "EndDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."StartDate",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TradingPeriod" as "OLD"
            on
               "IN"."StartDate" = "OLD"."StartDate" and
               "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
               "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
