PROCEDURE "sap.fsdm.procedures::CommunicationEventStatusDelReadOnly" (IN ROW "sap.fsdm.tabletypes::CommunicationEventStatusTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::CommunicationEventStatusTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CommunicationEventStatusTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CommunicationEventStatusCategory=' || TO_VARCHAR("CommunicationEventStatusCategory") || ' ' ||
                'ASSOC_CommunicationEvent.CommunicationEventID=' || TO_VARCHAR("ASSOC_CommunicationEvent.CommunicationEventID") || ' ' ||
                'ASSOC_CommunicationEvent.IDSystem=' || TO_VARCHAR("ASSOC_CommunicationEvent.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CommunicationEventStatusCategory",
                        "IN"."ASSOC_CommunicationEvent.CommunicationEventID",
                        "IN"."ASSOC_CommunicationEvent.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CommunicationEventStatusCategory",
                        "IN"."ASSOC_CommunicationEvent.CommunicationEventID",
                        "IN"."ASSOC_CommunicationEvent.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CommunicationEventStatusCategory",
                        "ASSOC_CommunicationEvent.CommunicationEventID",
                        "ASSOC_CommunicationEvent.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CommunicationEventStatusCategory",
                                    "IN"."ASSOC_CommunicationEvent.CommunicationEventID",
                                    "IN"."ASSOC_CommunicationEvent.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CommunicationEventStatusCategory",
                                    "IN"."ASSOC_CommunicationEvent.CommunicationEventID",
                                    "IN"."ASSOC_CommunicationEvent.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "CommunicationEventStatusCategory" is null and
            "ASSOC_CommunicationEvent.CommunicationEventID" is null and
            "ASSOC_CommunicationEvent.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "CommunicationEventStatusCategory",
            "ASSOC_CommunicationEvent.CommunicationEventID",
            "ASSOC_CommunicationEvent.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::CommunicationEventStatus" WHERE
            (
            "CommunicationEventStatusCategory" ,
            "ASSOC_CommunicationEvent.CommunicationEventID" ,
            "ASSOC_CommunicationEvent.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."CommunicationEventStatusCategory",
            "OLD"."ASSOC_CommunicationEvent.CommunicationEventID",
            "OLD"."ASSOC_CommunicationEvent.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CommunicationEventStatus" as "OLD"
        on
                              "IN"."CommunicationEventStatusCategory" = "OLD"."CommunicationEventStatusCategory" and
                              "IN"."ASSOC_CommunicationEvent.CommunicationEventID" = "OLD"."ASSOC_CommunicationEvent.CommunicationEventID" and
                              "IN"."ASSOC_CommunicationEvent.IDSystem" = "OLD"."ASSOC_CommunicationEvent.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "CommunicationEventStatusCategory",
        "ASSOC_CommunicationEvent.CommunicationEventID",
        "ASSOC_CommunicationEvent.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_StatusChanger.BusinessPartnerID",
        "FollowUpCommunicationNote",
        "Status",
        "StatusChangeReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_CommunicationEventStatusCategory" as "CommunicationEventStatusCategory" ,
            "OLD_ASSOC_CommunicationEvent.CommunicationEventID" as "ASSOC_CommunicationEvent.CommunicationEventID" ,
            "OLD_ASSOC_CommunicationEvent.IDSystem" as "ASSOC_CommunicationEvent.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ASSOC_StatusChanger.BusinessPartnerID" as "ASSOC_StatusChanger.BusinessPartnerID" ,
            "OLD_FollowUpCommunicationNote" as "FollowUpCommunicationNote" ,
            "OLD_Status" as "Status" ,
            "OLD_StatusChangeReason" as "StatusChangeReason" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."CommunicationEventStatusCategory",
                        "OLD"."ASSOC_CommunicationEvent.CommunicationEventID",
                        "OLD"."ASSOC_CommunicationEvent.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."CommunicationEventStatusCategory" AS "OLD_CommunicationEventStatusCategory" ,
                "OLD"."ASSOC_CommunicationEvent.CommunicationEventID" AS "OLD_ASSOC_CommunicationEvent.CommunicationEventID" ,
                "OLD"."ASSOC_CommunicationEvent.IDSystem" AS "OLD_ASSOC_CommunicationEvent.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_StatusChanger.BusinessPartnerID" AS "OLD_ASSOC_StatusChanger.BusinessPartnerID" ,
                "OLD"."FollowUpCommunicationNote" AS "OLD_FollowUpCommunicationNote" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeReason" AS "OLD_StatusChangeReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CommunicationEventStatus" as "OLD"
            on
                                      "IN"."CommunicationEventStatusCategory" = "OLD"."CommunicationEventStatusCategory" and
                                      "IN"."ASSOC_CommunicationEvent.CommunicationEventID" = "OLD"."ASSOC_CommunicationEvent.CommunicationEventID" and
                                      "IN"."ASSOC_CommunicationEvent.IDSystem" = "OLD"."ASSOC_CommunicationEvent.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_CommunicationEventStatusCategory" as "CommunicationEventStatusCategory",
            "OLD_ASSOC_CommunicationEvent.CommunicationEventID" as "ASSOC_CommunicationEvent.CommunicationEventID",
            "OLD_ASSOC_CommunicationEvent.IDSystem" as "ASSOC_CommunicationEvent.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ASSOC_StatusChanger.BusinessPartnerID" as "ASSOC_StatusChanger.BusinessPartnerID",
            "OLD_FollowUpCommunicationNote" as "FollowUpCommunicationNote",
            "OLD_Status" as "Status",
            "OLD_StatusChangeReason" as "StatusChangeReason",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."CommunicationEventStatusCategory",
                        "OLD"."ASSOC_CommunicationEvent.CommunicationEventID",
                        "OLD"."ASSOC_CommunicationEvent.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."CommunicationEventStatusCategory" AS "OLD_CommunicationEventStatusCategory" ,
                "OLD"."ASSOC_CommunicationEvent.CommunicationEventID" AS "OLD_ASSOC_CommunicationEvent.CommunicationEventID" ,
                "OLD"."ASSOC_CommunicationEvent.IDSystem" AS "OLD_ASSOC_CommunicationEvent.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_StatusChanger.BusinessPartnerID" AS "OLD_ASSOC_StatusChanger.BusinessPartnerID" ,
                "OLD"."FollowUpCommunicationNote" AS "OLD_FollowUpCommunicationNote" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeReason" AS "OLD_StatusChangeReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CommunicationEventStatus" as "OLD"
            on
               "IN"."CommunicationEventStatusCategory" = "OLD"."CommunicationEventStatusCategory" and
               "IN"."ASSOC_CommunicationEvent.CommunicationEventID" = "OLD"."ASSOC_CommunicationEvent.CommunicationEventID" and
               "IN"."ASSOC_CommunicationEvent.IDSystem" = "OLD"."ASSOC_CommunicationEvent.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
