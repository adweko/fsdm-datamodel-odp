PROCEDURE "sap.fsdm.procedures::InstrumentClassHierarchyRelationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::InstrumentClassHierarchyRelationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::InstrumentClassHierarchyRelationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::InstrumentClassHierarchyRelationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "InstrumentHierarchy" is null and
            "_Child.InstrumentClass" is null and
            "_Child.InstrumentClassificationSystem" is null and
            "_Parent.InstrumentClass" is null and
            "_Parent.InstrumentClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "InstrumentHierarchy" ,
                "_Child.InstrumentClass" ,
                "_Child.InstrumentClassificationSystem" ,
                "_Parent.InstrumentClass" ,
                "_Parent.InstrumentClassificationSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."InstrumentHierarchy" ,
                "OLD"."_Child.InstrumentClass" ,
                "OLD"."_Child.InstrumentClassificationSystem" ,
                "OLD"."_Parent.InstrumentClass" ,
                "OLD"."_Parent.InstrumentClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InstrumentClassHierarchyRelation" "OLD"
            on
                "IN"."InstrumentHierarchy" = "OLD"."InstrumentHierarchy" and
                "IN"."_Child.InstrumentClass" = "OLD"."_Child.InstrumentClass" and
                "IN"."_Child.InstrumentClassificationSystem" = "OLD"."_Child.InstrumentClassificationSystem" and
                "IN"."_Parent.InstrumentClass" = "OLD"."_Parent.InstrumentClass" and
                "IN"."_Parent.InstrumentClassificationSystem" = "OLD"."_Parent.InstrumentClassificationSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "InstrumentHierarchy" ,
            "_Child.InstrumentClass" ,
            "_Child.InstrumentClassificationSystem" ,
            "_Parent.InstrumentClass" ,
            "_Parent.InstrumentClassificationSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."InstrumentHierarchy" ,
                "OLD"."_Child.InstrumentClass" ,
                "OLD"."_Child.InstrumentClassificationSystem" ,
                "OLD"."_Parent.InstrumentClass" ,
                "OLD"."_Parent.InstrumentClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InstrumentClassHierarchyRelation_Historical" "OLD"
            on
                "IN"."InstrumentHierarchy" = "OLD"."InstrumentHierarchy" and
                "IN"."_Child.InstrumentClass" = "OLD"."_Child.InstrumentClass" and
                "IN"."_Child.InstrumentClassificationSystem" = "OLD"."_Child.InstrumentClassificationSystem" and
                "IN"."_Parent.InstrumentClass" = "OLD"."_Parent.InstrumentClass" and
                "IN"."_Parent.InstrumentClassificationSystem" = "OLD"."_Parent.InstrumentClassificationSystem" 
        );

END
