PROCEDURE "sap.fsdm.procedures::CollateralPoolKeyFiguresDelete" (IN ROW "sap.fsdm.tabletypes::CollateralPoolKeyFiguresTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Category=' || TO_VARCHAR("Category") || ' ' ||
                'KeyDate=' || TO_VARCHAR("KeyDate") || ' ' ||
                'Provider=' || TO_VARCHAR("Provider") || ' ' ||
                'Scenario=' || TO_VARCHAR("Scenario") || ' ' ||
                '_CollateralPool.CollectionID=' || TO_VARCHAR("_CollateralPool.CollectionID") || ' ' ||
                '_CollateralPool.IDSystem=' || TO_VARCHAR("_CollateralPool.IDSystem") || ' ' ||
                '_TimeBucket.MaturityBandID=' || TO_VARCHAR("_TimeBucket.MaturityBandID") || ' ' ||
                '_TimeBucket.TimeBucketID=' || TO_VARCHAR("_TimeBucket.TimeBucketID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Category",
                        "IN"."KeyDate",
                        "IN"."Provider",
                        "IN"."Scenario",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Category",
                        "IN"."KeyDate",
                        "IN"."Provider",
                        "IN"."Scenario",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Category",
                        "KeyDate",
                        "Provider",
                        "Scenario",
                        "_CollateralPool.CollectionID",
                        "_CollateralPool.IDSystem",
                        "_TimeBucket.MaturityBandID",
                        "_TimeBucket.TimeBucketID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Category",
                                    "IN"."KeyDate",
                                    "IN"."Provider",
                                    "IN"."Scenario",
                                    "IN"."_CollateralPool.CollectionID",
                                    "IN"."_CollateralPool.IDSystem",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Category",
                                    "IN"."KeyDate",
                                    "IN"."Provider",
                                    "IN"."Scenario",
                                    "IN"."_CollateralPool.CollectionID",
                                    "IN"."_CollateralPool.IDSystem",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "Category" is null and
            "KeyDate" is null and
            "Provider" is null and
            "Scenario" is null and
            "_CollateralPool.CollectionID" is null and
            "_CollateralPool.IDSystem" is null and
            "_TimeBucket.MaturityBandID" is null and
            "_TimeBucket.TimeBucketID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::CollateralPoolKeyFigures" (
        "Category",
        "KeyDate",
        "Provider",
        "Scenario",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CalculationMethod",
        "DepositAtBankInPercent",
        "Description",
        "ExcessCollateralInPercent",
        "LiquidCollateralInPercent",
        "Name",
        "ProlongedAmortizationAmount",
        "ProlongedAmortizationAmountCurrency",
        "RegularAmortizationAmount",
        "RegularAmortizationAmountCurrency",
        "TimeBucketEndDate",
        "TimeBucketStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Category" as "Category" ,
            "OLD_KeyDate" as "KeyDate" ,
            "OLD_Provider" as "Provider" ,
            "OLD_Scenario" as "Scenario" ,
            "OLD__CollateralPool.CollectionID" as "_CollateralPool.CollectionID" ,
            "OLD__CollateralPool.IDSystem" as "_CollateralPool.IDSystem" ,
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID" ,
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CalculationMethod" as "CalculationMethod" ,
            "OLD_DepositAtBankInPercent" as "DepositAtBankInPercent" ,
            "OLD_Description" as "Description" ,
            "OLD_ExcessCollateralInPercent" as "ExcessCollateralInPercent" ,
            "OLD_LiquidCollateralInPercent" as "LiquidCollateralInPercent" ,
            "OLD_Name" as "Name" ,
            "OLD_ProlongedAmortizationAmount" as "ProlongedAmortizationAmount" ,
            "OLD_ProlongedAmortizationAmountCurrency" as "ProlongedAmortizationAmountCurrency" ,
            "OLD_RegularAmortizationAmount" as "RegularAmortizationAmount" ,
            "OLD_RegularAmortizationAmountCurrency" as "RegularAmortizationAmountCurrency" ,
            "OLD_TimeBucketEndDate" as "TimeBucketEndDate" ,
            "OLD_TimeBucketStartDate" as "TimeBucketStartDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."Category",
                        "OLD"."KeyDate",
                        "OLD"."Provider",
                        "OLD"."Scenario",
                        "OLD"."_CollateralPool.CollectionID",
                        "OLD"."_CollateralPool.IDSystem",
                        "OLD"."_TimeBucket.MaturityBandID",
                        "OLD"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."Category" AS "OLD_Category" ,
                "OLD"."KeyDate" AS "OLD_KeyDate" ,
                "OLD"."Provider" AS "OLD_Provider" ,
                "OLD"."Scenario" AS "OLD_Scenario" ,
                "OLD"."_CollateralPool.CollectionID" AS "OLD__CollateralPool.CollectionID" ,
                "OLD"."_CollateralPool.IDSystem" AS "OLD__CollateralPool.IDSystem" ,
                "OLD"."_TimeBucket.MaturityBandID" AS "OLD__TimeBucket.MaturityBandID" ,
                "OLD"."_TimeBucket.TimeBucketID" AS "OLD__TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CalculationMethod" AS "OLD_CalculationMethod" ,
                "OLD"."DepositAtBankInPercent" AS "OLD_DepositAtBankInPercent" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."ExcessCollateralInPercent" AS "OLD_ExcessCollateralInPercent" ,
                "OLD"."LiquidCollateralInPercent" AS "OLD_LiquidCollateralInPercent" ,
                "OLD"."Name" AS "OLD_Name" ,
                "OLD"."ProlongedAmortizationAmount" AS "OLD_ProlongedAmortizationAmount" ,
                "OLD"."ProlongedAmortizationAmountCurrency" AS "OLD_ProlongedAmortizationAmountCurrency" ,
                "OLD"."RegularAmortizationAmount" AS "OLD_RegularAmortizationAmount" ,
                "OLD"."RegularAmortizationAmountCurrency" AS "OLD_RegularAmortizationAmountCurrency" ,
                "OLD"."TimeBucketEndDate" AS "OLD_TimeBucketEndDate" ,
                "OLD"."TimeBucketStartDate" AS "OLD_TimeBucketStartDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CollateralPoolKeyFigures" as "OLD"
            on
                      "IN"."Category" = "OLD"."Category" and
                      "IN"."KeyDate" = "OLD"."KeyDate" and
                      "IN"."Provider" = "OLD"."Provider" and
                      "IN"."Scenario" = "OLD"."Scenario" and
                      "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
                      "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
                      "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
                      "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CollateralPoolKeyFigures" (
        "Category",
        "KeyDate",
        "Provider",
        "Scenario",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CalculationMethod",
        "DepositAtBankInPercent",
        "Description",
        "ExcessCollateralInPercent",
        "LiquidCollateralInPercent",
        "Name",
        "ProlongedAmortizationAmount",
        "ProlongedAmortizationAmountCurrency",
        "RegularAmortizationAmount",
        "RegularAmortizationAmountCurrency",
        "TimeBucketEndDate",
        "TimeBucketStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Category" as "Category",
            "OLD_KeyDate" as "KeyDate",
            "OLD_Provider" as "Provider",
            "OLD_Scenario" as "Scenario",
            "OLD__CollateralPool.CollectionID" as "_CollateralPool.CollectionID",
            "OLD__CollateralPool.IDSystem" as "_CollateralPool.IDSystem",
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID",
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CalculationMethod" as "CalculationMethod",
            "OLD_DepositAtBankInPercent" as "DepositAtBankInPercent",
            "OLD_Description" as "Description",
            "OLD_ExcessCollateralInPercent" as "ExcessCollateralInPercent",
            "OLD_LiquidCollateralInPercent" as "LiquidCollateralInPercent",
            "OLD_Name" as "Name",
            "OLD_ProlongedAmortizationAmount" as "ProlongedAmortizationAmount",
            "OLD_ProlongedAmortizationAmountCurrency" as "ProlongedAmortizationAmountCurrency",
            "OLD_RegularAmortizationAmount" as "RegularAmortizationAmount",
            "OLD_RegularAmortizationAmountCurrency" as "RegularAmortizationAmountCurrency",
            "OLD_TimeBucketEndDate" as "TimeBucketEndDate",
            "OLD_TimeBucketStartDate" as "TimeBucketStartDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."Category",
                        "OLD"."KeyDate",
                        "OLD"."Provider",
                        "OLD"."Scenario",
                        "OLD"."_CollateralPool.CollectionID",
                        "OLD"."_CollateralPool.IDSystem",
                        "OLD"."_TimeBucket.MaturityBandID",
                        "OLD"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."Category" AS "OLD_Category" ,
                "OLD"."KeyDate" AS "OLD_KeyDate" ,
                "OLD"."Provider" AS "OLD_Provider" ,
                "OLD"."Scenario" AS "OLD_Scenario" ,
                "OLD"."_CollateralPool.CollectionID" AS "OLD__CollateralPool.CollectionID" ,
                "OLD"."_CollateralPool.IDSystem" AS "OLD__CollateralPool.IDSystem" ,
                "OLD"."_TimeBucket.MaturityBandID" AS "OLD__TimeBucket.MaturityBandID" ,
                "OLD"."_TimeBucket.TimeBucketID" AS "OLD__TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CalculationMethod" AS "OLD_CalculationMethod" ,
                "OLD"."DepositAtBankInPercent" AS "OLD_DepositAtBankInPercent" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."ExcessCollateralInPercent" AS "OLD_ExcessCollateralInPercent" ,
                "OLD"."LiquidCollateralInPercent" AS "OLD_LiquidCollateralInPercent" ,
                "OLD"."Name" AS "OLD_Name" ,
                "OLD"."ProlongedAmortizationAmount" AS "OLD_ProlongedAmortizationAmount" ,
                "OLD"."ProlongedAmortizationAmountCurrency" AS "OLD_ProlongedAmortizationAmountCurrency" ,
                "OLD"."RegularAmortizationAmount" AS "OLD_RegularAmortizationAmount" ,
                "OLD"."RegularAmortizationAmountCurrency" AS "OLD_RegularAmortizationAmountCurrency" ,
                "OLD"."TimeBucketEndDate" AS "OLD_TimeBucketEndDate" ,
                "OLD"."TimeBucketStartDate" AS "OLD_TimeBucketStartDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CollateralPoolKeyFigures" as "OLD"
            on
                                                "IN"."Category" = "OLD"."Category" and
                                                "IN"."KeyDate" = "OLD"."KeyDate" and
                                                "IN"."Provider" = "OLD"."Provider" and
                                                "IN"."Scenario" = "OLD"."Scenario" and
                                                "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
                                                "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
                                                "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
                                                "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::CollateralPoolKeyFigures"
    where (
        "Category",
        "KeyDate",
        "Provider",
        "Scenario",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."Category",
            "OLD"."KeyDate",
            "OLD"."Provider",
            "OLD"."Scenario",
            "OLD"."_CollateralPool.CollectionID",
            "OLD"."_CollateralPool.IDSystem",
            "OLD"."_TimeBucket.MaturityBandID",
            "OLD"."_TimeBucket.TimeBucketID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CollateralPoolKeyFigures" as "OLD"
        on
                                       "IN"."Category" = "OLD"."Category" and
                                       "IN"."KeyDate" = "OLD"."KeyDate" and
                                       "IN"."Provider" = "OLD"."Provider" and
                                       "IN"."Scenario" = "OLD"."Scenario" and
                                       "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
                                       "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
                                       "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
                                       "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
