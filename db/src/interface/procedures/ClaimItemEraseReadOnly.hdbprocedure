PROCEDURE "sap.fsdm.procedures::ClaimItemEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ClaimItemTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ClaimItemTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ClaimItemTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ID" is null and
            "_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" is null and
            "_Subclaim._Claimant._Claim.ID" is null and
            "_Subclaim._Claimant._Claim.IDSystem" is null and
            "_Subclaim._InsuranceCoverage.ID" is null and
            "_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" is null and
            "_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" is null and
            "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" is null and
            "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" is null and
            "_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "ID" ,
                "_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
                "_Subclaim._Claimant._Claim.ID" ,
                "_Subclaim._Claimant._Claim.IDSystem" ,
                "_Subclaim._InsuranceCoverage.ID" ,
                "_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."ID" ,
                "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Subclaim._Claimant._Claim.ID" ,
                "OLD"."_Subclaim._Claimant._Claim.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage.ID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ClaimItem" "OLD"
            on
                "IN"."ID" = "OLD"."ID" and
                "IN"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" = "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" and
                "IN"."_Subclaim._Claimant._Claim.ID" = "OLD"."_Subclaim._Claimant._Claim.ID" and
                "IN"."_Subclaim._Claimant._Claim.IDSystem" = "OLD"."_Subclaim._Claimant._Claim.IDSystem" and
                "IN"."_Subclaim._InsuranceCoverage.ID" = "OLD"."_Subclaim._InsuranceCoverage.ID" and
                "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" and
                "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" and
                "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "ID" ,
            "_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
            "_Subclaim._Claimant._Claim.ID" ,
            "_Subclaim._Claimant._Claim.IDSystem" ,
            "_Subclaim._InsuranceCoverage.ID" ,
            "_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
            "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."ID" ,
                "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Subclaim._Claimant._Claim.ID" ,
                "OLD"."_Subclaim._Claimant._Claim.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage.ID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ClaimItem_Historical" "OLD"
            on
                "IN"."ID" = "OLD"."ID" and
                "IN"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" = "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" and
                "IN"."_Subclaim._Claimant._Claim.ID" = "OLD"."_Subclaim._Claimant._Claim.ID" and
                "IN"."_Subclaim._Claimant._Claim.IDSystem" = "OLD"."_Subclaim._Claimant._Claim.IDSystem" and
                "IN"."_Subclaim._InsuranceCoverage.ID" = "OLD"."_Subclaim._InsuranceCoverage.ID" and
                "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" and
                "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" and
                "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

END
