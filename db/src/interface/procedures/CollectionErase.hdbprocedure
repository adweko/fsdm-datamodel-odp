PROCEDURE "sap.fsdm.procedures::CollectionErase" (IN ROW "sap.fsdm.tabletypes::CollectionTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "CollectionID" is null and
            "IDSystem" is null and
            "_Client.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::Collection"
        WHERE
        (            "CollectionID" ,
            "IDSystem" ,
            "_Client.BusinessPartnerID" 
        ) in
        (
            select                 "OLD"."CollectionID" ,
                "OLD"."IDSystem" ,
                "OLD"."_Client.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::Collection" "OLD"
            on
            "IN"."CollectionID" = "OLD"."CollectionID" and
            "IN"."IDSystem" = "OLD"."IDSystem" and
            "IN"."_Client.BusinessPartnerID" = "OLD"."_Client.BusinessPartnerID" 
        );

        --delete data from history table
        delete from "sap.fsdm::Collection_Historical"
        WHERE
        (
            "CollectionID" ,
            "IDSystem" ,
            "_Client.BusinessPartnerID" 
        ) in
        (
            select
                "OLD"."CollectionID" ,
                "OLD"."IDSystem" ,
                "OLD"."_Client.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::Collection_Historical" "OLD"
            on
                "IN"."CollectionID" = "OLD"."CollectionID" and
                "IN"."IDSystem" = "OLD"."IDSystem" and
                "IN"."_Client.BusinessPartnerID" = "OLD"."_Client.BusinessPartnerID" 
        );

END
