PROCEDURE "sap.fsdm.procedures::ValueAtRiskBacktestingResultDelReadOnly" (IN ROW "sap.fsdm.tabletypes::ValueAtRiskBacktestingResultTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::ValueAtRiskBacktestingResultTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::ValueAtRiskBacktestingResultTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ProfitAndLossValueType=' || TO_VARCHAR("ProfitAndLossValueType") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_ValueAtRiskResult.ConfidenceLevel=' || TO_VARCHAR("_ValueAtRiskResult.ConfidenceLevel") || ' ' ||
                '_ValueAtRiskResult.RiskGroup=' || TO_VARCHAR("_ValueAtRiskResult.RiskGroup") || ' ' ||
                '_ValueAtRiskResult.RiskProvisionScenario=' || TO_VARCHAR("_ValueAtRiskResult.RiskProvisionScenario") || ' ' ||
                '_ValueAtRiskResult.RoleOfCurrency=' || TO_VARCHAR("_ValueAtRiskResult.RoleOfCurrency") || ' ' ||
                '_ValueAtRiskResult._ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ValueAtRiskResult._ResultGroup.ResultDataProvider") || ' ' ||
                '_ValueAtRiskResult._ResultGroup.ResultGroupID=' || TO_VARCHAR("_ValueAtRiskResult._ResultGroup.ResultGroupID") || ' ' ||
                '_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID=' || TO_VARCHAR("_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ProfitAndLossValueType",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult.ConfidenceLevel",
                        "IN"."_ValueAtRiskResult.RiskGroup",
                        "IN"."_ValueAtRiskResult.RiskProvisionScenario",
                        "IN"."_ValueAtRiskResult.RoleOfCurrency",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ProfitAndLossValueType",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult.ConfidenceLevel",
                        "IN"."_ValueAtRiskResult.RiskGroup",
                        "IN"."_ValueAtRiskResult.RiskProvisionScenario",
                        "IN"."_ValueAtRiskResult.RoleOfCurrency",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ProfitAndLossValueType",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_ValueAtRiskResult.ConfidenceLevel",
                        "_ValueAtRiskResult.RiskGroup",
                        "_ValueAtRiskResult.RiskProvisionScenario",
                        "_ValueAtRiskResult.RoleOfCurrency",
                        "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "_ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ProfitAndLossValueType",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_ValueAtRiskResult.ConfidenceLevel",
                                    "IN"."_ValueAtRiskResult.RiskGroup",
                                    "IN"."_ValueAtRiskResult.RiskProvisionScenario",
                                    "IN"."_ValueAtRiskResult.RoleOfCurrency",
                                    "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                                    "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                                    "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ProfitAndLossValueType",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_ValueAtRiskResult.ConfidenceLevel",
                                    "IN"."_ValueAtRiskResult.RiskGroup",
                                    "IN"."_ValueAtRiskResult.RiskProvisionScenario",
                                    "IN"."_ValueAtRiskResult.RoleOfCurrency",
                                    "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                                    "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                                    "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ProfitAndLossValueType" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_ValueAtRiskResult.ConfidenceLevel" is null and
            "_ValueAtRiskResult.RiskGroup" is null and
            "_ValueAtRiskResult.RiskProvisionScenario" is null and
            "_ValueAtRiskResult.RoleOfCurrency" is null and
            "_ValueAtRiskResult._ResultGroup.ResultDataProvider" is null and
            "_ValueAtRiskResult._ResultGroup.ResultGroupID" is null and
            "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "ProfitAndLossValueType",
            "_ResultGroup.ResultDataProvider",
            "_ResultGroup.ResultGroupID",
            "_ValueAtRiskResult.ConfidenceLevel",
            "_ValueAtRiskResult.RiskGroup",
            "_ValueAtRiskResult.RiskProvisionScenario",
            "_ValueAtRiskResult.RoleOfCurrency",
            "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
            "_ValueAtRiskResult._ResultGroup.ResultGroupID",
            "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::ValueAtRiskBacktestingResult" WHERE
            (
            "ProfitAndLossValueType" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_ValueAtRiskResult.ConfidenceLevel" ,
            "_ValueAtRiskResult.RiskGroup" ,
            "_ValueAtRiskResult.RiskProvisionScenario" ,
            "_ValueAtRiskResult.RoleOfCurrency" ,
            "_ValueAtRiskResult._ResultGroup.ResultDataProvider" ,
            "_ValueAtRiskResult._ResultGroup.ResultGroupID" ,
            "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."ProfitAndLossValueType",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_ValueAtRiskResult.ConfidenceLevel",
            "OLD"."_ValueAtRiskResult.RiskGroup",
            "OLD"."_ValueAtRiskResult.RiskProvisionScenario",
            "OLD"."_ValueAtRiskResult.RoleOfCurrency",
            "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
            "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
            "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ValueAtRiskBacktestingResult" as "OLD"
        on
                              "IN"."ProfitAndLossValueType" = "OLD"."ProfitAndLossValueType" and
                              "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                              "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                              "IN"."_ValueAtRiskResult.ConfidenceLevel" = "OLD"."_ValueAtRiskResult.ConfidenceLevel" and
                              "IN"."_ValueAtRiskResult.RiskGroup" = "OLD"."_ValueAtRiskResult.RiskGroup" and
                              "IN"."_ValueAtRiskResult.RiskProvisionScenario" = "OLD"."_ValueAtRiskResult.RiskProvisionScenario" and
                              "IN"."_ValueAtRiskResult.RoleOfCurrency" = "OLD"."_ValueAtRiskResult.RoleOfCurrency" and
                              "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" = "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" and
                              "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID" = "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID" and
                              "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" = "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "ProfitAndLossValueType",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_ValueAtRiskResult.ConfidenceLevel",
        "_ValueAtRiskResult.RiskGroup",
        "_ValueAtRiskResult.RiskProvisionScenario",
        "_ValueAtRiskResult.RoleOfCurrency",
        "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
        "_ValueAtRiskResult._ResultGroup.ResultGroupID",
        "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ExceptionRate",
        "NumberOfComparisons",
        "NumberOfExceptions",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_ProfitAndLossValueType" as "ProfitAndLossValueType" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "OLD__ValueAtRiskResult.ConfidenceLevel" as "_ValueAtRiskResult.ConfidenceLevel" ,
            "OLD__ValueAtRiskResult.RiskGroup" as "_ValueAtRiskResult.RiskGroup" ,
            "OLD__ValueAtRiskResult.RiskProvisionScenario" as "_ValueAtRiskResult.RiskProvisionScenario" ,
            "OLD__ValueAtRiskResult.RoleOfCurrency" as "_ValueAtRiskResult.RoleOfCurrency" ,
            "OLD__ValueAtRiskResult._ResultGroup.ResultDataProvider" as "_ValueAtRiskResult._ResultGroup.ResultDataProvider" ,
            "OLD__ValueAtRiskResult._ResultGroup.ResultGroupID" as "_ValueAtRiskResult._ResultGroup.ResultGroupID" ,
            "OLD__ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" as "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ExceptionRate" as "ExceptionRate" ,
            "OLD_NumberOfComparisons" as "NumberOfComparisons" ,
            "OLD_NumberOfExceptions" as "NumberOfExceptions" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ProfitAndLossValueType",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."_ValueAtRiskResult.ConfidenceLevel",
                        "OLD"."_ValueAtRiskResult.RiskGroup",
                        "OLD"."_ValueAtRiskResult.RiskProvisionScenario",
                        "OLD"."_ValueAtRiskResult.RoleOfCurrency",
                        "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."ProfitAndLossValueType" AS "OLD_ProfitAndLossValueType" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."_ValueAtRiskResult.ConfidenceLevel" AS "OLD__ValueAtRiskResult.ConfidenceLevel" ,
                "OLD"."_ValueAtRiskResult.RiskGroup" AS "OLD__ValueAtRiskResult.RiskGroup" ,
                "OLD"."_ValueAtRiskResult.RiskProvisionScenario" AS "OLD__ValueAtRiskResult.RiskProvisionScenario" ,
                "OLD"."_ValueAtRiskResult.RoleOfCurrency" AS "OLD__ValueAtRiskResult.RoleOfCurrency" ,
                "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" AS "OLD__ValueAtRiskResult._ResultGroup.ResultDataProvider" ,
                "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID" AS "OLD__ValueAtRiskResult._ResultGroup.ResultGroupID" ,
                "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" AS "OLD__ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ExceptionRate" AS "OLD_ExceptionRate" ,
                "OLD"."NumberOfComparisons" AS "OLD_NumberOfComparisons" ,
                "OLD"."NumberOfExceptions" AS "OLD_NumberOfExceptions" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ValueAtRiskBacktestingResult" as "OLD"
            on
                                      "IN"."ProfitAndLossValueType" = "OLD"."ProfitAndLossValueType" and
                                      "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                                      "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                                      "IN"."_ValueAtRiskResult.ConfidenceLevel" = "OLD"."_ValueAtRiskResult.ConfidenceLevel" and
                                      "IN"."_ValueAtRiskResult.RiskGroup" = "OLD"."_ValueAtRiskResult.RiskGroup" and
                                      "IN"."_ValueAtRiskResult.RiskProvisionScenario" = "OLD"."_ValueAtRiskResult.RiskProvisionScenario" and
                                      "IN"."_ValueAtRiskResult.RoleOfCurrency" = "OLD"."_ValueAtRiskResult.RoleOfCurrency" and
                                      "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" = "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" and
                                      "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID" = "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID" and
                                      "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" = "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_ProfitAndLossValueType" as "ProfitAndLossValueType",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__ValueAtRiskResult.ConfidenceLevel" as "_ValueAtRiskResult.ConfidenceLevel",
            "OLD__ValueAtRiskResult.RiskGroup" as "_ValueAtRiskResult.RiskGroup",
            "OLD__ValueAtRiskResult.RiskProvisionScenario" as "_ValueAtRiskResult.RiskProvisionScenario",
            "OLD__ValueAtRiskResult.RoleOfCurrency" as "_ValueAtRiskResult.RoleOfCurrency",
            "OLD__ValueAtRiskResult._ResultGroup.ResultDataProvider" as "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
            "OLD__ValueAtRiskResult._ResultGroup.ResultGroupID" as "_ValueAtRiskResult._ResultGroup.ResultGroupID",
            "OLD__ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" as "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ExceptionRate" as "ExceptionRate",
            "OLD_NumberOfComparisons" as "NumberOfComparisons",
            "OLD_NumberOfExceptions" as "NumberOfExceptions",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ProfitAndLossValueType",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."_ValueAtRiskResult.ConfidenceLevel",
                        "OLD"."_ValueAtRiskResult.RiskGroup",
                        "OLD"."_ValueAtRiskResult.RiskProvisionScenario",
                        "OLD"."_ValueAtRiskResult.RoleOfCurrency",
                        "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ProfitAndLossValueType" AS "OLD_ProfitAndLossValueType" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."_ValueAtRiskResult.ConfidenceLevel" AS "OLD__ValueAtRiskResult.ConfidenceLevel" ,
                "OLD"."_ValueAtRiskResult.RiskGroup" AS "OLD__ValueAtRiskResult.RiskGroup" ,
                "OLD"."_ValueAtRiskResult.RiskProvisionScenario" AS "OLD__ValueAtRiskResult.RiskProvisionScenario" ,
                "OLD"."_ValueAtRiskResult.RoleOfCurrency" AS "OLD__ValueAtRiskResult.RoleOfCurrency" ,
                "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" AS "OLD__ValueAtRiskResult._ResultGroup.ResultDataProvider" ,
                "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID" AS "OLD__ValueAtRiskResult._ResultGroup.ResultGroupID" ,
                "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" AS "OLD__ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ExceptionRate" AS "OLD_ExceptionRate" ,
                "OLD"."NumberOfComparisons" AS "OLD_NumberOfComparisons" ,
                "OLD"."NumberOfExceptions" AS "OLD_NumberOfExceptions" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ValueAtRiskBacktestingResult" as "OLD"
            on
               "IN"."ProfitAndLossValueType" = "OLD"."ProfitAndLossValueType" and
               "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
               "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
               "IN"."_ValueAtRiskResult.ConfidenceLevel" = "OLD"."_ValueAtRiskResult.ConfidenceLevel" and
               "IN"."_ValueAtRiskResult.RiskGroup" = "OLD"."_ValueAtRiskResult.RiskGroup" and
               "IN"."_ValueAtRiskResult.RiskProvisionScenario" = "OLD"."_ValueAtRiskResult.RiskProvisionScenario" and
               "IN"."_ValueAtRiskResult.RoleOfCurrency" = "OLD"."_ValueAtRiskResult.RoleOfCurrency" and
               "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" = "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" and
               "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID" = "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID" and
               "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" = "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
