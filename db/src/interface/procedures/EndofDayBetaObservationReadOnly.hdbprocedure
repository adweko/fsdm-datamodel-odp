PROCEDURE "sap.fsdm.procedures::EndofDayBetaObservationReadOnly" (IN ROW "sap.fsdm.tabletypes::EndofDayBetaObservationTT", OUT CURR_DEL "sap.fsdm.tabletypes::EndofDayBetaObservationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::EndofDayBetaObservationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CalculationMethod=' || TO_VARCHAR("CalculationMethod") || ' ' ||
                'DataProvider=' || TO_VARCHAR("DataProvider") || ' ' ||
                'TimeHorizon=' || TO_VARCHAR("TimeHorizon") || ' ' ||
                'TimeUnit=' || TO_VARCHAR("TimeUnit") || ' ' ||
                '_BenchmarkIndex.IndexID=' || TO_VARCHAR("_BenchmarkIndex.IndexID") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                '_Collection._Client.BusinessPartnerID=' || TO_VARCHAR("_Collection._Client.BusinessPartnerID") || ' ' ||
                '_Commodity.CommodityID=' || TO_VARCHAR("_Commodity.CommodityID") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_Index.IndexID=' || TO_VARCHAR("_Index.IndexID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CalculationMethod",
                        "IN"."DataProvider",
                        "IN"."TimeHorizon",
                        "IN"."TimeUnit",
                        "IN"."_BenchmarkIndex.IndexID",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_Commodity.CommodityID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_Index.IndexID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CalculationMethod",
                        "IN"."DataProvider",
                        "IN"."TimeHorizon",
                        "IN"."TimeUnit",
                        "IN"."_BenchmarkIndex.IndexID",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_Commodity.CommodityID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_Index.IndexID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CalculationMethod",
                        "DataProvider",
                        "TimeHorizon",
                        "TimeUnit",
                        "_BenchmarkIndex.IndexID",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem",
                        "_Collection._Client.BusinessPartnerID",
                        "_Commodity.CommodityID",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_Index.IndexID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CalculationMethod",
                                    "IN"."DataProvider",
                                    "IN"."TimeHorizon",
                                    "IN"."TimeUnit",
                                    "IN"."_BenchmarkIndex.IndexID",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_Commodity.CommodityID",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_Index.IndexID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CalculationMethod",
                                    "IN"."DataProvider",
                                    "IN"."TimeHorizon",
                                    "IN"."TimeUnit",
                                    "IN"."_BenchmarkIndex.IndexID",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_Commodity.CommodityID",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_Index.IndexID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "CalculationMethod",
        "DataProvider",
        "TimeHorizon",
        "TimeUnit",
        "_BenchmarkIndex.IndexID",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_Commodity.CommodityID",
        "_FinancialInstrument.FinancialInstrumentID",
        "_Index.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::EndofDayBetaObservation" WHERE
        (            "CalculationMethod" ,
            "DataProvider" ,
            "TimeHorizon" ,
            "TimeUnit" ,
            "_BenchmarkIndex.IndexID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_Commodity.CommodityID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_Index.IndexID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."CalculationMethod",
            "OLD"."DataProvider",
            "OLD"."TimeHorizon",
            "OLD"."TimeUnit",
            "OLD"."_BenchmarkIndex.IndexID",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."_Collection._Client.BusinessPartnerID",
            "OLD"."_Commodity.CommodityID",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_Index.IndexID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::EndofDayBetaObservation" as "OLD"
            on
               ifnull( "IN"."CalculationMethod",'' ) = "OLD"."CalculationMethod" and
               ifnull( "IN"."DataProvider",'' ) = "OLD"."DataProvider" and
               ifnull( "IN"."TimeHorizon",0 ) = "OLD"."TimeHorizon" and
               ifnull( "IN"."TimeUnit",'' ) = "OLD"."TimeUnit" and
               ifnull( "IN"."_BenchmarkIndex.IndexID",'' ) = "OLD"."_BenchmarkIndex.IndexID" and
               ifnull( "IN"."_Collection.CollectionID",'' ) = "OLD"."_Collection.CollectionID" and
               ifnull( "IN"."_Collection.IDSystem",'' ) = "OLD"."_Collection.IDSystem" and
               ifnull( "IN"."_Collection._Client.BusinessPartnerID",'' ) = "OLD"."_Collection._Client.BusinessPartnerID" and
               ifnull( "IN"."_Commodity.CommodityID",'' ) = "OLD"."_Commodity.CommodityID" and
               ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID",'' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               ifnull( "IN"."_Index.IndexID",'' ) = "OLD"."_Index.IndexID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "CalculationMethod",
        "DataProvider",
        "TimeHorizon",
        "TimeUnit",
        "_BenchmarkIndex.IndexID",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_Commodity.CommodityID",
        "_FinancialInstrument.FinancialInstrumentID",
        "_Index.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Beta",
        "BetaObservationCategory",
        "ReturnsEndDate",
        "ReturnsPeriodLength",
        "ReturnsPeriodLengthTimeUnit",
        "ReturnsStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "CalculationMethod", '' ) as "CalculationMethod",
                    ifnull( "DataProvider", '' ) as "DataProvider",
                    ifnull( "TimeHorizon", 0 ) as "TimeHorizon",
                    ifnull( "TimeUnit", '' ) as "TimeUnit",
                    ifnull( "_BenchmarkIndex.IndexID", '' ) as "_BenchmarkIndex.IndexID",
                    ifnull( "_Collection.CollectionID", '' ) as "_Collection.CollectionID",
                    ifnull( "_Collection.IDSystem", '' ) as "_Collection.IDSystem",
                    ifnull( "_Collection._Client.BusinessPartnerID", '' ) as "_Collection._Client.BusinessPartnerID",
                    ifnull( "_Commodity.CommodityID", '' ) as "_Commodity.CommodityID",
                    ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
                    ifnull( "_Index.IndexID", '' ) as "_Index.IndexID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "Beta"  ,
                    "BetaObservationCategory"  ,
                    "ReturnsEndDate"  ,
                    "ReturnsPeriodLength"  ,
                    "ReturnsPeriodLengthTimeUnit"  ,
                    "ReturnsStartDate"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_CalculationMethod" as "CalculationMethod" ,
                    "OLD_DataProvider" as "DataProvider" ,
                    "OLD_TimeHorizon" as "TimeHorizon" ,
                    "OLD_TimeUnit" as "TimeUnit" ,
                    "OLD__BenchmarkIndex.IndexID" as "_BenchmarkIndex.IndexID" ,
                    "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
                    "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
                    "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID" ,
                    "OLD__Commodity.CommodityID" as "_Commodity.CommodityID" ,
                    "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
                    "OLD__Index.IndexID" as "_Index.IndexID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_Beta" as "Beta" ,
                    "OLD_BetaObservationCategory" as "BetaObservationCategory" ,
                    "OLD_ReturnsEndDate" as "ReturnsEndDate" ,
                    "OLD_ReturnsPeriodLength" as "ReturnsPeriodLength" ,
                    "OLD_ReturnsPeriodLengthTimeUnit" as "ReturnsPeriodLengthTimeUnit" ,
                    "OLD_ReturnsStartDate" as "ReturnsStartDate" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."CalculationMethod",
                        "IN"."DataProvider",
                        "IN"."TimeHorizon",
                        "IN"."TimeUnit",
                        "IN"."_BenchmarkIndex.IndexID",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_Commodity.CommodityID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_Index.IndexID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."CalculationMethod" as "OLD_CalculationMethod",
                                "OLD"."DataProvider" as "OLD_DataProvider",
                                "OLD"."TimeHorizon" as "OLD_TimeHorizon",
                                "OLD"."TimeUnit" as "OLD_TimeUnit",
                                "OLD"."_BenchmarkIndex.IndexID" as "OLD__BenchmarkIndex.IndexID",
                                "OLD"."_Collection.CollectionID" as "OLD__Collection.CollectionID",
                                "OLD"."_Collection.IDSystem" as "OLD__Collection.IDSystem",
                                "OLD"."_Collection._Client.BusinessPartnerID" as "OLD__Collection._Client.BusinessPartnerID",
                                "OLD"."_Commodity.CommodityID" as "OLD__Commodity.CommodityID",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_Index.IndexID" as "OLD__Index.IndexID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."Beta" as "OLD_Beta",
                                "OLD"."BetaObservationCategory" as "OLD_BetaObservationCategory",
                                "OLD"."ReturnsEndDate" as "OLD_ReturnsEndDate",
                                "OLD"."ReturnsPeriodLength" as "OLD_ReturnsPeriodLength",
                                "OLD"."ReturnsPeriodLengthTimeUnit" as "OLD_ReturnsPeriodLengthTimeUnit",
                                "OLD"."ReturnsStartDate" as "OLD_ReturnsStartDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::EndofDayBetaObservation" as "OLD"
            on
                ifnull( "IN"."CalculationMethod", '') = "OLD"."CalculationMethod" and
                ifnull( "IN"."DataProvider", '') = "OLD"."DataProvider" and
                ifnull( "IN"."TimeHorizon", 0) = "OLD"."TimeHorizon" and
                ifnull( "IN"."TimeUnit", '') = "OLD"."TimeUnit" and
                ifnull( "IN"."_BenchmarkIndex.IndexID", '') = "OLD"."_BenchmarkIndex.IndexID" and
                ifnull( "IN"."_Collection.CollectionID", '') = "OLD"."_Collection.CollectionID" and
                ifnull( "IN"."_Collection.IDSystem", '') = "OLD"."_Collection.IDSystem" and
                ifnull( "IN"."_Collection._Client.BusinessPartnerID", '') = "OLD"."_Collection._Client.BusinessPartnerID" and
                ifnull( "IN"."_Commodity.CommodityID", '') = "OLD"."_Commodity.CommodityID" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_Index.IndexID", '') = "OLD"."_Index.IndexID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_CalculationMethod" as "CalculationMethod",
            "OLD_DataProvider" as "DataProvider",
            "OLD_TimeHorizon" as "TimeHorizon",
            "OLD_TimeUnit" as "TimeUnit",
            "OLD__BenchmarkIndex.IndexID" as "_BenchmarkIndex.IndexID",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID",
            "OLD__Commodity.CommodityID" as "_Commodity.CommodityID",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__Index.IndexID" as "_Index.IndexID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Beta" as "Beta",
            "OLD_BetaObservationCategory" as "BetaObservationCategory",
            "OLD_ReturnsEndDate" as "ReturnsEndDate",
            "OLD_ReturnsPeriodLength" as "ReturnsPeriodLength",
            "OLD_ReturnsPeriodLengthTimeUnit" as "ReturnsPeriodLengthTimeUnit",
            "OLD_ReturnsStartDate" as "ReturnsStartDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."CalculationMethod",
                        "IN"."DataProvider",
                        "IN"."TimeHorizon",
                        "IN"."TimeUnit",
                        "IN"."_BenchmarkIndex.IndexID",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_Commodity.CommodityID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_Index.IndexID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."CalculationMethod" as "OLD_CalculationMethod",
                                "OLD"."DataProvider" as "OLD_DataProvider",
                                "OLD"."TimeHorizon" as "OLD_TimeHorizon",
                                "OLD"."TimeUnit" as "OLD_TimeUnit",
                                "OLD"."_BenchmarkIndex.IndexID" as "OLD__BenchmarkIndex.IndexID",
                                "OLD"."_Collection.CollectionID" as "OLD__Collection.CollectionID",
                                "OLD"."_Collection.IDSystem" as "OLD__Collection.IDSystem",
                                "OLD"."_Collection._Client.BusinessPartnerID" as "OLD__Collection._Client.BusinessPartnerID",
                                "OLD"."_Commodity.CommodityID" as "OLD__Commodity.CommodityID",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_Index.IndexID" as "OLD__Index.IndexID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."Beta" as "OLD_Beta",
                                "OLD"."BetaObservationCategory" as "OLD_BetaObservationCategory",
                                "OLD"."ReturnsEndDate" as "OLD_ReturnsEndDate",
                                "OLD"."ReturnsPeriodLength" as "OLD_ReturnsPeriodLength",
                                "OLD"."ReturnsPeriodLengthTimeUnit" as "OLD_ReturnsPeriodLengthTimeUnit",
                                "OLD"."ReturnsStartDate" as "OLD_ReturnsStartDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::EndofDayBetaObservation" as "OLD"
            on
                ifnull("IN"."CalculationMethod", '') = "OLD"."CalculationMethod" and
                ifnull("IN"."DataProvider", '') = "OLD"."DataProvider" and
                ifnull("IN"."TimeHorizon", 0) = "OLD"."TimeHorizon" and
                ifnull("IN"."TimeUnit", '') = "OLD"."TimeUnit" and
                ifnull("IN"."_BenchmarkIndex.IndexID", '') = "OLD"."_BenchmarkIndex.IndexID" and
                ifnull("IN"."_Collection.CollectionID", '') = "OLD"."_Collection.CollectionID" and
                ifnull("IN"."_Collection.IDSystem", '') = "OLD"."_Collection.IDSystem" and
                ifnull("IN"."_Collection._Client.BusinessPartnerID", '') = "OLD"."_Collection._Client.BusinessPartnerID" and
                ifnull("IN"."_Commodity.CommodityID", '') = "OLD"."_Commodity.CommodityID" and
                ifnull("IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull("IN"."_Index.IndexID", '') = "OLD"."_Index.IndexID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
