PROCEDURE "sap.fsdm.procedures::AssignedRightAsCollateralReadOnly" (IN ROW "sap.fsdm.tabletypes::AssignedRightAsCollateralTT", OUT CURR_DEL "sap.fsdm.tabletypes::AssignedRightAsCollateralTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::AssignedRightAsCollateralTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_SimpleCollateralAgreement.FinancialContractID=' || TO_VARCHAR("ASSOC_SimpleCollateralAgreement.FinancialContractID") || ' ' ||
                'ASSOC_SimpleCollateralAgreement.IDSystem=' || TO_VARCHAR("ASSOC_SimpleCollateralAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                        "IN"."ASSOC_SimpleCollateralAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                        "IN"."ASSOC_SimpleCollateralAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "ASSOC_SimpleCollateralAgreement.FinancialContractID",
                        "ASSOC_SimpleCollateralAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                                    "IN"."ASSOC_SimpleCollateralAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                                    "IN"."ASSOC_SimpleCollateralAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "SequenceNumber",
        "ASSOC_SimpleCollateralAgreement.FinancialContractID",
        "ASSOC_SimpleCollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::AssignedRightAsCollateral" WHERE
        (            "SequenceNumber" ,
            "ASSOC_SimpleCollateralAgreement.FinancialContractID" ,
            "ASSOC_SimpleCollateralAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
            "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::AssignedRightAsCollateral" as "OLD"
            on
               ifnull( "IN"."SequenceNumber",-1 ) = "OLD"."SequenceNumber" and
               ifnull( "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",'' ) = "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID" and
               ifnull( "IN"."ASSOC_SimpleCollateralAgreement.IDSystem",'' ) = "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "SequenceNumber",
        "ASSOC_SimpleCollateralAgreement.FinancialContractID",
        "ASSOC_SimpleCollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType",
        "ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
        "ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
        "ASSOC_AssignedFinancialContract.FinancialContractID",
        "ASSOC_AssignedFinancialContract.IDSystem",
        "ASSOC_AssignedReceivable.ReceivableID",
        "ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID",
        "AssignedAmount",
        "AssignedAmountCurrency",
        "AssignedPercentage",
        "AssignmentOfRightCategory",
        "DeathBenefitAmount",
        "DeathBenefitAmountCurrency",
        "Description",
        "InitialCollateralValue",
        "InitialCollateralValueCurrency",
        "InsuranceType",
        "RankAmongAssetsAssignedToCollateralAgreement",
        "RankAmongCollateralAgreementsCoveredByAsset",
        "RoleOfObligorInAssignedContract",
        "SurvivalBenefitAmount",
        "SurvivalBenefitAmountCurrency",
        "ThirdPartyAccountIdentificationSystem",
        "ThirdPartyBankAccountCurrency",
        "ThirdPartyBankAccountHolderName",
        "ThirdPartyBankAccountID",
        "ThirdPartyBankID",
        "ThirdPartyBankIdentificationSystem",
        "ThirdPartyBankName",
        "ThirdPartyContractCategory",
        "ThirdPartyContractEndDate",
        "ThirdPartyContractID",
        "ThirdPartyContractStartDate",
        "ThirdPartyIBAN",
        "ThirdPartySWIFT",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "SequenceNumber", -1 ) as "SequenceNumber",
                    ifnull( "ASSOC_SimpleCollateralAgreement.FinancialContractID", '' ) as "ASSOC_SimpleCollateralAgreement.FinancialContractID",
                    ifnull( "ASSOC_SimpleCollateralAgreement.IDSystem", '' ) as "ASSOC_SimpleCollateralAgreement.IDSystem",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType"  ,
                    "ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID"  ,
                    "ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID"  ,
                    "ASSOC_AssignedFinancialContract.FinancialContractID"  ,
                    "ASSOC_AssignedFinancialContract.IDSystem"  ,
                    "ASSOC_AssignedReceivable.ReceivableID"  ,
                    "ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID"  ,
                    "AssignedAmount"  ,
                    "AssignedAmountCurrency"  ,
                    "AssignedPercentage"  ,
                    "AssignmentOfRightCategory"  ,
                    "DeathBenefitAmount"  ,
                    "DeathBenefitAmountCurrency"  ,
                    "Description"  ,
                    "InitialCollateralValue"  ,
                    "InitialCollateralValueCurrency"  ,
                    "InsuranceType"  ,
                    "RankAmongAssetsAssignedToCollateralAgreement"  ,
                    "RankAmongCollateralAgreementsCoveredByAsset"  ,
                    "RoleOfObligorInAssignedContract"  ,
                    "SurvivalBenefitAmount"  ,
                    "SurvivalBenefitAmountCurrency"  ,
                    "ThirdPartyAccountIdentificationSystem"  ,
                    "ThirdPartyBankAccountCurrency"  ,
                    "ThirdPartyBankAccountHolderName"  ,
                    "ThirdPartyBankAccountID"  ,
                    "ThirdPartyBankID"  ,
                    "ThirdPartyBankIdentificationSystem"  ,
                    "ThirdPartyBankName"  ,
                    "ThirdPartyContractCategory"  ,
                    "ThirdPartyContractEndDate"  ,
                    "ThirdPartyContractID"  ,
                    "ThirdPartyContractStartDate"  ,
                    "ThirdPartyIBAN"  ,
                    "ThirdPartySWIFT"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_SequenceNumber" as "SequenceNumber" ,
                    "OLD_ASSOC_SimpleCollateralAgreement.FinancialContractID" as "ASSOC_SimpleCollateralAgreement.FinancialContractID" ,
                    "OLD_ASSOC_SimpleCollateralAgreement.IDSystem" as "ASSOC_SimpleCollateralAgreement.IDSystem" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" as "ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" ,
                    "OLD_ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
                    "OLD_ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
                    "OLD_ASSOC_AssignedFinancialContract.FinancialContractID" as "ASSOC_AssignedFinancialContract.FinancialContractID" ,
                    "OLD_ASSOC_AssignedFinancialContract.IDSystem" as "ASSOC_AssignedFinancialContract.IDSystem" ,
                    "OLD_ASSOC_AssignedReceivable.ReceivableID" as "ASSOC_AssignedReceivable.ReceivableID" ,
                    "OLD_ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" as "ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" ,
                    "OLD_AssignedAmount" as "AssignedAmount" ,
                    "OLD_AssignedAmountCurrency" as "AssignedAmountCurrency" ,
                    "OLD_AssignedPercentage" as "AssignedPercentage" ,
                    "OLD_AssignmentOfRightCategory" as "AssignmentOfRightCategory" ,
                    "OLD_DeathBenefitAmount" as "DeathBenefitAmount" ,
                    "OLD_DeathBenefitAmountCurrency" as "DeathBenefitAmountCurrency" ,
                    "OLD_Description" as "Description" ,
                    "OLD_InitialCollateralValue" as "InitialCollateralValue" ,
                    "OLD_InitialCollateralValueCurrency" as "InitialCollateralValueCurrency" ,
                    "OLD_InsuranceType" as "InsuranceType" ,
                    "OLD_RankAmongAssetsAssignedToCollateralAgreement" as "RankAmongAssetsAssignedToCollateralAgreement" ,
                    "OLD_RankAmongCollateralAgreementsCoveredByAsset" as "RankAmongCollateralAgreementsCoveredByAsset" ,
                    "OLD_RoleOfObligorInAssignedContract" as "RoleOfObligorInAssignedContract" ,
                    "OLD_SurvivalBenefitAmount" as "SurvivalBenefitAmount" ,
                    "OLD_SurvivalBenefitAmountCurrency" as "SurvivalBenefitAmountCurrency" ,
                    "OLD_ThirdPartyAccountIdentificationSystem" as "ThirdPartyAccountIdentificationSystem" ,
                    "OLD_ThirdPartyBankAccountCurrency" as "ThirdPartyBankAccountCurrency" ,
                    "OLD_ThirdPartyBankAccountHolderName" as "ThirdPartyBankAccountHolderName" ,
                    "OLD_ThirdPartyBankAccountID" as "ThirdPartyBankAccountID" ,
                    "OLD_ThirdPartyBankID" as "ThirdPartyBankID" ,
                    "OLD_ThirdPartyBankIdentificationSystem" as "ThirdPartyBankIdentificationSystem" ,
                    "OLD_ThirdPartyBankName" as "ThirdPartyBankName" ,
                    "OLD_ThirdPartyContractCategory" as "ThirdPartyContractCategory" ,
                    "OLD_ThirdPartyContractEndDate" as "ThirdPartyContractEndDate" ,
                    "OLD_ThirdPartyContractID" as "ThirdPartyContractID" ,
                    "OLD_ThirdPartyContractStartDate" as "ThirdPartyContractStartDate" ,
                    "OLD_ThirdPartyIBAN" as "ThirdPartyIBAN" ,
                    "OLD_ThirdPartySWIFT" as "ThirdPartySWIFT" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                        "IN"."ASSOC_SimpleCollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."SequenceNumber" as "OLD_SequenceNumber",
                                "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID" as "OLD_ASSOC_SimpleCollateralAgreement.FinancialContractID",
                                "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem" as "OLD_ASSOC_SimpleCollateralAgreement.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" as "OLD_ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType",
                                "OLD"."ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" as "OLD_ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
                                "OLD"."ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" as "OLD_ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
                                "OLD"."ASSOC_AssignedFinancialContract.FinancialContractID" as "OLD_ASSOC_AssignedFinancialContract.FinancialContractID",
                                "OLD"."ASSOC_AssignedFinancialContract.IDSystem" as "OLD_ASSOC_AssignedFinancialContract.IDSystem",
                                "OLD"."ASSOC_AssignedReceivable.ReceivableID" as "OLD_ASSOC_AssignedReceivable.ReceivableID",
                                "OLD"."ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" as "OLD_ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID",
                                "OLD"."AssignedAmount" as "OLD_AssignedAmount",
                                "OLD"."AssignedAmountCurrency" as "OLD_AssignedAmountCurrency",
                                "OLD"."AssignedPercentage" as "OLD_AssignedPercentage",
                                "OLD"."AssignmentOfRightCategory" as "OLD_AssignmentOfRightCategory",
                                "OLD"."DeathBenefitAmount" as "OLD_DeathBenefitAmount",
                                "OLD"."DeathBenefitAmountCurrency" as "OLD_DeathBenefitAmountCurrency",
                                "OLD"."Description" as "OLD_Description",
                                "OLD"."InitialCollateralValue" as "OLD_InitialCollateralValue",
                                "OLD"."InitialCollateralValueCurrency" as "OLD_InitialCollateralValueCurrency",
                                "OLD"."InsuranceType" as "OLD_InsuranceType",
                                "OLD"."RankAmongAssetsAssignedToCollateralAgreement" as "OLD_RankAmongAssetsAssignedToCollateralAgreement",
                                "OLD"."RankAmongCollateralAgreementsCoveredByAsset" as "OLD_RankAmongCollateralAgreementsCoveredByAsset",
                                "OLD"."RoleOfObligorInAssignedContract" as "OLD_RoleOfObligorInAssignedContract",
                                "OLD"."SurvivalBenefitAmount" as "OLD_SurvivalBenefitAmount",
                                "OLD"."SurvivalBenefitAmountCurrency" as "OLD_SurvivalBenefitAmountCurrency",
                                "OLD"."ThirdPartyAccountIdentificationSystem" as "OLD_ThirdPartyAccountIdentificationSystem",
                                "OLD"."ThirdPartyBankAccountCurrency" as "OLD_ThirdPartyBankAccountCurrency",
                                "OLD"."ThirdPartyBankAccountHolderName" as "OLD_ThirdPartyBankAccountHolderName",
                                "OLD"."ThirdPartyBankAccountID" as "OLD_ThirdPartyBankAccountID",
                                "OLD"."ThirdPartyBankID" as "OLD_ThirdPartyBankID",
                                "OLD"."ThirdPartyBankIdentificationSystem" as "OLD_ThirdPartyBankIdentificationSystem",
                                "OLD"."ThirdPartyBankName" as "OLD_ThirdPartyBankName",
                                "OLD"."ThirdPartyContractCategory" as "OLD_ThirdPartyContractCategory",
                                "OLD"."ThirdPartyContractEndDate" as "OLD_ThirdPartyContractEndDate",
                                "OLD"."ThirdPartyContractID" as "OLD_ThirdPartyContractID",
                                "OLD"."ThirdPartyContractStartDate" as "OLD_ThirdPartyContractStartDate",
                                "OLD"."ThirdPartyIBAN" as "OLD_ThirdPartyIBAN",
                                "OLD"."ThirdPartySWIFT" as "OLD_ThirdPartySWIFT",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::AssignedRightAsCollateral" as "OLD"
            on
                ifnull( "IN"."SequenceNumber", -1) = "OLD"."SequenceNumber" and
                ifnull( "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID", '') = "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID" and
                ifnull( "IN"."ASSOC_SimpleCollateralAgreement.IDSystem", '') = "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_SimpleCollateralAgreement.FinancialContractID" as "ASSOC_SimpleCollateralAgreement.FinancialContractID",
            "OLD_ASSOC_SimpleCollateralAgreement.IDSystem" as "ASSOC_SimpleCollateralAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" as "ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType",
            "OLD_ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
            "OLD_ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
            "OLD_ASSOC_AssignedFinancialContract.FinancialContractID" as "ASSOC_AssignedFinancialContract.FinancialContractID",
            "OLD_ASSOC_AssignedFinancialContract.IDSystem" as "ASSOC_AssignedFinancialContract.IDSystem",
            "OLD_ASSOC_AssignedReceivable.ReceivableID" as "ASSOC_AssignedReceivable.ReceivableID",
            "OLD_ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" as "ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID",
            "OLD_AssignedAmount" as "AssignedAmount",
            "OLD_AssignedAmountCurrency" as "AssignedAmountCurrency",
            "OLD_AssignedPercentage" as "AssignedPercentage",
            "OLD_AssignmentOfRightCategory" as "AssignmentOfRightCategory",
            "OLD_DeathBenefitAmount" as "DeathBenefitAmount",
            "OLD_DeathBenefitAmountCurrency" as "DeathBenefitAmountCurrency",
            "OLD_Description" as "Description",
            "OLD_InitialCollateralValue" as "InitialCollateralValue",
            "OLD_InitialCollateralValueCurrency" as "InitialCollateralValueCurrency",
            "OLD_InsuranceType" as "InsuranceType",
            "OLD_RankAmongAssetsAssignedToCollateralAgreement" as "RankAmongAssetsAssignedToCollateralAgreement",
            "OLD_RankAmongCollateralAgreementsCoveredByAsset" as "RankAmongCollateralAgreementsCoveredByAsset",
            "OLD_RoleOfObligorInAssignedContract" as "RoleOfObligorInAssignedContract",
            "OLD_SurvivalBenefitAmount" as "SurvivalBenefitAmount",
            "OLD_SurvivalBenefitAmountCurrency" as "SurvivalBenefitAmountCurrency",
            "OLD_ThirdPartyAccountIdentificationSystem" as "ThirdPartyAccountIdentificationSystem",
            "OLD_ThirdPartyBankAccountCurrency" as "ThirdPartyBankAccountCurrency",
            "OLD_ThirdPartyBankAccountHolderName" as "ThirdPartyBankAccountHolderName",
            "OLD_ThirdPartyBankAccountID" as "ThirdPartyBankAccountID",
            "OLD_ThirdPartyBankID" as "ThirdPartyBankID",
            "OLD_ThirdPartyBankIdentificationSystem" as "ThirdPartyBankIdentificationSystem",
            "OLD_ThirdPartyBankName" as "ThirdPartyBankName",
            "OLD_ThirdPartyContractCategory" as "ThirdPartyContractCategory",
            "OLD_ThirdPartyContractEndDate" as "ThirdPartyContractEndDate",
            "OLD_ThirdPartyContractID" as "ThirdPartyContractID",
            "OLD_ThirdPartyContractStartDate" as "ThirdPartyContractStartDate",
            "OLD_ThirdPartyIBAN" as "ThirdPartyIBAN",
            "OLD_ThirdPartySWIFT" as "ThirdPartySWIFT",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                        "IN"."ASSOC_SimpleCollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."SequenceNumber" as "OLD_SequenceNumber",
                                "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID" as "OLD_ASSOC_SimpleCollateralAgreement.FinancialContractID",
                                "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem" as "OLD_ASSOC_SimpleCollateralAgreement.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" as "OLD_ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType",
                                "OLD"."ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" as "OLD_ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
                                "OLD"."ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" as "OLD_ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
                                "OLD"."ASSOC_AssignedFinancialContract.FinancialContractID" as "OLD_ASSOC_AssignedFinancialContract.FinancialContractID",
                                "OLD"."ASSOC_AssignedFinancialContract.IDSystem" as "OLD_ASSOC_AssignedFinancialContract.IDSystem",
                                "OLD"."ASSOC_AssignedReceivable.ReceivableID" as "OLD_ASSOC_AssignedReceivable.ReceivableID",
                                "OLD"."ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" as "OLD_ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID",
                                "OLD"."AssignedAmount" as "OLD_AssignedAmount",
                                "OLD"."AssignedAmountCurrency" as "OLD_AssignedAmountCurrency",
                                "OLD"."AssignedPercentage" as "OLD_AssignedPercentage",
                                "OLD"."AssignmentOfRightCategory" as "OLD_AssignmentOfRightCategory",
                                "OLD"."DeathBenefitAmount" as "OLD_DeathBenefitAmount",
                                "OLD"."DeathBenefitAmountCurrency" as "OLD_DeathBenefitAmountCurrency",
                                "OLD"."Description" as "OLD_Description",
                                "OLD"."InitialCollateralValue" as "OLD_InitialCollateralValue",
                                "OLD"."InitialCollateralValueCurrency" as "OLD_InitialCollateralValueCurrency",
                                "OLD"."InsuranceType" as "OLD_InsuranceType",
                                "OLD"."RankAmongAssetsAssignedToCollateralAgreement" as "OLD_RankAmongAssetsAssignedToCollateralAgreement",
                                "OLD"."RankAmongCollateralAgreementsCoveredByAsset" as "OLD_RankAmongCollateralAgreementsCoveredByAsset",
                                "OLD"."RoleOfObligorInAssignedContract" as "OLD_RoleOfObligorInAssignedContract",
                                "OLD"."SurvivalBenefitAmount" as "OLD_SurvivalBenefitAmount",
                                "OLD"."SurvivalBenefitAmountCurrency" as "OLD_SurvivalBenefitAmountCurrency",
                                "OLD"."ThirdPartyAccountIdentificationSystem" as "OLD_ThirdPartyAccountIdentificationSystem",
                                "OLD"."ThirdPartyBankAccountCurrency" as "OLD_ThirdPartyBankAccountCurrency",
                                "OLD"."ThirdPartyBankAccountHolderName" as "OLD_ThirdPartyBankAccountHolderName",
                                "OLD"."ThirdPartyBankAccountID" as "OLD_ThirdPartyBankAccountID",
                                "OLD"."ThirdPartyBankID" as "OLD_ThirdPartyBankID",
                                "OLD"."ThirdPartyBankIdentificationSystem" as "OLD_ThirdPartyBankIdentificationSystem",
                                "OLD"."ThirdPartyBankName" as "OLD_ThirdPartyBankName",
                                "OLD"."ThirdPartyContractCategory" as "OLD_ThirdPartyContractCategory",
                                "OLD"."ThirdPartyContractEndDate" as "OLD_ThirdPartyContractEndDate",
                                "OLD"."ThirdPartyContractID" as "OLD_ThirdPartyContractID",
                                "OLD"."ThirdPartyContractStartDate" as "OLD_ThirdPartyContractStartDate",
                                "OLD"."ThirdPartyIBAN" as "OLD_ThirdPartyIBAN",
                                "OLD"."ThirdPartySWIFT" as "OLD_ThirdPartySWIFT",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::AssignedRightAsCollateral" as "OLD"
            on
                ifnull("IN"."SequenceNumber", -1) = "OLD"."SequenceNumber" and
                ifnull("IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID", '') = "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID" and
                ifnull("IN"."ASSOC_SimpleCollateralAgreement.IDSystem", '') = "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
