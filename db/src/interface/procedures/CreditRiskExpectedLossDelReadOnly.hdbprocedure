PROCEDURE "sap.fsdm.procedures::CreditRiskExpectedLossDelReadOnly" (IN ROW "sap.fsdm.tabletypes::CreditRiskExpectedLossTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::CreditRiskExpectedLossTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CreditRiskExpectedLossTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'BestEstimateELMethod=' || TO_VARCHAR("BestEstimateELMethod") || ' ' ||
                'BestEstimateELTimeHorizon=' || TO_VARCHAR("BestEstimateELTimeHorizon") || ' ' ||
                'BestEstimateELTimeUnit=' || TO_VARCHAR("BestEstimateELTimeUnit") || ' ' ||
                'ASSOC_CollateralPortion.PortionNumber=' || TO_VARCHAR("ASSOC_CollateralPortion.PortionNumber") || ' ' ||
                'ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID=' || TO_VARCHAR("ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID") || ' ' ||
                'ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem=' || TO_VARCHAR("ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                '_Collection._Client.BusinessPartnerID=' || TO_VARCHAR("_Collection._Client.BusinessPartnerID") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."BestEstimateELMethod",
                        "IN"."BestEstimateELTimeHorizon",
                        "IN"."BestEstimateELTimeUnit",
                        "IN"."ASSOC_CollateralPortion.PortionNumber",
                        "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."BestEstimateELMethod",
                        "IN"."BestEstimateELTimeHorizon",
                        "IN"."BestEstimateELTimeUnit",
                        "IN"."ASSOC_CollateralPortion.PortionNumber",
                        "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "BestEstimateELMethod",
                        "BestEstimateELTimeHorizon",
                        "BestEstimateELTimeUnit",
                        "ASSOC_CollateralPortion.PortionNumber",
                        "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem",
                        "_Collection._Client.BusinessPartnerID",
                        "_FinancialInstrument.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."BestEstimateELMethod",
                                    "IN"."BestEstimateELTimeHorizon",
                                    "IN"."BestEstimateELTimeUnit",
                                    "IN"."ASSOC_CollateralPortion.PortionNumber",
                                    "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                                    "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."BestEstimateELMethod",
                                    "IN"."BestEstimateELTimeHorizon",
                                    "IN"."BestEstimateELTimeUnit",
                                    "IN"."ASSOC_CollateralPortion.PortionNumber",
                                    "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                                    "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "BestEstimateELMethod" is null and
            "BestEstimateELTimeHorizon" is null and
            "BestEstimateELTimeUnit" is null and
            "ASSOC_CollateralPortion.PortionNumber" is null and
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" is null and
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "BestEstimateELMethod",
            "BestEstimateELTimeHorizon",
            "BestEstimateELTimeUnit",
            "ASSOC_CollateralPortion.PortionNumber",
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "ASSOC_FinancialContract.FinancialContractID",
            "ASSOC_FinancialContract.IDSystem",
            "_Collection.CollectionID",
            "_Collection.IDSystem",
            "_Collection._Client.BusinessPartnerID",
            "_FinancialInstrument.FinancialInstrumentID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::CreditRiskExpectedLoss" WHERE
            (
            "BestEstimateELMethod" ,
            "BestEstimateELTimeHorizon" ,
            "BestEstimateELTimeUnit" ,
            "ASSOC_CollateralPortion.PortionNumber" ,
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."BestEstimateELMethod",
            "OLD"."BestEstimateELTimeHorizon",
            "OLD"."BestEstimateELTimeUnit",
            "OLD"."ASSOC_CollateralPortion.PortionNumber",
            "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."_Collection._Client.BusinessPartnerID",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CreditRiskExpectedLoss" as "OLD"
        on
                              "IN"."BestEstimateELMethod" = "OLD"."BestEstimateELMethod" and
                              "IN"."BestEstimateELTimeHorizon" = "OLD"."BestEstimateELTimeHorizon" and
                              "IN"."BestEstimateELTimeUnit" = "OLD"."BestEstimateELTimeUnit" and
                              "IN"."ASSOC_CollateralPortion.PortionNumber" = "OLD"."ASSOC_CollateralPortion.PortionNumber" and
                              "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                              "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
                              "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                              "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                              "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                              "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                              "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                              "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "BestEstimateELMethod",
        "BestEstimateELTimeHorizon",
        "BestEstimateELTimeUnit",
        "ASSOC_CollateralPortion.PortionNumber",
        "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
        "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BestEstimateELCurrency",
        "BestEstimateExpectedLoss",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_BestEstimateELMethod" as "BestEstimateELMethod" ,
            "OLD_BestEstimateELTimeHorizon" as "BestEstimateELTimeHorizon" ,
            "OLD_BestEstimateELTimeUnit" as "BestEstimateELTimeUnit" ,
            "OLD_ASSOC_CollateralPortion.PortionNumber" as "ASSOC_CollateralPortion.PortionNumber" ,
            "OLD_ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" as "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "OLD_ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" as "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BestEstimateELCurrency" as "BestEstimateELCurrency" ,
            "OLD_BestEstimateExpectedLoss" as "BestEstimateExpectedLoss" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."BestEstimateELMethod",
                        "OLD"."BestEstimateELTimeHorizon",
                        "OLD"."BestEstimateELTimeUnit",
                        "OLD"."ASSOC_CollateralPortion.PortionNumber",
                        "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_Collection._Client.BusinessPartnerID",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."BestEstimateELMethod" AS "OLD_BestEstimateELMethod" ,
                "OLD"."BestEstimateELTimeHorizon" AS "OLD_BestEstimateELTimeHorizon" ,
                "OLD"."BestEstimateELTimeUnit" AS "OLD_BestEstimateELTimeUnit" ,
                "OLD"."ASSOC_CollateralPortion.PortionNumber" AS "OLD_ASSOC_CollateralPortion.PortionNumber" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AS "OLD_ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" AS "OLD_ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" AS "OLD__Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BestEstimateELCurrency" AS "OLD_BestEstimateELCurrency" ,
                "OLD"."BestEstimateExpectedLoss" AS "OLD_BestEstimateExpectedLoss" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CreditRiskExpectedLoss" as "OLD"
            on
                                      "IN"."BestEstimateELMethod" = "OLD"."BestEstimateELMethod" and
                                      "IN"."BestEstimateELTimeHorizon" = "OLD"."BestEstimateELTimeHorizon" and
                                      "IN"."BestEstimateELTimeUnit" = "OLD"."BestEstimateELTimeUnit" and
                                      "IN"."ASSOC_CollateralPortion.PortionNumber" = "OLD"."ASSOC_CollateralPortion.PortionNumber" and
                                      "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                                      "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
                                      "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                      "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                      "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                                      "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                                      "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_BestEstimateELMethod" as "BestEstimateELMethod",
            "OLD_BestEstimateELTimeHorizon" as "BestEstimateELTimeHorizon",
            "OLD_BestEstimateELTimeUnit" as "BestEstimateELTimeUnit",
            "OLD_ASSOC_CollateralPortion.PortionNumber" as "ASSOC_CollateralPortion.PortionNumber",
            "OLD_ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" as "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "OLD_ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" as "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_BestEstimateELCurrency" as "BestEstimateELCurrency",
            "OLD_BestEstimateExpectedLoss" as "BestEstimateExpectedLoss",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."BestEstimateELMethod",
                        "OLD"."BestEstimateELTimeHorizon",
                        "OLD"."BestEstimateELTimeUnit",
                        "OLD"."ASSOC_CollateralPortion.PortionNumber",
                        "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_Collection._Client.BusinessPartnerID",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."BestEstimateELMethod" AS "OLD_BestEstimateELMethod" ,
                "OLD"."BestEstimateELTimeHorizon" AS "OLD_BestEstimateELTimeHorizon" ,
                "OLD"."BestEstimateELTimeUnit" AS "OLD_BestEstimateELTimeUnit" ,
                "OLD"."ASSOC_CollateralPortion.PortionNumber" AS "OLD_ASSOC_CollateralPortion.PortionNumber" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AS "OLD_ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" AS "OLD_ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" AS "OLD__Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BestEstimateELCurrency" AS "OLD_BestEstimateELCurrency" ,
                "OLD"."BestEstimateExpectedLoss" AS "OLD_BestEstimateExpectedLoss" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CreditRiskExpectedLoss" as "OLD"
            on
               "IN"."BestEstimateELMethod" = "OLD"."BestEstimateELMethod" and
               "IN"."BestEstimateELTimeHorizon" = "OLD"."BestEstimateELTimeHorizon" and
               "IN"."BestEstimateELTimeUnit" = "OLD"."BestEstimateELTimeUnit" and
               "IN"."ASSOC_CollateralPortion.PortionNumber" = "OLD"."ASSOC_CollateralPortion.PortionNumber" and
               "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
               "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
               "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
               "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
               "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
               "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
               "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
               "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
