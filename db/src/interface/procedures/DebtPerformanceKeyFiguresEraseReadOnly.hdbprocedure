PROCEDURE "sap.fsdm.procedures::DebtPerformanceKeyFiguresEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::DebtPerformanceKeyFiguresTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::DebtPerformanceKeyFiguresTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::DebtPerformanceKeyFiguresTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Category" is null and
            "KeyDate" is null and
            "Provider" is null and
            "Scenario" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Category" ,
                "KeyDate" ,
                "Provider" ,
                "Scenario" ,
                "_FinancialContract.FinancialContractID" ,
                "_FinancialContract.IDSystem" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Category" ,
                "OLD"."KeyDate" ,
                "OLD"."Provider" ,
                "OLD"."Scenario" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::DebtPerformanceKeyFigures" "OLD"
            on
                "IN"."Category" = "OLD"."Category" and
                "IN"."KeyDate" = "OLD"."KeyDate" and
                "IN"."Provider" = "OLD"."Provider" and
                "IN"."Scenario" = "OLD"."Scenario" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Category" ,
            "KeyDate" ,
            "Provider" ,
            "Scenario" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Category" ,
                "OLD"."KeyDate" ,
                "OLD"."Provider" ,
                "OLD"."Scenario" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::DebtPerformanceKeyFigures_Historical" "OLD"
            on
                "IN"."Category" = "OLD"."Category" and
                "IN"."KeyDate" = "OLD"."KeyDate" and
                "IN"."Provider" = "OLD"."Provider" and
                "IN"."Scenario" = "OLD"."Scenario" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

END
