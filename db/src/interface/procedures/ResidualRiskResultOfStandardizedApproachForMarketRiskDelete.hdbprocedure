PROCEDURE "sap.fsdm.procedures::ResidualRiskResultOfStandardizedApproachForMarketRiskDelete" (IN ROW "sap.fsdm.tabletypes::ResidualRiskResultOfStandardizedApproachForMarketRiskTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'RiskClass=' || TO_VARCHAR("RiskClass") || ' ' ||
                'RiskProvisioningScenario=' || TO_VARCHAR("RiskProvisioningScenario") || ' ' ||
                'RoleOfCurrency=' || TO_VARCHAR("RoleOfCurrency") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_RiskReportingNode.RiskReportingNodeID=' || TO_VARCHAR("_RiskReportingNode.RiskReportingNodeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."RiskClass",
                        "IN"."RiskProvisioningScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."RiskClass",
                        "IN"."RiskProvisioningScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "RiskClass",
                        "RiskProvisioningScenario",
                        "RoleOfCurrency",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_RiskReportingNode.RiskReportingNodeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."RiskClass",
                                    "IN"."RiskProvisioningScenario",
                                    "IN"."RoleOfCurrency",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_RiskReportingNode.RiskReportingNodeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."RiskClass",
                                    "IN"."RiskProvisioningScenario",
                                    "IN"."RoleOfCurrency",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_RiskReportingNode.RiskReportingNodeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "RiskClass" is null and
            "RiskProvisioningScenario" is null and
            "RoleOfCurrency" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_RiskReportingNode.RiskReportingNodeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::ResidualRiskResultOfStandardizedApproachForMarketRisk" (
        "RiskClass",
        "RiskProvisioningScenario",
        "RoleOfCurrency",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Currency",
        "GrossNotionalValue",
        "OwnFundsRequirement",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_RiskClass" as "RiskClass" ,
            "OLD_RiskProvisioningScenario" as "RiskProvisioningScenario" ,
            "OLD_RoleOfCurrency" as "RoleOfCurrency" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "OLD__RiskReportingNode.RiskReportingNodeID" as "_RiskReportingNode.RiskReportingNodeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Currency" as "Currency" ,
            "OLD_GrossNotionalValue" as "GrossNotionalValue" ,
            "OLD_OwnFundsRequirement" as "OwnFundsRequirement" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."RiskClass",
                        "OLD"."RiskProvisioningScenario",
                        "OLD"."RoleOfCurrency",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."_RiskReportingNode.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."RiskClass" AS "OLD_RiskClass" ,
                "OLD"."RiskProvisioningScenario" AS "OLD_RiskProvisioningScenario" ,
                "OLD"."RoleOfCurrency" AS "OLD_RoleOfCurrency" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" AS "OLD__RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."GrossNotionalValue" AS "OLD_GrossNotionalValue" ,
                "OLD"."OwnFundsRequirement" AS "OLD_OwnFundsRequirement" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ResidualRiskResultOfStandardizedApproachForMarketRisk" as "OLD"
            on
                      "IN"."RiskClass" = "OLD"."RiskClass" and
                      "IN"."RiskProvisioningScenario" = "OLD"."RiskProvisioningScenario" and
                      "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                      "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                      "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                      "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::ResidualRiskResultOfStandardizedApproachForMarketRisk" (
        "RiskClass",
        "RiskProvisioningScenario",
        "RoleOfCurrency",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Currency",
        "GrossNotionalValue",
        "OwnFundsRequirement",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_RiskClass" as "RiskClass",
            "OLD_RiskProvisioningScenario" as "RiskProvisioningScenario",
            "OLD_RoleOfCurrency" as "RoleOfCurrency",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__RiskReportingNode.RiskReportingNodeID" as "_RiskReportingNode.RiskReportingNodeID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Currency" as "Currency",
            "OLD_GrossNotionalValue" as "GrossNotionalValue",
            "OLD_OwnFundsRequirement" as "OwnFundsRequirement",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."RiskClass",
                        "OLD"."RiskProvisioningScenario",
                        "OLD"."RoleOfCurrency",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."_RiskReportingNode.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."RiskClass" AS "OLD_RiskClass" ,
                "OLD"."RiskProvisioningScenario" AS "OLD_RiskProvisioningScenario" ,
                "OLD"."RoleOfCurrency" AS "OLD_RoleOfCurrency" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" AS "OLD__RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."GrossNotionalValue" AS "OLD_GrossNotionalValue" ,
                "OLD"."OwnFundsRequirement" AS "OLD_OwnFundsRequirement" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ResidualRiskResultOfStandardizedApproachForMarketRisk" as "OLD"
            on
                                                "IN"."RiskClass" = "OLD"."RiskClass" and
                                                "IN"."RiskProvisioningScenario" = "OLD"."RiskProvisioningScenario" and
                                                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                                                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                                                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                                                "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::ResidualRiskResultOfStandardizedApproachForMarketRisk"
    where (
        "RiskClass",
        "RiskProvisioningScenario",
        "RoleOfCurrency",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."RiskClass",
            "OLD"."RiskProvisioningScenario",
            "OLD"."RoleOfCurrency",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_RiskReportingNode.RiskReportingNodeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ResidualRiskResultOfStandardizedApproachForMarketRisk" as "OLD"
        on
                                       "IN"."RiskClass" = "OLD"."RiskClass" and
                                       "IN"."RiskProvisioningScenario" = "OLD"."RiskProvisioningScenario" and
                                       "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                                       "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                                       "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                                       "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
