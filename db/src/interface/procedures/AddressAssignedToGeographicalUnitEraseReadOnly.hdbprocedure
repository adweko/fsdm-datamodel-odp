PROCEDURE "sap.fsdm.procedures::AddressAssignedToGeographicalUnitEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::AddressAssignedToGeographicalUnitTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::AddressAssignedToGeographicalUnitTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::AddressAssignedToGeographicalUnitTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_Address.AddressType" is null and
            "ASSOC_Address.SequenceNumber" is null and
            "ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" is null and
            "ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" is null and
            "ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" is null and
            "ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" is null and
            "ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" is null and
            "ASSOC_Address._Claim.ID" is null and
            "ASSOC_Address._Claim.IDSystem" is null and
            "ASSOC_GeographicalUnit.GeographicalStructureID" is null and
            "ASSOC_GeographicalUnit.GeographicalUnitID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "ASSOC_Address.AddressType" ,
                "ASSOC_Address.SequenceNumber" ,
                "ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" ,
                "ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" ,
                "ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "ASSOC_Address._Claim.ID" ,
                "ASSOC_Address._Claim.IDSystem" ,
                "ASSOC_GeographicalUnit.GeographicalStructureID" ,
                "ASSOC_GeographicalUnit.GeographicalUnitID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."ASSOC_Address.AddressType" ,
                "OLD"."ASSOC_Address.SequenceNumber" ,
                "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."ASSOC_Address._Claim.ID" ,
                "OLD"."ASSOC_Address._Claim.IDSystem" ,
                "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID" ,
                "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::AddressAssignedToGeographicalUnit" "OLD"
            on
                "IN"."ASSOC_Address.AddressType" = "OLD"."ASSOC_Address.AddressType" and
                "IN"."ASSOC_Address.SequenceNumber" = "OLD"."ASSOC_Address.SequenceNumber" and
                "IN"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" = "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" and
                "IN"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" and
                "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" and
                "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" and
                "IN"."ASSOC_Address._Claim.ID" = "OLD"."ASSOC_Address._Claim.ID" and
                "IN"."ASSOC_Address._Claim.IDSystem" = "OLD"."ASSOC_Address._Claim.IDSystem" and
                "IN"."ASSOC_GeographicalUnit.GeographicalStructureID" = "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID" and
                "IN"."ASSOC_GeographicalUnit.GeographicalUnitID" = "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "ASSOC_Address.AddressType" ,
            "ASSOC_Address.SequenceNumber" ,
            "ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" ,
            "ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
            "ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" ,
            "ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
            "ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "ASSOC_Address._Claim.ID" ,
            "ASSOC_Address._Claim.IDSystem" ,
            "ASSOC_GeographicalUnit.GeographicalStructureID" ,
            "ASSOC_GeographicalUnit.GeographicalUnitID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."ASSOC_Address.AddressType" ,
                "OLD"."ASSOC_Address.SequenceNumber" ,
                "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."ASSOC_Address._Claim.ID" ,
                "OLD"."ASSOC_Address._Claim.IDSystem" ,
                "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID" ,
                "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::AddressAssignedToGeographicalUnit_Historical" "OLD"
            on
                "IN"."ASSOC_Address.AddressType" = "OLD"."ASSOC_Address.AddressType" and
                "IN"."ASSOC_Address.SequenceNumber" = "OLD"."ASSOC_Address.SequenceNumber" and
                "IN"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" = "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" and
                "IN"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" and
                "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" and
                "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" and
                "IN"."ASSOC_Address._Claim.ID" = "OLD"."ASSOC_Address._Claim.ID" and
                "IN"."ASSOC_Address._Claim.IDSystem" = "OLD"."ASSOC_Address._Claim.IDSystem" and
                "IN"."ASSOC_GeographicalUnit.GeographicalStructureID" = "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID" and
                "IN"."ASSOC_GeographicalUnit.GeographicalUnitID" = "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID" 
        );

END
