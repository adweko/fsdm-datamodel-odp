PROCEDURE "sap.fsdm.procedures::PhysicalAssetClassAssignmentEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::PhysicalAssetClassAssignmentTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::PhysicalAssetClassAssignmentTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::PhysicalAssetClassAssignmentTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_PhysicalAsset.PhysicalAssetID" is null and
            "_PhysicalAssetClass.PhysicalAssetClass" is null and
            "_PhysicalAssetClass.PhysicalAssetClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_PhysicalAsset.PhysicalAssetID" ,
                "_PhysicalAssetClass.PhysicalAssetClass" ,
                "_PhysicalAssetClass.PhysicalAssetClassificationSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_PhysicalAssetClass.PhysicalAssetClass" ,
                "OLD"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PhysicalAssetClassAssignment" "OLD"
            on
                "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                "IN"."_PhysicalAssetClass.PhysicalAssetClass" = "OLD"."_PhysicalAssetClass.PhysicalAssetClass" and
                "IN"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" = "OLD"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_PhysicalAsset.PhysicalAssetID" ,
            "_PhysicalAssetClass.PhysicalAssetClass" ,
            "_PhysicalAssetClass.PhysicalAssetClassificationSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_PhysicalAssetClass.PhysicalAssetClass" ,
                "OLD"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PhysicalAssetClassAssignment_Historical" "OLD"
            on
                "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                "IN"."_PhysicalAssetClass.PhysicalAssetClass" = "OLD"."_PhysicalAssetClass.PhysicalAssetClass" and
                "IN"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" = "OLD"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" 
        );

END
