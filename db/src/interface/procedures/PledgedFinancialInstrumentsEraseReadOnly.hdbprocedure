PROCEDURE "sap.fsdm.procedures::PledgedFinancialInstrumentsEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::PledgedFinancialInstrumentsTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::PledgedFinancialInstrumentsTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::PledgedFinancialInstrumentsTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_PledgedObject.SequenceNumber" is null and
            "_PledgedObject.ASSOC_Pledge.FinancialContractID" is null and
            "_PledgedObject.ASSOC_Pledge.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_FinancialInstrument.FinancialInstrumentID" ,
                "_PledgedObject.SequenceNumber" ,
                "_PledgedObject.ASSOC_Pledge.FinancialContractID" ,
                "_PledgedObject.ASSOC_Pledge.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_PledgedObject.SequenceNumber" ,
                "OLD"."_PledgedObject.ASSOC_Pledge.FinancialContractID" ,
                "OLD"."_PledgedObject.ASSOC_Pledge.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PledgedFinancialInstruments" "OLD"
            on
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_PledgedObject.SequenceNumber" = "OLD"."_PledgedObject.SequenceNumber" and
                "IN"."_PledgedObject.ASSOC_Pledge.FinancialContractID" = "OLD"."_PledgedObject.ASSOC_Pledge.FinancialContractID" and
                "IN"."_PledgedObject.ASSOC_Pledge.IDSystem" = "OLD"."_PledgedObject.ASSOC_Pledge.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_PledgedObject.SequenceNumber" ,
            "_PledgedObject.ASSOC_Pledge.FinancialContractID" ,
            "_PledgedObject.ASSOC_Pledge.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_PledgedObject.SequenceNumber" ,
                "OLD"."_PledgedObject.ASSOC_Pledge.FinancialContractID" ,
                "OLD"."_PledgedObject.ASSOC_Pledge.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PledgedFinancialInstruments_Historical" "OLD"
            on
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_PledgedObject.SequenceNumber" = "OLD"."_PledgedObject.SequenceNumber" and
                "IN"."_PledgedObject.ASSOC_Pledge.FinancialContractID" = "OLD"."_PledgedObject.ASSOC_Pledge.FinancialContractID" and
                "IN"."_PledgedObject.ASSOC_Pledge.IDSystem" = "OLD"."_PledgedObject.ASSOC_Pledge.IDSystem" 
        );

END
