PROCEDURE "sap.fsdm.procedures::RentalContractInformationDelete" (IN ROW "sap.fsdm.tabletypes::RentalContractInformationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'RentalContractInformationReferenceID=' || TO_VARCHAR("RentalContractInformationReferenceID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."RentalContractInformationReferenceID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."RentalContractInformationReferenceID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "RentalContractInformationReferenceID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."RentalContractInformationReferenceID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."RentalContractInformationReferenceID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "RentalContractInformationReferenceID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::RentalContractInformation" (
        "RentalContractInformationReferenceID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_ReferenceIndex.IndexID",
        "AdditionalInformation",
        "AdjustmentPercentage",
        "DateOfLastRentIncomeAdjustment",
        "EndDate",
        "FranchiseBrand",
        "Hurdle",
        "MinimumRentAmount",
        "MinimumRentAmountCurrency",
        "NetRentIncome",
        "NetRentIncomeCurrency",
        "RentFreePeriodLength",
        "RentFreePeriodTimeUnit",
        "RentIncomeAdjustmentType",
        "RentalIncomePeriodLength",
        "RentalIncomePeriodTimeUnit",
        "SalesPercentageRate",
        "StartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_RentalContractInformationReferenceID" as "RentalContractInformationReferenceID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__ReferenceIndex.IndexID" as "_ReferenceIndex.IndexID" ,
            "OLD_AdditionalInformation" as "AdditionalInformation" ,
            "OLD_AdjustmentPercentage" as "AdjustmentPercentage" ,
            "OLD_DateOfLastRentIncomeAdjustment" as "DateOfLastRentIncomeAdjustment" ,
            "OLD_EndDate" as "EndDate" ,
            "OLD_FranchiseBrand" as "FranchiseBrand" ,
            "OLD_Hurdle" as "Hurdle" ,
            "OLD_MinimumRentAmount" as "MinimumRentAmount" ,
            "OLD_MinimumRentAmountCurrency" as "MinimumRentAmountCurrency" ,
            "OLD_NetRentIncome" as "NetRentIncome" ,
            "OLD_NetRentIncomeCurrency" as "NetRentIncomeCurrency" ,
            "OLD_RentFreePeriodLength" as "RentFreePeriodLength" ,
            "OLD_RentFreePeriodTimeUnit" as "RentFreePeriodTimeUnit" ,
            "OLD_RentIncomeAdjustmentType" as "RentIncomeAdjustmentType" ,
            "OLD_RentalIncomePeriodLength" as "RentalIncomePeriodLength" ,
            "OLD_RentalIncomePeriodTimeUnit" as "RentalIncomePeriodTimeUnit" ,
            "OLD_SalesPercentageRate" as "SalesPercentageRate" ,
            "OLD_StartDate" as "StartDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."RentalContractInformationReferenceID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."RentalContractInformationReferenceID" AS "OLD_RentalContractInformationReferenceID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_ReferenceIndex.IndexID" AS "OLD__ReferenceIndex.IndexID" ,
                "OLD"."AdditionalInformation" AS "OLD_AdditionalInformation" ,
                "OLD"."AdjustmentPercentage" AS "OLD_AdjustmentPercentage" ,
                "OLD"."DateOfLastRentIncomeAdjustment" AS "OLD_DateOfLastRentIncomeAdjustment" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."FranchiseBrand" AS "OLD_FranchiseBrand" ,
                "OLD"."Hurdle" AS "OLD_Hurdle" ,
                "OLD"."MinimumRentAmount" AS "OLD_MinimumRentAmount" ,
                "OLD"."MinimumRentAmountCurrency" AS "OLD_MinimumRentAmountCurrency" ,
                "OLD"."NetRentIncome" AS "OLD_NetRentIncome" ,
                "OLD"."NetRentIncomeCurrency" AS "OLD_NetRentIncomeCurrency" ,
                "OLD"."RentFreePeriodLength" AS "OLD_RentFreePeriodLength" ,
                "OLD"."RentFreePeriodTimeUnit" AS "OLD_RentFreePeriodTimeUnit" ,
                "OLD"."RentIncomeAdjustmentType" AS "OLD_RentIncomeAdjustmentType" ,
                "OLD"."RentalIncomePeriodLength" AS "OLD_RentalIncomePeriodLength" ,
                "OLD"."RentalIncomePeriodTimeUnit" AS "OLD_RentalIncomePeriodTimeUnit" ,
                "OLD"."SalesPercentageRate" AS "OLD_SalesPercentageRate" ,
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::RentalContractInformation" as "OLD"
            on
                      "IN"."RentalContractInformationReferenceID" = "OLD"."RentalContractInformationReferenceID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::RentalContractInformation" (
        "RentalContractInformationReferenceID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_ReferenceIndex.IndexID",
        "AdditionalInformation",
        "AdjustmentPercentage",
        "DateOfLastRentIncomeAdjustment",
        "EndDate",
        "FranchiseBrand",
        "Hurdle",
        "MinimumRentAmount",
        "MinimumRentAmountCurrency",
        "NetRentIncome",
        "NetRentIncomeCurrency",
        "RentFreePeriodLength",
        "RentFreePeriodTimeUnit",
        "RentIncomeAdjustmentType",
        "RentalIncomePeriodLength",
        "RentalIncomePeriodTimeUnit",
        "SalesPercentageRate",
        "StartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_RentalContractInformationReferenceID" as "RentalContractInformationReferenceID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__ReferenceIndex.IndexID" as "_ReferenceIndex.IndexID",
            "OLD_AdditionalInformation" as "AdditionalInformation",
            "OLD_AdjustmentPercentage" as "AdjustmentPercentage",
            "OLD_DateOfLastRentIncomeAdjustment" as "DateOfLastRentIncomeAdjustment",
            "OLD_EndDate" as "EndDate",
            "OLD_FranchiseBrand" as "FranchiseBrand",
            "OLD_Hurdle" as "Hurdle",
            "OLD_MinimumRentAmount" as "MinimumRentAmount",
            "OLD_MinimumRentAmountCurrency" as "MinimumRentAmountCurrency",
            "OLD_NetRentIncome" as "NetRentIncome",
            "OLD_NetRentIncomeCurrency" as "NetRentIncomeCurrency",
            "OLD_RentFreePeriodLength" as "RentFreePeriodLength",
            "OLD_RentFreePeriodTimeUnit" as "RentFreePeriodTimeUnit",
            "OLD_RentIncomeAdjustmentType" as "RentIncomeAdjustmentType",
            "OLD_RentalIncomePeriodLength" as "RentalIncomePeriodLength",
            "OLD_RentalIncomePeriodTimeUnit" as "RentalIncomePeriodTimeUnit",
            "OLD_SalesPercentageRate" as "SalesPercentageRate",
            "OLD_StartDate" as "StartDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."RentalContractInformationReferenceID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."RentalContractInformationReferenceID" AS "OLD_RentalContractInformationReferenceID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_ReferenceIndex.IndexID" AS "OLD__ReferenceIndex.IndexID" ,
                "OLD"."AdditionalInformation" AS "OLD_AdditionalInformation" ,
                "OLD"."AdjustmentPercentage" AS "OLD_AdjustmentPercentage" ,
                "OLD"."DateOfLastRentIncomeAdjustment" AS "OLD_DateOfLastRentIncomeAdjustment" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."FranchiseBrand" AS "OLD_FranchiseBrand" ,
                "OLD"."Hurdle" AS "OLD_Hurdle" ,
                "OLD"."MinimumRentAmount" AS "OLD_MinimumRentAmount" ,
                "OLD"."MinimumRentAmountCurrency" AS "OLD_MinimumRentAmountCurrency" ,
                "OLD"."NetRentIncome" AS "OLD_NetRentIncome" ,
                "OLD"."NetRentIncomeCurrency" AS "OLD_NetRentIncomeCurrency" ,
                "OLD"."RentFreePeriodLength" AS "OLD_RentFreePeriodLength" ,
                "OLD"."RentFreePeriodTimeUnit" AS "OLD_RentFreePeriodTimeUnit" ,
                "OLD"."RentIncomeAdjustmentType" AS "OLD_RentIncomeAdjustmentType" ,
                "OLD"."RentalIncomePeriodLength" AS "OLD_RentalIncomePeriodLength" ,
                "OLD"."RentalIncomePeriodTimeUnit" AS "OLD_RentalIncomePeriodTimeUnit" ,
                "OLD"."SalesPercentageRate" AS "OLD_SalesPercentageRate" ,
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::RentalContractInformation" as "OLD"
            on
                                                "IN"."RentalContractInformationReferenceID" = "OLD"."RentalContractInformationReferenceID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::RentalContractInformation"
    where (
        "RentalContractInformationReferenceID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."RentalContractInformationReferenceID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::RentalContractInformation" as "OLD"
        on
                                       "IN"."RentalContractInformationReferenceID" = "OLD"."RentalContractInformationReferenceID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
