PROCEDURE "sap.fsdm.procedures::AddressDelReadOnly" (IN ROW "sap.fsdm.tabletypes::AddressTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::AddressTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::AddressTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AddressType=' || TO_VARCHAR("AddressType") || ' ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_BankingChannel.BankingChannelID=' || TO_VARCHAR("ASSOC_BankingChannel.BankingChannelID") || ' ' ||
                'ASSOC_BusinessPartnerID.BusinessPartnerID=' || TO_VARCHAR("ASSOC_BusinessPartnerID.BusinessPartnerID") || ' ' ||
                'ASSOC_OrganizationalUnit.IDSystem=' || TO_VARCHAR("ASSOC_OrganizationalUnit.IDSystem") || ' ' ||
                'ASSOC_OrganizationalUnit.OrganizationalUnitID=' || TO_VARCHAR("ASSOC_OrganizationalUnit.OrganizationalUnitID") || ' ' ||
                'ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID=' || TO_VARCHAR("ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID") || ' ' ||
                'ASSOC_PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("ASSOC_PhysicalAsset.PhysicalAssetID") || ' ' ||
                '_Claim.ID=' || TO_VARCHAR("_Claim.ID") || ' ' ||
                '_Claim.IDSystem=' || TO_VARCHAR("_Claim.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AddressType",
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_BankingChannel.BankingChannelID",
                        "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "IN"."ASSOC_OrganizationalUnit.IDSystem",
                        "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "IN"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "IN"."_Claim.ID",
                        "IN"."_Claim.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AddressType",
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_BankingChannel.BankingChannelID",
                        "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "IN"."ASSOC_OrganizationalUnit.IDSystem",
                        "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "IN"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "IN"."_Claim.ID",
                        "IN"."_Claim.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AddressType",
                        "SequenceNumber",
                        "ASSOC_BankingChannel.BankingChannelID",
                        "ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "ASSOC_OrganizationalUnit.IDSystem",
                        "ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "ASSOC_PhysicalAsset.PhysicalAssetID",
                        "_Claim.ID",
                        "_Claim.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AddressType",
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_BankingChannel.BankingChannelID",
                                    "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                                    "IN"."ASSOC_OrganizationalUnit.IDSystem",
                                    "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                                    "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                    "IN"."ASSOC_PhysicalAsset.PhysicalAssetID",
                                    "IN"."_Claim.ID",
                                    "IN"."_Claim.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AddressType",
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_BankingChannel.BankingChannelID",
                                    "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                                    "IN"."ASSOC_OrganizationalUnit.IDSystem",
                                    "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                                    "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                    "IN"."ASSOC_PhysicalAsset.PhysicalAssetID",
                                    "IN"."_Claim.ID",
                                    "IN"."_Claim.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "AddressType" is null and
            "SequenceNumber" is null and
            "ASSOC_BankingChannel.BankingChannelID" is null and
            "ASSOC_BusinessPartnerID.BusinessPartnerID" is null and
            "ASSOC_OrganizationalUnit.IDSystem" is null and
            "ASSOC_OrganizationalUnit.OrganizationalUnitID" is null and
            "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "ASSOC_PhysicalAsset.PhysicalAssetID" is null and
            "_Claim.ID" is null and
            "_Claim.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "AddressType",
            "SequenceNumber",
            "ASSOC_BankingChannel.BankingChannelID",
            "ASSOC_BusinessPartnerID.BusinessPartnerID",
            "ASSOC_OrganizationalUnit.IDSystem",
            "ASSOC_OrganizationalUnit.OrganizationalUnitID",
            "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "ASSOC_PhysicalAsset.PhysicalAssetID",
            "_Claim.ID",
            "_Claim.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::Address" WHERE
            (
            "AddressType" ,
            "SequenceNumber" ,
            "ASSOC_BankingChannel.BankingChannelID" ,
            "ASSOC_BusinessPartnerID.BusinessPartnerID" ,
            "ASSOC_OrganizationalUnit.IDSystem" ,
            "ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
            "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "_Claim.ID" ,
            "_Claim.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."AddressType",
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_BankingChannel.BankingChannelID",
            "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID",
            "OLD"."ASSOC_OrganizationalUnit.IDSystem",
            "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
            "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
            "OLD"."_Claim.ID",
            "OLD"."_Claim.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Address" as "OLD"
        on
                              "IN"."AddressType" = "OLD"."AddressType" and
                              "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                              "IN"."ASSOC_BankingChannel.BankingChannelID" = "OLD"."ASSOC_BankingChannel.BankingChannelID" and
                              "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" and
                              "IN"."ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_OrganizationalUnit.IDSystem" and
                              "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                              "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                              "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" and
                              "IN"."_Claim.ID" = "OLD"."_Claim.ID" and
                              "IN"."_Claim.IDSystem" = "OLD"."_Claim.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "AddressType",
        "SequenceNumber",
        "ASSOC_BankingChannel.BankingChannelID",
        "ASSOC_BusinessPartnerID.BusinessPartnerID",
        "ASSOC_OrganizationalUnit.IDSystem",
        "ASSOC_OrganizationalUnit.OrganizationalUnitID",
        "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "ASSOC_PhysicalAsset.PhysicalAssetID",
        "_Claim.ID",
        "_Claim.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AdditionalAddressLine",
        "AddressCategory",
        "AdministrativeHeadquarter",
        "Building",
        "City",
        "Country",
        "District",
        "Floor",
        "HouseNumber",
        "Latitude",
        "Longitude",
        "MainCorrespondenceAddress",
        "MainResidence",
        "PostalCode",
        "PostalShippingInformation",
        "PostboxNumber",
        "PreferredCorrespondenceLanguage",
        "Region",
        "RepresentsRegisteredOffice",
        "Salutation",
        "Street",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_AddressType" as "AddressType" ,
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_BankingChannel.BankingChannelID" as "ASSOC_BankingChannel.BankingChannelID" ,
            "OLD_ASSOC_BusinessPartnerID.BusinessPartnerID" as "ASSOC_BusinessPartnerID.BusinessPartnerID" ,
            "OLD_ASSOC_OrganizationalUnit.IDSystem" as "ASSOC_OrganizationalUnit.IDSystem" ,
            "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" as "ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
            "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" as "ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "OLD__Claim.ID" as "_Claim.ID" ,
            "OLD__Claim.IDSystem" as "_Claim.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AdditionalAddressLine" as "AdditionalAddressLine" ,
            "OLD_AddressCategory" as "AddressCategory" ,
            "OLD_AdministrativeHeadquarter" as "AdministrativeHeadquarter" ,
            "OLD_Building" as "Building" ,
            "OLD_City" as "City" ,
            "OLD_Country" as "Country" ,
            "OLD_District" as "District" ,
            "OLD_Floor" as "Floor" ,
            "OLD_HouseNumber" as "HouseNumber" ,
            "OLD_Latitude" as "Latitude" ,
            "OLD_Longitude" as "Longitude" ,
            "OLD_MainCorrespondenceAddress" as "MainCorrespondenceAddress" ,
            "OLD_MainResidence" as "MainResidence" ,
            "OLD_PostalCode" as "PostalCode" ,
            "OLD_PostalShippingInformation" as "PostalShippingInformation" ,
            "OLD_PostboxNumber" as "PostboxNumber" ,
            "OLD_PreferredCorrespondenceLanguage" as "PreferredCorrespondenceLanguage" ,
            "OLD_Region" as "Region" ,
            "OLD_RepresentsRegisteredOffice" as "RepresentsRegisteredOffice" ,
            "OLD_Salutation" as "Salutation" ,
            "OLD_Street" as "Street" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."AddressType",
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_BankingChannel.BankingChannelID",
                        "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "OLD"."ASSOC_OrganizationalUnit.IDSystem",
                        "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "OLD"."_Claim.ID",
                        "OLD"."_Claim.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."AddressType" AS "OLD_AddressType" ,
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_BankingChannel.BankingChannelID" AS "OLD_ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_OrganizationalUnit.IDSystem" AS "OLD_ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Claim.ID" AS "OLD__Claim.ID" ,
                "OLD"."_Claim.IDSystem" AS "OLD__Claim.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AdditionalAddressLine" AS "OLD_AdditionalAddressLine" ,
                "OLD"."AddressCategory" AS "OLD_AddressCategory" ,
                "OLD"."AdministrativeHeadquarter" AS "OLD_AdministrativeHeadquarter" ,
                "OLD"."Building" AS "OLD_Building" ,
                "OLD"."City" AS "OLD_City" ,
                "OLD"."Country" AS "OLD_Country" ,
                "OLD"."District" AS "OLD_District" ,
                "OLD"."Floor" AS "OLD_Floor" ,
                "OLD"."HouseNumber" AS "OLD_HouseNumber" ,
                "OLD"."Latitude" AS "OLD_Latitude" ,
                "OLD"."Longitude" AS "OLD_Longitude" ,
                "OLD"."MainCorrespondenceAddress" AS "OLD_MainCorrespondenceAddress" ,
                "OLD"."MainResidence" AS "OLD_MainResidence" ,
                "OLD"."PostalCode" AS "OLD_PostalCode" ,
                "OLD"."PostalShippingInformation" AS "OLD_PostalShippingInformation" ,
                "OLD"."PostboxNumber" AS "OLD_PostboxNumber" ,
                "OLD"."PreferredCorrespondenceLanguage" AS "OLD_PreferredCorrespondenceLanguage" ,
                "OLD"."Region" AS "OLD_Region" ,
                "OLD"."RepresentsRegisteredOffice" AS "OLD_RepresentsRegisteredOffice" ,
                "OLD"."Salutation" AS "OLD_Salutation" ,
                "OLD"."Street" AS "OLD_Street" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Address" as "OLD"
            on
                                      "IN"."AddressType" = "OLD"."AddressType" and
                                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                      "IN"."ASSOC_BankingChannel.BankingChannelID" = "OLD"."ASSOC_BankingChannel.BankingChannelID" and
                                      "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" and
                                      "IN"."ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_OrganizationalUnit.IDSystem" and
                                      "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                                      "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                                      "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" and
                                      "IN"."_Claim.ID" = "OLD"."_Claim.ID" and
                                      "IN"."_Claim.IDSystem" = "OLD"."_Claim.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_AddressType" as "AddressType",
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_BankingChannel.BankingChannelID" as "ASSOC_BankingChannel.BankingChannelID",
            "OLD_ASSOC_BusinessPartnerID.BusinessPartnerID" as "ASSOC_BusinessPartnerID.BusinessPartnerID",
            "OLD_ASSOC_OrganizationalUnit.IDSystem" as "ASSOC_OrganizationalUnit.IDSystem",
            "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" as "ASSOC_OrganizationalUnit.OrganizationalUnitID",
            "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" as "ASSOC_PhysicalAsset.PhysicalAssetID",
            "OLD__Claim.ID" as "_Claim.ID",
            "OLD__Claim.IDSystem" as "_Claim.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AdditionalAddressLine" as "AdditionalAddressLine",
            "OLD_AddressCategory" as "AddressCategory",
            "OLD_AdministrativeHeadquarter" as "AdministrativeHeadquarter",
            "OLD_Building" as "Building",
            "OLD_City" as "City",
            "OLD_Country" as "Country",
            "OLD_District" as "District",
            "OLD_Floor" as "Floor",
            "OLD_HouseNumber" as "HouseNumber",
            "OLD_Latitude" as "Latitude",
            "OLD_Longitude" as "Longitude",
            "OLD_MainCorrespondenceAddress" as "MainCorrespondenceAddress",
            "OLD_MainResidence" as "MainResidence",
            "OLD_PostalCode" as "PostalCode",
            "OLD_PostalShippingInformation" as "PostalShippingInformation",
            "OLD_PostboxNumber" as "PostboxNumber",
            "OLD_PreferredCorrespondenceLanguage" as "PreferredCorrespondenceLanguage",
            "OLD_Region" as "Region",
            "OLD_RepresentsRegisteredOffice" as "RepresentsRegisteredOffice",
            "OLD_Salutation" as "Salutation",
            "OLD_Street" as "Street",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."AddressType",
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_BankingChannel.BankingChannelID",
                        "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "OLD"."ASSOC_OrganizationalUnit.IDSystem",
                        "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "OLD"."_Claim.ID",
                        "OLD"."_Claim.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."AddressType" AS "OLD_AddressType" ,
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_BankingChannel.BankingChannelID" AS "OLD_ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_OrganizationalUnit.IDSystem" AS "OLD_ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Claim.ID" AS "OLD__Claim.ID" ,
                "OLD"."_Claim.IDSystem" AS "OLD__Claim.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AdditionalAddressLine" AS "OLD_AdditionalAddressLine" ,
                "OLD"."AddressCategory" AS "OLD_AddressCategory" ,
                "OLD"."AdministrativeHeadquarter" AS "OLD_AdministrativeHeadquarter" ,
                "OLD"."Building" AS "OLD_Building" ,
                "OLD"."City" AS "OLD_City" ,
                "OLD"."Country" AS "OLD_Country" ,
                "OLD"."District" AS "OLD_District" ,
                "OLD"."Floor" AS "OLD_Floor" ,
                "OLD"."HouseNumber" AS "OLD_HouseNumber" ,
                "OLD"."Latitude" AS "OLD_Latitude" ,
                "OLD"."Longitude" AS "OLD_Longitude" ,
                "OLD"."MainCorrespondenceAddress" AS "OLD_MainCorrespondenceAddress" ,
                "OLD"."MainResidence" AS "OLD_MainResidence" ,
                "OLD"."PostalCode" AS "OLD_PostalCode" ,
                "OLD"."PostalShippingInformation" AS "OLD_PostalShippingInformation" ,
                "OLD"."PostboxNumber" AS "OLD_PostboxNumber" ,
                "OLD"."PreferredCorrespondenceLanguage" AS "OLD_PreferredCorrespondenceLanguage" ,
                "OLD"."Region" AS "OLD_Region" ,
                "OLD"."RepresentsRegisteredOffice" AS "OLD_RepresentsRegisteredOffice" ,
                "OLD"."Salutation" AS "OLD_Salutation" ,
                "OLD"."Street" AS "OLD_Street" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Address" as "OLD"
            on
               "IN"."AddressType" = "OLD"."AddressType" and
               "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
               "IN"."ASSOC_BankingChannel.BankingChannelID" = "OLD"."ASSOC_BankingChannel.BankingChannelID" and
               "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" and
               "IN"."ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_OrganizationalUnit.IDSystem" and
               "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" and
               "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
               "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" and
               "IN"."_Claim.ID" = "OLD"."_Claim.ID" and
               "IN"."_Claim.IDSystem" = "OLD"."_Claim.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
