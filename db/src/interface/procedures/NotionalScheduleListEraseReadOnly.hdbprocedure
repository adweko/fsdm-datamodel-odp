PROCEDURE "sap.fsdm.procedures::NotionalScheduleListEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::NotionalScheduleTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::NotionalScheduleTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::NotionalScheduleTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_SwapForSchedule.FinancialContractID" is null and
            "_SwapForSchedule.IDSystem" is null and
            "RoleOfPayer" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "NotionalIntervalEndDate" ,
                "NotionalIntervalStartDate" ,
                "RoleOfPayer" ,
                "_SwapForSchedule.FinancialContractID" ,
                "_SwapForSchedule.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."NotionalIntervalEndDate" ,
                "OLD"."NotionalIntervalStartDate" ,
                "OLD"."RoleOfPayer" ,
                "OLD"."_SwapForSchedule.FinancialContractID" ,
                "OLD"."_SwapForSchedule.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::NotionalSchedule" "OLD"
            on
                "IN"."_SwapForSchedule.FinancialContractID" = "OLD"."_SwapForSchedule.FinancialContractID" and
                "IN"."_SwapForSchedule.IDSystem" = "OLD"."_SwapForSchedule.IDSystem" and
                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "NotionalIntervalEndDate" ,
            "NotionalIntervalStartDate" ,
            "RoleOfPayer" ,
            "_SwapForSchedule.FinancialContractID" ,
            "_SwapForSchedule.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."NotionalIntervalEndDate" ,
                "OLD"."NotionalIntervalStartDate" ,
                "OLD"."RoleOfPayer" ,
                "OLD"."_SwapForSchedule.FinancialContractID" ,
                "OLD"."_SwapForSchedule.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::NotionalSchedule_Historical" "OLD"
            on
                "IN"."_SwapForSchedule.FinancialContractID" = "OLD"."_SwapForSchedule.FinancialContractID" and
                "IN"."_SwapForSchedule.IDSystem" = "OLD"."_SwapForSchedule.IDSystem" and
                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" 
        );

END
