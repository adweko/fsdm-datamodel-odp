PROCEDURE "sap.fsdm.procedures::FSSubledgerDocumentListErase" (IN ROW "sap.fsdm.tabletypes::FSSubledgerDocumentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "CompanyCode" is null and
            "FiscalYear" is null and
            "DocumentNumber" is null and
            "PostingDate" is null and
            "_AccountingSystem.AccountingSystemID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::FSSubledgerDocument"
        WHERE
        (            "CompanyCode" ,
            "DocumentNumber" ,
            "FiscalYear" ,
            "ItemNumber" ,
            "PostingDate" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_BusinessSegment.IDSystem" ,
            "_BusinessSegment.OrganizationalUnitID" ,
            "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_BusinessSegmentAtCounterparty.IDSystem" ,
            "_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
            "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
        ) in
        (
            select                 "OLD"."CompanyCode" ,
                "OLD"."DocumentNumber" ,
                "OLD"."FiscalYear" ,
                "OLD"."ItemNumber" ,
                "OLD"."PostingDate" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_BusinessSegment.IDSystem" ,
                "OLD"."_BusinessSegment.OrganizationalUnitID" ,
                "OLD"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_BusinessSegmentAtCounterparty.IDSystem" ,
                "OLD"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
                "OLD"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::FSSubledgerDocument" "OLD"
            on
            "IN"."CompanyCode" = "OLD"."CompanyCode" and
            "IN"."FiscalYear" = "OLD"."FiscalYear" and
            "IN"."DocumentNumber" = "OLD"."DocumentNumber" and
            "IN"."PostingDate" = "OLD"."PostingDate" and
            "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" 
        );

        --delete data from history table
        delete from "sap.fsdm::FSSubledgerDocument_Historical"
        WHERE
        (
            "CompanyCode" ,
            "DocumentNumber" ,
            "FiscalYear" ,
            "ItemNumber" ,
            "PostingDate" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_BusinessSegment.IDSystem" ,
            "_BusinessSegment.OrganizationalUnitID" ,
            "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_BusinessSegmentAtCounterparty.IDSystem" ,
            "_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
            "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
        ) in
        (
            select
                "OLD"."CompanyCode" ,
                "OLD"."DocumentNumber" ,
                "OLD"."FiscalYear" ,
                "OLD"."ItemNumber" ,
                "OLD"."PostingDate" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_BusinessSegment.IDSystem" ,
                "OLD"."_BusinessSegment.OrganizationalUnitID" ,
                "OLD"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_BusinessSegmentAtCounterparty.IDSystem" ,
                "OLD"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
                "OLD"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::FSSubledgerDocument_Historical" "OLD"
            on
                "IN"."CompanyCode" = "OLD"."CompanyCode" and
                "IN"."FiscalYear" = "OLD"."FiscalYear" and
                "IN"."DocumentNumber" = "OLD"."DocumentNumber" and
                "IN"."PostingDate" = "OLD"."PostingDate" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" 
        );

END
