PROCEDURE "sap.fsdm.procedures::GLDocumentListDel" (IN ROW "sap.fsdm.tabletypes::GLDocumentTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_AccountingSystem.AccountingSystemID" is null and
            "_CompanyCode.BusinessPartnerID" is null and
            "FiscalYear" is null and
            "Ledger" is null and
            "DocumentNumber" is null and
            "PostingDate" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::GLDocument"
        WHERE
        (            "CompanyCode" ,
            "DocumentNumber" ,
            "FiscalYear" ,
            "ItemNumber" ,
            "Ledger" ,
            "PostingDate" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_BusinessSegment.IDSystem" ,
            "_BusinessSegment.OrganizationalUnitID" ,
            "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_BusinessSegmentAtCounterparty.IDSystem" ,
            "_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
            "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_CompanyCode.BusinessPartnerID" ,
            "_ProductCatalogItem.ProductCatalogItem" ,
            "_ProductCatalogItem._ProductCatalog.CatalogID" 
        ) in
        (
            select
                "OLD"."CompanyCode" ,
                "OLD"."DocumentNumber" ,
                "OLD"."FiscalYear" ,
                "OLD"."ItemNumber" ,
                "OLD"."Ledger" ,
                "OLD"."PostingDate" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_BusinessSegment.IDSystem" ,
                "OLD"."_BusinessSegment.OrganizationalUnitID" ,
                "OLD"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_BusinessSegmentAtCounterparty.IDSystem" ,
                "OLD"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
                "OLD"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_CompanyCode.BusinessPartnerID" ,
                "OLD"."_ProductCatalogItem.ProductCatalogItem" ,
                "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
            from :ROW "IN"
            inner join "sap.fsdm::GLDocument" "OLD"
            on
            "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
            "IN"."_CompanyCode.BusinessPartnerID" = "OLD"."_CompanyCode.BusinessPartnerID" and
            "IN"."FiscalYear" = "OLD"."FiscalYear" and
            "IN"."Ledger" = "OLD"."Ledger" and
            "IN"."DocumentNumber" = "OLD"."DocumentNumber" and
            "IN"."PostingDate" = "OLD"."PostingDate" 
        );

END
