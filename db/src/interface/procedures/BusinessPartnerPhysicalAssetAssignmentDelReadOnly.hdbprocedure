PROCEDURE "sap.fsdm.procedures::BusinessPartnerPhysicalAssetAssignmentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::BusinessPartnerPhysicalAssetAssignmentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::BusinessPartnerPhysicalAssetAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::BusinessPartnerPhysicalAssetAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'BusinessPartnerRole=' || TO_VARCHAR("BusinessPartnerRole") || ' ' ||
                'RentedPropertyType=' || TO_VARCHAR("RentedPropertyType") || ' ' ||
                'ASSOC_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("ASSOC_BusinessPartner.BusinessPartnerID") || ' ' ||
                'ASSOC_PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("ASSOC_PhysicalAsset.PhysicalAssetID") || ' ' ||
                '_RentalContractInformation.RentalContractInformationReferenceID=' || TO_VARCHAR("_RentalContractInformation.RentalContractInformationReferenceID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."BusinessPartnerRole",
                        "IN"."RentedPropertyType",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "IN"."_RentalContractInformation.RentalContractInformationReferenceID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."BusinessPartnerRole",
                        "IN"."RentedPropertyType",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "IN"."_RentalContractInformation.RentalContractInformationReferenceID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "BusinessPartnerRole",
                        "RentedPropertyType",
                        "ASSOC_BusinessPartner.BusinessPartnerID",
                        "ASSOC_PhysicalAsset.PhysicalAssetID",
                        "_RentalContractInformation.RentalContractInformationReferenceID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."BusinessPartnerRole",
                                    "IN"."RentedPropertyType",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_PhysicalAsset.PhysicalAssetID",
                                    "IN"."_RentalContractInformation.RentalContractInformationReferenceID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."BusinessPartnerRole",
                                    "IN"."RentedPropertyType",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_PhysicalAsset.PhysicalAssetID",
                                    "IN"."_RentalContractInformation.RentalContractInformationReferenceID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "BusinessPartnerRole" is null and
            "RentedPropertyType" is null and
            "ASSOC_BusinessPartner.BusinessPartnerID" is null and
            "ASSOC_PhysicalAsset.PhysicalAssetID" is null and
            "_RentalContractInformation.RentalContractInformationReferenceID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "BusinessPartnerRole",
            "RentedPropertyType",
            "ASSOC_BusinessPartner.BusinessPartnerID",
            "ASSOC_PhysicalAsset.PhysicalAssetID",
            "_RentalContractInformation.RentalContractInformationReferenceID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::BusinessPartnerPhysicalAssetAssignment" WHERE
            (
            "BusinessPartnerRole" ,
            "RentedPropertyType" ,
            "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "_RentalContractInformation.RentalContractInformationReferenceID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."BusinessPartnerRole",
            "OLD"."RentedPropertyType",
            "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
            "OLD"."_RentalContractInformation.RentalContractInformationReferenceID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::BusinessPartnerPhysicalAssetAssignment" as "OLD"
        on
                              "IN"."BusinessPartnerRole" = "OLD"."BusinessPartnerRole" and
                              "IN"."RentedPropertyType" = "OLD"."RentedPropertyType" and
                              "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                              "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" and
                              "IN"."_RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "BusinessPartnerRole",
        "RentedPropertyType",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_PhysicalAsset.PhysicalAssetID",
        "_RentalContractInformation.RentalContractInformationReferenceID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AcquisitionDate",
        "BusinessPartnerPhysicalAssetAssignmentCategory",
        "NetRentIncome",
        "NetRentIncomeCurrency",
        "PartialOwnership",
        "RentGuaranteedAmount",
        "RentGuaranteedAmountCurrency",
        "RentGuaranteedExpirationDate",
        "RentGuaranteedUsageType",
        "RentalIncomePeriodLength",
        "RentalIncomePeriodTimeUnit",
        "RentedPropertyQuantity",
        "RentedPropertyUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_BusinessPartnerRole" as "BusinessPartnerRole" ,
            "OLD_RentedPropertyType" as "RentedPropertyType" ,
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" as "ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "OLD__RentalContractInformation.RentalContractInformationReferenceID" as "_RentalContractInformation.RentalContractInformationReferenceID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AcquisitionDate" as "AcquisitionDate" ,
            "OLD_BusinessPartnerPhysicalAssetAssignmentCategory" as "BusinessPartnerPhysicalAssetAssignmentCategory" ,
            "OLD_NetRentIncome" as "NetRentIncome" ,
            "OLD_NetRentIncomeCurrency" as "NetRentIncomeCurrency" ,
            "OLD_PartialOwnership" as "PartialOwnership" ,
            "OLD_RentGuaranteedAmount" as "RentGuaranteedAmount" ,
            "OLD_RentGuaranteedAmountCurrency" as "RentGuaranteedAmountCurrency" ,
            "OLD_RentGuaranteedExpirationDate" as "RentGuaranteedExpirationDate" ,
            "OLD_RentGuaranteedUsageType" as "RentGuaranteedUsageType" ,
            "OLD_RentalIncomePeriodLength" as "RentalIncomePeriodLength" ,
            "OLD_RentalIncomePeriodTimeUnit" as "RentalIncomePeriodTimeUnit" ,
            "OLD_RentedPropertyQuantity" as "RentedPropertyQuantity" ,
            "OLD_RentedPropertyUnit" as "RentedPropertyUnit" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."BusinessPartnerRole",
                        "OLD"."RentedPropertyType",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "OLD"."_RentalContractInformation.RentalContractInformationReferenceID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."BusinessPartnerRole" AS "OLD_BusinessPartnerRole" ,
                "OLD"."RentedPropertyType" AS "OLD_RentedPropertyType" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" AS "OLD__RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AcquisitionDate" AS "OLD_AcquisitionDate" ,
                "OLD"."BusinessPartnerPhysicalAssetAssignmentCategory" AS "OLD_BusinessPartnerPhysicalAssetAssignmentCategory" ,
                "OLD"."NetRentIncome" AS "OLD_NetRentIncome" ,
                "OLD"."NetRentIncomeCurrency" AS "OLD_NetRentIncomeCurrency" ,
                "OLD"."PartialOwnership" AS "OLD_PartialOwnership" ,
                "OLD"."RentGuaranteedAmount" AS "OLD_RentGuaranteedAmount" ,
                "OLD"."RentGuaranteedAmountCurrency" AS "OLD_RentGuaranteedAmountCurrency" ,
                "OLD"."RentGuaranteedExpirationDate" AS "OLD_RentGuaranteedExpirationDate" ,
                "OLD"."RentGuaranteedUsageType" AS "OLD_RentGuaranteedUsageType" ,
                "OLD"."RentalIncomePeriodLength" AS "OLD_RentalIncomePeriodLength" ,
                "OLD"."RentalIncomePeriodTimeUnit" AS "OLD_RentalIncomePeriodTimeUnit" ,
                "OLD"."RentedPropertyQuantity" AS "OLD_RentedPropertyQuantity" ,
                "OLD"."RentedPropertyUnit" AS "OLD_RentedPropertyUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartnerPhysicalAssetAssignment" as "OLD"
            on
                                      "IN"."BusinessPartnerRole" = "OLD"."BusinessPartnerRole" and
                                      "IN"."RentedPropertyType" = "OLD"."RentedPropertyType" and
                                      "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                                      "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" and
                                      "IN"."_RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_BusinessPartnerRole" as "BusinessPartnerRole",
            "OLD_RentedPropertyType" as "RentedPropertyType",
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" as "ASSOC_PhysicalAsset.PhysicalAssetID",
            "OLD__RentalContractInformation.RentalContractInformationReferenceID" as "_RentalContractInformation.RentalContractInformationReferenceID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AcquisitionDate" as "AcquisitionDate",
            "OLD_BusinessPartnerPhysicalAssetAssignmentCategory" as "BusinessPartnerPhysicalAssetAssignmentCategory",
            "OLD_NetRentIncome" as "NetRentIncome",
            "OLD_NetRentIncomeCurrency" as "NetRentIncomeCurrency",
            "OLD_PartialOwnership" as "PartialOwnership",
            "OLD_RentGuaranteedAmount" as "RentGuaranteedAmount",
            "OLD_RentGuaranteedAmountCurrency" as "RentGuaranteedAmountCurrency",
            "OLD_RentGuaranteedExpirationDate" as "RentGuaranteedExpirationDate",
            "OLD_RentGuaranteedUsageType" as "RentGuaranteedUsageType",
            "OLD_RentalIncomePeriodLength" as "RentalIncomePeriodLength",
            "OLD_RentalIncomePeriodTimeUnit" as "RentalIncomePeriodTimeUnit",
            "OLD_RentedPropertyQuantity" as "RentedPropertyQuantity",
            "OLD_RentedPropertyUnit" as "RentedPropertyUnit",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."BusinessPartnerRole",
                        "OLD"."RentedPropertyType",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "OLD"."_RentalContractInformation.RentalContractInformationReferenceID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."BusinessPartnerRole" AS "OLD_BusinessPartnerRole" ,
                "OLD"."RentedPropertyType" AS "OLD_RentedPropertyType" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" AS "OLD__RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AcquisitionDate" AS "OLD_AcquisitionDate" ,
                "OLD"."BusinessPartnerPhysicalAssetAssignmentCategory" AS "OLD_BusinessPartnerPhysicalAssetAssignmentCategory" ,
                "OLD"."NetRentIncome" AS "OLD_NetRentIncome" ,
                "OLD"."NetRentIncomeCurrency" AS "OLD_NetRentIncomeCurrency" ,
                "OLD"."PartialOwnership" AS "OLD_PartialOwnership" ,
                "OLD"."RentGuaranteedAmount" AS "OLD_RentGuaranteedAmount" ,
                "OLD"."RentGuaranteedAmountCurrency" AS "OLD_RentGuaranteedAmountCurrency" ,
                "OLD"."RentGuaranteedExpirationDate" AS "OLD_RentGuaranteedExpirationDate" ,
                "OLD"."RentGuaranteedUsageType" AS "OLD_RentGuaranteedUsageType" ,
                "OLD"."RentalIncomePeriodLength" AS "OLD_RentalIncomePeriodLength" ,
                "OLD"."RentalIncomePeriodTimeUnit" AS "OLD_RentalIncomePeriodTimeUnit" ,
                "OLD"."RentedPropertyQuantity" AS "OLD_RentedPropertyQuantity" ,
                "OLD"."RentedPropertyUnit" AS "OLD_RentedPropertyUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartnerPhysicalAssetAssignment" as "OLD"
            on
               "IN"."BusinessPartnerRole" = "OLD"."BusinessPartnerRole" and
               "IN"."RentedPropertyType" = "OLD"."RentedPropertyType" and
               "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
               "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" and
               "IN"."_RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
