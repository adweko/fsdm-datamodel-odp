PROCEDURE "sap.fsdm.procedures::MacroeconomicFactorDelete" (IN ROW "sap.fsdm.tabletypes::MacroeconomicFactorTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CalculationPeriodID=' || TO_VARCHAR("CalculationPeriodID") || ' ' ||
                'Provider=' || TO_VARCHAR("Provider") || ' ' ||
                'Type=' || TO_VARCHAR("Type") || ' ' ||
                '_Segment.SegmentID=' || TO_VARCHAR("_Segment.SegmentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CalculationPeriodID",
                        "IN"."Provider",
                        "IN"."Type",
                        "IN"."_Segment.SegmentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CalculationPeriodID",
                        "IN"."Provider",
                        "IN"."Type",
                        "IN"."_Segment.SegmentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CalculationPeriodID",
                        "Provider",
                        "Type",
                        "_Segment.SegmentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CalculationPeriodID",
                                    "IN"."Provider",
                                    "IN"."Type",
                                    "IN"."_Segment.SegmentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CalculationPeriodID",
                                    "IN"."Provider",
                                    "IN"."Type",
                                    "IN"."_Segment.SegmentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "CalculationPeriodID" is null and
            "Provider" is null and
            "Type" is null and
            "_Segment.SegmentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::MacroeconomicFactor" (
        "CalculationPeriodID",
        "Provider",
        "Type",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Amount",
        "CalculationPeriodLength",
        "CalculationPeriodStartDate",
        "CalculationPeriodTimeUnit",
        "Currency",
        "Description",
        "PeriodOverPeriodLength",
        "PeriodOverPeriodTimeUnit",
        "PublicationDate",
        "Rate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CalculationPeriodID" as "CalculationPeriodID" ,
            "OLD_Provider" as "Provider" ,
            "OLD_Type" as "Type" ,
            "OLD__Segment.SegmentID" as "_Segment.SegmentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Amount" as "Amount" ,
            "OLD_CalculationPeriodLength" as "CalculationPeriodLength" ,
            "OLD_CalculationPeriodStartDate" as "CalculationPeriodStartDate" ,
            "OLD_CalculationPeriodTimeUnit" as "CalculationPeriodTimeUnit" ,
            "OLD_Currency" as "Currency" ,
            "OLD_Description" as "Description" ,
            "OLD_PeriodOverPeriodLength" as "PeriodOverPeriodLength" ,
            "OLD_PeriodOverPeriodTimeUnit" as "PeriodOverPeriodTimeUnit" ,
            "OLD_PublicationDate" as "PublicationDate" ,
            "OLD_Rate" as "Rate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."CalculationPeriodID",
                        "OLD"."Provider",
                        "OLD"."Type",
                        "OLD"."_Segment.SegmentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."CalculationPeriodID" AS "OLD_CalculationPeriodID" ,
                "OLD"."Provider" AS "OLD_Provider" ,
                "OLD"."Type" AS "OLD_Type" ,
                "OLD"."_Segment.SegmentID" AS "OLD__Segment.SegmentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Amount" AS "OLD_Amount" ,
                "OLD"."CalculationPeriodLength" AS "OLD_CalculationPeriodLength" ,
                "OLD"."CalculationPeriodStartDate" AS "OLD_CalculationPeriodStartDate" ,
                "OLD"."CalculationPeriodTimeUnit" AS "OLD_CalculationPeriodTimeUnit" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."PeriodOverPeriodLength" AS "OLD_PeriodOverPeriodLength" ,
                "OLD"."PeriodOverPeriodTimeUnit" AS "OLD_PeriodOverPeriodTimeUnit" ,
                "OLD"."PublicationDate" AS "OLD_PublicationDate" ,
                "OLD"."Rate" AS "OLD_Rate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::MacroeconomicFactor" as "OLD"
            on
                      "IN"."CalculationPeriodID" = "OLD"."CalculationPeriodID" and
                      "IN"."Provider" = "OLD"."Provider" and
                      "IN"."Type" = "OLD"."Type" and
                      "IN"."_Segment.SegmentID" = "OLD"."_Segment.SegmentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::MacroeconomicFactor" (
        "CalculationPeriodID",
        "Provider",
        "Type",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Amount",
        "CalculationPeriodLength",
        "CalculationPeriodStartDate",
        "CalculationPeriodTimeUnit",
        "Currency",
        "Description",
        "PeriodOverPeriodLength",
        "PeriodOverPeriodTimeUnit",
        "PublicationDate",
        "Rate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CalculationPeriodID" as "CalculationPeriodID",
            "OLD_Provider" as "Provider",
            "OLD_Type" as "Type",
            "OLD__Segment.SegmentID" as "_Segment.SegmentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Amount" as "Amount",
            "OLD_CalculationPeriodLength" as "CalculationPeriodLength",
            "OLD_CalculationPeriodStartDate" as "CalculationPeriodStartDate",
            "OLD_CalculationPeriodTimeUnit" as "CalculationPeriodTimeUnit",
            "OLD_Currency" as "Currency",
            "OLD_Description" as "Description",
            "OLD_PeriodOverPeriodLength" as "PeriodOverPeriodLength",
            "OLD_PeriodOverPeriodTimeUnit" as "PeriodOverPeriodTimeUnit",
            "OLD_PublicationDate" as "PublicationDate",
            "OLD_Rate" as "Rate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."CalculationPeriodID",
                        "OLD"."Provider",
                        "OLD"."Type",
                        "OLD"."_Segment.SegmentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."CalculationPeriodID" AS "OLD_CalculationPeriodID" ,
                "OLD"."Provider" AS "OLD_Provider" ,
                "OLD"."Type" AS "OLD_Type" ,
                "OLD"."_Segment.SegmentID" AS "OLD__Segment.SegmentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Amount" AS "OLD_Amount" ,
                "OLD"."CalculationPeriodLength" AS "OLD_CalculationPeriodLength" ,
                "OLD"."CalculationPeriodStartDate" AS "OLD_CalculationPeriodStartDate" ,
                "OLD"."CalculationPeriodTimeUnit" AS "OLD_CalculationPeriodTimeUnit" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."PeriodOverPeriodLength" AS "OLD_PeriodOverPeriodLength" ,
                "OLD"."PeriodOverPeriodTimeUnit" AS "OLD_PeriodOverPeriodTimeUnit" ,
                "OLD"."PublicationDate" AS "OLD_PublicationDate" ,
                "OLD"."Rate" AS "OLD_Rate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::MacroeconomicFactor" as "OLD"
            on
                                                "IN"."CalculationPeriodID" = "OLD"."CalculationPeriodID" and
                                                "IN"."Provider" = "OLD"."Provider" and
                                                "IN"."Type" = "OLD"."Type" and
                                                "IN"."_Segment.SegmentID" = "OLD"."_Segment.SegmentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::MacroeconomicFactor"
    where (
        "CalculationPeriodID",
        "Provider",
        "Type",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."CalculationPeriodID",
            "OLD"."Provider",
            "OLD"."Type",
            "OLD"."_Segment.SegmentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::MacroeconomicFactor" as "OLD"
        on
                                       "IN"."CalculationPeriodID" = "OLD"."CalculationPeriodID" and
                                       "IN"."Provider" = "OLD"."Provider" and
                                       "IN"."Type" = "OLD"."Type" and
                                       "IN"."_Segment.SegmentID" = "OLD"."_Segment.SegmentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
