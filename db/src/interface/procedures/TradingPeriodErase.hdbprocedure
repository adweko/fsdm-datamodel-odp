PROCEDURE "sap.fsdm.procedures::TradingPeriodErase" (IN ROW "sap.fsdm.tabletypes::TradingPeriodTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "StartDate" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::TradingPeriod"
        WHERE
        (            "StartDate" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" 
        ) in
        (
            select                 "OLD"."StartDate" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::TradingPeriod" "OLD"
            on
            "IN"."StartDate" = "OLD"."StartDate" and
            "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
            "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::TradingPeriod_Historical"
        WHERE
        (
            "StartDate" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" 
        ) in
        (
            select
                "OLD"."StartDate" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::TradingPeriod_Historical" "OLD"
            on
                "IN"."StartDate" = "OLD"."StartDate" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" 
        );

END
