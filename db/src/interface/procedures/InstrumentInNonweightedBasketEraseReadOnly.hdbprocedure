PROCEDURE "sap.fsdm.procedures::InstrumentInNonweightedBasketEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::InstrumentInNonweightedBasketTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::InstrumentInNonweightedBasketTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::InstrumentInNonweightedBasketTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Basket.FinancialInstrumentID" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_Basket.FinancialInstrumentID" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_Basket.FinancialInstrumentID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InstrumentInNonweightedBasket" "OLD"
            on
                "IN"."_Basket.FinancialInstrumentID" = "OLD"."_Basket.FinancialInstrumentID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_Basket.FinancialInstrumentID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_Basket.FinancialInstrumentID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InstrumentInNonweightedBasket_Historical" "OLD"
            on
                "IN"."_Basket.FinancialInstrumentID" = "OLD"."_Basket.FinancialInstrumentID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

END
