PROCEDURE "sap.fsdm.procedures::ExemptionOrderSnapshotEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ExemptionOrderSnapshotTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ExemptionOrderSnapshotTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ExemptionOrderSnapshotTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_ExemptionOrder.Category" is null and
            "_ExemptionOrder._BusinessPartner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_ExemptionOrder.Category" ,
                "_ExemptionOrder._BusinessPartner.BusinessPartnerID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_ExemptionOrder.Category" ,
                "OLD"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ExemptionOrderSnapshot" "OLD"
            on
                "IN"."_ExemptionOrder.Category" = "OLD"."_ExemptionOrder.Category" and
                "IN"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" = "OLD"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_ExemptionOrder.Category" ,
            "_ExemptionOrder._BusinessPartner.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_ExemptionOrder.Category" ,
                "OLD"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ExemptionOrderSnapshot_Historical" "OLD"
            on
                "IN"."_ExemptionOrder.Category" = "OLD"."_ExemptionOrder.Category" and
                "IN"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" = "OLD"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" 
        );

END
