PROCEDURE "sap.fsdm.procedures::ProlongationOptionDelete" (IN ROW "sap.fsdm.tabletypes::ProlongationOptionTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                '_RentalContractInformation.RentalContractInformationReferenceID=' || TO_VARCHAR("_RentalContractInformation.RentalContractInformationReferenceID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_RentalContractInformation.RentalContractInformationReferenceID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_RentalContractInformation.RentalContractInformationReferenceID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "_RentalContractInformation.RentalContractInformationReferenceID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_RentalContractInformation.RentalContractInformationReferenceID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_RentalContractInformation.RentalContractInformationReferenceID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_RentalContractInformation.RentalContractInformationReferenceID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::ProlongationOption" (
        "SequenceNumber",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_RentalContractInformation.RentalContractInformationReferenceID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AdditionalTermLength",
        "AdditionalTermLengthTimeUnit",
        "AutomaticProlongation",
        "AutomaticProlongationTriggerPeriodLength",
        "AutomaticProlongationTriggerPeriodTimeUnit",
        "BeginOfExercisePeriod",
        "BusinessCalendar",
        "BusinessDayConvention",
        "ConditionAcceptanceDate",
        "ConditionFixedPeriodEndDate",
        "ConditionFixedPeriodStartDate",
        "EndOfExercisePeriod",
        "NoticePeriodLength",
        "NoticePeriodTimeUnit",
        "ProlongationOptionType",
        "ProlongationRightHolder",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD__RentalContractInformation.RentalContractInformationReferenceID" as "_RentalContractInformation.RentalContractInformationReferenceID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AdditionalTermLength" as "AdditionalTermLength" ,
            "OLD_AdditionalTermLengthTimeUnit" as "AdditionalTermLengthTimeUnit" ,
            "OLD_AutomaticProlongation" as "AutomaticProlongation" ,
            "OLD_AutomaticProlongationTriggerPeriodLength" as "AutomaticProlongationTriggerPeriodLength" ,
            "OLD_AutomaticProlongationTriggerPeriodTimeUnit" as "AutomaticProlongationTriggerPeriodTimeUnit" ,
            "OLD_BeginOfExercisePeriod" as "BeginOfExercisePeriod" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_ConditionAcceptanceDate" as "ConditionAcceptanceDate" ,
            "OLD_ConditionFixedPeriodEndDate" as "ConditionFixedPeriodEndDate" ,
            "OLD_ConditionFixedPeriodStartDate" as "ConditionFixedPeriodStartDate" ,
            "OLD_EndOfExercisePeriod" as "EndOfExercisePeriod" ,
            "OLD_NoticePeriodLength" as "NoticePeriodLength" ,
            "OLD_NoticePeriodTimeUnit" as "NoticePeriodTimeUnit" ,
            "OLD_ProlongationOptionType" as "ProlongationOptionType" ,
            "OLD_ProlongationRightHolder" as "ProlongationRightHolder" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_RentalContractInformation.RentalContractInformationReferenceID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" AS "OLD__RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AdditionalTermLength" AS "OLD_AdditionalTermLength" ,
                "OLD"."AdditionalTermLengthTimeUnit" AS "OLD_AdditionalTermLengthTimeUnit" ,
                "OLD"."AutomaticProlongation" AS "OLD_AutomaticProlongation" ,
                "OLD"."AutomaticProlongationTriggerPeriodLength" AS "OLD_AutomaticProlongationTriggerPeriodLength" ,
                "OLD"."AutomaticProlongationTriggerPeriodTimeUnit" AS "OLD_AutomaticProlongationTriggerPeriodTimeUnit" ,
                "OLD"."BeginOfExercisePeriod" AS "OLD_BeginOfExercisePeriod" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."ConditionAcceptanceDate" AS "OLD_ConditionAcceptanceDate" ,
                "OLD"."ConditionFixedPeriodEndDate" AS "OLD_ConditionFixedPeriodEndDate" ,
                "OLD"."ConditionFixedPeriodStartDate" AS "OLD_ConditionFixedPeriodStartDate" ,
                "OLD"."EndOfExercisePeriod" AS "OLD_EndOfExercisePeriod" ,
                "OLD"."NoticePeriodLength" AS "OLD_NoticePeriodLength" ,
                "OLD"."NoticePeriodTimeUnit" AS "OLD_NoticePeriodTimeUnit" ,
                "OLD"."ProlongationOptionType" AS "OLD_ProlongationOptionType" ,
                "OLD"."ProlongationRightHolder" AS "OLD_ProlongationRightHolder" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ProlongationOption" as "OLD"
            on
                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                      "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                      "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                      "IN"."_RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::ProlongationOption" (
        "SequenceNumber",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_RentalContractInformation.RentalContractInformationReferenceID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AdditionalTermLength",
        "AdditionalTermLengthTimeUnit",
        "AutomaticProlongation",
        "AutomaticProlongationTriggerPeriodLength",
        "AutomaticProlongationTriggerPeriodTimeUnit",
        "BeginOfExercisePeriod",
        "BusinessCalendar",
        "BusinessDayConvention",
        "ConditionAcceptanceDate",
        "ConditionFixedPeriodEndDate",
        "ConditionFixedPeriodStartDate",
        "EndOfExercisePeriod",
        "NoticePeriodLength",
        "NoticePeriodTimeUnit",
        "ProlongationOptionType",
        "ProlongationRightHolder",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD__RentalContractInformation.RentalContractInformationReferenceID" as "_RentalContractInformation.RentalContractInformationReferenceID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AdditionalTermLength" as "AdditionalTermLength",
            "OLD_AdditionalTermLengthTimeUnit" as "AdditionalTermLengthTimeUnit",
            "OLD_AutomaticProlongation" as "AutomaticProlongation",
            "OLD_AutomaticProlongationTriggerPeriodLength" as "AutomaticProlongationTriggerPeriodLength",
            "OLD_AutomaticProlongationTriggerPeriodTimeUnit" as "AutomaticProlongationTriggerPeriodTimeUnit",
            "OLD_BeginOfExercisePeriod" as "BeginOfExercisePeriod",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_ConditionAcceptanceDate" as "ConditionAcceptanceDate",
            "OLD_ConditionFixedPeriodEndDate" as "ConditionFixedPeriodEndDate",
            "OLD_ConditionFixedPeriodStartDate" as "ConditionFixedPeriodStartDate",
            "OLD_EndOfExercisePeriod" as "EndOfExercisePeriod",
            "OLD_NoticePeriodLength" as "NoticePeriodLength",
            "OLD_NoticePeriodTimeUnit" as "NoticePeriodTimeUnit",
            "OLD_ProlongationOptionType" as "ProlongationOptionType",
            "OLD_ProlongationRightHolder" as "ProlongationRightHolder",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_RentalContractInformation.RentalContractInformationReferenceID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" AS "OLD__RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AdditionalTermLength" AS "OLD_AdditionalTermLength" ,
                "OLD"."AdditionalTermLengthTimeUnit" AS "OLD_AdditionalTermLengthTimeUnit" ,
                "OLD"."AutomaticProlongation" AS "OLD_AutomaticProlongation" ,
                "OLD"."AutomaticProlongationTriggerPeriodLength" AS "OLD_AutomaticProlongationTriggerPeriodLength" ,
                "OLD"."AutomaticProlongationTriggerPeriodTimeUnit" AS "OLD_AutomaticProlongationTriggerPeriodTimeUnit" ,
                "OLD"."BeginOfExercisePeriod" AS "OLD_BeginOfExercisePeriod" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."ConditionAcceptanceDate" AS "OLD_ConditionAcceptanceDate" ,
                "OLD"."ConditionFixedPeriodEndDate" AS "OLD_ConditionFixedPeriodEndDate" ,
                "OLD"."ConditionFixedPeriodStartDate" AS "OLD_ConditionFixedPeriodStartDate" ,
                "OLD"."EndOfExercisePeriod" AS "OLD_EndOfExercisePeriod" ,
                "OLD"."NoticePeriodLength" AS "OLD_NoticePeriodLength" ,
                "OLD"."NoticePeriodTimeUnit" AS "OLD_NoticePeriodTimeUnit" ,
                "OLD"."ProlongationOptionType" AS "OLD_ProlongationOptionType" ,
                "OLD"."ProlongationRightHolder" AS "OLD_ProlongationRightHolder" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ProlongationOption" as "OLD"
            on
                                                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                                "IN"."_RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::ProlongationOption"
    where (
        "SequenceNumber",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_RentalContractInformation.RentalContractInformationReferenceID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."_RentalContractInformation.RentalContractInformationReferenceID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ProlongationOption" as "OLD"
        on
                                       "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                       "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                       "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                       "IN"."_RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
