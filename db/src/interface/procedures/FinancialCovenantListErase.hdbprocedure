PROCEDURE "sap.fsdm.procedures::FinancialCovenantListErase" (IN ROW "sap.fsdm.tabletypes::FinancialCovenantTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::FinancialCovenant"
        WHERE
        (            "SequenceNumber" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_ContingentConvertibleInstrument.FinancialInstrumentID" ,
            "_ContractBundle.ContractBundleID" ,
            "_PhysicalAsset.PhysicalAssetID" ,
            "_TrancheInSyndication.TrancheSequenceNumber" ,
            "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_TrancheInSyndication._SyndicationAgreement.IDSystem" 
        ) in
        (
            select                 "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."_ContractBundle.ContractBundleID" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialCovenant" "OLD"
            on
            "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
            "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::FinancialCovenant_Historical"
        WHERE
        (
            "SequenceNumber" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_ContingentConvertibleInstrument.FinancialInstrumentID" ,
            "_ContractBundle.ContractBundleID" ,
            "_PhysicalAsset.PhysicalAssetID" ,
            "_TrancheInSyndication.TrancheSequenceNumber" ,
            "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_TrancheInSyndication._SyndicationAgreement.IDSystem" 
        ) in
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."_ContractBundle.ContractBundleID" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialCovenant_Historical" "OLD"
            on
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" 
        );

END
