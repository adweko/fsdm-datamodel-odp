PROCEDURE "sap.fsdm.procedures::FundsTransferPricingRateErase" (IN ROW "sap.fsdm.tabletypes::FundsTransferPricingRateTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SplittingTranche" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" is null and
            "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" is null and
            "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" is null and
            "_BusinessPartner.BusinessPartnerID" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_OrganizationalUnit.IDSystem" is null and
            "_OrganizationalUnit.OrganizationalUnitID" is null and
            "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_RiskReportingNode.RiskReportingNodeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::FundsTransferPricingRate"
        WHERE
        (            "SplittingTranche" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
            "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_OrganizationalUnit.IDSystem" ,
            "_OrganizationalUnit.OrganizationalUnitID" ,
            "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" 
        ) in
        (
            select                 "OLD"."SplittingTranche" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_OrganizationalUnit.IDSystem" ,
                "OLD"."_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" 
            from :ROW "IN"
            inner join "sap.fsdm::FundsTransferPricingRate" "OLD"
            on
            "IN"."SplittingTranche" = "OLD"."SplittingTranche" and
            "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
            "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
            "IN"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" and
            "IN"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
            "IN"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" and
            "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
            "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
            "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
            "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
            "IN"."_OrganizationalUnit.IDSystem" = "OLD"."_OrganizationalUnit.IDSystem" and
            "IN"."_OrganizationalUnit.OrganizationalUnitID" = "OLD"."_OrganizationalUnit.OrganizationalUnitID" and
            "IN"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
            "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
            "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
            "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" 
        );

        --delete data from history table
        delete from "sap.fsdm::FundsTransferPricingRate_Historical"
        WHERE
        (
            "SplittingTranche" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
            "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_OrganizationalUnit.IDSystem" ,
            "_OrganizationalUnit.OrganizationalUnitID" ,
            "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" 
        ) in
        (
            select
                "OLD"."SplittingTranche" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_OrganizationalUnit.IDSystem" ,
                "OLD"."_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" 
            from :ROW "IN"
            inner join "sap.fsdm::FundsTransferPricingRate_Historical" "OLD"
            on
                "IN"."SplittingTranche" = "OLD"."SplittingTranche" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" and
                "IN"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_OrganizationalUnit.IDSystem" = "OLD"."_OrganizationalUnit.IDSystem" and
                "IN"."_OrganizationalUnit.OrganizationalUnitID" = "OLD"."_OrganizationalUnit.OrganizationalUnitID" and
                "IN"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" 
        );

END
