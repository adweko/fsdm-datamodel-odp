PROCEDURE "sap.fsdm.procedures::BusinessPartnerCreditRiskAssessmentLoad" (IN ROW "sap.fsdm.tabletypes::BusinessPartnerCreditRiskAssessmentTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'BusinessPartnerCreditRiskAssessmentReason=' || TO_VARCHAR("BusinessPartnerCreditRiskAssessmentReason") || ' ' ||
                'CreditRiskAssessmentDate=' || TO_VARCHAR("CreditRiskAssessmentDate") || ' ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."BusinessPartnerCreditRiskAssessmentReason",
                        "IN"."CreditRiskAssessmentDate",
                        "IN"."_BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."BusinessPartnerCreditRiskAssessmentReason",
                        "IN"."CreditRiskAssessmentDate",
                        "IN"."_BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "BusinessPartnerCreditRiskAssessmentReason",
                        "CreditRiskAssessmentDate",
                        "_BusinessPartner.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."BusinessPartnerCreditRiskAssessmentReason",
                                    "IN"."CreditRiskAssessmentDate",
                                    "IN"."_BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."BusinessPartnerCreditRiskAssessmentReason",
                                    "IN"."CreditRiskAssessmentDate",
                                    "IN"."_BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::BusinessPartnerCreditRiskAssessment" (
        "BusinessPartnerCreditRiskAssessmentReason",
        "CreditRiskAssessmentDate",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ComplianceStatus",
        "ComplianceStatusValidityEndDate",
        "ComplianceStatusValidityStartDate",
        "DataRequestDate",
        "DisclosureRequirement",
        "FollowUpDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_BusinessPartnerCreditRiskAssessmentReason" as "BusinessPartnerCreditRiskAssessmentReason" ,
            "OLD_CreditRiskAssessmentDate" as "CreditRiskAssessmentDate" ,
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ComplianceStatus" as "ComplianceStatus" ,
            "OLD_ComplianceStatusValidityEndDate" as "ComplianceStatusValidityEndDate" ,
            "OLD_ComplianceStatusValidityStartDate" as "ComplianceStatusValidityStartDate" ,
            "OLD_DataRequestDate" as "DataRequestDate" ,
            "OLD_DisclosureRequirement" as "DisclosureRequirement" ,
            "OLD_FollowUpDate" as "FollowUpDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."BusinessPartnerCreditRiskAssessmentReason",
                        "IN"."CreditRiskAssessmentDate",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."BusinessPartnerCreditRiskAssessmentReason" as "OLD_BusinessPartnerCreditRiskAssessmentReason",
                                "OLD"."CreditRiskAssessmentDate" as "OLD_CreditRiskAssessmentDate",
                                "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."ComplianceStatus" as "OLD_ComplianceStatus",
                                "OLD"."ComplianceStatusValidityEndDate" as "OLD_ComplianceStatusValidityEndDate",
                                "OLD"."ComplianceStatusValidityStartDate" as "OLD_ComplianceStatusValidityStartDate",
                                "OLD"."DataRequestDate" as "OLD_DataRequestDate",
                                "OLD"."DisclosureRequirement" as "OLD_DisclosureRequirement",
                                "OLD"."FollowUpDate" as "OLD_FollowUpDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartnerCreditRiskAssessment" as "OLD"
            on
                ifnull( "IN"."BusinessPartnerCreditRiskAssessmentReason", '') = "OLD"."BusinessPartnerCreditRiskAssessmentReason" and
                ifnull( "IN"."CreditRiskAssessmentDate", '0001-01-01') = "OLD"."CreditRiskAssessmentDate" and
                ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '') = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::BusinessPartnerCreditRiskAssessment" (
        "BusinessPartnerCreditRiskAssessmentReason",
        "CreditRiskAssessmentDate",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ComplianceStatus",
        "ComplianceStatusValidityEndDate",
        "ComplianceStatusValidityStartDate",
        "DataRequestDate",
        "DisclosureRequirement",
        "FollowUpDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_BusinessPartnerCreditRiskAssessmentReason"  as "BusinessPartnerCreditRiskAssessmentReason",
            "OLD_CreditRiskAssessmentDate"  as "CreditRiskAssessmentDate",
            "OLD__BusinessPartner.BusinessPartnerID"  as "_BusinessPartner.BusinessPartnerID",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_ComplianceStatus"  as "ComplianceStatus",
            "OLD_ComplianceStatusValidityEndDate"  as "ComplianceStatusValidityEndDate",
            "OLD_ComplianceStatusValidityStartDate"  as "ComplianceStatusValidityStartDate",
            "OLD_DataRequestDate"  as "DataRequestDate",
            "OLD_DisclosureRequirement"  as "DisclosureRequirement",
            "OLD_FollowUpDate"  as "FollowUpDate",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."BusinessPartnerCreditRiskAssessmentReason",
                        "IN"."CreditRiskAssessmentDate",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."BusinessPartnerCreditRiskAssessmentReason" as "OLD_BusinessPartnerCreditRiskAssessmentReason",
                        "OLD"."CreditRiskAssessmentDate" as "OLD_CreditRiskAssessmentDate",
                        "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."ComplianceStatus" as "OLD_ComplianceStatus",
                        "OLD"."ComplianceStatusValidityEndDate" as "OLD_ComplianceStatusValidityEndDate",
                        "OLD"."ComplianceStatusValidityStartDate" as "OLD_ComplianceStatusValidityStartDate",
                        "OLD"."DataRequestDate" as "OLD_DataRequestDate",
                        "OLD"."DisclosureRequirement" as "OLD_DisclosureRequirement",
                        "OLD"."FollowUpDate" as "OLD_FollowUpDate",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartnerCreditRiskAssessment" as "OLD"
            on
                ifnull( "IN"."BusinessPartnerCreditRiskAssessmentReason", '' ) = "OLD"."BusinessPartnerCreditRiskAssessmentReason" and
                ifnull( "IN"."CreditRiskAssessmentDate", '0001-01-01' ) = "OLD"."CreditRiskAssessmentDate" and
                ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '' ) = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::BusinessPartnerCreditRiskAssessment"
    where (
        "BusinessPartnerCreditRiskAssessmentReason",
        "CreditRiskAssessmentDate",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."BusinessPartnerCreditRiskAssessmentReason",
            "OLD"."CreditRiskAssessmentDate",
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::BusinessPartnerCreditRiskAssessment" as "OLD"
        on
           ifnull( "IN"."BusinessPartnerCreditRiskAssessmentReason", '' ) = "OLD"."BusinessPartnerCreditRiskAssessmentReason" and
           ifnull( "IN"."CreditRiskAssessmentDate", '0001-01-01' ) = "OLD"."CreditRiskAssessmentDate" and
           ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '' ) = "OLD"."_BusinessPartner.BusinessPartnerID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::BusinessPartnerCreditRiskAssessment" (
        "BusinessPartnerCreditRiskAssessmentReason",
        "CreditRiskAssessmentDate",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ComplianceStatus",
        "ComplianceStatusValidityEndDate",
        "ComplianceStatusValidityStartDate",
        "DataRequestDate",
        "DisclosureRequirement",
        "FollowUpDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "BusinessPartnerCreditRiskAssessmentReason", '' ) as "BusinessPartnerCreditRiskAssessmentReason",
            ifnull( "CreditRiskAssessmentDate", '0001-01-01' ) as "CreditRiskAssessmentDate",
            ifnull( "_BusinessPartner.BusinessPartnerID", '' ) as "_BusinessPartner.BusinessPartnerID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "ComplianceStatus"  ,
            "ComplianceStatusValidityEndDate"  ,
            "ComplianceStatusValidityStartDate"  ,
            "DataRequestDate"  ,
            "DisclosureRequirement"  ,
            "FollowUpDate"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END