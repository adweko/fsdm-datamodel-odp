PROCEDURE "sap.fsdm.procedures::SpecialRuleErase" (IN ROW "sap.fsdm.tabletypes::SpecialRuleTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SpecialRuleValidityStartDate" is null and
            "_BusinessCalendar.BusinessCalendar" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::SpecialRule"
        WHERE
        (            "SpecialRuleValidityStartDate" ,
            "_BusinessCalendar.BusinessCalendar" 
        ) in
        (
            select                 "OLD"."SpecialRuleValidityStartDate" ,
                "OLD"."_BusinessCalendar.BusinessCalendar" 
            from :ROW "IN"
            inner join "sap.fsdm::SpecialRule" "OLD"
            on
            "IN"."SpecialRuleValidityStartDate" = "OLD"."SpecialRuleValidityStartDate" and
            "IN"."_BusinessCalendar.BusinessCalendar" = "OLD"."_BusinessCalendar.BusinessCalendar" 
        );

        --delete data from history table
        delete from "sap.fsdm::SpecialRule_Historical"
        WHERE
        (
            "SpecialRuleValidityStartDate" ,
            "_BusinessCalendar.BusinessCalendar" 
        ) in
        (
            select
                "OLD"."SpecialRuleValidityStartDate" ,
                "OLD"."_BusinessCalendar.BusinessCalendar" 
            from :ROW "IN"
            inner join "sap.fsdm::SpecialRule_Historical" "OLD"
            on
                "IN"."SpecialRuleValidityStartDate" = "OLD"."SpecialRuleValidityStartDate" and
                "IN"."_BusinessCalendar.BusinessCalendar" = "OLD"."_BusinessCalendar.BusinessCalendar" 
        );

END
