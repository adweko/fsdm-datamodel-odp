PROCEDURE "sap.fsdm.procedures::RegulatoryIndicatorForTradeDelete" (IN ROW "sap.fsdm.tabletypes::RegulatoryIndicatorForTradeTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Criterion=' || TO_VARCHAR("Criterion") || ' ' ||
                'Regulation=' || TO_VARCHAR("Regulation") || ' ' ||
                '_Trade.IDSystem=' || TO_VARCHAR("_Trade.IDSystem") || ' ' ||
                '_Trade.TradeID=' || TO_VARCHAR("_Trade.TradeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Criterion",
                        "IN"."Regulation",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Criterion",
                        "IN"."Regulation",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Criterion",
                        "Regulation",
                        "_Trade.IDSystem",
                        "_Trade.TradeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Criterion",
                                    "IN"."Regulation",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Criterion",
                                    "IN"."Regulation",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "Criterion" is null and
            "Regulation" is null and
            "_Trade.IDSystem" is null and
            "_Trade.TradeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::RegulatoryIndicatorForTrade" (
        "Criterion",
        "Regulation",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Comment",
        "IndicatorAmount",
        "IndicatorAmountCurrency",
        "IndicatorBooleanValue",
        "IndicatorCharValue",
        "IndicatorDataType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Criterion" as "Criterion" ,
            "OLD_Regulation" as "Regulation" ,
            "OLD__Trade.IDSystem" as "_Trade.IDSystem" ,
            "OLD__Trade.TradeID" as "_Trade.TradeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Comment" as "Comment" ,
            "OLD_IndicatorAmount" as "IndicatorAmount" ,
            "OLD_IndicatorAmountCurrency" as "IndicatorAmountCurrency" ,
            "OLD_IndicatorBooleanValue" as "IndicatorBooleanValue" ,
            "OLD_IndicatorCharValue" as "IndicatorCharValue" ,
            "OLD_IndicatorDataType" as "IndicatorDataType" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."Criterion",
                        "OLD"."Regulation",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."Criterion" AS "OLD_Criterion" ,
                "OLD"."Regulation" AS "OLD_Regulation" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Comment" AS "OLD_Comment" ,
                "OLD"."IndicatorAmount" AS "OLD_IndicatorAmount" ,
                "OLD"."IndicatorAmountCurrency" AS "OLD_IndicatorAmountCurrency" ,
                "OLD"."IndicatorBooleanValue" AS "OLD_IndicatorBooleanValue" ,
                "OLD"."IndicatorCharValue" AS "OLD_IndicatorCharValue" ,
                "OLD"."IndicatorDataType" AS "OLD_IndicatorDataType" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::RegulatoryIndicatorForTrade" as "OLD"
            on
                      "IN"."Criterion" = "OLD"."Criterion" and
                      "IN"."Regulation" = "OLD"."Regulation" and
                      "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                      "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::RegulatoryIndicatorForTrade" (
        "Criterion",
        "Regulation",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Comment",
        "IndicatorAmount",
        "IndicatorAmountCurrency",
        "IndicatorBooleanValue",
        "IndicatorCharValue",
        "IndicatorDataType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Criterion" as "Criterion",
            "OLD_Regulation" as "Regulation",
            "OLD__Trade.IDSystem" as "_Trade.IDSystem",
            "OLD__Trade.TradeID" as "_Trade.TradeID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Comment" as "Comment",
            "OLD_IndicatorAmount" as "IndicatorAmount",
            "OLD_IndicatorAmountCurrency" as "IndicatorAmountCurrency",
            "OLD_IndicatorBooleanValue" as "IndicatorBooleanValue",
            "OLD_IndicatorCharValue" as "IndicatorCharValue",
            "OLD_IndicatorDataType" as "IndicatorDataType",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."Criterion",
                        "OLD"."Regulation",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."Criterion" AS "OLD_Criterion" ,
                "OLD"."Regulation" AS "OLD_Regulation" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Comment" AS "OLD_Comment" ,
                "OLD"."IndicatorAmount" AS "OLD_IndicatorAmount" ,
                "OLD"."IndicatorAmountCurrency" AS "OLD_IndicatorAmountCurrency" ,
                "OLD"."IndicatorBooleanValue" AS "OLD_IndicatorBooleanValue" ,
                "OLD"."IndicatorCharValue" AS "OLD_IndicatorCharValue" ,
                "OLD"."IndicatorDataType" AS "OLD_IndicatorDataType" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::RegulatoryIndicatorForTrade" as "OLD"
            on
                                                "IN"."Criterion" = "OLD"."Criterion" and
                                                "IN"."Regulation" = "OLD"."Regulation" and
                                                "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                                "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::RegulatoryIndicatorForTrade"
    where (
        "Criterion",
        "Regulation",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."Criterion",
            "OLD"."Regulation",
            "OLD"."_Trade.IDSystem",
            "OLD"."_Trade.TradeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::RegulatoryIndicatorForTrade" as "OLD"
        on
                                       "IN"."Criterion" = "OLD"."Criterion" and
                                       "IN"."Regulation" = "OLD"."Regulation" and
                                       "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                       "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
