PROCEDURE "sap.fsdm.procedures::BenchmarkInterestErase" (IN ROW "sap.fsdm.tabletypes::BenchmarkInterestTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "BenchmarkInterestRateCategory" is null and
            "BenchmarkInterestRateCurrency" is null and
            "BenchmarkInterestRateDataProvider" is null and
            "CountryCodeForBenchmarkInterest" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::BenchmarkInterest"
        WHERE
        (            "BenchmarkInterestRateCategory" ,
            "BenchmarkInterestRateCurrency" ,
            "BenchmarkInterestRateDataProvider" ,
            "CountryCodeForBenchmarkInterest" 
        ) in
        (
            select                 "OLD"."BenchmarkInterestRateCategory" ,
                "OLD"."BenchmarkInterestRateCurrency" ,
                "OLD"."BenchmarkInterestRateDataProvider" ,
                "OLD"."CountryCodeForBenchmarkInterest" 
            from :ROW "IN"
            inner join "sap.fsdm::BenchmarkInterest" "OLD"
            on
            "IN"."BenchmarkInterestRateCategory" = "OLD"."BenchmarkInterestRateCategory" and
            "IN"."BenchmarkInterestRateCurrency" = "OLD"."BenchmarkInterestRateCurrency" and
            "IN"."BenchmarkInterestRateDataProvider" = "OLD"."BenchmarkInterestRateDataProvider" and
            "IN"."CountryCodeForBenchmarkInterest" = "OLD"."CountryCodeForBenchmarkInterest" 
        );

        --delete data from history table
        delete from "sap.fsdm::BenchmarkInterest_Historical"
        WHERE
        (
            "BenchmarkInterestRateCategory" ,
            "BenchmarkInterestRateCurrency" ,
            "BenchmarkInterestRateDataProvider" ,
            "CountryCodeForBenchmarkInterest" 
        ) in
        (
            select
                "OLD"."BenchmarkInterestRateCategory" ,
                "OLD"."BenchmarkInterestRateCurrency" ,
                "OLD"."BenchmarkInterestRateDataProvider" ,
                "OLD"."CountryCodeForBenchmarkInterest" 
            from :ROW "IN"
            inner join "sap.fsdm::BenchmarkInterest_Historical" "OLD"
            on
                "IN"."BenchmarkInterestRateCategory" = "OLD"."BenchmarkInterestRateCategory" and
                "IN"."BenchmarkInterestRateCurrency" = "OLD"."BenchmarkInterestRateCurrency" and
                "IN"."BenchmarkInterestRateDataProvider" = "OLD"."BenchmarkInterestRateDataProvider" and
                "IN"."CountryCodeForBenchmarkInterest" = "OLD"."CountryCodeForBenchmarkInterest" 
        );

END
