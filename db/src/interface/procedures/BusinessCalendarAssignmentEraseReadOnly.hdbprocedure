PROCEDURE "sap.fsdm.procedures::BusinessCalendarAssignmentEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::BusinessCalendarAssignmentTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::BusinessCalendarAssignmentTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::BusinessCalendarAssignmentTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_ChildBusinessCalendar.BusinessCalendar" is null and
            "_GroupBusinessCalendar.BusinessCalendar" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_ChildBusinessCalendar.BusinessCalendar" ,
                "_GroupBusinessCalendar.BusinessCalendar" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_ChildBusinessCalendar.BusinessCalendar" ,
                "OLD"."_GroupBusinessCalendar.BusinessCalendar" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::BusinessCalendarAssignment" "OLD"
            on
                "IN"."_ChildBusinessCalendar.BusinessCalendar" = "OLD"."_ChildBusinessCalendar.BusinessCalendar" and
                "IN"."_GroupBusinessCalendar.BusinessCalendar" = "OLD"."_GroupBusinessCalendar.BusinessCalendar" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_ChildBusinessCalendar.BusinessCalendar" ,
            "_GroupBusinessCalendar.BusinessCalendar" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_ChildBusinessCalendar.BusinessCalendar" ,
                "OLD"."_GroupBusinessCalendar.BusinessCalendar" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::BusinessCalendarAssignment_Historical" "OLD"
            on
                "IN"."_ChildBusinessCalendar.BusinessCalendar" = "OLD"."_ChildBusinessCalendar.BusinessCalendar" and
                "IN"."_GroupBusinessCalendar.BusinessCalendar" = "OLD"."_GroupBusinessCalendar.BusinessCalendar" 
        );

END
