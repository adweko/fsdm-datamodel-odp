PROCEDURE "sap.fsdm.procedures::InterestAndBalanceScheduleDelReadOnly" (IN ROW "sap.fsdm.tabletypes::InterestAndBalanceScheduleTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::InterestAndBalanceScheduleTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::InterestAndBalanceScheduleTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AssetOrLiability=' || TO_VARCHAR("AssetOrLiability") || ' ' ||
                'KeyDate=' || TO_VARCHAR("KeyDate") || ' ' ||
                'Scenario=' || TO_VARCHAR("Scenario") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_TimeBucket.MaturityBandID=' || TO_VARCHAR("_TimeBucket.MaturityBandID") || ' ' ||
                '_TimeBucket.TimeBucketID=' || TO_VARCHAR("_TimeBucket.TimeBucketID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AssetOrLiability",
                        "IN"."KeyDate",
                        "IN"."Scenario",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AssetOrLiability",
                        "IN"."KeyDate",
                        "IN"."Scenario",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AssetOrLiability",
                        "KeyDate",
                        "Scenario",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_TimeBucket.MaturityBandID",
                        "_TimeBucket.TimeBucketID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AssetOrLiability",
                                    "IN"."KeyDate",
                                    "IN"."Scenario",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AssetOrLiability",
                                    "IN"."KeyDate",
                                    "IN"."Scenario",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "AssetOrLiability" is null and
            "KeyDate" is null and
            "Scenario" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_TimeBucket.MaturityBandID" is null and
            "_TimeBucket.TimeBucketID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "AssetOrLiability",
            "KeyDate",
            "Scenario",
            "_Collection.CollectionID",
            "_Collection.IDSystem",
            "_ResultGroup.ResultDataProvider",
            "_ResultGroup.ResultGroupID",
            "_TimeBucket.MaturityBandID",
            "_TimeBucket.TimeBucketID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::InterestAndBalanceSchedule" WHERE
            (
            "AssetOrLiability" ,
            "KeyDate" ,
            "Scenario" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_TimeBucket.MaturityBandID" ,
            "_TimeBucket.TimeBucketID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."AssetOrLiability",
            "OLD"."KeyDate",
            "OLD"."Scenario",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_TimeBucket.MaturityBandID",
            "OLD"."_TimeBucket.TimeBucketID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::InterestAndBalanceSchedule" as "OLD"
        on
                              "IN"."AssetOrLiability" = "OLD"."AssetOrLiability" and
                              "IN"."KeyDate" = "OLD"."KeyDate" and
                              "IN"."Scenario" = "OLD"."Scenario" and
                              "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                              "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                              "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                              "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                              "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
                              "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "AssetOrLiability",
        "KeyDate",
        "Scenario",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AmortizationAmount",
        "CumulatedInterestAmount",
        "CumulatedPresentValueOfAmortizationAmount",
        "CumulatedPresentValueOfInterestAmount",
        "Currency",
        "DiscountInterestRate",
        "InterestAmount",
        "NominalInterestRate",
        "PresentValueOfAmortizationAmount",
        "PresentValueOfInterestAmount",
        "PrincipalAmount",
        "TimeBucketEndDate",
        "TimeBucketStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_AssetOrLiability" as "AssetOrLiability" ,
            "OLD_KeyDate" as "KeyDate" ,
            "OLD_Scenario" as "Scenario" ,
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID" ,
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AmortizationAmount" as "AmortizationAmount" ,
            "OLD_CumulatedInterestAmount" as "CumulatedInterestAmount" ,
            "OLD_CumulatedPresentValueOfAmortizationAmount" as "CumulatedPresentValueOfAmortizationAmount" ,
            "OLD_CumulatedPresentValueOfInterestAmount" as "CumulatedPresentValueOfInterestAmount" ,
            "OLD_Currency" as "Currency" ,
            "OLD_DiscountInterestRate" as "DiscountInterestRate" ,
            "OLD_InterestAmount" as "InterestAmount" ,
            "OLD_NominalInterestRate" as "NominalInterestRate" ,
            "OLD_PresentValueOfAmortizationAmount" as "PresentValueOfAmortizationAmount" ,
            "OLD_PresentValueOfInterestAmount" as "PresentValueOfInterestAmount" ,
            "OLD_PrincipalAmount" as "PrincipalAmount" ,
            "OLD_TimeBucketEndDate" as "TimeBucketEndDate" ,
            "OLD_TimeBucketStartDate" as "TimeBucketStartDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."AssetOrLiability",
                        "OLD"."KeyDate",
                        "OLD"."Scenario",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."_TimeBucket.MaturityBandID",
                        "OLD"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."AssetOrLiability" AS "OLD_AssetOrLiability" ,
                "OLD"."KeyDate" AS "OLD_KeyDate" ,
                "OLD"."Scenario" AS "OLD_Scenario" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."_TimeBucket.MaturityBandID" AS "OLD__TimeBucket.MaturityBandID" ,
                "OLD"."_TimeBucket.TimeBucketID" AS "OLD__TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AmortizationAmount" AS "OLD_AmortizationAmount" ,
                "OLD"."CumulatedInterestAmount" AS "OLD_CumulatedInterestAmount" ,
                "OLD"."CumulatedPresentValueOfAmortizationAmount" AS "OLD_CumulatedPresentValueOfAmortizationAmount" ,
                "OLD"."CumulatedPresentValueOfInterestAmount" AS "OLD_CumulatedPresentValueOfInterestAmount" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."DiscountInterestRate" AS "OLD_DiscountInterestRate" ,
                "OLD"."InterestAmount" AS "OLD_InterestAmount" ,
                "OLD"."NominalInterestRate" AS "OLD_NominalInterestRate" ,
                "OLD"."PresentValueOfAmortizationAmount" AS "OLD_PresentValueOfAmortizationAmount" ,
                "OLD"."PresentValueOfInterestAmount" AS "OLD_PresentValueOfInterestAmount" ,
                "OLD"."PrincipalAmount" AS "OLD_PrincipalAmount" ,
                "OLD"."TimeBucketEndDate" AS "OLD_TimeBucketEndDate" ,
                "OLD"."TimeBucketStartDate" AS "OLD_TimeBucketStartDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InterestAndBalanceSchedule" as "OLD"
            on
                                      "IN"."AssetOrLiability" = "OLD"."AssetOrLiability" and
                                      "IN"."KeyDate" = "OLD"."KeyDate" and
                                      "IN"."Scenario" = "OLD"."Scenario" and
                                      "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                                      "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                                      "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                                      "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                                      "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
                                      "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_AssetOrLiability" as "AssetOrLiability",
            "OLD_KeyDate" as "KeyDate",
            "OLD_Scenario" as "Scenario",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID",
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AmortizationAmount" as "AmortizationAmount",
            "OLD_CumulatedInterestAmount" as "CumulatedInterestAmount",
            "OLD_CumulatedPresentValueOfAmortizationAmount" as "CumulatedPresentValueOfAmortizationAmount",
            "OLD_CumulatedPresentValueOfInterestAmount" as "CumulatedPresentValueOfInterestAmount",
            "OLD_Currency" as "Currency",
            "OLD_DiscountInterestRate" as "DiscountInterestRate",
            "OLD_InterestAmount" as "InterestAmount",
            "OLD_NominalInterestRate" as "NominalInterestRate",
            "OLD_PresentValueOfAmortizationAmount" as "PresentValueOfAmortizationAmount",
            "OLD_PresentValueOfInterestAmount" as "PresentValueOfInterestAmount",
            "OLD_PrincipalAmount" as "PrincipalAmount",
            "OLD_TimeBucketEndDate" as "TimeBucketEndDate",
            "OLD_TimeBucketStartDate" as "TimeBucketStartDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."AssetOrLiability",
                        "OLD"."KeyDate",
                        "OLD"."Scenario",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."_TimeBucket.MaturityBandID",
                        "OLD"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."AssetOrLiability" AS "OLD_AssetOrLiability" ,
                "OLD"."KeyDate" AS "OLD_KeyDate" ,
                "OLD"."Scenario" AS "OLD_Scenario" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."_TimeBucket.MaturityBandID" AS "OLD__TimeBucket.MaturityBandID" ,
                "OLD"."_TimeBucket.TimeBucketID" AS "OLD__TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AmortizationAmount" AS "OLD_AmortizationAmount" ,
                "OLD"."CumulatedInterestAmount" AS "OLD_CumulatedInterestAmount" ,
                "OLD"."CumulatedPresentValueOfAmortizationAmount" AS "OLD_CumulatedPresentValueOfAmortizationAmount" ,
                "OLD"."CumulatedPresentValueOfInterestAmount" AS "OLD_CumulatedPresentValueOfInterestAmount" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."DiscountInterestRate" AS "OLD_DiscountInterestRate" ,
                "OLD"."InterestAmount" AS "OLD_InterestAmount" ,
                "OLD"."NominalInterestRate" AS "OLD_NominalInterestRate" ,
                "OLD"."PresentValueOfAmortizationAmount" AS "OLD_PresentValueOfAmortizationAmount" ,
                "OLD"."PresentValueOfInterestAmount" AS "OLD_PresentValueOfInterestAmount" ,
                "OLD"."PrincipalAmount" AS "OLD_PrincipalAmount" ,
                "OLD"."TimeBucketEndDate" AS "OLD_TimeBucketEndDate" ,
                "OLD"."TimeBucketStartDate" AS "OLD_TimeBucketStartDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InterestAndBalanceSchedule" as "OLD"
            on
               "IN"."AssetOrLiability" = "OLD"."AssetOrLiability" and
               "IN"."KeyDate" = "OLD"."KeyDate" and
               "IN"."Scenario" = "OLD"."Scenario" and
               "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
               "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
               "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
               "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
               "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
               "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
