PROCEDURE "sap.fsdm.procedures::PrincipalExchangeScheduleDelete" (IN ROW "sap.fsdm.tabletypes::PrincipalExchangeScheduleTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PrincipalExchangeDate=' || TO_VARCHAR("PrincipalExchangeDate") || ' ' ||
                'RoleOfPayer=' || TO_VARCHAR("RoleOfPayer") || ' ' ||
                '_SwapForSchedule.FinancialContractID=' || TO_VARCHAR("_SwapForSchedule.FinancialContractID") || ' ' ||
                '_SwapForSchedule.IDSystem=' || TO_VARCHAR("_SwapForSchedule.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PrincipalExchangeDate",
                        "IN"."RoleOfPayer",
                        "IN"."_SwapForSchedule.FinancialContractID",
                        "IN"."_SwapForSchedule.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PrincipalExchangeDate",
                        "IN"."RoleOfPayer",
                        "IN"."_SwapForSchedule.FinancialContractID",
                        "IN"."_SwapForSchedule.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PrincipalExchangeDate",
                        "RoleOfPayer",
                        "_SwapForSchedule.FinancialContractID",
                        "_SwapForSchedule.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PrincipalExchangeDate",
                                    "IN"."RoleOfPayer",
                                    "IN"."_SwapForSchedule.FinancialContractID",
                                    "IN"."_SwapForSchedule.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PrincipalExchangeDate",
                                    "IN"."RoleOfPayer",
                                    "IN"."_SwapForSchedule.FinancialContractID",
                                    "IN"."_SwapForSchedule.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "PrincipalExchangeDate" is null and
            "RoleOfPayer" is null and
            "_SwapForSchedule.FinancialContractID" is null and
            "_SwapForSchedule.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::PrincipalExchangeSchedule" (
        "PrincipalExchangeDate",
        "RoleOfPayer",
        "_SwapForSchedule.FinancialContractID",
        "_SwapForSchedule.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "PrincipalExchangeAmount",
        "PrincipalExchangeAmountCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PrincipalExchangeDate" as "PrincipalExchangeDate" ,
            "OLD_RoleOfPayer" as "RoleOfPayer" ,
            "OLD__SwapForSchedule.FinancialContractID" as "_SwapForSchedule.FinancialContractID" ,
            "OLD__SwapForSchedule.IDSystem" as "_SwapForSchedule.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_PrincipalExchangeAmount" as "PrincipalExchangeAmount" ,
            "OLD_PrincipalExchangeAmountCurrency" as "PrincipalExchangeAmountCurrency" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."PrincipalExchangeDate",
                        "OLD"."RoleOfPayer",
                        "OLD"."_SwapForSchedule.FinancialContractID",
                        "OLD"."_SwapForSchedule.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."PrincipalExchangeDate" AS "OLD_PrincipalExchangeDate" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."_SwapForSchedule.FinancialContractID" AS "OLD__SwapForSchedule.FinancialContractID" ,
                "OLD"."_SwapForSchedule.IDSystem" AS "OLD__SwapForSchedule.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."PrincipalExchangeAmount" AS "OLD_PrincipalExchangeAmount" ,
                "OLD"."PrincipalExchangeAmountCurrency" AS "OLD_PrincipalExchangeAmountCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PrincipalExchangeSchedule" as "OLD"
            on
                      "IN"."PrincipalExchangeDate" = "OLD"."PrincipalExchangeDate" and
                      "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                      "IN"."_SwapForSchedule.FinancialContractID" = "OLD"."_SwapForSchedule.FinancialContractID" and
                      "IN"."_SwapForSchedule.IDSystem" = "OLD"."_SwapForSchedule.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::PrincipalExchangeSchedule" (
        "PrincipalExchangeDate",
        "RoleOfPayer",
        "_SwapForSchedule.FinancialContractID",
        "_SwapForSchedule.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "PrincipalExchangeAmount",
        "PrincipalExchangeAmountCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PrincipalExchangeDate" as "PrincipalExchangeDate",
            "OLD_RoleOfPayer" as "RoleOfPayer",
            "OLD__SwapForSchedule.FinancialContractID" as "_SwapForSchedule.FinancialContractID",
            "OLD__SwapForSchedule.IDSystem" as "_SwapForSchedule.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_PrincipalExchangeAmount" as "PrincipalExchangeAmount",
            "OLD_PrincipalExchangeAmountCurrency" as "PrincipalExchangeAmountCurrency",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."PrincipalExchangeDate",
                        "OLD"."RoleOfPayer",
                        "OLD"."_SwapForSchedule.FinancialContractID",
                        "OLD"."_SwapForSchedule.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."PrincipalExchangeDate" AS "OLD_PrincipalExchangeDate" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."_SwapForSchedule.FinancialContractID" AS "OLD__SwapForSchedule.FinancialContractID" ,
                "OLD"."_SwapForSchedule.IDSystem" AS "OLD__SwapForSchedule.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."PrincipalExchangeAmount" AS "OLD_PrincipalExchangeAmount" ,
                "OLD"."PrincipalExchangeAmountCurrency" AS "OLD_PrincipalExchangeAmountCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PrincipalExchangeSchedule" as "OLD"
            on
                                                "IN"."PrincipalExchangeDate" = "OLD"."PrincipalExchangeDate" and
                                                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                                                "IN"."_SwapForSchedule.FinancialContractID" = "OLD"."_SwapForSchedule.FinancialContractID" and
                                                "IN"."_SwapForSchedule.IDSystem" = "OLD"."_SwapForSchedule.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::PrincipalExchangeSchedule"
    where (
        "PrincipalExchangeDate",
        "RoleOfPayer",
        "_SwapForSchedule.FinancialContractID",
        "_SwapForSchedule.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."PrincipalExchangeDate",
            "OLD"."RoleOfPayer",
            "OLD"."_SwapForSchedule.FinancialContractID",
            "OLD"."_SwapForSchedule.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PrincipalExchangeSchedule" as "OLD"
        on
                                       "IN"."PrincipalExchangeDate" = "OLD"."PrincipalExchangeDate" and
                                       "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                                       "IN"."_SwapForSchedule.FinancialContractID" = "OLD"."_SwapForSchedule.FinancialContractID" and
                                       "IN"."_SwapForSchedule.IDSystem" = "OLD"."_SwapForSchedule.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
