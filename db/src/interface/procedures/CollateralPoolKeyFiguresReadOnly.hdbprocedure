PROCEDURE "sap.fsdm.procedures::CollateralPoolKeyFiguresReadOnly" (IN ROW "sap.fsdm.tabletypes::CollateralPoolKeyFiguresTT", OUT CURR_DEL "sap.fsdm.tabletypes::CollateralPoolKeyFiguresTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CollateralPoolKeyFiguresTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Category=' || TO_VARCHAR("Category") || ' ' ||
                'KeyDate=' || TO_VARCHAR("KeyDate") || ' ' ||
                'Provider=' || TO_VARCHAR("Provider") || ' ' ||
                'Scenario=' || TO_VARCHAR("Scenario") || ' ' ||
                '_CollateralPool.CollectionID=' || TO_VARCHAR("_CollateralPool.CollectionID") || ' ' ||
                '_CollateralPool.IDSystem=' || TO_VARCHAR("_CollateralPool.IDSystem") || ' ' ||
                '_TimeBucket.MaturityBandID=' || TO_VARCHAR("_TimeBucket.MaturityBandID") || ' ' ||
                '_TimeBucket.TimeBucketID=' || TO_VARCHAR("_TimeBucket.TimeBucketID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Category",
                        "IN"."KeyDate",
                        "IN"."Provider",
                        "IN"."Scenario",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Category",
                        "IN"."KeyDate",
                        "IN"."Provider",
                        "IN"."Scenario",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Category",
                        "KeyDate",
                        "Provider",
                        "Scenario",
                        "_CollateralPool.CollectionID",
                        "_CollateralPool.IDSystem",
                        "_TimeBucket.MaturityBandID",
                        "_TimeBucket.TimeBucketID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Category",
                                    "IN"."KeyDate",
                                    "IN"."Provider",
                                    "IN"."Scenario",
                                    "IN"."_CollateralPool.CollectionID",
                                    "IN"."_CollateralPool.IDSystem",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Category",
                                    "IN"."KeyDate",
                                    "IN"."Provider",
                                    "IN"."Scenario",
                                    "IN"."_CollateralPool.CollectionID",
                                    "IN"."_CollateralPool.IDSystem",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "Category",
        "KeyDate",
        "Provider",
        "Scenario",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::CollateralPoolKeyFigures" WHERE
        (            "Category" ,
            "KeyDate" ,
            "Provider" ,
            "Scenario" ,
            "_CollateralPool.CollectionID" ,
            "_CollateralPool.IDSystem" ,
            "_TimeBucket.MaturityBandID" ,
            "_TimeBucket.TimeBucketID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."Category",
            "OLD"."KeyDate",
            "OLD"."Provider",
            "OLD"."Scenario",
            "OLD"."_CollateralPool.CollectionID",
            "OLD"."_CollateralPool.IDSystem",
            "OLD"."_TimeBucket.MaturityBandID",
            "OLD"."_TimeBucket.TimeBucketID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::CollateralPoolKeyFigures" as "OLD"
            on
               ifnull( "IN"."Category",'' ) = "OLD"."Category" and
               ifnull( "IN"."KeyDate",'0001-01-01' ) = "OLD"."KeyDate" and
               ifnull( "IN"."Provider",'' ) = "OLD"."Provider" and
               ifnull( "IN"."Scenario",'' ) = "OLD"."Scenario" and
               ifnull( "IN"."_CollateralPool.CollectionID",'' ) = "OLD"."_CollateralPool.CollectionID" and
               ifnull( "IN"."_CollateralPool.IDSystem",'' ) = "OLD"."_CollateralPool.IDSystem" and
               ifnull( "IN"."_TimeBucket.MaturityBandID",'' ) = "OLD"."_TimeBucket.MaturityBandID" and
               ifnull( "IN"."_TimeBucket.TimeBucketID",'' ) = "OLD"."_TimeBucket.TimeBucketID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "Category",
        "KeyDate",
        "Provider",
        "Scenario",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CalculationMethod",
        "DepositAtBankInPercent",
        "Description",
        "ExcessCollateralInPercent",
        "LiquidCollateralInPercent",
        "Name",
        "ProlongedAmortizationAmount",
        "ProlongedAmortizationAmountCurrency",
        "RegularAmortizationAmount",
        "RegularAmortizationAmountCurrency",
        "TimeBucketEndDate",
        "TimeBucketStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "Category", '' ) as "Category",
                    ifnull( "KeyDate", '0001-01-01' ) as "KeyDate",
                    ifnull( "Provider", '' ) as "Provider",
                    ifnull( "Scenario", '' ) as "Scenario",
                    ifnull( "_CollateralPool.CollectionID", '' ) as "_CollateralPool.CollectionID",
                    ifnull( "_CollateralPool.IDSystem", '' ) as "_CollateralPool.IDSystem",
                    ifnull( "_TimeBucket.MaturityBandID", '' ) as "_TimeBucket.MaturityBandID",
                    ifnull( "_TimeBucket.TimeBucketID", '' ) as "_TimeBucket.TimeBucketID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "CalculationMethod"  ,
                    "DepositAtBankInPercent"  ,
                    "Description"  ,
                    "ExcessCollateralInPercent"  ,
                    "LiquidCollateralInPercent"  ,
                    "Name"  ,
                    "ProlongedAmortizationAmount"  ,
                    "ProlongedAmortizationAmountCurrency"  ,
                    "RegularAmortizationAmount"  ,
                    "RegularAmortizationAmountCurrency"  ,
                    "TimeBucketEndDate"  ,
                    "TimeBucketStartDate"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_Category" as "Category" ,
                    "OLD_KeyDate" as "KeyDate" ,
                    "OLD_Provider" as "Provider" ,
                    "OLD_Scenario" as "Scenario" ,
                    "OLD__CollateralPool.CollectionID" as "_CollateralPool.CollectionID" ,
                    "OLD__CollateralPool.IDSystem" as "_CollateralPool.IDSystem" ,
                    "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID" ,
                    "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_CalculationMethod" as "CalculationMethod" ,
                    "OLD_DepositAtBankInPercent" as "DepositAtBankInPercent" ,
                    "OLD_Description" as "Description" ,
                    "OLD_ExcessCollateralInPercent" as "ExcessCollateralInPercent" ,
                    "OLD_LiquidCollateralInPercent" as "LiquidCollateralInPercent" ,
                    "OLD_Name" as "Name" ,
                    "OLD_ProlongedAmortizationAmount" as "ProlongedAmortizationAmount" ,
                    "OLD_ProlongedAmortizationAmountCurrency" as "ProlongedAmortizationAmountCurrency" ,
                    "OLD_RegularAmortizationAmount" as "RegularAmortizationAmount" ,
                    "OLD_RegularAmortizationAmountCurrency" as "RegularAmortizationAmountCurrency" ,
                    "OLD_TimeBucketEndDate" as "TimeBucketEndDate" ,
                    "OLD_TimeBucketStartDate" as "TimeBucketStartDate" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."Category",
                        "IN"."KeyDate",
                        "IN"."Provider",
                        "IN"."Scenario",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."Category" as "OLD_Category",
                                "OLD"."KeyDate" as "OLD_KeyDate",
                                "OLD"."Provider" as "OLD_Provider",
                                "OLD"."Scenario" as "OLD_Scenario",
                                "OLD"."_CollateralPool.CollectionID" as "OLD__CollateralPool.CollectionID",
                                "OLD"."_CollateralPool.IDSystem" as "OLD__CollateralPool.IDSystem",
                                "OLD"."_TimeBucket.MaturityBandID" as "OLD__TimeBucket.MaturityBandID",
                                "OLD"."_TimeBucket.TimeBucketID" as "OLD__TimeBucket.TimeBucketID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."CalculationMethod" as "OLD_CalculationMethod",
                                "OLD"."DepositAtBankInPercent" as "OLD_DepositAtBankInPercent",
                                "OLD"."Description" as "OLD_Description",
                                "OLD"."ExcessCollateralInPercent" as "OLD_ExcessCollateralInPercent",
                                "OLD"."LiquidCollateralInPercent" as "OLD_LiquidCollateralInPercent",
                                "OLD"."Name" as "OLD_Name",
                                "OLD"."ProlongedAmortizationAmount" as "OLD_ProlongedAmortizationAmount",
                                "OLD"."ProlongedAmortizationAmountCurrency" as "OLD_ProlongedAmortizationAmountCurrency",
                                "OLD"."RegularAmortizationAmount" as "OLD_RegularAmortizationAmount",
                                "OLD"."RegularAmortizationAmountCurrency" as "OLD_RegularAmortizationAmountCurrency",
                                "OLD"."TimeBucketEndDate" as "OLD_TimeBucketEndDate",
                                "OLD"."TimeBucketStartDate" as "OLD_TimeBucketStartDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CollateralPoolKeyFigures" as "OLD"
            on
                ifnull( "IN"."Category", '') = "OLD"."Category" and
                ifnull( "IN"."KeyDate", '0001-01-01') = "OLD"."KeyDate" and
                ifnull( "IN"."Provider", '') = "OLD"."Provider" and
                ifnull( "IN"."Scenario", '') = "OLD"."Scenario" and
                ifnull( "IN"."_CollateralPool.CollectionID", '') = "OLD"."_CollateralPool.CollectionID" and
                ifnull( "IN"."_CollateralPool.IDSystem", '') = "OLD"."_CollateralPool.IDSystem" and
                ifnull( "IN"."_TimeBucket.MaturityBandID", '') = "OLD"."_TimeBucket.MaturityBandID" and
                ifnull( "IN"."_TimeBucket.TimeBucketID", '') = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_Category" as "Category",
            "OLD_KeyDate" as "KeyDate",
            "OLD_Provider" as "Provider",
            "OLD_Scenario" as "Scenario",
            "OLD__CollateralPool.CollectionID" as "_CollateralPool.CollectionID",
            "OLD__CollateralPool.IDSystem" as "_CollateralPool.IDSystem",
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID",
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CalculationMethod" as "CalculationMethod",
            "OLD_DepositAtBankInPercent" as "DepositAtBankInPercent",
            "OLD_Description" as "Description",
            "OLD_ExcessCollateralInPercent" as "ExcessCollateralInPercent",
            "OLD_LiquidCollateralInPercent" as "LiquidCollateralInPercent",
            "OLD_Name" as "Name",
            "OLD_ProlongedAmortizationAmount" as "ProlongedAmortizationAmount",
            "OLD_ProlongedAmortizationAmountCurrency" as "ProlongedAmortizationAmountCurrency",
            "OLD_RegularAmortizationAmount" as "RegularAmortizationAmount",
            "OLD_RegularAmortizationAmountCurrency" as "RegularAmortizationAmountCurrency",
            "OLD_TimeBucketEndDate" as "TimeBucketEndDate",
            "OLD_TimeBucketStartDate" as "TimeBucketStartDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."Category",
                        "IN"."KeyDate",
                        "IN"."Provider",
                        "IN"."Scenario",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."Category" as "OLD_Category",
                                "OLD"."KeyDate" as "OLD_KeyDate",
                                "OLD"."Provider" as "OLD_Provider",
                                "OLD"."Scenario" as "OLD_Scenario",
                                "OLD"."_CollateralPool.CollectionID" as "OLD__CollateralPool.CollectionID",
                                "OLD"."_CollateralPool.IDSystem" as "OLD__CollateralPool.IDSystem",
                                "OLD"."_TimeBucket.MaturityBandID" as "OLD__TimeBucket.MaturityBandID",
                                "OLD"."_TimeBucket.TimeBucketID" as "OLD__TimeBucket.TimeBucketID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."CalculationMethod" as "OLD_CalculationMethod",
                                "OLD"."DepositAtBankInPercent" as "OLD_DepositAtBankInPercent",
                                "OLD"."Description" as "OLD_Description",
                                "OLD"."ExcessCollateralInPercent" as "OLD_ExcessCollateralInPercent",
                                "OLD"."LiquidCollateralInPercent" as "OLD_LiquidCollateralInPercent",
                                "OLD"."Name" as "OLD_Name",
                                "OLD"."ProlongedAmortizationAmount" as "OLD_ProlongedAmortizationAmount",
                                "OLD"."ProlongedAmortizationAmountCurrency" as "OLD_ProlongedAmortizationAmountCurrency",
                                "OLD"."RegularAmortizationAmount" as "OLD_RegularAmortizationAmount",
                                "OLD"."RegularAmortizationAmountCurrency" as "OLD_RegularAmortizationAmountCurrency",
                                "OLD"."TimeBucketEndDate" as "OLD_TimeBucketEndDate",
                                "OLD"."TimeBucketStartDate" as "OLD_TimeBucketStartDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CollateralPoolKeyFigures" as "OLD"
            on
                ifnull("IN"."Category", '') = "OLD"."Category" and
                ifnull("IN"."KeyDate", '0001-01-01') = "OLD"."KeyDate" and
                ifnull("IN"."Provider", '') = "OLD"."Provider" and
                ifnull("IN"."Scenario", '') = "OLD"."Scenario" and
                ifnull("IN"."_CollateralPool.CollectionID", '') = "OLD"."_CollateralPool.CollectionID" and
                ifnull("IN"."_CollateralPool.IDSystem", '') = "OLD"."_CollateralPool.IDSystem" and
                ifnull("IN"."_TimeBucket.MaturityBandID", '') = "OLD"."_TimeBucket.MaturityBandID" and
                ifnull("IN"."_TimeBucket.TimeBucketID", '') = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
