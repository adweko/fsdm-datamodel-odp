PROCEDURE "sap.fsdm.procedures::BookDesignationDelete" (IN ROW "sap.fsdm.tabletypes::BookDesignationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Regime=' || TO_VARCHAR("Regime") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                '_Collection._Client.BusinessPartnerID=' || TO_VARCHAR("_Collection._Client.BusinessPartnerID") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_InvestmentAccount.FinancialContractID=' || TO_VARCHAR("_InvestmentAccount.FinancialContractID") || ' ' ||
                '_InvestmentAccount.IDSystem=' || TO_VARCHAR("_InvestmentAccount.IDSystem") || ' ' ||
                '_TradingLimit.LimitID=' || TO_VARCHAR("_TradingLimit.LimitID") || ' ' ||
                '_TradingLimit._TimeBucket.MaturityBandID=' || TO_VARCHAR("_TradingLimit._TimeBucket.MaturityBandID") || ' ' ||
                '_TradingLimit._TimeBucket.TimeBucketID=' || TO_VARCHAR("_TradingLimit._TimeBucket.TimeBucketID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Regime",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InvestmentAccount.FinancialContractID",
                        "IN"."_InvestmentAccount.IDSystem",
                        "IN"."_TradingLimit.LimitID",
                        "IN"."_TradingLimit._TimeBucket.MaturityBandID",
                        "IN"."_TradingLimit._TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Regime",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InvestmentAccount.FinancialContractID",
                        "IN"."_InvestmentAccount.IDSystem",
                        "IN"."_TradingLimit.LimitID",
                        "IN"."_TradingLimit._TimeBucket.MaturityBandID",
                        "IN"."_TradingLimit._TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Regime",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem",
                        "_Collection._Client.BusinessPartnerID",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_InvestmentAccount.FinancialContractID",
                        "_InvestmentAccount.IDSystem",
                        "_TradingLimit.LimitID",
                        "_TradingLimit._TimeBucket.MaturityBandID",
                        "_TradingLimit._TimeBucket.TimeBucketID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Regime",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_InvestmentAccount.FinancialContractID",
                                    "IN"."_InvestmentAccount.IDSystem",
                                    "IN"."_TradingLimit.LimitID",
                                    "IN"."_TradingLimit._TimeBucket.MaturityBandID",
                                    "IN"."_TradingLimit._TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Regime",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_InvestmentAccount.FinancialContractID",
                                    "IN"."_InvestmentAccount.IDSystem",
                                    "IN"."_TradingLimit.LimitID",
                                    "IN"."_TradingLimit._TimeBucket.MaturityBandID",
                                    "IN"."_TradingLimit._TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "Regime" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_InvestmentAccount.FinancialContractID" is null and
            "_InvestmentAccount.IDSystem" is null and
            "_TradingLimit.LimitID" is null and
            "_TradingLimit._TimeBucket.MaturityBandID" is null and
            "_TradingLimit._TimeBucket.TimeBucketID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::BookDesignation" (
        "Regime",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InvestmentAccount.FinancialContractID",
        "_InvestmentAccount.IDSystem",
        "_TradingLimit.LimitID",
        "_TradingLimit._TimeBucket.MaturityBandID",
        "_TradingLimit._TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Book",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Regime" as "Regime" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__InvestmentAccount.FinancialContractID" as "_InvestmentAccount.FinancialContractID" ,
            "OLD__InvestmentAccount.IDSystem" as "_InvestmentAccount.IDSystem" ,
            "OLD__TradingLimit.LimitID" as "_TradingLimit.LimitID" ,
            "OLD__TradingLimit._TimeBucket.MaturityBandID" as "_TradingLimit._TimeBucket.MaturityBandID" ,
            "OLD__TradingLimit._TimeBucket.TimeBucketID" as "_TradingLimit._TimeBucket.TimeBucketID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Book" as "Book" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."Regime",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_Collection._Client.BusinessPartnerID",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_InvestmentAccount.FinancialContractID",
                        "OLD"."_InvestmentAccount.IDSystem",
                        "OLD"."_TradingLimit.LimitID",
                        "OLD"."_TradingLimit._TimeBucket.MaturityBandID",
                        "OLD"."_TradingLimit._TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."Regime" AS "OLD_Regime" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" AS "OLD__Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" AS "OLD__InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" AS "OLD__InvestmentAccount.IDSystem" ,
                "OLD"."_TradingLimit.LimitID" AS "OLD__TradingLimit.LimitID" ,
                "OLD"."_TradingLimit._TimeBucket.MaturityBandID" AS "OLD__TradingLimit._TimeBucket.MaturityBandID" ,
                "OLD"."_TradingLimit._TimeBucket.TimeBucketID" AS "OLD__TradingLimit._TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Book" AS "OLD_Book" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BookDesignation" as "OLD"
            on
                      "IN"."Regime" = "OLD"."Regime" and
                      "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                      "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                      "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                      "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                      "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                      "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                      "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" and
                      "IN"."_TradingLimit.LimitID" = "OLD"."_TradingLimit.LimitID" and
                      "IN"."_TradingLimit._TimeBucket.MaturityBandID" = "OLD"."_TradingLimit._TimeBucket.MaturityBandID" and
                      "IN"."_TradingLimit._TimeBucket.TimeBucketID" = "OLD"."_TradingLimit._TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::BookDesignation" (
        "Regime",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InvestmentAccount.FinancialContractID",
        "_InvestmentAccount.IDSystem",
        "_TradingLimit.LimitID",
        "_TradingLimit._TimeBucket.MaturityBandID",
        "_TradingLimit._TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Book",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Regime" as "Regime",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__InvestmentAccount.FinancialContractID" as "_InvestmentAccount.FinancialContractID",
            "OLD__InvestmentAccount.IDSystem" as "_InvestmentAccount.IDSystem",
            "OLD__TradingLimit.LimitID" as "_TradingLimit.LimitID",
            "OLD__TradingLimit._TimeBucket.MaturityBandID" as "_TradingLimit._TimeBucket.MaturityBandID",
            "OLD__TradingLimit._TimeBucket.TimeBucketID" as "_TradingLimit._TimeBucket.TimeBucketID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Book" as "Book",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."Regime",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_Collection._Client.BusinessPartnerID",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_InvestmentAccount.FinancialContractID",
                        "OLD"."_InvestmentAccount.IDSystem",
                        "OLD"."_TradingLimit.LimitID",
                        "OLD"."_TradingLimit._TimeBucket.MaturityBandID",
                        "OLD"."_TradingLimit._TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."Regime" AS "OLD_Regime" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" AS "OLD__Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" AS "OLD__InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" AS "OLD__InvestmentAccount.IDSystem" ,
                "OLD"."_TradingLimit.LimitID" AS "OLD__TradingLimit.LimitID" ,
                "OLD"."_TradingLimit._TimeBucket.MaturityBandID" AS "OLD__TradingLimit._TimeBucket.MaturityBandID" ,
                "OLD"."_TradingLimit._TimeBucket.TimeBucketID" AS "OLD__TradingLimit._TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Book" AS "OLD_Book" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BookDesignation" as "OLD"
            on
                                                "IN"."Regime" = "OLD"."Regime" and
                                                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                                                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                                                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                                                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                                                "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                                                "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" and
                                                "IN"."_TradingLimit.LimitID" = "OLD"."_TradingLimit.LimitID" and
                                                "IN"."_TradingLimit._TimeBucket.MaturityBandID" = "OLD"."_TradingLimit._TimeBucket.MaturityBandID" and
                                                "IN"."_TradingLimit._TimeBucket.TimeBucketID" = "OLD"."_TradingLimit._TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::BookDesignation"
    where (
        "Regime",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InvestmentAccount.FinancialContractID",
        "_InvestmentAccount.IDSystem",
        "_TradingLimit.LimitID",
        "_TradingLimit._TimeBucket.MaturityBandID",
        "_TradingLimit._TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."Regime",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."_Collection._Client.BusinessPartnerID",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_InvestmentAccount.FinancialContractID",
            "OLD"."_InvestmentAccount.IDSystem",
            "OLD"."_TradingLimit.LimitID",
            "OLD"."_TradingLimit._TimeBucket.MaturityBandID",
            "OLD"."_TradingLimit._TimeBucket.TimeBucketID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::BookDesignation" as "OLD"
        on
                                       "IN"."Regime" = "OLD"."Regime" and
                                       "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                       "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                       "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                                       "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                                       "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                                       "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                                       "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                                       "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" and
                                       "IN"."_TradingLimit.LimitID" = "OLD"."_TradingLimit.LimitID" and
                                       "IN"."_TradingLimit._TimeBucket.MaturityBandID" = "OLD"."_TradingLimit._TimeBucket.MaturityBandID" and
                                       "IN"."_TradingLimit._TimeBucket.TimeBucketID" = "OLD"."_TradingLimit._TimeBucket.TimeBucketID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
