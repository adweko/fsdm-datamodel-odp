PROCEDURE "sap.fsdm.procedures::AmortizedValuationCostErase" (IN ROW "sap.fsdm.tabletypes::AmortizedValuationCostTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "AVCCalculationMethod" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_InvestmentAccount.FinancialContractID" is null and
            "_InvestmentAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::AmortizedValuationCost"
        WHERE
        (            "AVCCalculationMethod" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InvestmentAccount.FinancialContractID" ,
            "_InvestmentAccount.IDSystem" 
        ) in
        (
            select                 "OLD"."AVCCalculationMethod" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::AmortizedValuationCost" "OLD"
            on
            "IN"."AVCCalculationMethod" = "OLD"."AVCCalculationMethod" and
            "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
            "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
            "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::AmortizedValuationCost_Historical"
        WHERE
        (
            "AVCCalculationMethod" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InvestmentAccount.FinancialContractID" ,
            "_InvestmentAccount.IDSystem" 
        ) in
        (
            select
                "OLD"."AVCCalculationMethod" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::AmortizedValuationCost_Historical" "OLD"
            on
                "IN"."AVCCalculationMethod" = "OLD"."AVCCalculationMethod" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" 
        );

END
