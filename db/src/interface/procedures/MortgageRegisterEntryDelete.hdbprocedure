PROCEDURE "sap.fsdm.procedures::MortgageRegisterEntryDelete" (IN ROW "sap.fsdm.tabletypes::MortgageRegisterEntryTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Beneficiary=' || TO_VARCHAR("Beneficiary") || ' ' ||
                'MortgageRegister=' || TO_VARCHAR("MortgageRegister") || ' ' ||
                'Rank=' || TO_VARCHAR("Rank") || ' ' ||
                'Section=' || TO_VARCHAR("Section") || ' ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("ASSOC_PhysicalAsset.PhysicalAssetID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Beneficiary",
                        "IN"."MortgageRegister",
                        "IN"."Rank",
                        "IN"."Section",
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Beneficiary",
                        "IN"."MortgageRegister",
                        "IN"."Rank",
                        "IN"."Section",
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Beneficiary",
                        "MortgageRegister",
                        "Rank",
                        "Section",
                        "SequenceNumber",
                        "ASSOC_PhysicalAsset.PhysicalAssetID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Beneficiary",
                                    "IN"."MortgageRegister",
                                    "IN"."Rank",
                                    "IN"."Section",
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Beneficiary",
                                    "IN"."MortgageRegister",
                                    "IN"."Rank",
                                    "IN"."Section",
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "Beneficiary" is null and
            "MortgageRegister" is null and
            "Rank" is null and
            "Section" is null and
            "SequenceNumber" is null and
            "ASSOC_PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::MortgageRegisterEntry" (
        "Beneficiary",
        "MortgageRegister",
        "Rank",
        "Section",
        "SequenceNumber",
        "ASSOC_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_LandChargeArrangement.FinancialContractID",
        "ASSOC_LandChargeArrangement.IDSystem",
        "_MovablesMortgageArrangement.FinancialContractID",
        "_MovablesMortgageArrangement.IDSystem",
        "ActualUsableProtectionAmount",
        "ActualUsableProtectionAmountCurrency",
        "AdministrativeArea",
        "AdministrativeSection",
        "Area",
        "AreaUnit",
        "MortgageRegisterType",
        "ThirdPartyRights",
        "TitleID",
        "UnitID",
        "UsableProtectionAmount",
        "UsableProtectionAmountCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Beneficiary" as "Beneficiary" ,
            "OLD_MortgageRegister" as "MortgageRegister" ,
            "OLD_Rank" as "Rank" ,
            "OLD_Section" as "Section" ,
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" as "ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ASSOC_LandChargeArrangement.FinancialContractID" as "ASSOC_LandChargeArrangement.FinancialContractID" ,
            "OLD_ASSOC_LandChargeArrangement.IDSystem" as "ASSOC_LandChargeArrangement.IDSystem" ,
            "OLD__MovablesMortgageArrangement.FinancialContractID" as "_MovablesMortgageArrangement.FinancialContractID" ,
            "OLD__MovablesMortgageArrangement.IDSystem" as "_MovablesMortgageArrangement.IDSystem" ,
            "OLD_ActualUsableProtectionAmount" as "ActualUsableProtectionAmount" ,
            "OLD_ActualUsableProtectionAmountCurrency" as "ActualUsableProtectionAmountCurrency" ,
            "OLD_AdministrativeArea" as "AdministrativeArea" ,
            "OLD_AdministrativeSection" as "AdministrativeSection" ,
            "OLD_Area" as "Area" ,
            "OLD_AreaUnit" as "AreaUnit" ,
            "OLD_MortgageRegisterType" as "MortgageRegisterType" ,
            "OLD_ThirdPartyRights" as "ThirdPartyRights" ,
            "OLD_TitleID" as "TitleID" ,
            "OLD_UnitID" as "UnitID" ,
            "OLD_UsableProtectionAmount" as "UsableProtectionAmount" ,
            "OLD_UsableProtectionAmountCurrency" as "UsableProtectionAmountCurrency" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."Beneficiary",
                        "OLD"."MortgageRegister",
                        "OLD"."Rank",
                        "OLD"."Section",
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."Beneficiary" AS "OLD_Beneficiary" ,
                "OLD"."MortgageRegister" AS "OLD_MortgageRegister" ,
                "OLD"."Rank" AS "OLD_Rank" ,
                "OLD"."Section" AS "OLD_Section" ,
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_LandChargeArrangement.FinancialContractID" AS "OLD_ASSOC_LandChargeArrangement.FinancialContractID" ,
                "OLD"."ASSOC_LandChargeArrangement.IDSystem" AS "OLD_ASSOC_LandChargeArrangement.IDSystem" ,
                "OLD"."_MovablesMortgageArrangement.FinancialContractID" AS "OLD__MovablesMortgageArrangement.FinancialContractID" ,
                "OLD"."_MovablesMortgageArrangement.IDSystem" AS "OLD__MovablesMortgageArrangement.IDSystem" ,
                "OLD"."ActualUsableProtectionAmount" AS "OLD_ActualUsableProtectionAmount" ,
                "OLD"."ActualUsableProtectionAmountCurrency" AS "OLD_ActualUsableProtectionAmountCurrency" ,
                "OLD"."AdministrativeArea" AS "OLD_AdministrativeArea" ,
                "OLD"."AdministrativeSection" AS "OLD_AdministrativeSection" ,
                "OLD"."Area" AS "OLD_Area" ,
                "OLD"."AreaUnit" AS "OLD_AreaUnit" ,
                "OLD"."MortgageRegisterType" AS "OLD_MortgageRegisterType" ,
                "OLD"."ThirdPartyRights" AS "OLD_ThirdPartyRights" ,
                "OLD"."TitleID" AS "OLD_TitleID" ,
                "OLD"."UnitID" AS "OLD_UnitID" ,
                "OLD"."UsableProtectionAmount" AS "OLD_UsableProtectionAmount" ,
                "OLD"."UsableProtectionAmountCurrency" AS "OLD_UsableProtectionAmountCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::MortgageRegisterEntry" as "OLD"
            on
                      "IN"."Beneficiary" = "OLD"."Beneficiary" and
                      "IN"."MortgageRegister" = "OLD"."MortgageRegister" and
                      "IN"."Rank" = "OLD"."Rank" and
                      "IN"."Section" = "OLD"."Section" and
                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                      "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::MortgageRegisterEntry" (
        "Beneficiary",
        "MortgageRegister",
        "Rank",
        "Section",
        "SequenceNumber",
        "ASSOC_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_LandChargeArrangement.FinancialContractID",
        "ASSOC_LandChargeArrangement.IDSystem",
        "_MovablesMortgageArrangement.FinancialContractID",
        "_MovablesMortgageArrangement.IDSystem",
        "ActualUsableProtectionAmount",
        "ActualUsableProtectionAmountCurrency",
        "AdministrativeArea",
        "AdministrativeSection",
        "Area",
        "AreaUnit",
        "MortgageRegisterType",
        "ThirdPartyRights",
        "TitleID",
        "UnitID",
        "UsableProtectionAmount",
        "UsableProtectionAmountCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Beneficiary" as "Beneficiary",
            "OLD_MortgageRegister" as "MortgageRegister",
            "OLD_Rank" as "Rank",
            "OLD_Section" as "Section",
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" as "ASSOC_PhysicalAsset.PhysicalAssetID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ASSOC_LandChargeArrangement.FinancialContractID" as "ASSOC_LandChargeArrangement.FinancialContractID",
            "OLD_ASSOC_LandChargeArrangement.IDSystem" as "ASSOC_LandChargeArrangement.IDSystem",
            "OLD__MovablesMortgageArrangement.FinancialContractID" as "_MovablesMortgageArrangement.FinancialContractID",
            "OLD__MovablesMortgageArrangement.IDSystem" as "_MovablesMortgageArrangement.IDSystem",
            "OLD_ActualUsableProtectionAmount" as "ActualUsableProtectionAmount",
            "OLD_ActualUsableProtectionAmountCurrency" as "ActualUsableProtectionAmountCurrency",
            "OLD_AdministrativeArea" as "AdministrativeArea",
            "OLD_AdministrativeSection" as "AdministrativeSection",
            "OLD_Area" as "Area",
            "OLD_AreaUnit" as "AreaUnit",
            "OLD_MortgageRegisterType" as "MortgageRegisterType",
            "OLD_ThirdPartyRights" as "ThirdPartyRights",
            "OLD_TitleID" as "TitleID",
            "OLD_UnitID" as "UnitID",
            "OLD_UsableProtectionAmount" as "UsableProtectionAmount",
            "OLD_UsableProtectionAmountCurrency" as "UsableProtectionAmountCurrency",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."Beneficiary",
                        "OLD"."MortgageRegister",
                        "OLD"."Rank",
                        "OLD"."Section",
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."Beneficiary" AS "OLD_Beneficiary" ,
                "OLD"."MortgageRegister" AS "OLD_MortgageRegister" ,
                "OLD"."Rank" AS "OLD_Rank" ,
                "OLD"."Section" AS "OLD_Section" ,
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_LandChargeArrangement.FinancialContractID" AS "OLD_ASSOC_LandChargeArrangement.FinancialContractID" ,
                "OLD"."ASSOC_LandChargeArrangement.IDSystem" AS "OLD_ASSOC_LandChargeArrangement.IDSystem" ,
                "OLD"."_MovablesMortgageArrangement.FinancialContractID" AS "OLD__MovablesMortgageArrangement.FinancialContractID" ,
                "OLD"."_MovablesMortgageArrangement.IDSystem" AS "OLD__MovablesMortgageArrangement.IDSystem" ,
                "OLD"."ActualUsableProtectionAmount" AS "OLD_ActualUsableProtectionAmount" ,
                "OLD"."ActualUsableProtectionAmountCurrency" AS "OLD_ActualUsableProtectionAmountCurrency" ,
                "OLD"."AdministrativeArea" AS "OLD_AdministrativeArea" ,
                "OLD"."AdministrativeSection" AS "OLD_AdministrativeSection" ,
                "OLD"."Area" AS "OLD_Area" ,
                "OLD"."AreaUnit" AS "OLD_AreaUnit" ,
                "OLD"."MortgageRegisterType" AS "OLD_MortgageRegisterType" ,
                "OLD"."ThirdPartyRights" AS "OLD_ThirdPartyRights" ,
                "OLD"."TitleID" AS "OLD_TitleID" ,
                "OLD"."UnitID" AS "OLD_UnitID" ,
                "OLD"."UsableProtectionAmount" AS "OLD_UsableProtectionAmount" ,
                "OLD"."UsableProtectionAmountCurrency" AS "OLD_UsableProtectionAmountCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::MortgageRegisterEntry" as "OLD"
            on
                                                "IN"."Beneficiary" = "OLD"."Beneficiary" and
                                                "IN"."MortgageRegister" = "OLD"."MortgageRegister" and
                                                "IN"."Rank" = "OLD"."Rank" and
                                                "IN"."Section" = "OLD"."Section" and
                                                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                                "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::MortgageRegisterEntry"
    where (
        "Beneficiary",
        "MortgageRegister",
        "Rank",
        "Section",
        "SequenceNumber",
        "ASSOC_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."Beneficiary",
            "OLD"."MortgageRegister",
            "OLD"."Rank",
            "OLD"."Section",
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::MortgageRegisterEntry" as "OLD"
        on
                                       "IN"."Beneficiary" = "OLD"."Beneficiary" and
                                       "IN"."MortgageRegister" = "OLD"."MortgageRegister" and
                                       "IN"."Rank" = "OLD"."Rank" and
                                       "IN"."Section" = "OLD"."Section" and
                                       "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                       "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
