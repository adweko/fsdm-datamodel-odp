PROCEDURE "sap.fsdm.procedures::ClassificationOfTransferOrderDelete" (IN ROW "sap.fsdm.tabletypes::ClassificationOfTransferOrderTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ASSOC_TransferOrder.IDSystem=' || TO_VARCHAR("ASSOC_TransferOrder.IDSystem") || ' ' ||
                'ASSOC_TransferOrder.TransferOrderID=' || TO_VARCHAR("ASSOC_TransferOrder.TransferOrderID") || ' ' ||
                'ASSOC_TransferOrderClass.TransferOrderClass=' || TO_VARCHAR("ASSOC_TransferOrderClass.TransferOrderClass") || ' ' ||
                'ASSOC_TransferOrderClass.TransferOrderClassificationSystem=' || TO_VARCHAR("ASSOC_TransferOrderClass.TransferOrderClassificationSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ASSOC_TransferOrder.IDSystem",
                        "IN"."ASSOC_TransferOrder.TransferOrderID",
                        "IN"."ASSOC_TransferOrderClass.TransferOrderClass",
                        "IN"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ASSOC_TransferOrder.IDSystem",
                        "IN"."ASSOC_TransferOrder.TransferOrderID",
                        "IN"."ASSOC_TransferOrderClass.TransferOrderClass",
                        "IN"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ASSOC_TransferOrder.IDSystem",
                        "ASSOC_TransferOrder.TransferOrderID",
                        "ASSOC_TransferOrderClass.TransferOrderClass",
                        "ASSOC_TransferOrderClass.TransferOrderClassificationSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ASSOC_TransferOrder.IDSystem",
                                    "IN"."ASSOC_TransferOrder.TransferOrderID",
                                    "IN"."ASSOC_TransferOrderClass.TransferOrderClass",
                                    "IN"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ASSOC_TransferOrder.IDSystem",
                                    "IN"."ASSOC_TransferOrder.TransferOrderID",
                                    "IN"."ASSOC_TransferOrderClass.TransferOrderClass",
                                    "IN"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_TransferOrder.IDSystem" is null and
            "ASSOC_TransferOrder.TransferOrderID" is null and
            "ASSOC_TransferOrderClass.TransferOrderClass" is null and
            "ASSOC_TransferOrderClass.TransferOrderClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::ClassificationOfTransferOrder" (
        "ASSOC_TransferOrder.IDSystem",
        "ASSOC_TransferOrder.TransferOrderID",
        "ASSOC_TransferOrderClass.TransferOrderClass",
        "ASSOC_TransferOrderClass.TransferOrderClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ASSOC_TransferOrder.IDSystem" as "ASSOC_TransferOrder.IDSystem" ,
            "OLD_ASSOC_TransferOrder.TransferOrderID" as "ASSOC_TransferOrder.TransferOrderID" ,
            "OLD_ASSOC_TransferOrderClass.TransferOrderClass" as "ASSOC_TransferOrderClass.TransferOrderClass" ,
            "OLD_ASSOC_TransferOrderClass.TransferOrderClassificationSystem" as "ASSOC_TransferOrderClass.TransferOrderClassificationSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ASSOC_TransferOrder.IDSystem",
                        "OLD"."ASSOC_TransferOrder.TransferOrderID",
                        "OLD"."ASSOC_TransferOrderClass.TransferOrderClass",
                        "OLD"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."ASSOC_TransferOrder.IDSystem" AS "OLD_ASSOC_TransferOrder.IDSystem" ,
                "OLD"."ASSOC_TransferOrder.TransferOrderID" AS "OLD_ASSOC_TransferOrder.TransferOrderID" ,
                "OLD"."ASSOC_TransferOrderClass.TransferOrderClass" AS "OLD_ASSOC_TransferOrderClass.TransferOrderClass" ,
                "OLD"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem" AS "OLD_ASSOC_TransferOrderClass.TransferOrderClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ClassificationOfTransferOrder" as "OLD"
            on
                      "IN"."ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_TransferOrder.IDSystem" and
                      "IN"."ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_TransferOrder.TransferOrderID" and
                      "IN"."ASSOC_TransferOrderClass.TransferOrderClass" = "OLD"."ASSOC_TransferOrderClass.TransferOrderClass" and
                      "IN"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem" = "OLD"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::ClassificationOfTransferOrder" (
        "ASSOC_TransferOrder.IDSystem",
        "ASSOC_TransferOrder.TransferOrderID",
        "ASSOC_TransferOrderClass.TransferOrderClass",
        "ASSOC_TransferOrderClass.TransferOrderClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ASSOC_TransferOrder.IDSystem" as "ASSOC_TransferOrder.IDSystem",
            "OLD_ASSOC_TransferOrder.TransferOrderID" as "ASSOC_TransferOrder.TransferOrderID",
            "OLD_ASSOC_TransferOrderClass.TransferOrderClass" as "ASSOC_TransferOrderClass.TransferOrderClass",
            "OLD_ASSOC_TransferOrderClass.TransferOrderClassificationSystem" as "ASSOC_TransferOrderClass.TransferOrderClassificationSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ASSOC_TransferOrder.IDSystem",
                        "OLD"."ASSOC_TransferOrder.TransferOrderID",
                        "OLD"."ASSOC_TransferOrderClass.TransferOrderClass",
                        "OLD"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ASSOC_TransferOrder.IDSystem" AS "OLD_ASSOC_TransferOrder.IDSystem" ,
                "OLD"."ASSOC_TransferOrder.TransferOrderID" AS "OLD_ASSOC_TransferOrder.TransferOrderID" ,
                "OLD"."ASSOC_TransferOrderClass.TransferOrderClass" AS "OLD_ASSOC_TransferOrderClass.TransferOrderClass" ,
                "OLD"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem" AS "OLD_ASSOC_TransferOrderClass.TransferOrderClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ClassificationOfTransferOrder" as "OLD"
            on
                                                "IN"."ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_TransferOrder.IDSystem" and
                                                "IN"."ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_TransferOrder.TransferOrderID" and
                                                "IN"."ASSOC_TransferOrderClass.TransferOrderClass" = "OLD"."ASSOC_TransferOrderClass.TransferOrderClass" and
                                                "IN"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem" = "OLD"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::ClassificationOfTransferOrder"
    where (
        "ASSOC_TransferOrder.IDSystem",
        "ASSOC_TransferOrder.TransferOrderID",
        "ASSOC_TransferOrderClass.TransferOrderClass",
        "ASSOC_TransferOrderClass.TransferOrderClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ASSOC_TransferOrder.IDSystem",
            "OLD"."ASSOC_TransferOrder.TransferOrderID",
            "OLD"."ASSOC_TransferOrderClass.TransferOrderClass",
            "OLD"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ClassificationOfTransferOrder" as "OLD"
        on
                                       "IN"."ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_TransferOrder.IDSystem" and
                                       "IN"."ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_TransferOrder.TransferOrderID" and
                                       "IN"."ASSOC_TransferOrderClass.TransferOrderClass" = "OLD"."ASSOC_TransferOrderClass.TransferOrderClass" and
                                       "IN"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem" = "OLD"."ASSOC_TransferOrderClass.TransferOrderClassificationSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
