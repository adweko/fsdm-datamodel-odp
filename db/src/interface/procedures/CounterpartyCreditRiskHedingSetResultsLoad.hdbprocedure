PROCEDURE "sap.fsdm.procedures::CounterpartyCreditRiskHedingSetResultsLoad" (IN ROW "sap.fsdm.tabletypes::CounterpartyCreditRiskHedingSetResultsTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'HedgingSetID=' || TO_VARCHAR("HedgingSetID") || ' ' ||
                'MarginedNettingSet=' || TO_VARCHAR("MarginedNettingSet") || ' ' ||
                '_ExchangeTradedNettingSet.FinancialInstrumentID=' || TO_VARCHAR("_ExchangeTradedNettingSet.FinancialInstrumentID") || ' ' ||
                '_FinancialContractNettingSet.FinancialContractID=' || TO_VARCHAR("_FinancialContractNettingSet.FinancialContractID") || ' ' ||
                '_FinancialContractNettingSet.IDSystem=' || TO_VARCHAR("_FinancialContractNettingSet.IDSystem") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID") || ' ' ||
                '_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem=' || TO_VARCHAR("_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."HedgingSetID",
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."HedgingSetID",
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "HedgingSetID",
                        "MarginedNettingSet",
                        "_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "_FinancialContractNettingSet.FinancialContractID",
                        "_FinancialContractNettingSet.IDSystem",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."HedgingSetID",
                                    "IN"."MarginedNettingSet",
                                    "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                                    "IN"."_FinancialContractNettingSet.FinancialContractID",
                                    "IN"."_FinancialContractNettingSet.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."HedgingSetID",
                                    "IN"."MarginedNettingSet",
                                    "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                                    "IN"."_FinancialContractNettingSet.FinancialContractID",
                                    "IN"."_FinancialContractNettingSet.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::CounterpartyCreditRiskHedingSetResults" (
        "HedgingSetID",
        "MarginedNettingSet",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AddOnAmount",
        "AddOnAmountCurrency",
        "AssetClass",
        "EffectiveNotionalAmount",
        "EffectiveNotionalAmountCurrency",
        "HedgingSetCurrency",
        "HedgingSetRiskFactor",
        "HedgingSetSecondCurrency",
        "HedgingSetSecondRiskFactor",
        "NumberOfContractsInHedgingSet",
        "SumOfMarketValues",
        "SumOfMarketValuesCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_HedgingSetID" as "HedgingSetID" ,
            "OLD_MarginedNettingSet" as "MarginedNettingSet" ,
            "OLD__ExchangeTradedNettingSet.FinancialInstrumentID" as "_ExchangeTradedNettingSet.FinancialInstrumentID" ,
            "OLD__FinancialContractNettingSet.FinancialContractID" as "_FinancialContractNettingSet.FinancialContractID" ,
            "OLD__FinancialContractNettingSet.IDSystem" as "_FinancialContractNettingSet.IDSystem" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AddOnAmount" as "AddOnAmount" ,
            "OLD_AddOnAmountCurrency" as "AddOnAmountCurrency" ,
            "OLD_AssetClass" as "AssetClass" ,
            "OLD_EffectiveNotionalAmount" as "EffectiveNotionalAmount" ,
            "OLD_EffectiveNotionalAmountCurrency" as "EffectiveNotionalAmountCurrency" ,
            "OLD_HedgingSetCurrency" as "HedgingSetCurrency" ,
            "OLD_HedgingSetRiskFactor" as "HedgingSetRiskFactor" ,
            "OLD_HedgingSetSecondCurrency" as "HedgingSetSecondCurrency" ,
            "OLD_HedgingSetSecondRiskFactor" as "HedgingSetSecondRiskFactor" ,
            "OLD_NumberOfContractsInHedgingSet" as "NumberOfContractsInHedgingSet" ,
            "OLD_SumOfMarketValues" as "SumOfMarketValues" ,
            "OLD_SumOfMarketValuesCurrency" as "SumOfMarketValuesCurrency" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."HedgingSetID",
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."HedgingSetID" as "OLD_HedgingSetID",
                                "OLD"."MarginedNettingSet" as "OLD_MarginedNettingSet",
                                "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" as "OLD__ExchangeTradedNettingSet.FinancialInstrumentID",
                                "OLD"."_FinancialContractNettingSet.FinancialContractID" as "OLD__FinancialContractNettingSet.FinancialContractID",
                                "OLD"."_FinancialContractNettingSet.IDSystem" as "OLD__FinancialContractNettingSet.IDSystem",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AddOnAmount" as "OLD_AddOnAmount",
                                "OLD"."AddOnAmountCurrency" as "OLD_AddOnAmountCurrency",
                                "OLD"."AssetClass" as "OLD_AssetClass",
                                "OLD"."EffectiveNotionalAmount" as "OLD_EffectiveNotionalAmount",
                                "OLD"."EffectiveNotionalAmountCurrency" as "OLD_EffectiveNotionalAmountCurrency",
                                "OLD"."HedgingSetCurrency" as "OLD_HedgingSetCurrency",
                                "OLD"."HedgingSetRiskFactor" as "OLD_HedgingSetRiskFactor",
                                "OLD"."HedgingSetSecondCurrency" as "OLD_HedgingSetSecondCurrency",
                                "OLD"."HedgingSetSecondRiskFactor" as "OLD_HedgingSetSecondRiskFactor",
                                "OLD"."NumberOfContractsInHedgingSet" as "OLD_NumberOfContractsInHedgingSet",
                                "OLD"."SumOfMarketValues" as "OLD_SumOfMarketValues",
                                "OLD"."SumOfMarketValuesCurrency" as "OLD_SumOfMarketValuesCurrency",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskHedingSetResults" as "OLD"
            on
                ifnull( "IN"."HedgingSetID", '') = "OLD"."HedgingSetID" and
                ifnull( "IN"."MarginedNettingSet", false) = "OLD"."MarginedNettingSet" and
                ifnull( "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID", '') = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
                ifnull( "IN"."_FinancialContractNettingSet.FinancialContractID", '') = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
                ifnull( "IN"."_FinancialContractNettingSet.IDSystem", '') = "OLD"."_FinancialContractNettingSet.IDSystem" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '') = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '') = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CounterpartyCreditRiskHedingSetResults" (
        "HedgingSetID",
        "MarginedNettingSet",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AddOnAmount",
        "AddOnAmountCurrency",
        "AssetClass",
        "EffectiveNotionalAmount",
        "EffectiveNotionalAmountCurrency",
        "HedgingSetCurrency",
        "HedgingSetRiskFactor",
        "HedgingSetSecondCurrency",
        "HedgingSetSecondRiskFactor",
        "NumberOfContractsInHedgingSet",
        "SumOfMarketValues",
        "SumOfMarketValuesCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_HedgingSetID"  as "HedgingSetID",
            "OLD_MarginedNettingSet"  as "MarginedNettingSet",
            "OLD__ExchangeTradedNettingSet.FinancialInstrumentID"  as "_ExchangeTradedNettingSet.FinancialInstrumentID",
            "OLD__FinancialContractNettingSet.FinancialContractID"  as "_FinancialContractNettingSet.FinancialContractID",
            "OLD__FinancialContractNettingSet.IDSystem"  as "_FinancialContractNettingSet.IDSystem",
            "OLD__ResultGroup.ResultDataProvider"  as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID"  as "_ResultGroup.ResultGroupID",
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID"  as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"  as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_AddOnAmount"  as "AddOnAmount",
            "OLD_AddOnAmountCurrency"  as "AddOnAmountCurrency",
            "OLD_AssetClass"  as "AssetClass",
            "OLD_EffectiveNotionalAmount"  as "EffectiveNotionalAmount",
            "OLD_EffectiveNotionalAmountCurrency"  as "EffectiveNotionalAmountCurrency",
            "OLD_HedgingSetCurrency"  as "HedgingSetCurrency",
            "OLD_HedgingSetRiskFactor"  as "HedgingSetRiskFactor",
            "OLD_HedgingSetSecondCurrency"  as "HedgingSetSecondCurrency",
            "OLD_HedgingSetSecondRiskFactor"  as "HedgingSetSecondRiskFactor",
            "OLD_NumberOfContractsInHedgingSet"  as "NumberOfContractsInHedgingSet",
            "OLD_SumOfMarketValues"  as "SumOfMarketValues",
            "OLD_SumOfMarketValuesCurrency"  as "SumOfMarketValuesCurrency",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."HedgingSetID",
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."HedgingSetID" as "OLD_HedgingSetID",
                        "OLD"."MarginedNettingSet" as "OLD_MarginedNettingSet",
                        "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" as "OLD__ExchangeTradedNettingSet.FinancialInstrumentID",
                        "OLD"."_FinancialContractNettingSet.FinancialContractID" as "OLD__FinancialContractNettingSet.FinancialContractID",
                        "OLD"."_FinancialContractNettingSet.IDSystem" as "OLD__FinancialContractNettingSet.IDSystem",
                        "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                        "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."AddOnAmount" as "OLD_AddOnAmount",
                        "OLD"."AddOnAmountCurrency" as "OLD_AddOnAmountCurrency",
                        "OLD"."AssetClass" as "OLD_AssetClass",
                        "OLD"."EffectiveNotionalAmount" as "OLD_EffectiveNotionalAmount",
                        "OLD"."EffectiveNotionalAmountCurrency" as "OLD_EffectiveNotionalAmountCurrency",
                        "OLD"."HedgingSetCurrency" as "OLD_HedgingSetCurrency",
                        "OLD"."HedgingSetRiskFactor" as "OLD_HedgingSetRiskFactor",
                        "OLD"."HedgingSetSecondCurrency" as "OLD_HedgingSetSecondCurrency",
                        "OLD"."HedgingSetSecondRiskFactor" as "OLD_HedgingSetSecondRiskFactor",
                        "OLD"."NumberOfContractsInHedgingSet" as "OLD_NumberOfContractsInHedgingSet",
                        "OLD"."SumOfMarketValues" as "OLD_SumOfMarketValues",
                        "OLD"."SumOfMarketValuesCurrency" as "OLD_SumOfMarketValuesCurrency",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskHedingSetResults" as "OLD"
            on
                ifnull( "IN"."HedgingSetID", '' ) = "OLD"."HedgingSetID" and
                ifnull( "IN"."MarginedNettingSet", false ) = "OLD"."MarginedNettingSet" and
                ifnull( "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID", '' ) = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
                ifnull( "IN"."_FinancialContractNettingSet.FinancialContractID", '' ) = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
                ifnull( "IN"."_FinancialContractNettingSet.IDSystem", '' ) = "OLD"."_FinancialContractNettingSet.IDSystem" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '' ) = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '' ) = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::CounterpartyCreditRiskHedingSetResults"
    where (
        "HedgingSetID",
        "MarginedNettingSet",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."HedgingSetID",
            "OLD"."MarginedNettingSet",
            "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID",
            "OLD"."_FinancialContractNettingSet.FinancialContractID",
            "OLD"."_FinancialContractNettingSet.IDSystem",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CounterpartyCreditRiskHedingSetResults" as "OLD"
        on
           ifnull( "IN"."HedgingSetID", '' ) = "OLD"."HedgingSetID" and
           ifnull( "IN"."MarginedNettingSet", false ) = "OLD"."MarginedNettingSet" and
           ifnull( "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID", '' ) = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
           ifnull( "IN"."_FinancialContractNettingSet.FinancialContractID", '' ) = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
           ifnull( "IN"."_FinancialContractNettingSet.IDSystem", '' ) = "OLD"."_FinancialContractNettingSet.IDSystem" and
           ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
           ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" and
           ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '' ) = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
           ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '' ) = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::CounterpartyCreditRiskHedingSetResults" (
        "HedgingSetID",
        "MarginedNettingSet",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AddOnAmount",
        "AddOnAmountCurrency",
        "AssetClass",
        "EffectiveNotionalAmount",
        "EffectiveNotionalAmountCurrency",
        "HedgingSetCurrency",
        "HedgingSetRiskFactor",
        "HedgingSetSecondCurrency",
        "HedgingSetSecondRiskFactor",
        "NumberOfContractsInHedgingSet",
        "SumOfMarketValues",
        "SumOfMarketValuesCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "HedgingSetID", '' ) as "HedgingSetID",
            ifnull( "MarginedNettingSet", false ) as "MarginedNettingSet",
            ifnull( "_ExchangeTradedNettingSet.FinancialInstrumentID", '' ) as "_ExchangeTradedNettingSet.FinancialInstrumentID",
            ifnull( "_FinancialContractNettingSet.FinancialContractID", '' ) as "_FinancialContractNettingSet.FinancialContractID",
            ifnull( "_FinancialContractNettingSet.IDSystem", '' ) as "_FinancialContractNettingSet.IDSystem",
            ifnull( "_ResultGroup.ResultDataProvider", '' ) as "_ResultGroup.ResultDataProvider",
            ifnull( "_ResultGroup.ResultGroupID", '' ) as "_ResultGroup.ResultGroupID",
            ifnull( "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '' ) as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            ifnull( "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '' ) as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "AddOnAmount"  ,
            "AddOnAmountCurrency"  ,
            "AssetClass"  ,
            "EffectiveNotionalAmount"  ,
            "EffectiveNotionalAmountCurrency"  ,
            "HedgingSetCurrency"  ,
            "HedgingSetRiskFactor"  ,
            "HedgingSetSecondCurrency"  ,
            "HedgingSetSecondRiskFactor"  ,
            "NumberOfContractsInHedgingSet"  ,
            "SumOfMarketValues"  ,
            "SumOfMarketValuesCurrency"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END