PROCEDURE "sap.fsdm.procedures::CollateralPortionBusinessPartnerAssignmentLoad" (IN ROW "sap.fsdm.tabletypes::CollateralPortionBusinessPartnerAssignmentTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                '_CollateralPortion.PortionNumber=' || TO_VARCHAR("_CollateralPortion.PortionNumber") || ' ' ||
                '_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID=' || TO_VARCHAR("_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID") || ' ' ||
                '_CollateralPortion.ASSOC_CollateralAgreement.IDSystem=' || TO_VARCHAR("_CollateralPortion.ASSOC_CollateralAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_CollateralPortion.PortionNumber",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_CollateralPortion.PortionNumber",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_BusinessPartner.BusinessPartnerID",
                        "_CollateralPortion.PortionNumber",
                        "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_CollateralPortion.PortionNumber",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_CollateralPortion.PortionNumber",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::CollateralPortionBusinessPartnerAssignment" (
        "_BusinessPartner.BusinessPartnerID",
        "_CollateralPortion.PortionNumber",
        "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
        "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CollateralPortionConsideredInBusinessPartnerRating",
        "RankAmongBusinessPartnersCoveredByTheCollateral",
        "RankAmongCollateralsCoveringTheBusinessPartner",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
            "OLD__CollateralPortion.PortionNumber" as "_CollateralPortion.PortionNumber" ,
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" as "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem" as "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CollateralPortionConsideredInBusinessPartnerRating" as "CollateralPortionConsideredInBusinessPartnerRating" ,
            "OLD_RankAmongBusinessPartnersCoveredByTheCollateral" as "RankAmongBusinessPartnersCoveredByTheCollateral" ,
            "OLD_RankAmongCollateralsCoveringTheBusinessPartner" as "RankAmongCollateralsCoveringTheBusinessPartner" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_CollateralPortion.PortionNumber",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                                "OLD"."_CollateralPortion.PortionNumber" as "OLD__CollateralPortion.PortionNumber",
                                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" as "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" as "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."CollateralPortionConsideredInBusinessPartnerRating" as "OLD_CollateralPortionConsideredInBusinessPartnerRating",
                                "OLD"."RankAmongBusinessPartnersCoveredByTheCollateral" as "OLD_RankAmongBusinessPartnersCoveredByTheCollateral",
                                "OLD"."RankAmongCollateralsCoveringTheBusinessPartner" as "OLD_RankAmongCollateralsCoveringTheBusinessPartner",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CollateralPortionBusinessPartnerAssignment" as "OLD"
            on
                ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '') = "OLD"."_BusinessPartner.BusinessPartnerID" and
                ifnull( "IN"."_CollateralPortion.PortionNumber", -1) = "OLD"."_CollateralPortion.PortionNumber" and
                ifnull( "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID", '') = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                ifnull( "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem", '') = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CollateralPortionBusinessPartnerAssignment" (
        "_BusinessPartner.BusinessPartnerID",
        "_CollateralPortion.PortionNumber",
        "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
        "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CollateralPortionConsideredInBusinessPartnerRating",
        "RankAmongBusinessPartnersCoveredByTheCollateral",
        "RankAmongCollateralsCoveringTheBusinessPartner",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__BusinessPartner.BusinessPartnerID"  as "_BusinessPartner.BusinessPartnerID",
            "OLD__CollateralPortion.PortionNumber"  as "_CollateralPortion.PortionNumber",
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID"  as "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem"  as "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_CollateralPortionConsideredInBusinessPartnerRating"  as "CollateralPortionConsideredInBusinessPartnerRating",
            "OLD_RankAmongBusinessPartnersCoveredByTheCollateral"  as "RankAmongBusinessPartnersCoveredByTheCollateral",
            "OLD_RankAmongCollateralsCoveringTheBusinessPartner"  as "RankAmongCollateralsCoveringTheBusinessPartner",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_CollateralPortion.PortionNumber",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                        "OLD"."_CollateralPortion.PortionNumber" as "OLD__CollateralPortion.PortionNumber",
                        "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" as "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" as "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."CollateralPortionConsideredInBusinessPartnerRating" as "OLD_CollateralPortionConsideredInBusinessPartnerRating",
                        "OLD"."RankAmongBusinessPartnersCoveredByTheCollateral" as "OLD_RankAmongBusinessPartnersCoveredByTheCollateral",
                        "OLD"."RankAmongCollateralsCoveringTheBusinessPartner" as "OLD_RankAmongCollateralsCoveringTheBusinessPartner",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CollateralPortionBusinessPartnerAssignment" as "OLD"
            on
                ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '' ) = "OLD"."_BusinessPartner.BusinessPartnerID" and
                ifnull( "IN"."_CollateralPortion.PortionNumber", -1 ) = "OLD"."_CollateralPortion.PortionNumber" and
                ifnull( "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID", '' ) = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                ifnull( "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem", '' ) = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::CollateralPortionBusinessPartnerAssignment"
    where (
        "_BusinessPartner.BusinessPartnerID",
        "_CollateralPortion.PortionNumber",
        "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
        "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."_CollateralPortion.PortionNumber",
            "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CollateralPortionBusinessPartnerAssignment" as "OLD"
        on
           ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '' ) = "OLD"."_BusinessPartner.BusinessPartnerID" and
           ifnull( "IN"."_CollateralPortion.PortionNumber", -1 ) = "OLD"."_CollateralPortion.PortionNumber" and
           ifnull( "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID", '' ) = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
           ifnull( "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem", '' ) = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::CollateralPortionBusinessPartnerAssignment" (
        "_BusinessPartner.BusinessPartnerID",
        "_CollateralPortion.PortionNumber",
        "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
        "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CollateralPortionConsideredInBusinessPartnerRating",
        "RankAmongBusinessPartnersCoveredByTheCollateral",
        "RankAmongCollateralsCoveringTheBusinessPartner",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "_BusinessPartner.BusinessPartnerID", '' ) as "_BusinessPartner.BusinessPartnerID",
            ifnull( "_CollateralPortion.PortionNumber", -1 ) as "_CollateralPortion.PortionNumber",
            ifnull( "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID", '' ) as "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            ifnull( "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem", '' ) as "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "CollateralPortionConsideredInBusinessPartnerRating"  ,
            "RankAmongBusinessPartnersCoveredByTheCollateral"  ,
            "RankAmongCollateralsCoveringTheBusinessPartner"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END