PROCEDURE "sap.fsdm.procedures::TransferOrderListDelReadOnly" (IN ROW "sap.fsdm.tabletypes::TransferOrderTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::TransferOrderTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::TransferOrderTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'IDSystem=' || TO_VARCHAR("IDSystem") || ' ' ||
                'ItemNumber=' || TO_VARCHAR("ItemNumber") || ' ' ||
                'TransferOrderID=' || TO_VARCHAR("TransferOrderID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."IDSystem",
                        "IN"."ItemNumber",
                        "IN"."TransferOrderID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."IDSystem",
                        "IN"."ItemNumber",
                        "IN"."TransferOrderID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "IDSystem",
                        "ItemNumber",
                        "TransferOrderID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."IDSystem",
                                    "IN"."ItemNumber",
                                    "IN"."TransferOrderID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."IDSystem",
                                    "IN"."ItemNumber",
                                    "IN"."TransferOrderID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "IDSystem" is null and
            "TransferOrderID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "IDSystem",
            "ItemNumber",
            "TransferOrderID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::TransferOrder" WHERE
            (
            "IDSystem" ,
            "ItemNumber" ,
            "TransferOrderID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."IDSystem",
            "OLD"."ItemNumber",
            "OLD"."TransferOrderID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TransferOrder" as "OLD"
        on
                              "IN"."IDSystem" = "OLD"."IDSystem" and
                              "IN"."TransferOrderID" = "OLD"."TransferOrderID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "IDSystem",
        "ItemNumber",
        "TransferOrderID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_BankingChannel.BankingChannelID",
        "ASSOC_BusinessEventDataOwner.BusinessPartnerID",
        "ASSOC_DestinationAccount.FinancialContractID",
        "ASSOC_DestinationAccount.IDSystem",
        "ASSOC_InitiatingAccount.FinancialContractID",
        "ASSOC_InitiatingAccount.IDSystem",
        "ASSOC_Initiator.BusinessPartnerID",
        "ASSOC_OrganizationalUnit.IDSystem",
        "ASSOC_OrganizationalUnit.OrganizationalUnitID",
        "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency",
        "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
        "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem",
        "_ClaimPaymentRequestItem.ID",
        "_ClaimPaymentRequestItem.ItemNumber",
        "_ClaimPaymentRequestItem._Claim.ID",
        "_ClaimPaymentRequestItem._Claim.IDSystem",
        "_ClaimPaymentRequestItem._Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
        "_ClaimPaymentRequestItem._Subclaim._Claimant._Claim.ID",
        "_ClaimPaymentRequestItem._Subclaim._Claimant._Claim.IDSystem",
        "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage.ID",
        "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
        "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
        "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
        "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
        "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
        "_Security.FinancialInstrumentID",
        "BusinessCalendar",
        "BusinessDayConvention",
        "DestinationAccountIdentificationSystem",
        "DestinationBankAccountCurrency",
        "DestinationBankAccountHolderName",
        "DestinationBankAccountID",
        "DestinationBankID",
        "DestinationBankIdentificationSystem",
        "DestinationBankName",
        "DestinationCountry",
        "DestinationIBAN",
        "DestinationSWIFT",
        "DirectionOfTransfer",
        "FeeBillingMethod",
        "LifecycleStatus",
        "RecurringTransferOrderEndOfExecution",
        "RecurringTransferOrderPeriodLength",
        "RecurringTransferOrderPeriodTimeUnit",
        "TransferAgreementCategory",
        "TransferAmount",
        "TransferAmountCurrency",
        "TransferOrderFirstExecutionDate",
        "TransferOrderIsRecurring",
        "TransferOrderReceivedDate",
        "TransferOrderType",
        "TransferableNominalAmount",
        "TransferableNominalAmountCurrency",
        "TransferableQuantity",
        "TransferableUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_IDSystem" as "IDSystem" ,
            "OLD_ItemNumber" as "ItemNumber" ,
            "OLD_TransferOrderID" as "TransferOrderID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ASSOC_BankingChannel.BankingChannelID" as "ASSOC_BankingChannel.BankingChannelID" ,
            "OLD_ASSOC_BusinessEventDataOwner.BusinessPartnerID" as "ASSOC_BusinessEventDataOwner.BusinessPartnerID" ,
            "OLD_ASSOC_DestinationAccount.FinancialContractID" as "ASSOC_DestinationAccount.FinancialContractID" ,
            "OLD_ASSOC_DestinationAccount.IDSystem" as "ASSOC_DestinationAccount.IDSystem" ,
            "OLD_ASSOC_InitiatingAccount.FinancialContractID" as "ASSOC_InitiatingAccount.FinancialContractID" ,
            "OLD_ASSOC_InitiatingAccount.IDSystem" as "ASSOC_InitiatingAccount.IDSystem" ,
            "OLD_ASSOC_Initiator.BusinessPartnerID" as "ASSOC_Initiator.BusinessPartnerID" ,
            "OLD_ASSOC_OrganizationalUnit.IDSystem" as "ASSOC_OrganizationalUnit.IDSystem" ,
            "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" as "ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
            "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD_ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" as "ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
            "OLD_ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" as "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "OLD_ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" as "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "OLD__ClaimPaymentRequestItem.ID" as "_ClaimPaymentRequestItem.ID" ,
            "OLD__ClaimPaymentRequestItem.ItemNumber" as "_ClaimPaymentRequestItem.ItemNumber" ,
            "OLD__ClaimPaymentRequestItem._Claim.ID" as "_ClaimPaymentRequestItem._Claim.ID" ,
            "OLD__ClaimPaymentRequestItem._Claim.IDSystem" as "_ClaimPaymentRequestItem._Claim.IDSystem" ,
            "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" as "_ClaimPaymentRequestItem._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
            "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._Claim.ID" as "_ClaimPaymentRequestItem._Subclaim._Claimant._Claim.ID" ,
            "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._Claim.IDSystem" as "_ClaimPaymentRequestItem._Subclaim._Claimant._Claim.IDSystem" ,
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage.ID" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage.ID" ,
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "OLD__Security.FinancialInstrumentID" as "_Security.FinancialInstrumentID" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_DestinationAccountIdentificationSystem" as "DestinationAccountIdentificationSystem" ,
            "OLD_DestinationBankAccountCurrency" as "DestinationBankAccountCurrency" ,
            "OLD_DestinationBankAccountHolderName" as "DestinationBankAccountHolderName" ,
            "OLD_DestinationBankAccountID" as "DestinationBankAccountID" ,
            "OLD_DestinationBankID" as "DestinationBankID" ,
            "OLD_DestinationBankIdentificationSystem" as "DestinationBankIdentificationSystem" ,
            "OLD_DestinationBankName" as "DestinationBankName" ,
            "OLD_DestinationCountry" as "DestinationCountry" ,
            "OLD_DestinationIBAN" as "DestinationIBAN" ,
            "OLD_DestinationSWIFT" as "DestinationSWIFT" ,
            "OLD_DirectionOfTransfer" as "DirectionOfTransfer" ,
            "OLD_FeeBillingMethod" as "FeeBillingMethod" ,
            "OLD_LifecycleStatus" as "LifecycleStatus" ,
            "OLD_RecurringTransferOrderEndOfExecution" as "RecurringTransferOrderEndOfExecution" ,
            "OLD_RecurringTransferOrderPeriodLength" as "RecurringTransferOrderPeriodLength" ,
            "OLD_RecurringTransferOrderPeriodTimeUnit" as "RecurringTransferOrderPeriodTimeUnit" ,
            "OLD_TransferAgreementCategory" as "TransferAgreementCategory" ,
            "OLD_TransferAmount" as "TransferAmount" ,
            "OLD_TransferAmountCurrency" as "TransferAmountCurrency" ,
            "OLD_TransferOrderFirstExecutionDate" as "TransferOrderFirstExecutionDate" ,
            "OLD_TransferOrderIsRecurring" as "TransferOrderIsRecurring" ,
            "OLD_TransferOrderReceivedDate" as "TransferOrderReceivedDate" ,
            "OLD_TransferOrderType" as "TransferOrderType" ,
            "OLD_TransferableNominalAmount" as "TransferableNominalAmount" ,
            "OLD_TransferableNominalAmountCurrency" as "TransferableNominalAmountCurrency" ,
            "OLD_TransferableQuantity" as "TransferableQuantity" ,
            "OLD_TransferableUnit" as "TransferableUnit" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."IDSystem",
                        "OLD"."TransferOrderID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."IDSystem" AS "OLD_IDSystem" ,
                "OLD"."ItemNumber" AS "OLD_ItemNumber" ,
                "OLD"."TransferOrderID" AS "OLD_TransferOrderID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_BankingChannel.BankingChannelID" AS "OLD_ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."ASSOC_BusinessEventDataOwner.BusinessPartnerID" AS "OLD_ASSOC_BusinessEventDataOwner.BusinessPartnerID" ,
                "OLD"."ASSOC_DestinationAccount.FinancialContractID" AS "OLD_ASSOC_DestinationAccount.FinancialContractID" ,
                "OLD"."ASSOC_DestinationAccount.IDSystem" AS "OLD_ASSOC_DestinationAccount.IDSystem" ,
                "OLD"."ASSOC_InitiatingAccount.FinancialContractID" AS "OLD_ASSOC_InitiatingAccount.FinancialContractID" ,
                "OLD"."ASSOC_InitiatingAccount.IDSystem" AS "OLD_ASSOC_InitiatingAccount.IDSystem" ,
                "OLD"."ASSOC_Initiator.BusinessPartnerID" AS "OLD_ASSOC_Initiator.BusinessPartnerID" ,
                "OLD"."ASSOC_OrganizationalUnit.IDSystem" AS "OLD_ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" AS "OLD_ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" AS "OLD_ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" AS "OLD_ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_ClaimPaymentRequestItem.ID" AS "OLD__ClaimPaymentRequestItem.ID" ,
                "OLD"."_ClaimPaymentRequestItem.ItemNumber" AS "OLD__ClaimPaymentRequestItem.ItemNumber" ,
                "OLD"."_ClaimPaymentRequestItem._Claim.ID" AS "OLD__ClaimPaymentRequestItem._Claim.ID" ,
                "OLD"."_ClaimPaymentRequestItem._Claim.IDSystem" AS "OLD__ClaimPaymentRequestItem._Claim.IDSystem" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" AS "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._Claimant._Claim.ID" AS "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._Claim.ID" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._Claimant._Claim.IDSystem" AS "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._Claim.IDSystem" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage.ID" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage.ID" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Security.FinancialInstrumentID" AS "OLD__Security.FinancialInstrumentID" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."DestinationAccountIdentificationSystem" AS "OLD_DestinationAccountIdentificationSystem" ,
                "OLD"."DestinationBankAccountCurrency" AS "OLD_DestinationBankAccountCurrency" ,
                "OLD"."DestinationBankAccountHolderName" AS "OLD_DestinationBankAccountHolderName" ,
                "OLD"."DestinationBankAccountID" AS "OLD_DestinationBankAccountID" ,
                "OLD"."DestinationBankID" AS "OLD_DestinationBankID" ,
                "OLD"."DestinationBankIdentificationSystem" AS "OLD_DestinationBankIdentificationSystem" ,
                "OLD"."DestinationBankName" AS "OLD_DestinationBankName" ,
                "OLD"."DestinationCountry" AS "OLD_DestinationCountry" ,
                "OLD"."DestinationIBAN" AS "OLD_DestinationIBAN" ,
                "OLD"."DestinationSWIFT" AS "OLD_DestinationSWIFT" ,
                "OLD"."DirectionOfTransfer" AS "OLD_DirectionOfTransfer" ,
                "OLD"."FeeBillingMethod" AS "OLD_FeeBillingMethod" ,
                "OLD"."LifecycleStatus" AS "OLD_LifecycleStatus" ,
                "OLD"."RecurringTransferOrderEndOfExecution" AS "OLD_RecurringTransferOrderEndOfExecution" ,
                "OLD"."RecurringTransferOrderPeriodLength" AS "OLD_RecurringTransferOrderPeriodLength" ,
                "OLD"."RecurringTransferOrderPeriodTimeUnit" AS "OLD_RecurringTransferOrderPeriodTimeUnit" ,
                "OLD"."TransferAgreementCategory" AS "OLD_TransferAgreementCategory" ,
                "OLD"."TransferAmount" AS "OLD_TransferAmount" ,
                "OLD"."TransferAmountCurrency" AS "OLD_TransferAmountCurrency" ,
                "OLD"."TransferOrderFirstExecutionDate" AS "OLD_TransferOrderFirstExecutionDate" ,
                "OLD"."TransferOrderIsRecurring" AS "OLD_TransferOrderIsRecurring" ,
                "OLD"."TransferOrderReceivedDate" AS "OLD_TransferOrderReceivedDate" ,
                "OLD"."TransferOrderType" AS "OLD_TransferOrderType" ,
                "OLD"."TransferableNominalAmount" AS "OLD_TransferableNominalAmount" ,
                "OLD"."TransferableNominalAmountCurrency" AS "OLD_TransferableNominalAmountCurrency" ,
                "OLD"."TransferableQuantity" AS "OLD_TransferableQuantity" ,
                "OLD"."TransferableUnit" AS "OLD_TransferableUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TransferOrder" as "OLD"
            on
                                      "IN"."IDSystem" = "OLD"."IDSystem" and
                                      "IN"."TransferOrderID" = "OLD"."TransferOrderID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_IDSystem" as "IDSystem",
            "OLD_ItemNumber" as "ItemNumber",
            "OLD_TransferOrderID" as "TransferOrderID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ASSOC_BankingChannel.BankingChannelID" as "ASSOC_BankingChannel.BankingChannelID",
            "OLD_ASSOC_BusinessEventDataOwner.BusinessPartnerID" as "ASSOC_BusinessEventDataOwner.BusinessPartnerID",
            "OLD_ASSOC_DestinationAccount.FinancialContractID" as "ASSOC_DestinationAccount.FinancialContractID",
            "OLD_ASSOC_DestinationAccount.IDSystem" as "ASSOC_DestinationAccount.IDSystem",
            "OLD_ASSOC_InitiatingAccount.FinancialContractID" as "ASSOC_InitiatingAccount.FinancialContractID",
            "OLD_ASSOC_InitiatingAccount.IDSystem" as "ASSOC_InitiatingAccount.IDSystem",
            "OLD_ASSOC_Initiator.BusinessPartnerID" as "ASSOC_Initiator.BusinessPartnerID",
            "OLD_ASSOC_OrganizationalUnit.IDSystem" as "ASSOC_OrganizationalUnit.IDSystem",
            "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" as "ASSOC_OrganizationalUnit.OrganizationalUnitID",
            "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD_ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" as "ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency",
            "OLD_ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" as "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
            "OLD_ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" as "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem",
            "OLD__ClaimPaymentRequestItem.ID" as "_ClaimPaymentRequestItem.ID",
            "OLD__ClaimPaymentRequestItem.ItemNumber" as "_ClaimPaymentRequestItem.ItemNumber",
            "OLD__ClaimPaymentRequestItem._Claim.ID" as "_ClaimPaymentRequestItem._Claim.ID",
            "OLD__ClaimPaymentRequestItem._Claim.IDSystem" as "_ClaimPaymentRequestItem._Claim.IDSystem",
            "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" as "_ClaimPaymentRequestItem._Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
            "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._Claim.ID" as "_ClaimPaymentRequestItem._Subclaim._Claimant._Claim.ID",
            "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._Claim.IDSystem" as "_ClaimPaymentRequestItem._Subclaim._Claimant._Claim.IDSystem",
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage.ID" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage.ID",
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
            "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" as "_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
            "OLD__Security.FinancialInstrumentID" as "_Security.FinancialInstrumentID",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_DestinationAccountIdentificationSystem" as "DestinationAccountIdentificationSystem",
            "OLD_DestinationBankAccountCurrency" as "DestinationBankAccountCurrency",
            "OLD_DestinationBankAccountHolderName" as "DestinationBankAccountHolderName",
            "OLD_DestinationBankAccountID" as "DestinationBankAccountID",
            "OLD_DestinationBankID" as "DestinationBankID",
            "OLD_DestinationBankIdentificationSystem" as "DestinationBankIdentificationSystem",
            "OLD_DestinationBankName" as "DestinationBankName",
            "OLD_DestinationCountry" as "DestinationCountry",
            "OLD_DestinationIBAN" as "DestinationIBAN",
            "OLD_DestinationSWIFT" as "DestinationSWIFT",
            "OLD_DirectionOfTransfer" as "DirectionOfTransfer",
            "OLD_FeeBillingMethod" as "FeeBillingMethod",
            "OLD_LifecycleStatus" as "LifecycleStatus",
            "OLD_RecurringTransferOrderEndOfExecution" as "RecurringTransferOrderEndOfExecution",
            "OLD_RecurringTransferOrderPeriodLength" as "RecurringTransferOrderPeriodLength",
            "OLD_RecurringTransferOrderPeriodTimeUnit" as "RecurringTransferOrderPeriodTimeUnit",
            "OLD_TransferAgreementCategory" as "TransferAgreementCategory",
            "OLD_TransferAmount" as "TransferAmount",
            "OLD_TransferAmountCurrency" as "TransferAmountCurrency",
            "OLD_TransferOrderFirstExecutionDate" as "TransferOrderFirstExecutionDate",
            "OLD_TransferOrderIsRecurring" as "TransferOrderIsRecurring",
            "OLD_TransferOrderReceivedDate" as "TransferOrderReceivedDate",
            "OLD_TransferOrderType" as "TransferOrderType",
            "OLD_TransferableNominalAmount" as "TransferableNominalAmount",
            "OLD_TransferableNominalAmountCurrency" as "TransferableNominalAmountCurrency",
            "OLD_TransferableQuantity" as "TransferableQuantity",
            "OLD_TransferableUnit" as "TransferableUnit",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."IDSystem",
                        "OLD"."TransferOrderID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."IDSystem" AS "OLD_IDSystem" ,
                "OLD"."ItemNumber" AS "OLD_ItemNumber" ,
                "OLD"."TransferOrderID" AS "OLD_TransferOrderID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_BankingChannel.BankingChannelID" AS "OLD_ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."ASSOC_BusinessEventDataOwner.BusinessPartnerID" AS "OLD_ASSOC_BusinessEventDataOwner.BusinessPartnerID" ,
                "OLD"."ASSOC_DestinationAccount.FinancialContractID" AS "OLD_ASSOC_DestinationAccount.FinancialContractID" ,
                "OLD"."ASSOC_DestinationAccount.IDSystem" AS "OLD_ASSOC_DestinationAccount.IDSystem" ,
                "OLD"."ASSOC_InitiatingAccount.FinancialContractID" AS "OLD_ASSOC_InitiatingAccount.FinancialContractID" ,
                "OLD"."ASSOC_InitiatingAccount.IDSystem" AS "OLD_ASSOC_InitiatingAccount.IDSystem" ,
                "OLD"."ASSOC_Initiator.BusinessPartnerID" AS "OLD_ASSOC_Initiator.BusinessPartnerID" ,
                "OLD"."ASSOC_OrganizationalUnit.IDSystem" AS "OLD_ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" AS "OLD_ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" AS "OLD_ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" AS "OLD_ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_ClaimPaymentRequestItem.ID" AS "OLD__ClaimPaymentRequestItem.ID" ,
                "OLD"."_ClaimPaymentRequestItem.ItemNumber" AS "OLD__ClaimPaymentRequestItem.ItemNumber" ,
                "OLD"."_ClaimPaymentRequestItem._Claim.ID" AS "OLD__ClaimPaymentRequestItem._Claim.ID" ,
                "OLD"."_ClaimPaymentRequestItem._Claim.IDSystem" AS "OLD__ClaimPaymentRequestItem._Claim.IDSystem" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" AS "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._Claimant._Claim.ID" AS "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._Claim.ID" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._Claimant._Claim.IDSystem" AS "OLD__ClaimPaymentRequestItem._Subclaim._Claimant._Claim.IDSystem" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage.ID" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage.ID" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" AS "OLD__ClaimPaymentRequestItem._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Security.FinancialInstrumentID" AS "OLD__Security.FinancialInstrumentID" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."DestinationAccountIdentificationSystem" AS "OLD_DestinationAccountIdentificationSystem" ,
                "OLD"."DestinationBankAccountCurrency" AS "OLD_DestinationBankAccountCurrency" ,
                "OLD"."DestinationBankAccountHolderName" AS "OLD_DestinationBankAccountHolderName" ,
                "OLD"."DestinationBankAccountID" AS "OLD_DestinationBankAccountID" ,
                "OLD"."DestinationBankID" AS "OLD_DestinationBankID" ,
                "OLD"."DestinationBankIdentificationSystem" AS "OLD_DestinationBankIdentificationSystem" ,
                "OLD"."DestinationBankName" AS "OLD_DestinationBankName" ,
                "OLD"."DestinationCountry" AS "OLD_DestinationCountry" ,
                "OLD"."DestinationIBAN" AS "OLD_DestinationIBAN" ,
                "OLD"."DestinationSWIFT" AS "OLD_DestinationSWIFT" ,
                "OLD"."DirectionOfTransfer" AS "OLD_DirectionOfTransfer" ,
                "OLD"."FeeBillingMethod" AS "OLD_FeeBillingMethod" ,
                "OLD"."LifecycleStatus" AS "OLD_LifecycleStatus" ,
                "OLD"."RecurringTransferOrderEndOfExecution" AS "OLD_RecurringTransferOrderEndOfExecution" ,
                "OLD"."RecurringTransferOrderPeriodLength" AS "OLD_RecurringTransferOrderPeriodLength" ,
                "OLD"."RecurringTransferOrderPeriodTimeUnit" AS "OLD_RecurringTransferOrderPeriodTimeUnit" ,
                "OLD"."TransferAgreementCategory" AS "OLD_TransferAgreementCategory" ,
                "OLD"."TransferAmount" AS "OLD_TransferAmount" ,
                "OLD"."TransferAmountCurrency" AS "OLD_TransferAmountCurrency" ,
                "OLD"."TransferOrderFirstExecutionDate" AS "OLD_TransferOrderFirstExecutionDate" ,
                "OLD"."TransferOrderIsRecurring" AS "OLD_TransferOrderIsRecurring" ,
                "OLD"."TransferOrderReceivedDate" AS "OLD_TransferOrderReceivedDate" ,
                "OLD"."TransferOrderType" AS "OLD_TransferOrderType" ,
                "OLD"."TransferableNominalAmount" AS "OLD_TransferableNominalAmount" ,
                "OLD"."TransferableNominalAmountCurrency" AS "OLD_TransferableNominalAmountCurrency" ,
                "OLD"."TransferableQuantity" AS "OLD_TransferableQuantity" ,
                "OLD"."TransferableUnit" AS "OLD_TransferableUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TransferOrder" as "OLD"
            on
               "IN"."IDSystem" = "OLD"."IDSystem" and
               "IN"."TransferOrderID" = "OLD"."TransferOrderID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
