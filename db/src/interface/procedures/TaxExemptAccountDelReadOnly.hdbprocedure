PROCEDURE "sap.fsdm.procedures::TaxExemptAccountDelReadOnly" (IN ROW "sap.fsdm.tabletypes::TaxExemptAccountTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::TaxExemptAccountTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::TaxExemptAccountTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_BankAccount.FinancialContractID=' || TO_VARCHAR("_BankAccount.FinancialContractID") || ' ' ||
                '_BankAccount.IDSystem=' || TO_VARCHAR("_BankAccount.IDSystem") || ' ' ||
                '_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category=' || TO_VARCHAR("_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category") || ' ' ||
                '_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_BankAccount.FinancialContractID",
                        "IN"."_BankAccount.IDSystem",
                        "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
                        "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_BankAccount.FinancialContractID",
                        "IN"."_BankAccount.IDSystem",
                        "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
                        "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_BankAccount.FinancialContractID",
                        "_BankAccount.IDSystem",
                        "_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
                        "_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_BankAccount.FinancialContractID",
                                    "IN"."_BankAccount.IDSystem",
                                    "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
                                    "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_BankAccount.FinancialContractID",
                                    "IN"."_BankAccount.IDSystem",
                                    "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
                                    "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_BankAccount.FinancialContractID" is null and
            "_BankAccount.IDSystem" is null and
            "_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" is null and
            "_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_BankAccount.FinancialContractID",
            "_BankAccount.IDSystem",
            "_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
            "_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::TaxExemptAccount" WHERE
            (
            "_BankAccount.FinancialContractID" ,
            "_BankAccount.IDSystem" ,
            "_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" ,
            "_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_BankAccount.FinancialContractID",
            "OLD"."_BankAccount.IDSystem",
            "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
            "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TaxExemptAccount" as "OLD"
        on
                              "IN"."_BankAccount.FinancialContractID" = "OLD"."_BankAccount.FinancialContractID" and
                              "IN"."_BankAccount.IDSystem" = "OLD"."_BankAccount.IDSystem" and
                              "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" = "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" and
                              "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" = "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_BankAccount.FinancialContractID",
        "_BankAccount.IDSystem",
        "_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
        "_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__BankAccount.FinancialContractID" as "_BankAccount.FinancialContractID" ,
            "OLD__BankAccount.IDSystem" as "_BankAccount.IDSystem" ,
            "OLD__TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" as "_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" ,
            "OLD__TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" as "_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_BankAccount.FinancialContractID",
                        "OLD"."_BankAccount.IDSystem",
                        "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
                        "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_BankAccount.FinancialContractID" AS "OLD__BankAccount.FinancialContractID" ,
                "OLD"."_BankAccount.IDSystem" AS "OLD__BankAccount.IDSystem" ,
                "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" AS "OLD__TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" ,
                "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" AS "OLD__TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TaxExemptAccount" as "OLD"
            on
                                      "IN"."_BankAccount.FinancialContractID" = "OLD"."_BankAccount.FinancialContractID" and
                                      "IN"."_BankAccount.IDSystem" = "OLD"."_BankAccount.IDSystem" and
                                      "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" = "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" and
                                      "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" = "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__BankAccount.FinancialContractID" as "_BankAccount.FinancialContractID",
            "OLD__BankAccount.IDSystem" as "_BankAccount.IDSystem",
            "OLD__TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" as "_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
            "OLD__TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" as "_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_BankAccount.FinancialContractID",
                        "OLD"."_BankAccount.IDSystem",
                        "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category",
                        "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_BankAccount.FinancialContractID" AS "OLD__BankAccount.FinancialContractID" ,
                "OLD"."_BankAccount.IDSystem" AS "OLD__BankAccount.IDSystem" ,
                "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" AS "OLD__TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" ,
                "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" AS "OLD__TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TaxExemptAccount" as "OLD"
            on
               "IN"."_BankAccount.FinancialContractID" = "OLD"."_BankAccount.FinancialContractID" and
               "IN"."_BankAccount.IDSystem" = "OLD"."_BankAccount.IDSystem" and
               "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" = "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount.Category" and
               "IN"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" = "OLD"."_TaxExemptStatusDocumentApplicableForTaxExemptAccount._BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
