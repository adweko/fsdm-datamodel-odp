PROCEDURE "sap.fsdm.procedures::GLDocumentErase" (IN ROW "sap.fsdm.tabletypes::GLDocumentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "CompanyCode" is null and
            "DocumentNumber" is null and
            "FiscalYear" is null and
            "ItemNumber" is null and
            "Ledger" is null and
            "PostingDate" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_BusinessSegment.IDSystem" is null and
            "_BusinessSegment.OrganizationalUnitID" is null and
            "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "_BusinessSegmentAtCounterparty.IDSystem" is null and
            "_BusinessSegmentAtCounterparty.OrganizationalUnitID" is null and
            "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "_CompanyCode.BusinessPartnerID" is null and
            "_ProductCatalogItem.ProductCatalogItem" is null and
            "_ProductCatalogItem._ProductCatalog.CatalogID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::GLDocument"
        WHERE
        (            "CompanyCode" ,
            "DocumentNumber" ,
            "FiscalYear" ,
            "ItemNumber" ,
            "Ledger" ,
            "PostingDate" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_BusinessSegment.IDSystem" ,
            "_BusinessSegment.OrganizationalUnitID" ,
            "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_BusinessSegmentAtCounterparty.IDSystem" ,
            "_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
            "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_CompanyCode.BusinessPartnerID" ,
            "_ProductCatalogItem.ProductCatalogItem" ,
            "_ProductCatalogItem._ProductCatalog.CatalogID" 
        ) in
        (
            select                 "OLD"."CompanyCode" ,
                "OLD"."DocumentNumber" ,
                "OLD"."FiscalYear" ,
                "OLD"."ItemNumber" ,
                "OLD"."Ledger" ,
                "OLD"."PostingDate" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_BusinessSegment.IDSystem" ,
                "OLD"."_BusinessSegment.OrganizationalUnitID" ,
                "OLD"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_BusinessSegmentAtCounterparty.IDSystem" ,
                "OLD"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
                "OLD"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_CompanyCode.BusinessPartnerID" ,
                "OLD"."_ProductCatalogItem.ProductCatalogItem" ,
                "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
            from :ROW "IN"
            inner join "sap.fsdm::GLDocument" "OLD"
            on
            "IN"."CompanyCode" = "OLD"."CompanyCode" and
            "IN"."DocumentNumber" = "OLD"."DocumentNumber" and
            "IN"."FiscalYear" = "OLD"."FiscalYear" and
            "IN"."ItemNumber" = "OLD"."ItemNumber" and
            "IN"."Ledger" = "OLD"."Ledger" and
            "IN"."PostingDate" = "OLD"."PostingDate" and
            "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
            "IN"."_BusinessSegment.IDSystem" = "OLD"."_BusinessSegment.IDSystem" and
            "IN"."_BusinessSegment.OrganizationalUnitID" = "OLD"."_BusinessSegment.OrganizationalUnitID" and
            "IN"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
            "IN"."_BusinessSegmentAtCounterparty.IDSystem" = "OLD"."_BusinessSegmentAtCounterparty.IDSystem" and
            "IN"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" = "OLD"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" and
            "IN"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
            "IN"."_CompanyCode.BusinessPartnerID" = "OLD"."_CompanyCode.BusinessPartnerID" and
            "IN"."_ProductCatalogItem.ProductCatalogItem" = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
            "IN"."_ProductCatalogItem._ProductCatalog.CatalogID" = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
        );


END
