PROCEDURE "sap.fsdm.procedures::IndividualRatingsUsedForTotalESGRatingAssignmentErase" (IN ROW "sap.fsdm.tabletypes::IndividualRatingsUsedForTotalESGRatingAssignmentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Rating.IsFxRating" is null and
            "_Rating.RatingAgency" is null and
            "_Rating.RatingCategory" is null and
            "_Rating.RatingMethod" is null and
            "_Rating.RatingStatus" is null and
            "_Rating.TimeHorizon" is null and
            "_Rating.ASSOC_BusinessPartner.BusinessPartnerID" is null and
            "_Rating.ASSOC_FinancialContract.FinancialContractID" is null and
            "_Rating.ASSOC_FinancialContract.IDSystem" is null and
            "_Rating.ASSOC_GeographicalRegion.GeographicalStructureID" is null and
            "_Rating.ASSOC_GeographicalRegion.GeographicalUnitID" is null and
            "_Rating._PhysicalAsset.PhysicalAssetID" is null and
            "_Rating._Security.FinancialInstrumentID" is null and
            "_TotalESGRating.IsFxRating" is null and
            "_TotalESGRating.RatingAgency" is null and
            "_TotalESGRating.RatingCategory" is null and
            "_TotalESGRating.RatingMethod" is null and
            "_TotalESGRating.RatingStatus" is null and
            "_TotalESGRating.TimeHorizon" is null and
            "_TotalESGRating.ASSOC_BusinessPartner.BusinessPartnerID" is null and
            "_TotalESGRating.ASSOC_FinancialContract.FinancialContractID" is null and
            "_TotalESGRating.ASSOC_FinancialContract.IDSystem" is null and
            "_TotalESGRating.ASSOC_GeographicalRegion.GeographicalStructureID" is null and
            "_TotalESGRating.ASSOC_GeographicalRegion.GeographicalUnitID" is null and
            "_TotalESGRating._PhysicalAsset.PhysicalAssetID" is null and
            "_TotalESGRating._Security.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::IndividualRatingsUsedForTotalESGRatingAssignment"
        WHERE
        (            "_Rating.IsFxRating" ,
            "_Rating.RatingAgency" ,
            "_Rating.RatingCategory" ,
            "_Rating.RatingMethod" ,
            "_Rating.RatingStatus" ,
            "_Rating.TimeHorizon" ,
            "_Rating.ASSOC_BusinessPartner.BusinessPartnerID" ,
            "_Rating.ASSOC_FinancialContract.FinancialContractID" ,
            "_Rating.ASSOC_FinancialContract.IDSystem" ,
            "_Rating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
            "_Rating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
            "_Rating._PhysicalAsset.PhysicalAssetID" ,
            "_Rating._Security.FinancialInstrumentID" ,
            "_TotalESGRating.IsFxRating" ,
            "_TotalESGRating.RatingAgency" ,
            "_TotalESGRating.RatingCategory" ,
            "_TotalESGRating.RatingMethod" ,
            "_TotalESGRating.RatingStatus" ,
            "_TotalESGRating.TimeHorizon" ,
            "_TotalESGRating.ASSOC_BusinessPartner.BusinessPartnerID" ,
            "_TotalESGRating.ASSOC_FinancialContract.FinancialContractID" ,
            "_TotalESGRating.ASSOC_FinancialContract.IDSystem" ,
            "_TotalESGRating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
            "_TotalESGRating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
            "_TotalESGRating._PhysicalAsset.PhysicalAssetID" ,
            "_TotalESGRating._Security.FinancialInstrumentID" 
        ) in
        (
            select                 "OLD"."_Rating.IsFxRating" ,
                "OLD"."_Rating.RatingAgency" ,
                "OLD"."_Rating.RatingCategory" ,
                "OLD"."_Rating.RatingMethod" ,
                "OLD"."_Rating.RatingStatus" ,
                "OLD"."_Rating.TimeHorizon" ,
                "OLD"."_Rating.ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Rating.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_Rating.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Rating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "OLD"."_Rating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "OLD"."_Rating._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Rating._Security.FinancialInstrumentID" ,
                "OLD"."_TotalESGRating.IsFxRating" ,
                "OLD"."_TotalESGRating.RatingAgency" ,
                "OLD"."_TotalESGRating.RatingCategory" ,
                "OLD"."_TotalESGRating.RatingMethod" ,
                "OLD"."_TotalESGRating.RatingStatus" ,
                "OLD"."_TotalESGRating.TimeHorizon" ,
                "OLD"."_TotalESGRating.ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_TotalESGRating.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_TotalESGRating.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "OLD"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "OLD"."_TotalESGRating._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_TotalESGRating._Security.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::IndividualRatingsUsedForTotalESGRatingAssignment" "OLD"
            on
            "IN"."_Rating.IsFxRating" = "OLD"."_Rating.IsFxRating" and
            "IN"."_Rating.RatingAgency" = "OLD"."_Rating.RatingAgency" and
            "IN"."_Rating.RatingCategory" = "OLD"."_Rating.RatingCategory" and
            "IN"."_Rating.RatingMethod" = "OLD"."_Rating.RatingMethod" and
            "IN"."_Rating.RatingStatus" = "OLD"."_Rating.RatingStatus" and
            "IN"."_Rating.TimeHorizon" = "OLD"."_Rating.TimeHorizon" and
            "IN"."_Rating.ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."_Rating.ASSOC_BusinessPartner.BusinessPartnerID" and
            "IN"."_Rating.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_Rating.ASSOC_FinancialContract.FinancialContractID" and
            "IN"."_Rating.ASSOC_FinancialContract.IDSystem" = "OLD"."_Rating.ASSOC_FinancialContract.IDSystem" and
            "IN"."_Rating.ASSOC_GeographicalRegion.GeographicalStructureID" = "OLD"."_Rating.ASSOC_GeographicalRegion.GeographicalStructureID" and
            "IN"."_Rating.ASSOC_GeographicalRegion.GeographicalUnitID" = "OLD"."_Rating.ASSOC_GeographicalRegion.GeographicalUnitID" and
            "IN"."_Rating._PhysicalAsset.PhysicalAssetID" = "OLD"."_Rating._PhysicalAsset.PhysicalAssetID" and
            "IN"."_Rating._Security.FinancialInstrumentID" = "OLD"."_Rating._Security.FinancialInstrumentID" and
            "IN"."_TotalESGRating.IsFxRating" = "OLD"."_TotalESGRating.IsFxRating" and
            "IN"."_TotalESGRating.RatingAgency" = "OLD"."_TotalESGRating.RatingAgency" and
            "IN"."_TotalESGRating.RatingCategory" = "OLD"."_TotalESGRating.RatingCategory" and
            "IN"."_TotalESGRating.RatingMethod" = "OLD"."_TotalESGRating.RatingMethod" and
            "IN"."_TotalESGRating.RatingStatus" = "OLD"."_TotalESGRating.RatingStatus" and
            "IN"."_TotalESGRating.TimeHorizon" = "OLD"."_TotalESGRating.TimeHorizon" and
            "IN"."_TotalESGRating.ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."_TotalESGRating.ASSOC_BusinessPartner.BusinessPartnerID" and
            "IN"."_TotalESGRating.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_TotalESGRating.ASSOC_FinancialContract.FinancialContractID" and
            "IN"."_TotalESGRating.ASSOC_FinancialContract.IDSystem" = "OLD"."_TotalESGRating.ASSOC_FinancialContract.IDSystem" and
            "IN"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalStructureID" = "OLD"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalStructureID" and
            "IN"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalUnitID" = "OLD"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalUnitID" and
            "IN"."_TotalESGRating._PhysicalAsset.PhysicalAssetID" = "OLD"."_TotalESGRating._PhysicalAsset.PhysicalAssetID" and
            "IN"."_TotalESGRating._Security.FinancialInstrumentID" = "OLD"."_TotalESGRating._Security.FinancialInstrumentID" 
        );

        --delete data from history table
        delete from "sap.fsdm::IndividualRatingsUsedForTotalESGRatingAssignment_Historical"
        WHERE
        (
            "_Rating.IsFxRating" ,
            "_Rating.RatingAgency" ,
            "_Rating.RatingCategory" ,
            "_Rating.RatingMethod" ,
            "_Rating.RatingStatus" ,
            "_Rating.TimeHorizon" ,
            "_Rating.ASSOC_BusinessPartner.BusinessPartnerID" ,
            "_Rating.ASSOC_FinancialContract.FinancialContractID" ,
            "_Rating.ASSOC_FinancialContract.IDSystem" ,
            "_Rating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
            "_Rating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
            "_Rating._PhysicalAsset.PhysicalAssetID" ,
            "_Rating._Security.FinancialInstrumentID" ,
            "_TotalESGRating.IsFxRating" ,
            "_TotalESGRating.RatingAgency" ,
            "_TotalESGRating.RatingCategory" ,
            "_TotalESGRating.RatingMethod" ,
            "_TotalESGRating.RatingStatus" ,
            "_TotalESGRating.TimeHorizon" ,
            "_TotalESGRating.ASSOC_BusinessPartner.BusinessPartnerID" ,
            "_TotalESGRating.ASSOC_FinancialContract.FinancialContractID" ,
            "_TotalESGRating.ASSOC_FinancialContract.IDSystem" ,
            "_TotalESGRating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
            "_TotalESGRating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
            "_TotalESGRating._PhysicalAsset.PhysicalAssetID" ,
            "_TotalESGRating._Security.FinancialInstrumentID" 
        ) in
        (
            select
                "OLD"."_Rating.IsFxRating" ,
                "OLD"."_Rating.RatingAgency" ,
                "OLD"."_Rating.RatingCategory" ,
                "OLD"."_Rating.RatingMethod" ,
                "OLD"."_Rating.RatingStatus" ,
                "OLD"."_Rating.TimeHorizon" ,
                "OLD"."_Rating.ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Rating.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_Rating.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Rating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "OLD"."_Rating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "OLD"."_Rating._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Rating._Security.FinancialInstrumentID" ,
                "OLD"."_TotalESGRating.IsFxRating" ,
                "OLD"."_TotalESGRating.RatingAgency" ,
                "OLD"."_TotalESGRating.RatingCategory" ,
                "OLD"."_TotalESGRating.RatingMethod" ,
                "OLD"."_TotalESGRating.RatingStatus" ,
                "OLD"."_TotalESGRating.TimeHorizon" ,
                "OLD"."_TotalESGRating.ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_TotalESGRating.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_TotalESGRating.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "OLD"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "OLD"."_TotalESGRating._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_TotalESGRating._Security.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::IndividualRatingsUsedForTotalESGRatingAssignment_Historical" "OLD"
            on
                "IN"."_Rating.IsFxRating" = "OLD"."_Rating.IsFxRating" and
                "IN"."_Rating.RatingAgency" = "OLD"."_Rating.RatingAgency" and
                "IN"."_Rating.RatingCategory" = "OLD"."_Rating.RatingCategory" and
                "IN"."_Rating.RatingMethod" = "OLD"."_Rating.RatingMethod" and
                "IN"."_Rating.RatingStatus" = "OLD"."_Rating.RatingStatus" and
                "IN"."_Rating.TimeHorizon" = "OLD"."_Rating.TimeHorizon" and
                "IN"."_Rating.ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."_Rating.ASSOC_BusinessPartner.BusinessPartnerID" and
                "IN"."_Rating.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_Rating.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."_Rating.ASSOC_FinancialContract.IDSystem" = "OLD"."_Rating.ASSOC_FinancialContract.IDSystem" and
                "IN"."_Rating.ASSOC_GeographicalRegion.GeographicalStructureID" = "OLD"."_Rating.ASSOC_GeographicalRegion.GeographicalStructureID" and
                "IN"."_Rating.ASSOC_GeographicalRegion.GeographicalUnitID" = "OLD"."_Rating.ASSOC_GeographicalRegion.GeographicalUnitID" and
                "IN"."_Rating._PhysicalAsset.PhysicalAssetID" = "OLD"."_Rating._PhysicalAsset.PhysicalAssetID" and
                "IN"."_Rating._Security.FinancialInstrumentID" = "OLD"."_Rating._Security.FinancialInstrumentID" and
                "IN"."_TotalESGRating.IsFxRating" = "OLD"."_TotalESGRating.IsFxRating" and
                "IN"."_TotalESGRating.RatingAgency" = "OLD"."_TotalESGRating.RatingAgency" and
                "IN"."_TotalESGRating.RatingCategory" = "OLD"."_TotalESGRating.RatingCategory" and
                "IN"."_TotalESGRating.RatingMethod" = "OLD"."_TotalESGRating.RatingMethod" and
                "IN"."_TotalESGRating.RatingStatus" = "OLD"."_TotalESGRating.RatingStatus" and
                "IN"."_TotalESGRating.TimeHorizon" = "OLD"."_TotalESGRating.TimeHorizon" and
                "IN"."_TotalESGRating.ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."_TotalESGRating.ASSOC_BusinessPartner.BusinessPartnerID" and
                "IN"."_TotalESGRating.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_TotalESGRating.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."_TotalESGRating.ASSOC_FinancialContract.IDSystem" = "OLD"."_TotalESGRating.ASSOC_FinancialContract.IDSystem" and
                "IN"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalStructureID" = "OLD"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalStructureID" and
                "IN"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalUnitID" = "OLD"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalUnitID" and
                "IN"."_TotalESGRating._PhysicalAsset.PhysicalAssetID" = "OLD"."_TotalESGRating._PhysicalAsset.PhysicalAssetID" and
                "IN"."_TotalESGRating._Security.FinancialInstrumentID" = "OLD"."_TotalESGRating._Security.FinancialInstrumentID" 
        );

END
