PROCEDURE "sap.fsdm.procedures::CardDelReadOnly" (IN ROW "sap.fsdm.tabletypes::CardTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::CardTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CardTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CardIssueSequenceID=' || TO_VARCHAR("CardIssueSequenceID") || ' ' ||
                'ASSOC_CardIssue.CardIssueID=' || TO_VARCHAR("ASSOC_CardIssue.CardIssueID") || ' ' ||
                'ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID=' || TO_VARCHAR("ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID") || ' ' ||
                'ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem=' || TO_VARCHAR("ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem") || ' ' ||
                'ASSOC_CardIssue._BankAccount.FinancialContractID=' || TO_VARCHAR("ASSOC_CardIssue._BankAccount.FinancialContractID") || ' ' ||
                'ASSOC_CardIssue._BankAccount.IDSystem=' || TO_VARCHAR("ASSOC_CardIssue._BankAccount.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CardIssueSequenceID",
                        "IN"."ASSOC_CardIssue.CardIssueID",
                        "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                        "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                        "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                        "IN"."ASSOC_CardIssue._BankAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CardIssueSequenceID",
                        "IN"."ASSOC_CardIssue.CardIssueID",
                        "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                        "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                        "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                        "IN"."ASSOC_CardIssue._BankAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CardIssueSequenceID",
                        "ASSOC_CardIssue.CardIssueID",
                        "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                        "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                        "ASSOC_CardIssue._BankAccount.FinancialContractID",
                        "ASSOC_CardIssue._BankAccount.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CardIssueSequenceID",
                                    "IN"."ASSOC_CardIssue.CardIssueID",
                                    "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                                    "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                                    "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                                    "IN"."ASSOC_CardIssue._BankAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CardIssueSequenceID",
                                    "IN"."ASSOC_CardIssue.CardIssueID",
                                    "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                                    "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                                    "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                                    "IN"."ASSOC_CardIssue._BankAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "CardIssueSequenceID" is null and
            "ASSOC_CardIssue.CardIssueID" is null and
            "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" is null and
            "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" is null and
            "ASSOC_CardIssue._BankAccount.FinancialContractID" is null and
            "ASSOC_CardIssue._BankAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "CardIssueSequenceID",
            "ASSOC_CardIssue.CardIssueID",
            "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
            "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
            "ASSOC_CardIssue._BankAccount.FinancialContractID",
            "ASSOC_CardIssue._BankAccount.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::Card" WHERE
            (
            "CardIssueSequenceID" ,
            "ASSOC_CardIssue.CardIssueID" ,
            "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
            "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
            "ASSOC_CardIssue._BankAccount.FinancialContractID" ,
            "ASSOC_CardIssue._BankAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."CardIssueSequenceID",
            "OLD"."ASSOC_CardIssue.CardIssueID",
            "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
            "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
            "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID",
            "OLD"."ASSOC_CardIssue._BankAccount.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Card" as "OLD"
        on
                              "IN"."CardIssueSequenceID" = "OLD"."CardIssueSequenceID" and
                              "IN"."ASSOC_CardIssue.CardIssueID" = "OLD"."ASSOC_CardIssue.CardIssueID" and
                              "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" and
                              "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" and
                              "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID" = "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" and
                              "IN"."ASSOC_CardIssue._BankAccount.IDSystem" = "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "CardIssueSequenceID",
        "ASSOC_CardIssue.CardIssueID",
        "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
        "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
        "ASSOC_CardIssue._BankAccount.FinancialContractID",
        "ASSOC_CardIssue._BankAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CardDeliveryDate",
        "IssueReason",
        "LifeCycleStatusReason",
        "LifecycleStatus",
        "LifecycleStatusChangeDate",
        "NumberOnCard",
        "PINDeliveryDate",
        "ShippingMethod",
        "ValidFromOnCard",
        "ValidToOnCard",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_CardIssueSequenceID" as "CardIssueSequenceID" ,
            "OLD_ASSOC_CardIssue.CardIssueID" as "ASSOC_CardIssue.CardIssueID" ,
            "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" as "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
            "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" as "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
            "OLD_ASSOC_CardIssue._BankAccount.FinancialContractID" as "ASSOC_CardIssue._BankAccount.FinancialContractID" ,
            "OLD_ASSOC_CardIssue._BankAccount.IDSystem" as "ASSOC_CardIssue._BankAccount.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CardDeliveryDate" as "CardDeliveryDate" ,
            "OLD_IssueReason" as "IssueReason" ,
            "OLD_LifeCycleStatusReason" as "LifeCycleStatusReason" ,
            "OLD_LifecycleStatus" as "LifecycleStatus" ,
            "OLD_LifecycleStatusChangeDate" as "LifecycleStatusChangeDate" ,
            "OLD_NumberOnCard" as "NumberOnCard" ,
            "OLD_PINDeliveryDate" as "PINDeliveryDate" ,
            "OLD_ShippingMethod" as "ShippingMethod" ,
            "OLD_ValidFromOnCard" as "ValidFromOnCard" ,
            "OLD_ValidToOnCard" as "ValidToOnCard" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."CardIssueSequenceID",
                        "OLD"."ASSOC_CardIssue.CardIssueID",
                        "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                        "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                        "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                        "OLD"."ASSOC_CardIssue._BankAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."CardIssueSequenceID" AS "OLD_CardIssueSequenceID" ,
                "OLD"."ASSOC_CardIssue.CardIssueID" AS "OLD_ASSOC_CardIssue.CardIssueID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" AS "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" AS "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
                "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" AS "OLD_ASSOC_CardIssue._BankAccount.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" AS "OLD_ASSOC_CardIssue._BankAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CardDeliveryDate" AS "OLD_CardDeliveryDate" ,
                "OLD"."IssueReason" AS "OLD_IssueReason" ,
                "OLD"."LifeCycleStatusReason" AS "OLD_LifeCycleStatusReason" ,
                "OLD"."LifecycleStatus" AS "OLD_LifecycleStatus" ,
                "OLD"."LifecycleStatusChangeDate" AS "OLD_LifecycleStatusChangeDate" ,
                "OLD"."NumberOnCard" AS "OLD_NumberOnCard" ,
                "OLD"."PINDeliveryDate" AS "OLD_PINDeliveryDate" ,
                "OLD"."ShippingMethod" AS "OLD_ShippingMethod" ,
                "OLD"."ValidFromOnCard" AS "OLD_ValidFromOnCard" ,
                "OLD"."ValidToOnCard" AS "OLD_ValidToOnCard" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Card" as "OLD"
            on
                                      "IN"."CardIssueSequenceID" = "OLD"."CardIssueSequenceID" and
                                      "IN"."ASSOC_CardIssue.CardIssueID" = "OLD"."ASSOC_CardIssue.CardIssueID" and
                                      "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" and
                                      "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" and
                                      "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID" = "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" and
                                      "IN"."ASSOC_CardIssue._BankAccount.IDSystem" = "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_CardIssueSequenceID" as "CardIssueSequenceID",
            "OLD_ASSOC_CardIssue.CardIssueID" as "ASSOC_CardIssue.CardIssueID",
            "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" as "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
            "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" as "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
            "OLD_ASSOC_CardIssue._BankAccount.FinancialContractID" as "ASSOC_CardIssue._BankAccount.FinancialContractID",
            "OLD_ASSOC_CardIssue._BankAccount.IDSystem" as "ASSOC_CardIssue._BankAccount.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CardDeliveryDate" as "CardDeliveryDate",
            "OLD_IssueReason" as "IssueReason",
            "OLD_LifeCycleStatusReason" as "LifeCycleStatusReason",
            "OLD_LifecycleStatus" as "LifecycleStatus",
            "OLD_LifecycleStatusChangeDate" as "LifecycleStatusChangeDate",
            "OLD_NumberOnCard" as "NumberOnCard",
            "OLD_PINDeliveryDate" as "PINDeliveryDate",
            "OLD_ShippingMethod" as "ShippingMethod",
            "OLD_ValidFromOnCard" as "ValidFromOnCard",
            "OLD_ValidToOnCard" as "ValidToOnCard",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."CardIssueSequenceID",
                        "OLD"."ASSOC_CardIssue.CardIssueID",
                        "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                        "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                        "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                        "OLD"."ASSOC_CardIssue._BankAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."CardIssueSequenceID" AS "OLD_CardIssueSequenceID" ,
                "OLD"."ASSOC_CardIssue.CardIssueID" AS "OLD_ASSOC_CardIssue.CardIssueID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" AS "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" AS "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
                "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" AS "OLD_ASSOC_CardIssue._BankAccount.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" AS "OLD_ASSOC_CardIssue._BankAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CardDeliveryDate" AS "OLD_CardDeliveryDate" ,
                "OLD"."IssueReason" AS "OLD_IssueReason" ,
                "OLD"."LifeCycleStatusReason" AS "OLD_LifeCycleStatusReason" ,
                "OLD"."LifecycleStatus" AS "OLD_LifecycleStatus" ,
                "OLD"."LifecycleStatusChangeDate" AS "OLD_LifecycleStatusChangeDate" ,
                "OLD"."NumberOnCard" AS "OLD_NumberOnCard" ,
                "OLD"."PINDeliveryDate" AS "OLD_PINDeliveryDate" ,
                "OLD"."ShippingMethod" AS "OLD_ShippingMethod" ,
                "OLD"."ValidFromOnCard" AS "OLD_ValidFromOnCard" ,
                "OLD"."ValidToOnCard" AS "OLD_ValidToOnCard" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Card" as "OLD"
            on
               "IN"."CardIssueSequenceID" = "OLD"."CardIssueSequenceID" and
               "IN"."ASSOC_CardIssue.CardIssueID" = "OLD"."ASSOC_CardIssue.CardIssueID" and
               "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" and
               "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" and
               "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID" = "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" and
               "IN"."ASSOC_CardIssue._BankAccount.IDSystem" = "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
