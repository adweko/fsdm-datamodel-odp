PROCEDURE "sap.fsdm.procedures::BookDesignationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::BookDesignationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::BookDesignationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::BookDesignationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Regime" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_InvestmentAccount.FinancialContractID" is null and
            "_InvestmentAccount.IDSystem" is null and
            "_TradingLimit.LimitID" is null and
            "_TradingLimit._TimeBucket.MaturityBandID" is null and
            "_TradingLimit._TimeBucket.TimeBucketID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Regime" ,
                "ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_FinancialContract.IDSystem" ,
                "_Collection.CollectionID" ,
                "_Collection.IDSystem" ,
                "_Collection._Client.BusinessPartnerID" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "_InvestmentAccount.FinancialContractID" ,
                "_InvestmentAccount.IDSystem" ,
                "_TradingLimit.LimitID" ,
                "_TradingLimit._TimeBucket.MaturityBandID" ,
                "_TradingLimit._TimeBucket.TimeBucketID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Regime" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" ,
                "OLD"."_TradingLimit.LimitID" ,
                "OLD"."_TradingLimit._TimeBucket.MaturityBandID" ,
                "OLD"."_TradingLimit._TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::BookDesignation" "OLD"
            on
                "IN"."Regime" = "OLD"."Regime" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" and
                "IN"."_TradingLimit.LimitID" = "OLD"."_TradingLimit.LimitID" and
                "IN"."_TradingLimit._TimeBucket.MaturityBandID" = "OLD"."_TradingLimit._TimeBucket.MaturityBandID" and
                "IN"."_TradingLimit._TimeBucket.TimeBucketID" = "OLD"."_TradingLimit._TimeBucket.TimeBucketID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Regime" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InvestmentAccount.FinancialContractID" ,
            "_InvestmentAccount.IDSystem" ,
            "_TradingLimit.LimitID" ,
            "_TradingLimit._TimeBucket.MaturityBandID" ,
            "_TradingLimit._TimeBucket.TimeBucketID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Regime" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" ,
                "OLD"."_TradingLimit.LimitID" ,
                "OLD"."_TradingLimit._TimeBucket.MaturityBandID" ,
                "OLD"."_TradingLimit._TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::BookDesignation_Historical" "OLD"
            on
                "IN"."Regime" = "OLD"."Regime" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" and
                "IN"."_TradingLimit.LimitID" = "OLD"."_TradingLimit.LimitID" and
                "IN"."_TradingLimit._TimeBucket.MaturityBandID" = "OLD"."_TradingLimit._TimeBucket.MaturityBandID" and
                "IN"."_TradingLimit._TimeBucket.TimeBucketID" = "OLD"."_TradingLimit._TimeBucket.TimeBucketID" 
        );

END
