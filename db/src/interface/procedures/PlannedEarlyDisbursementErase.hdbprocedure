PROCEDURE "sap.fsdm.procedures::PlannedEarlyDisbursementErase" (IN ROW "sap.fsdm.tabletypes::PlannedEarlyDisbursementTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "DisbursementDifferentiationCriterium" is null and
            "PlannedDisbursementDate" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::PlannedEarlyDisbursement"
        WHERE
        (            "DisbursementDifferentiationCriterium" ,
            "PlannedDisbursementDate" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" 
        ) in
        (
            select                 "OLD"."DisbursementDifferentiationCriterium" ,
                "OLD"."PlannedDisbursementDate" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::PlannedEarlyDisbursement" "OLD"
            on
            "IN"."DisbursementDifferentiationCriterium" = "OLD"."DisbursementDifferentiationCriterium" and
            "IN"."PlannedDisbursementDate" = "OLD"."PlannedDisbursementDate" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::PlannedEarlyDisbursement_Historical"
        WHERE
        (
            "DisbursementDifferentiationCriterium" ,
            "PlannedDisbursementDate" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" 
        ) in
        (
            select
                "OLD"."DisbursementDifferentiationCriterium" ,
                "OLD"."PlannedDisbursementDate" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::PlannedEarlyDisbursement_Historical" "OLD"
            on
                "IN"."DisbursementDifferentiationCriterium" = "OLD"."DisbursementDifferentiationCriterium" and
                "IN"."PlannedDisbursementDate" = "OLD"."PlannedDisbursementDate" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        );

END
