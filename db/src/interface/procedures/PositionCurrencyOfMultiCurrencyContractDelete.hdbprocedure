PROCEDURE "sap.fsdm.procedures::PositionCurrencyOfMultiCurrencyContractDelete" (IN ROW "sap.fsdm.tabletypes::PositionCurrencyOfMultiCurrencyContractTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PositionCurrency=' || TO_VARCHAR("PositionCurrency") || ' ' ||
                'ASSOC_MultiCcyAccnt.FinancialContractID=' || TO_VARCHAR("ASSOC_MultiCcyAccnt.FinancialContractID") || ' ' ||
                'ASSOC_MultiCcyAccnt.IDSystem=' || TO_VARCHAR("ASSOC_MultiCcyAccnt.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PositionCurrency",
                        "IN"."ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."ASSOC_MultiCcyAccnt.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PositionCurrency",
                        "IN"."ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."ASSOC_MultiCcyAccnt.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PositionCurrency",
                        "ASSOC_MultiCcyAccnt.FinancialContractID",
                        "ASSOC_MultiCcyAccnt.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PositionCurrency",
                                    "IN"."ASSOC_MultiCcyAccnt.FinancialContractID",
                                    "IN"."ASSOC_MultiCcyAccnt.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PositionCurrency",
                                    "IN"."ASSOC_MultiCcyAccnt.FinancialContractID",
                                    "IN"."ASSOC_MultiCcyAccnt.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "PositionCurrency" is null and
            "ASSOC_MultiCcyAccnt.FinancialContractID" is null and
            "ASSOC_MultiCcyAccnt.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::PositionCurrencyOfMultiCurrencyContract" (
        "PositionCurrency",
        "ASSOC_MultiCcyAccnt.FinancialContractID",
        "ASSOC_MultiCcyAccnt.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "FixedPeriodEndDate",
        "FixedPeriodStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PositionCurrency" as "PositionCurrency" ,
            "OLD_ASSOC_MultiCcyAccnt.FinancialContractID" as "ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "OLD_ASSOC_MultiCcyAccnt.IDSystem" as "ASSOC_MultiCcyAccnt.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_FixedPeriodEndDate" as "FixedPeriodEndDate" ,
            "OLD_FixedPeriodStartDate" as "FixedPeriodStartDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."PositionCurrency",
                        "OLD"."ASSOC_MultiCcyAccnt.FinancialContractID",
                        "OLD"."ASSOC_MultiCcyAccnt.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."PositionCurrency" AS "OLD_PositionCurrency" ,
                "OLD"."ASSOC_MultiCcyAccnt.FinancialContractID" AS "OLD_ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_MultiCcyAccnt.IDSystem" AS "OLD_ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."FixedPeriodEndDate" AS "OLD_FixedPeriodEndDate" ,
                "OLD"."FixedPeriodStartDate" AS "OLD_FixedPeriodStartDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PositionCurrencyOfMultiCurrencyContract" as "OLD"
            on
                      "IN"."PositionCurrency" = "OLD"."PositionCurrency" and
                      "IN"."ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_MultiCcyAccnt.FinancialContractID" and
                      "IN"."ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_MultiCcyAccnt.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::PositionCurrencyOfMultiCurrencyContract" (
        "PositionCurrency",
        "ASSOC_MultiCcyAccnt.FinancialContractID",
        "ASSOC_MultiCcyAccnt.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "FixedPeriodEndDate",
        "FixedPeriodStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PositionCurrency" as "PositionCurrency",
            "OLD_ASSOC_MultiCcyAccnt.FinancialContractID" as "ASSOC_MultiCcyAccnt.FinancialContractID",
            "OLD_ASSOC_MultiCcyAccnt.IDSystem" as "ASSOC_MultiCcyAccnt.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_FixedPeriodEndDate" as "FixedPeriodEndDate",
            "OLD_FixedPeriodStartDate" as "FixedPeriodStartDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."PositionCurrency",
                        "OLD"."ASSOC_MultiCcyAccnt.FinancialContractID",
                        "OLD"."ASSOC_MultiCcyAccnt.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."PositionCurrency" AS "OLD_PositionCurrency" ,
                "OLD"."ASSOC_MultiCcyAccnt.FinancialContractID" AS "OLD_ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_MultiCcyAccnt.IDSystem" AS "OLD_ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."FixedPeriodEndDate" AS "OLD_FixedPeriodEndDate" ,
                "OLD"."FixedPeriodStartDate" AS "OLD_FixedPeriodStartDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PositionCurrencyOfMultiCurrencyContract" as "OLD"
            on
                                                "IN"."PositionCurrency" = "OLD"."PositionCurrency" and
                                                "IN"."ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_MultiCcyAccnt.FinancialContractID" and
                                                "IN"."ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_MultiCcyAccnt.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::PositionCurrencyOfMultiCurrencyContract"
    where (
        "PositionCurrency",
        "ASSOC_MultiCcyAccnt.FinancialContractID",
        "ASSOC_MultiCcyAccnt.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."PositionCurrency",
            "OLD"."ASSOC_MultiCcyAccnt.FinancialContractID",
            "OLD"."ASSOC_MultiCcyAccnt.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PositionCurrencyOfMultiCurrencyContract" as "OLD"
        on
                                       "IN"."PositionCurrency" = "OLD"."PositionCurrency" and
                                       "IN"."ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_MultiCcyAccnt.FinancialContractID" and
                                       "IN"."ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_MultiCcyAccnt.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
