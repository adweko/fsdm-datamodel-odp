PROCEDURE "sap.fsdm.procedures::ValueAdjustmentAnalysisResultsEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ValueAdjustmentAnalysisResultsTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ValueAdjustmentAnalysisResultsTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ValueAdjustmentAnalysisResultsTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "CalculationMethod" is null and
            "ConfidenceLevel" is null and
            "RiskProvisionScenario" is null and
            "RoleOfCurrency" is null and
            "_BusinessPartner.BusinessPartnerID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_RiskReportingNode.RiskReportingNodeID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null and
            "_TimeBucket.MaturityBandID" is null and
            "_TimeBucket.TimeBucketID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "CalculationMethod" ,
                "ConfidenceLevel" ,
                "RiskProvisionScenario" ,
                "RoleOfCurrency" ,
                "_BusinessPartner.BusinessPartnerID" ,
                "_FinancialContract.FinancialContractID" ,
                "_FinancialContract.IDSystem" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "_ResultGroup.ResultDataProvider" ,
                "_ResultGroup.ResultGroupID" ,
                "_RiskReportingNode.RiskReportingNodeID" ,
                "_SecuritiesAccount.FinancialContractID" ,
                "_SecuritiesAccount.IDSystem" ,
                "_TimeBucket.MaturityBandID" ,
                "_TimeBucket.TimeBucketID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."CalculationMethod" ,
                "OLD"."ConfidenceLevel" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."_TimeBucket.MaturityBandID" ,
                "OLD"."_TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ValueAdjustmentAnalysisResults" "OLD"
            on
                "IN"."CalculationMethod" = "OLD"."CalculationMethod" and
                "IN"."ConfidenceLevel" = "OLD"."ConfidenceLevel" and
                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
                "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
                "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "CalculationMethod" ,
            "ConfidenceLevel" ,
            "RiskProvisionScenario" ,
            "RoleOfCurrency" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "_TimeBucket.MaturityBandID" ,
            "_TimeBucket.TimeBucketID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."CalculationMethod" ,
                "OLD"."ConfidenceLevel" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."_TimeBucket.MaturityBandID" ,
                "OLD"."_TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ValueAdjustmentAnalysisResults_Historical" "OLD"
            on
                "IN"."CalculationMethod" = "OLD"."CalculationMethod" and
                "IN"."ConfidenceLevel" = "OLD"."ConfidenceLevel" and
                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
                "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
                "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
        );

END
