PROCEDURE "sap.fsdm.procedures::SwapAmortizationSpecificationReadOnly" (IN ROW "sap.fsdm.tabletypes::SwapAmortizationSpecificationTT", OUT CURR_DEL "sap.fsdm.tabletypes::SwapAmortizationSpecificationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::SwapAmortizationSpecificationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AmortizationEndDate=' || TO_VARCHAR("AmortizationEndDate") || ' ' ||
                'AmortizationStartDate=' || TO_VARCHAR("AmortizationStartDate") || ' ' ||
                'RoleOfPayer=' || TO_VARCHAR("RoleOfPayer") || ' ' ||
                '_InterestBearingSwap.FinancialContractID=' || TO_VARCHAR("_InterestBearingSwap.FinancialContractID") || ' ' ||
                '_InterestBearingSwap.IDSystem=' || TO_VARCHAR("_InterestBearingSwap.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AmortizationEndDate",
                        "IN"."AmortizationStartDate",
                        "IN"."RoleOfPayer",
                        "IN"."_InterestBearingSwap.FinancialContractID",
                        "IN"."_InterestBearingSwap.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AmortizationEndDate",
                        "IN"."AmortizationStartDate",
                        "IN"."RoleOfPayer",
                        "IN"."_InterestBearingSwap.FinancialContractID",
                        "IN"."_InterestBearingSwap.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AmortizationEndDate",
                        "AmortizationStartDate",
                        "RoleOfPayer",
                        "_InterestBearingSwap.FinancialContractID",
                        "_InterestBearingSwap.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AmortizationEndDate",
                                    "IN"."AmortizationStartDate",
                                    "IN"."RoleOfPayer",
                                    "IN"."_InterestBearingSwap.FinancialContractID",
                                    "IN"."_InterestBearingSwap.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AmortizationEndDate",
                                    "IN"."AmortizationStartDate",
                                    "IN"."RoleOfPayer",
                                    "IN"."_InterestBearingSwap.FinancialContractID",
                                    "IN"."_InterestBearingSwap.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "AmortizationEndDate",
        "AmortizationStartDate",
        "RoleOfPayer",
        "_InterestBearingSwap.FinancialContractID",
        "_InterestBearingSwap.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::SwapAmortizationSpecification" WHERE
        (            "AmortizationEndDate" ,
            "AmortizationStartDate" ,
            "RoleOfPayer" ,
            "_InterestBearingSwap.FinancialContractID" ,
            "_InterestBearingSwap.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."AmortizationEndDate",
            "OLD"."AmortizationStartDate",
            "OLD"."RoleOfPayer",
            "OLD"."_InterestBearingSwap.FinancialContractID",
            "OLD"."_InterestBearingSwap.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::SwapAmortizationSpecification" as "OLD"
            on
               ifnull( "IN"."AmortizationEndDate",'0001-01-01' ) = "OLD"."AmortizationEndDate" and
               ifnull( "IN"."AmortizationStartDate",'0001-01-01' ) = "OLD"."AmortizationStartDate" and
               ifnull( "IN"."RoleOfPayer",'' ) = "OLD"."RoleOfPayer" and
               ifnull( "IN"."_InterestBearingSwap.FinancialContractID",'' ) = "OLD"."_InterestBearingSwap.FinancialContractID" and
               ifnull( "IN"."_InterestBearingSwap.IDSystem",'' ) = "OLD"."_InterestBearingSwap.IDSystem" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "AmortizationEndDate",
        "AmortizationStartDate",
        "RoleOfPayer",
        "_InterestBearingSwap.FinancialContractID",
        "_InterestBearingSwap.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AmortizationAmount",
        "AmortizationFrequency",
        "AmortizationFrequencyTimeUnit",
        "AmortizationRate",
        "AmortizationRule",
        "AnnuityAmount",
        "CouponReinvestmentPercentage",
        "PayFullCoupon",
        "TargetNotionalAmount",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "AmortizationEndDate", '0001-01-01' ) as "AmortizationEndDate",
                    ifnull( "AmortizationStartDate", '0001-01-01' ) as "AmortizationStartDate",
                    ifnull( "RoleOfPayer", '' ) as "RoleOfPayer",
                    ifnull( "_InterestBearingSwap.FinancialContractID", '' ) as "_InterestBearingSwap.FinancialContractID",
                    ifnull( "_InterestBearingSwap.IDSystem", '' ) as "_InterestBearingSwap.IDSystem",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "AmortizationAmount"  ,
                    "AmortizationFrequency"  ,
                    "AmortizationFrequencyTimeUnit"  ,
                    "AmortizationRate"  ,
                    "AmortizationRule"  ,
                    "AnnuityAmount"  ,
                    "CouponReinvestmentPercentage"  ,
                    "PayFullCoupon"  ,
                    "TargetNotionalAmount"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_AmortizationEndDate" as "AmortizationEndDate" ,
                    "OLD_AmortizationStartDate" as "AmortizationStartDate" ,
                    "OLD_RoleOfPayer" as "RoleOfPayer" ,
                    "OLD__InterestBearingSwap.FinancialContractID" as "_InterestBearingSwap.FinancialContractID" ,
                    "OLD__InterestBearingSwap.IDSystem" as "_InterestBearingSwap.IDSystem" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_AmortizationAmount" as "AmortizationAmount" ,
                    "OLD_AmortizationFrequency" as "AmortizationFrequency" ,
                    "OLD_AmortizationFrequencyTimeUnit" as "AmortizationFrequencyTimeUnit" ,
                    "OLD_AmortizationRate" as "AmortizationRate" ,
                    "OLD_AmortizationRule" as "AmortizationRule" ,
                    "OLD_AnnuityAmount" as "AnnuityAmount" ,
                    "OLD_CouponReinvestmentPercentage" as "CouponReinvestmentPercentage" ,
                    "OLD_PayFullCoupon" as "PayFullCoupon" ,
                    "OLD_TargetNotionalAmount" as "TargetNotionalAmount" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."AmortizationEndDate",
                        "IN"."AmortizationStartDate",
                        "IN"."RoleOfPayer",
                        "IN"."_InterestBearingSwap.FinancialContractID",
                        "IN"."_InterestBearingSwap.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."AmortizationEndDate" as "OLD_AmortizationEndDate",
                                "OLD"."AmortizationStartDate" as "OLD_AmortizationStartDate",
                                "OLD"."RoleOfPayer" as "OLD_RoleOfPayer",
                                "OLD"."_InterestBearingSwap.FinancialContractID" as "OLD__InterestBearingSwap.FinancialContractID",
                                "OLD"."_InterestBearingSwap.IDSystem" as "OLD__InterestBearingSwap.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AmortizationAmount" as "OLD_AmortizationAmount",
                                "OLD"."AmortizationFrequency" as "OLD_AmortizationFrequency",
                                "OLD"."AmortizationFrequencyTimeUnit" as "OLD_AmortizationFrequencyTimeUnit",
                                "OLD"."AmortizationRate" as "OLD_AmortizationRate",
                                "OLD"."AmortizationRule" as "OLD_AmortizationRule",
                                "OLD"."AnnuityAmount" as "OLD_AnnuityAmount",
                                "OLD"."CouponReinvestmentPercentage" as "OLD_CouponReinvestmentPercentage",
                                "OLD"."PayFullCoupon" as "OLD_PayFullCoupon",
                                "OLD"."TargetNotionalAmount" as "OLD_TargetNotionalAmount",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::SwapAmortizationSpecification" as "OLD"
            on
                ifnull( "IN"."AmortizationEndDate", '0001-01-01') = "OLD"."AmortizationEndDate" and
                ifnull( "IN"."AmortizationStartDate", '0001-01-01') = "OLD"."AmortizationStartDate" and
                ifnull( "IN"."RoleOfPayer", '') = "OLD"."RoleOfPayer" and
                ifnull( "IN"."_InterestBearingSwap.FinancialContractID", '') = "OLD"."_InterestBearingSwap.FinancialContractID" and
                ifnull( "IN"."_InterestBearingSwap.IDSystem", '') = "OLD"."_InterestBearingSwap.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_AmortizationEndDate" as "AmortizationEndDate",
            "OLD_AmortizationStartDate" as "AmortizationStartDate",
            "OLD_RoleOfPayer" as "RoleOfPayer",
            "OLD__InterestBearingSwap.FinancialContractID" as "_InterestBearingSwap.FinancialContractID",
            "OLD__InterestBearingSwap.IDSystem" as "_InterestBearingSwap.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AmortizationAmount" as "AmortizationAmount",
            "OLD_AmortizationFrequency" as "AmortizationFrequency",
            "OLD_AmortizationFrequencyTimeUnit" as "AmortizationFrequencyTimeUnit",
            "OLD_AmortizationRate" as "AmortizationRate",
            "OLD_AmortizationRule" as "AmortizationRule",
            "OLD_AnnuityAmount" as "AnnuityAmount",
            "OLD_CouponReinvestmentPercentage" as "CouponReinvestmentPercentage",
            "OLD_PayFullCoupon" as "PayFullCoupon",
            "OLD_TargetNotionalAmount" as "TargetNotionalAmount",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."AmortizationEndDate",
                        "IN"."AmortizationStartDate",
                        "IN"."RoleOfPayer",
                        "IN"."_InterestBearingSwap.FinancialContractID",
                        "IN"."_InterestBearingSwap.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."AmortizationEndDate" as "OLD_AmortizationEndDate",
                                "OLD"."AmortizationStartDate" as "OLD_AmortizationStartDate",
                                "OLD"."RoleOfPayer" as "OLD_RoleOfPayer",
                                "OLD"."_InterestBearingSwap.FinancialContractID" as "OLD__InterestBearingSwap.FinancialContractID",
                                "OLD"."_InterestBearingSwap.IDSystem" as "OLD__InterestBearingSwap.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AmortizationAmount" as "OLD_AmortizationAmount",
                                "OLD"."AmortizationFrequency" as "OLD_AmortizationFrequency",
                                "OLD"."AmortizationFrequencyTimeUnit" as "OLD_AmortizationFrequencyTimeUnit",
                                "OLD"."AmortizationRate" as "OLD_AmortizationRate",
                                "OLD"."AmortizationRule" as "OLD_AmortizationRule",
                                "OLD"."AnnuityAmount" as "OLD_AnnuityAmount",
                                "OLD"."CouponReinvestmentPercentage" as "OLD_CouponReinvestmentPercentage",
                                "OLD"."PayFullCoupon" as "OLD_PayFullCoupon",
                                "OLD"."TargetNotionalAmount" as "OLD_TargetNotionalAmount",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::SwapAmortizationSpecification" as "OLD"
            on
                ifnull("IN"."AmortizationEndDate", '0001-01-01') = "OLD"."AmortizationEndDate" and
                ifnull("IN"."AmortizationStartDate", '0001-01-01') = "OLD"."AmortizationStartDate" and
                ifnull("IN"."RoleOfPayer", '') = "OLD"."RoleOfPayer" and
                ifnull("IN"."_InterestBearingSwap.FinancialContractID", '') = "OLD"."_InterestBearingSwap.FinancialContractID" and
                ifnull("IN"."_InterestBearingSwap.IDSystem", '') = "OLD"."_InterestBearingSwap.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
