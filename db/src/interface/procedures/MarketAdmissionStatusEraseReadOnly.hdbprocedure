PROCEDURE "sap.fsdm.procedures::MarketAdmissionStatusEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::MarketAdmissionStatusTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::MarketAdmissionStatusTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::MarketAdmissionStatusTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "MarketAdmissionLevel" is null and
            "_BusinessPartner.BusinessPartnerID" is null and
            "_Exchange.MarketIdentifierCode" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "MarketAdmissionLevel" ,
                "_BusinessPartner.BusinessPartnerID" ,
                "_Exchange.MarketIdentifierCode" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."MarketAdmissionLevel" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Exchange.MarketIdentifierCode" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::MarketAdmissionStatus" "OLD"
            on
                "IN"."MarketAdmissionLevel" = "OLD"."MarketAdmissionLevel" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "MarketAdmissionLevel" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_Exchange.MarketIdentifierCode" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."MarketAdmissionLevel" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Exchange.MarketIdentifierCode" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::MarketAdmissionStatus_Historical" "OLD"
            on
                "IN"."MarketAdmissionLevel" = "OLD"."MarketAdmissionLevel" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" 
        );

END
