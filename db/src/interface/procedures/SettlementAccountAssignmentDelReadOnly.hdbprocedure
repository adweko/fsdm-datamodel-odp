PROCEDURE "sap.fsdm.procedures::SettlementAccountAssignmentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::SettlementAccountAssignmentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::SettlementAccountAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::SettlementAccountAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SettlementAccountRole=' || TO_VARCHAR("SettlementAccountRole") || ' ' ||
                'ASSOC_BaseContract.FinancialContractID=' || TO_VARCHAR("ASSOC_BaseContract.FinancialContractID") || ' ' ||
                'ASSOC_BaseContract.IDSystem=' || TO_VARCHAR("ASSOC_BaseContract.IDSystem") || ' ' ||
                'ASSOC_SettlementAccount.FinancialContractID=' || TO_VARCHAR("ASSOC_SettlementAccount.FinancialContractID") || ' ' ||
                'ASSOC_SettlementAccount.IDSystem=' || TO_VARCHAR("ASSOC_SettlementAccount.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SettlementAccountRole",
                        "IN"."ASSOC_BaseContract.FinancialContractID",
                        "IN"."ASSOC_BaseContract.IDSystem",
                        "IN"."ASSOC_SettlementAccount.FinancialContractID",
                        "IN"."ASSOC_SettlementAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SettlementAccountRole",
                        "IN"."ASSOC_BaseContract.FinancialContractID",
                        "IN"."ASSOC_BaseContract.IDSystem",
                        "IN"."ASSOC_SettlementAccount.FinancialContractID",
                        "IN"."ASSOC_SettlementAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SettlementAccountRole",
                        "ASSOC_BaseContract.FinancialContractID",
                        "ASSOC_BaseContract.IDSystem",
                        "ASSOC_SettlementAccount.FinancialContractID",
                        "ASSOC_SettlementAccount.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SettlementAccountRole",
                                    "IN"."ASSOC_BaseContract.FinancialContractID",
                                    "IN"."ASSOC_BaseContract.IDSystem",
                                    "IN"."ASSOC_SettlementAccount.FinancialContractID",
                                    "IN"."ASSOC_SettlementAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SettlementAccountRole",
                                    "IN"."ASSOC_BaseContract.FinancialContractID",
                                    "IN"."ASSOC_BaseContract.IDSystem",
                                    "IN"."ASSOC_SettlementAccount.FinancialContractID",
                                    "IN"."ASSOC_SettlementAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "SettlementAccountRole" is null and
            "ASSOC_BaseContract.FinancialContractID" is null and
            "ASSOC_BaseContract.IDSystem" is null and
            "ASSOC_SettlementAccount.FinancialContractID" is null and
            "ASSOC_SettlementAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "SettlementAccountRole",
            "ASSOC_BaseContract.FinancialContractID",
            "ASSOC_BaseContract.IDSystem",
            "ASSOC_SettlementAccount.FinancialContractID",
            "ASSOC_SettlementAccount.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::SettlementAccountAssignment" WHERE
            (
            "SettlementAccountRole" ,
            "ASSOC_BaseContract.FinancialContractID" ,
            "ASSOC_BaseContract.IDSystem" ,
            "ASSOC_SettlementAccount.FinancialContractID" ,
            "ASSOC_SettlementAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."SettlementAccountRole",
            "OLD"."ASSOC_BaseContract.FinancialContractID",
            "OLD"."ASSOC_BaseContract.IDSystem",
            "OLD"."ASSOC_SettlementAccount.FinancialContractID",
            "OLD"."ASSOC_SettlementAccount.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::SettlementAccountAssignment" as "OLD"
        on
                              "IN"."SettlementAccountRole" = "OLD"."SettlementAccountRole" and
                              "IN"."ASSOC_BaseContract.FinancialContractID" = "OLD"."ASSOC_BaseContract.FinancialContractID" and
                              "IN"."ASSOC_BaseContract.IDSystem" = "OLD"."ASSOC_BaseContract.IDSystem" and
                              "IN"."ASSOC_SettlementAccount.FinancialContractID" = "OLD"."ASSOC_SettlementAccount.FinancialContractID" and
                              "IN"."ASSOC_SettlementAccount.IDSystem" = "OLD"."ASSOC_SettlementAccount.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "SettlementAccountRole",
        "ASSOC_BaseContract.FinancialContractID",
        "ASSOC_BaseContract.IDSystem",
        "ASSOC_SettlementAccount.FinancialContractID",
        "ASSOC_SettlementAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_ElectronicFundsTransferMethod.ElectronicFundsTransferMethodName",
        "_ElectronicFundsTransferMethod.ElectronicFundsTransferMethodType",
        "_ElectronicFundsTransferMethod._BusinessPartner.BusinessPartnerID",
        "DayOfMonthOfSettlement",
        "DirectSettlement",
        "ExternalSettlementAccountIdentificationSystem",
        "ExternalSettlementBankAccountCurrency",
        "ExternalSettlementBankAccountHolderName",
        "ExternalSettlementBankAccountID",
        "ExternalSettlementBankID",
        "ExternalSettlementBankIdentificationSystem",
        "ExternalSettlementBankName",
        "ExternalSettlementIBAN",
        "ExternalSettlementSWIFT",
        "SettlementContractAssignmentCategory",
        "SettlementMethod",
        "SettlementPeriodLength",
        "SettlementPeriodTimeUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_SettlementAccountRole" as "SettlementAccountRole" ,
            "OLD_ASSOC_BaseContract.FinancialContractID" as "ASSOC_BaseContract.FinancialContractID" ,
            "OLD_ASSOC_BaseContract.IDSystem" as "ASSOC_BaseContract.IDSystem" ,
            "OLD_ASSOC_SettlementAccount.FinancialContractID" as "ASSOC_SettlementAccount.FinancialContractID" ,
            "OLD_ASSOC_SettlementAccount.IDSystem" as "ASSOC_SettlementAccount.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__ElectronicFundsTransferMethod.ElectronicFundsTransferMethodName" as "_ElectronicFundsTransferMethod.ElectronicFundsTransferMethodName" ,
            "OLD__ElectronicFundsTransferMethod.ElectronicFundsTransferMethodType" as "_ElectronicFundsTransferMethod.ElectronicFundsTransferMethodType" ,
            "OLD__ElectronicFundsTransferMethod._BusinessPartner.BusinessPartnerID" as "_ElectronicFundsTransferMethod._BusinessPartner.BusinessPartnerID" ,
            "OLD_DayOfMonthOfSettlement" as "DayOfMonthOfSettlement" ,
            "OLD_DirectSettlement" as "DirectSettlement" ,
            "OLD_ExternalSettlementAccountIdentificationSystem" as "ExternalSettlementAccountIdentificationSystem" ,
            "OLD_ExternalSettlementBankAccountCurrency" as "ExternalSettlementBankAccountCurrency" ,
            "OLD_ExternalSettlementBankAccountHolderName" as "ExternalSettlementBankAccountHolderName" ,
            "OLD_ExternalSettlementBankAccountID" as "ExternalSettlementBankAccountID" ,
            "OLD_ExternalSettlementBankID" as "ExternalSettlementBankID" ,
            "OLD_ExternalSettlementBankIdentificationSystem" as "ExternalSettlementBankIdentificationSystem" ,
            "OLD_ExternalSettlementBankName" as "ExternalSettlementBankName" ,
            "OLD_ExternalSettlementIBAN" as "ExternalSettlementIBAN" ,
            "OLD_ExternalSettlementSWIFT" as "ExternalSettlementSWIFT" ,
            "OLD_SettlementContractAssignmentCategory" as "SettlementContractAssignmentCategory" ,
            "OLD_SettlementMethod" as "SettlementMethod" ,
            "OLD_SettlementPeriodLength" as "SettlementPeriodLength" ,
            "OLD_SettlementPeriodTimeUnit" as "SettlementPeriodTimeUnit" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."SettlementAccountRole",
                        "OLD"."ASSOC_BaseContract.FinancialContractID",
                        "OLD"."ASSOC_BaseContract.IDSystem",
                        "OLD"."ASSOC_SettlementAccount.FinancialContractID",
                        "OLD"."ASSOC_SettlementAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."SettlementAccountRole" AS "OLD_SettlementAccountRole" ,
                "OLD"."ASSOC_BaseContract.FinancialContractID" AS "OLD_ASSOC_BaseContract.FinancialContractID" ,
                "OLD"."ASSOC_BaseContract.IDSystem" AS "OLD_ASSOC_BaseContract.IDSystem" ,
                "OLD"."ASSOC_SettlementAccount.FinancialContractID" AS "OLD_ASSOC_SettlementAccount.FinancialContractID" ,
                "OLD"."ASSOC_SettlementAccount.IDSystem" AS "OLD_ASSOC_SettlementAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_ElectronicFundsTransferMethod.ElectronicFundsTransferMethodName" AS "OLD__ElectronicFundsTransferMethod.ElectronicFundsTransferMethodName" ,
                "OLD"."_ElectronicFundsTransferMethod.ElectronicFundsTransferMethodType" AS "OLD__ElectronicFundsTransferMethod.ElectronicFundsTransferMethodType" ,
                "OLD"."_ElectronicFundsTransferMethod._BusinessPartner.BusinessPartnerID" AS "OLD__ElectronicFundsTransferMethod._BusinessPartner.BusinessPartnerID" ,
                "OLD"."DayOfMonthOfSettlement" AS "OLD_DayOfMonthOfSettlement" ,
                "OLD"."DirectSettlement" AS "OLD_DirectSettlement" ,
                "OLD"."ExternalSettlementAccountIdentificationSystem" AS "OLD_ExternalSettlementAccountIdentificationSystem" ,
                "OLD"."ExternalSettlementBankAccountCurrency" AS "OLD_ExternalSettlementBankAccountCurrency" ,
                "OLD"."ExternalSettlementBankAccountHolderName" AS "OLD_ExternalSettlementBankAccountHolderName" ,
                "OLD"."ExternalSettlementBankAccountID" AS "OLD_ExternalSettlementBankAccountID" ,
                "OLD"."ExternalSettlementBankID" AS "OLD_ExternalSettlementBankID" ,
                "OLD"."ExternalSettlementBankIdentificationSystem" AS "OLD_ExternalSettlementBankIdentificationSystem" ,
                "OLD"."ExternalSettlementBankName" AS "OLD_ExternalSettlementBankName" ,
                "OLD"."ExternalSettlementIBAN" AS "OLD_ExternalSettlementIBAN" ,
                "OLD"."ExternalSettlementSWIFT" AS "OLD_ExternalSettlementSWIFT" ,
                "OLD"."SettlementContractAssignmentCategory" AS "OLD_SettlementContractAssignmentCategory" ,
                "OLD"."SettlementMethod" AS "OLD_SettlementMethod" ,
                "OLD"."SettlementPeriodLength" AS "OLD_SettlementPeriodLength" ,
                "OLD"."SettlementPeriodTimeUnit" AS "OLD_SettlementPeriodTimeUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SettlementAccountAssignment" as "OLD"
            on
                                      "IN"."SettlementAccountRole" = "OLD"."SettlementAccountRole" and
                                      "IN"."ASSOC_BaseContract.FinancialContractID" = "OLD"."ASSOC_BaseContract.FinancialContractID" and
                                      "IN"."ASSOC_BaseContract.IDSystem" = "OLD"."ASSOC_BaseContract.IDSystem" and
                                      "IN"."ASSOC_SettlementAccount.FinancialContractID" = "OLD"."ASSOC_SettlementAccount.FinancialContractID" and
                                      "IN"."ASSOC_SettlementAccount.IDSystem" = "OLD"."ASSOC_SettlementAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_SettlementAccountRole" as "SettlementAccountRole",
            "OLD_ASSOC_BaseContract.FinancialContractID" as "ASSOC_BaseContract.FinancialContractID",
            "OLD_ASSOC_BaseContract.IDSystem" as "ASSOC_BaseContract.IDSystem",
            "OLD_ASSOC_SettlementAccount.FinancialContractID" as "ASSOC_SettlementAccount.FinancialContractID",
            "OLD_ASSOC_SettlementAccount.IDSystem" as "ASSOC_SettlementAccount.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__ElectronicFundsTransferMethod.ElectronicFundsTransferMethodName" as "_ElectronicFundsTransferMethod.ElectronicFundsTransferMethodName",
            "OLD__ElectronicFundsTransferMethod.ElectronicFundsTransferMethodType" as "_ElectronicFundsTransferMethod.ElectronicFundsTransferMethodType",
            "OLD__ElectronicFundsTransferMethod._BusinessPartner.BusinessPartnerID" as "_ElectronicFundsTransferMethod._BusinessPartner.BusinessPartnerID",
            "OLD_DayOfMonthOfSettlement" as "DayOfMonthOfSettlement",
            "OLD_DirectSettlement" as "DirectSettlement",
            "OLD_ExternalSettlementAccountIdentificationSystem" as "ExternalSettlementAccountIdentificationSystem",
            "OLD_ExternalSettlementBankAccountCurrency" as "ExternalSettlementBankAccountCurrency",
            "OLD_ExternalSettlementBankAccountHolderName" as "ExternalSettlementBankAccountHolderName",
            "OLD_ExternalSettlementBankAccountID" as "ExternalSettlementBankAccountID",
            "OLD_ExternalSettlementBankID" as "ExternalSettlementBankID",
            "OLD_ExternalSettlementBankIdentificationSystem" as "ExternalSettlementBankIdentificationSystem",
            "OLD_ExternalSettlementBankName" as "ExternalSettlementBankName",
            "OLD_ExternalSettlementIBAN" as "ExternalSettlementIBAN",
            "OLD_ExternalSettlementSWIFT" as "ExternalSettlementSWIFT",
            "OLD_SettlementContractAssignmentCategory" as "SettlementContractAssignmentCategory",
            "OLD_SettlementMethod" as "SettlementMethod",
            "OLD_SettlementPeriodLength" as "SettlementPeriodLength",
            "OLD_SettlementPeriodTimeUnit" as "SettlementPeriodTimeUnit",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."SettlementAccountRole",
                        "OLD"."ASSOC_BaseContract.FinancialContractID",
                        "OLD"."ASSOC_BaseContract.IDSystem",
                        "OLD"."ASSOC_SettlementAccount.FinancialContractID",
                        "OLD"."ASSOC_SettlementAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SettlementAccountRole" AS "OLD_SettlementAccountRole" ,
                "OLD"."ASSOC_BaseContract.FinancialContractID" AS "OLD_ASSOC_BaseContract.FinancialContractID" ,
                "OLD"."ASSOC_BaseContract.IDSystem" AS "OLD_ASSOC_BaseContract.IDSystem" ,
                "OLD"."ASSOC_SettlementAccount.FinancialContractID" AS "OLD_ASSOC_SettlementAccount.FinancialContractID" ,
                "OLD"."ASSOC_SettlementAccount.IDSystem" AS "OLD_ASSOC_SettlementAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_ElectronicFundsTransferMethod.ElectronicFundsTransferMethodName" AS "OLD__ElectronicFundsTransferMethod.ElectronicFundsTransferMethodName" ,
                "OLD"."_ElectronicFundsTransferMethod.ElectronicFundsTransferMethodType" AS "OLD__ElectronicFundsTransferMethod.ElectronicFundsTransferMethodType" ,
                "OLD"."_ElectronicFundsTransferMethod._BusinessPartner.BusinessPartnerID" AS "OLD__ElectronicFundsTransferMethod._BusinessPartner.BusinessPartnerID" ,
                "OLD"."DayOfMonthOfSettlement" AS "OLD_DayOfMonthOfSettlement" ,
                "OLD"."DirectSettlement" AS "OLD_DirectSettlement" ,
                "OLD"."ExternalSettlementAccountIdentificationSystem" AS "OLD_ExternalSettlementAccountIdentificationSystem" ,
                "OLD"."ExternalSettlementBankAccountCurrency" AS "OLD_ExternalSettlementBankAccountCurrency" ,
                "OLD"."ExternalSettlementBankAccountHolderName" AS "OLD_ExternalSettlementBankAccountHolderName" ,
                "OLD"."ExternalSettlementBankAccountID" AS "OLD_ExternalSettlementBankAccountID" ,
                "OLD"."ExternalSettlementBankID" AS "OLD_ExternalSettlementBankID" ,
                "OLD"."ExternalSettlementBankIdentificationSystem" AS "OLD_ExternalSettlementBankIdentificationSystem" ,
                "OLD"."ExternalSettlementBankName" AS "OLD_ExternalSettlementBankName" ,
                "OLD"."ExternalSettlementIBAN" AS "OLD_ExternalSettlementIBAN" ,
                "OLD"."ExternalSettlementSWIFT" AS "OLD_ExternalSettlementSWIFT" ,
                "OLD"."SettlementContractAssignmentCategory" AS "OLD_SettlementContractAssignmentCategory" ,
                "OLD"."SettlementMethod" AS "OLD_SettlementMethod" ,
                "OLD"."SettlementPeriodLength" AS "OLD_SettlementPeriodLength" ,
                "OLD"."SettlementPeriodTimeUnit" AS "OLD_SettlementPeriodTimeUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SettlementAccountAssignment" as "OLD"
            on
               "IN"."SettlementAccountRole" = "OLD"."SettlementAccountRole" and
               "IN"."ASSOC_BaseContract.FinancialContractID" = "OLD"."ASSOC_BaseContract.FinancialContractID" and
               "IN"."ASSOC_BaseContract.IDSystem" = "OLD"."ASSOC_BaseContract.IDSystem" and
               "IN"."ASSOC_SettlementAccount.FinancialContractID" = "OLD"."ASSOC_SettlementAccount.FinancialContractID" and
               "IN"."ASSOC_SettlementAccount.IDSystem" = "OLD"."ASSOC_SettlementAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
