PROCEDURE "sap.fsdm.procedures::SpecialRuleLoad" (IN ROW "sap.fsdm.tabletypes::SpecialRuleTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SpecialRuleValidityStartDate=' || TO_VARCHAR("SpecialRuleValidityStartDate") || ' ' ||
                '_BusinessCalendar.BusinessCalendar=' || TO_VARCHAR("_BusinessCalendar.BusinessCalendar") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SpecialRuleValidityStartDate",
                        "IN"."_BusinessCalendar.BusinessCalendar"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SpecialRuleValidityStartDate",
                        "IN"."_BusinessCalendar.BusinessCalendar"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SpecialRuleValidityStartDate",
                        "_BusinessCalendar.BusinessCalendar"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SpecialRuleValidityStartDate",
                                    "IN"."_BusinessCalendar.BusinessCalendar"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SpecialRuleValidityStartDate",
                                    "IN"."_BusinessCalendar.BusinessCalendar"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::SpecialRule" (
        "SpecialRuleValidityStartDate",
        "_BusinessCalendar.BusinessCalendar",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Description",
        "IsWorkingDay",
        "SpecialRuleValidityEndDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SpecialRuleValidityStartDate" as "SpecialRuleValidityStartDate" ,
            "OLD__BusinessCalendar.BusinessCalendar" as "_BusinessCalendar.BusinessCalendar" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Description" as "Description" ,
            "OLD_IsWorkingDay" as "IsWorkingDay" ,
            "OLD_SpecialRuleValidityEndDate" as "SpecialRuleValidityEndDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."SpecialRuleValidityStartDate",
                        "IN"."_BusinessCalendar.BusinessCalendar",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."SpecialRuleValidityStartDate" as "OLD_SpecialRuleValidityStartDate",
                                "OLD"."_BusinessCalendar.BusinessCalendar" as "OLD__BusinessCalendar.BusinessCalendar",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."Description" as "OLD_Description",
                                "OLD"."IsWorkingDay" as "OLD_IsWorkingDay",
                                "OLD"."SpecialRuleValidityEndDate" as "OLD_SpecialRuleValidityEndDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::SpecialRule" as "OLD"
            on
                ifnull( "IN"."SpecialRuleValidityStartDate", '0001-01-01') = "OLD"."SpecialRuleValidityStartDate" and
                ifnull( "IN"."_BusinessCalendar.BusinessCalendar", '') = "OLD"."_BusinessCalendar.BusinessCalendar" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::SpecialRule" (
        "SpecialRuleValidityStartDate",
        "_BusinessCalendar.BusinessCalendar",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Description",
        "IsWorkingDay",
        "SpecialRuleValidityEndDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SpecialRuleValidityStartDate"  as "SpecialRuleValidityStartDate",
            "OLD__BusinessCalendar.BusinessCalendar"  as "_BusinessCalendar.BusinessCalendar",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_Description"  as "Description",
            "OLD_IsWorkingDay"  as "IsWorkingDay",
            "OLD_SpecialRuleValidityEndDate"  as "SpecialRuleValidityEndDate",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."SpecialRuleValidityStartDate",
                        "IN"."_BusinessCalendar.BusinessCalendar",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."SpecialRuleValidityStartDate" as "OLD_SpecialRuleValidityStartDate",
                        "OLD"."_BusinessCalendar.BusinessCalendar" as "OLD__BusinessCalendar.BusinessCalendar",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."Description" as "OLD_Description",
                        "OLD"."IsWorkingDay" as "OLD_IsWorkingDay",
                        "OLD"."SpecialRuleValidityEndDate" as "OLD_SpecialRuleValidityEndDate",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::SpecialRule" as "OLD"
            on
                ifnull( "IN"."SpecialRuleValidityStartDate", '0001-01-01' ) = "OLD"."SpecialRuleValidityStartDate" and
                ifnull( "IN"."_BusinessCalendar.BusinessCalendar", '' ) = "OLD"."_BusinessCalendar.BusinessCalendar" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::SpecialRule"
    where (
        "SpecialRuleValidityStartDate",
        "_BusinessCalendar.BusinessCalendar",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."SpecialRuleValidityStartDate",
            "OLD"."_BusinessCalendar.BusinessCalendar",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::SpecialRule" as "OLD"
        on
           ifnull( "IN"."SpecialRuleValidityStartDate", '0001-01-01' ) = "OLD"."SpecialRuleValidityStartDate" and
           ifnull( "IN"."_BusinessCalendar.BusinessCalendar", '' ) = "OLD"."_BusinessCalendar.BusinessCalendar" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::SpecialRule" (
        "SpecialRuleValidityStartDate",
        "_BusinessCalendar.BusinessCalendar",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Description",
        "IsWorkingDay",
        "SpecialRuleValidityEndDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "SpecialRuleValidityStartDate", '0001-01-01' ) as "SpecialRuleValidityStartDate",
            ifnull( "_BusinessCalendar.BusinessCalendar", '' ) as "_BusinessCalendar.BusinessCalendar",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "Description"  ,
            "IsWorkingDay"  ,
            "SpecialRuleValidityEndDate"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END