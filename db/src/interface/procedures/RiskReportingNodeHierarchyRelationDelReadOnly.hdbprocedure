PROCEDURE "sap.fsdm.procedures::RiskReportingNodeHierarchyRelationDelReadOnly" (IN ROW "sap.fsdm.tabletypes::RiskReportingNodeHierarchyRelationTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::RiskReportingNodeHierarchyRelationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::RiskReportingNodeHierarchyRelationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'RiskReportingHierarchyID=' || TO_VARCHAR("RiskReportingHierarchyID") || ' ' ||
                '_Child.RiskReportingNodeID=' || TO_VARCHAR("_Child.RiskReportingNodeID") || ' ' ||
                '_Parent.RiskReportingNodeID=' || TO_VARCHAR("_Parent.RiskReportingNodeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."RiskReportingHierarchyID",
                        "IN"."_Child.RiskReportingNodeID",
                        "IN"."_Parent.RiskReportingNodeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."RiskReportingHierarchyID",
                        "IN"."_Child.RiskReportingNodeID",
                        "IN"."_Parent.RiskReportingNodeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "RiskReportingHierarchyID",
                        "_Child.RiskReportingNodeID",
                        "_Parent.RiskReportingNodeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."RiskReportingHierarchyID",
                                    "IN"."_Child.RiskReportingNodeID",
                                    "IN"."_Parent.RiskReportingNodeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."RiskReportingHierarchyID",
                                    "IN"."_Child.RiskReportingNodeID",
                                    "IN"."_Parent.RiskReportingNodeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "RiskReportingHierarchyID" is null and
            "_Child.RiskReportingNodeID" is null and
            "_Parent.RiskReportingNodeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "RiskReportingHierarchyID",
            "_Child.RiskReportingNodeID",
            "_Parent.RiskReportingNodeID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::RiskReportingNodeHierarchyRelation" WHERE
            (
            "RiskReportingHierarchyID" ,
            "_Child.RiskReportingNodeID" ,
            "_Parent.RiskReportingNodeID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."RiskReportingHierarchyID",
            "OLD"."_Child.RiskReportingNodeID",
            "OLD"."_Parent.RiskReportingNodeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::RiskReportingNodeHierarchyRelation" as "OLD"
        on
                              "IN"."RiskReportingHierarchyID" = "OLD"."RiskReportingHierarchyID" and
                              "IN"."_Child.RiskReportingNodeID" = "OLD"."_Child.RiskReportingNodeID" and
                              "IN"."_Parent.RiskReportingNodeID" = "OLD"."_Parent.RiskReportingNodeID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "RiskReportingHierarchyID",
        "_Child.RiskReportingNodeID",
        "_Parent.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "HierarchyRank",
        "RiskReportingHierarchyName",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_RiskReportingHierarchyID" as "RiskReportingHierarchyID" ,
            "OLD__Child.RiskReportingNodeID" as "_Child.RiskReportingNodeID" ,
            "OLD__Parent.RiskReportingNodeID" as "_Parent.RiskReportingNodeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_HierarchyRank" as "HierarchyRank" ,
            "OLD_RiskReportingHierarchyName" as "RiskReportingHierarchyName" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."RiskReportingHierarchyID",
                        "OLD"."_Child.RiskReportingNodeID",
                        "OLD"."_Parent.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."RiskReportingHierarchyID" AS "OLD_RiskReportingHierarchyID" ,
                "OLD"."_Child.RiskReportingNodeID" AS "OLD__Child.RiskReportingNodeID" ,
                "OLD"."_Parent.RiskReportingNodeID" AS "OLD__Parent.RiskReportingNodeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."HierarchyRank" AS "OLD_HierarchyRank" ,
                "OLD"."RiskReportingHierarchyName" AS "OLD_RiskReportingHierarchyName" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::RiskReportingNodeHierarchyRelation" as "OLD"
            on
                                      "IN"."RiskReportingHierarchyID" = "OLD"."RiskReportingHierarchyID" and
                                      "IN"."_Child.RiskReportingNodeID" = "OLD"."_Child.RiskReportingNodeID" and
                                      "IN"."_Parent.RiskReportingNodeID" = "OLD"."_Parent.RiskReportingNodeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_RiskReportingHierarchyID" as "RiskReportingHierarchyID",
            "OLD__Child.RiskReportingNodeID" as "_Child.RiskReportingNodeID",
            "OLD__Parent.RiskReportingNodeID" as "_Parent.RiskReportingNodeID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_HierarchyRank" as "HierarchyRank",
            "OLD_RiskReportingHierarchyName" as "RiskReportingHierarchyName",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."RiskReportingHierarchyID",
                        "OLD"."_Child.RiskReportingNodeID",
                        "OLD"."_Parent.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."RiskReportingHierarchyID" AS "OLD_RiskReportingHierarchyID" ,
                "OLD"."_Child.RiskReportingNodeID" AS "OLD__Child.RiskReportingNodeID" ,
                "OLD"."_Parent.RiskReportingNodeID" AS "OLD__Parent.RiskReportingNodeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."HierarchyRank" AS "OLD_HierarchyRank" ,
                "OLD"."RiskReportingHierarchyName" AS "OLD_RiskReportingHierarchyName" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::RiskReportingNodeHierarchyRelation" as "OLD"
            on
               "IN"."RiskReportingHierarchyID" = "OLD"."RiskReportingHierarchyID" and
               "IN"."_Child.RiskReportingNodeID" = "OLD"."_Child.RiskReportingNodeID" and
               "IN"."_Parent.RiskReportingNodeID" = "OLD"."_Parent.RiskReportingNodeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
