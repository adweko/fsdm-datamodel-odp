PROCEDURE "sap.fsdm.procedures::TimeIntervalPriceObservationErase" (IN ROW "sap.fsdm.tabletypes::TimeIntervalPriceObservationTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "CleanDirtyIndicator" is null and
            "PriceDataProvider" is null and
            "PriceSeriesType" is null and
            "TimeIntervalEnd" is null and
            "TimeIntervalStart" is null and
            "_Exchange.MarketIdentifierCode" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::TimeIntervalPriceObservation"
        WHERE
        (            "CleanDirtyIndicator" ,
            "PriceDataProvider" ,
            "PriceSeriesType" ,
            "TimeIntervalEnd" ,
            "TimeIntervalStart" ,
            "_Exchange.MarketIdentifierCode" ,
            "_FinancialInstrument.FinancialInstrumentID" 
        ) in
        (
            select                 "OLD"."CleanDirtyIndicator" ,
                "OLD"."PriceDataProvider" ,
                "OLD"."PriceSeriesType" ,
                "OLD"."TimeIntervalEnd" ,
                "OLD"."TimeIntervalStart" ,
                "OLD"."_Exchange.MarketIdentifierCode" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::TimeIntervalPriceObservation" "OLD"
            on
            "IN"."CleanDirtyIndicator" = "OLD"."CleanDirtyIndicator" and
            "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
            "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
            "IN"."TimeIntervalEnd" = "OLD"."TimeIntervalEnd" and
            "IN"."TimeIntervalStart" = "OLD"."TimeIntervalStart" and
            "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

        --delete data from history table
        delete from "sap.fsdm::TimeIntervalPriceObservation_Historical"
        WHERE
        (
            "CleanDirtyIndicator" ,
            "PriceDataProvider" ,
            "PriceSeriesType" ,
            "TimeIntervalEnd" ,
            "TimeIntervalStart" ,
            "_Exchange.MarketIdentifierCode" ,
            "_FinancialInstrument.FinancialInstrumentID" 
        ) in
        (
            select
                "OLD"."CleanDirtyIndicator" ,
                "OLD"."PriceDataProvider" ,
                "OLD"."PriceSeriesType" ,
                "OLD"."TimeIntervalEnd" ,
                "OLD"."TimeIntervalStart" ,
                "OLD"."_Exchange.MarketIdentifierCode" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::TimeIntervalPriceObservation_Historical" "OLD"
            on
                "IN"."CleanDirtyIndicator" = "OLD"."CleanDirtyIndicator" and
                "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
                "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
                "IN"."TimeIntervalEnd" = "OLD"."TimeIntervalEnd" and
                "IN"."TimeIntervalStart" = "OLD"."TimeIntervalStart" and
                "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

END
