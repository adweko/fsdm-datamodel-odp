PROCEDURE "sap.fsdm.procedures::PhysicalAssetStatusDelete" (IN ROW "sap.fsdm.tabletypes::PhysicalAssetStatusTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PhysicalAssetStatusCategory=' || TO_VARCHAR("PhysicalAssetStatusCategory") || ' ' ||
                'PhysicalAssetStatusType=' || TO_VARCHAR("PhysicalAssetStatusType") || ' ' ||
                '_PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_PhysicalAsset.PhysicalAssetID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PhysicalAssetStatusCategory",
                        "IN"."PhysicalAssetStatusType",
                        "IN"."_PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PhysicalAssetStatusCategory",
                        "IN"."PhysicalAssetStatusType",
                        "IN"."_PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PhysicalAssetStatusCategory",
                        "PhysicalAssetStatusType",
                        "_PhysicalAsset.PhysicalAssetID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PhysicalAssetStatusCategory",
                                    "IN"."PhysicalAssetStatusType",
                                    "IN"."_PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PhysicalAssetStatusCategory",
                                    "IN"."PhysicalAssetStatusType",
                                    "IN"."_PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "PhysicalAssetStatusCategory" is null and
            "PhysicalAssetStatusType" is null and
            "_PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::PhysicalAssetStatus" (
        "PhysicalAssetStatusCategory",
        "PhysicalAssetStatusType",
        "_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_EmployeeResponsibleForStatusChange.BusinessPartnerID",
        "NextStatusReviewDate",
        "Status",
        "StatusChangeDate",
        "StatusChangeReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PhysicalAssetStatusCategory" as "PhysicalAssetStatusCategory" ,
            "OLD_PhysicalAssetStatusType" as "PhysicalAssetStatusType" ,
            "OLD__PhysicalAsset.PhysicalAssetID" as "_PhysicalAsset.PhysicalAssetID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__EmployeeResponsibleForStatusChange.BusinessPartnerID" as "_EmployeeResponsibleForStatusChange.BusinessPartnerID" ,
            "OLD_NextStatusReviewDate" as "NextStatusReviewDate" ,
            "OLD_Status" as "Status" ,
            "OLD_StatusChangeDate" as "StatusChangeDate" ,
            "OLD_StatusChangeReason" as "StatusChangeReason" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."PhysicalAssetStatusCategory",
                        "OLD"."PhysicalAssetStatusType",
                        "OLD"."_PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."PhysicalAssetStatusCategory" AS "OLD_PhysicalAssetStatusCategory" ,
                "OLD"."PhysicalAssetStatusType" AS "OLD_PhysicalAssetStatusType" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" AS "OLD__PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_EmployeeResponsibleForStatusChange.BusinessPartnerID" AS "OLD__EmployeeResponsibleForStatusChange.BusinessPartnerID" ,
                "OLD"."NextStatusReviewDate" AS "OLD_NextStatusReviewDate" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeDate" AS "OLD_StatusChangeDate" ,
                "OLD"."StatusChangeReason" AS "OLD_StatusChangeReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PhysicalAssetStatus" as "OLD"
            on
                      "IN"."PhysicalAssetStatusCategory" = "OLD"."PhysicalAssetStatusCategory" and
                      "IN"."PhysicalAssetStatusType" = "OLD"."PhysicalAssetStatusType" and
                      "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::PhysicalAssetStatus" (
        "PhysicalAssetStatusCategory",
        "PhysicalAssetStatusType",
        "_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_EmployeeResponsibleForStatusChange.BusinessPartnerID",
        "NextStatusReviewDate",
        "Status",
        "StatusChangeDate",
        "StatusChangeReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PhysicalAssetStatusCategory" as "PhysicalAssetStatusCategory",
            "OLD_PhysicalAssetStatusType" as "PhysicalAssetStatusType",
            "OLD__PhysicalAsset.PhysicalAssetID" as "_PhysicalAsset.PhysicalAssetID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__EmployeeResponsibleForStatusChange.BusinessPartnerID" as "_EmployeeResponsibleForStatusChange.BusinessPartnerID",
            "OLD_NextStatusReviewDate" as "NextStatusReviewDate",
            "OLD_Status" as "Status",
            "OLD_StatusChangeDate" as "StatusChangeDate",
            "OLD_StatusChangeReason" as "StatusChangeReason",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."PhysicalAssetStatusCategory",
                        "OLD"."PhysicalAssetStatusType",
                        "OLD"."_PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."PhysicalAssetStatusCategory" AS "OLD_PhysicalAssetStatusCategory" ,
                "OLD"."PhysicalAssetStatusType" AS "OLD_PhysicalAssetStatusType" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" AS "OLD__PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_EmployeeResponsibleForStatusChange.BusinessPartnerID" AS "OLD__EmployeeResponsibleForStatusChange.BusinessPartnerID" ,
                "OLD"."NextStatusReviewDate" AS "OLD_NextStatusReviewDate" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeDate" AS "OLD_StatusChangeDate" ,
                "OLD"."StatusChangeReason" AS "OLD_StatusChangeReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PhysicalAssetStatus" as "OLD"
            on
                                                "IN"."PhysicalAssetStatusCategory" = "OLD"."PhysicalAssetStatusCategory" and
                                                "IN"."PhysicalAssetStatusType" = "OLD"."PhysicalAssetStatusType" and
                                                "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::PhysicalAssetStatus"
    where (
        "PhysicalAssetStatusCategory",
        "PhysicalAssetStatusType",
        "_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."PhysicalAssetStatusCategory",
            "OLD"."PhysicalAssetStatusType",
            "OLD"."_PhysicalAsset.PhysicalAssetID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PhysicalAssetStatus" as "OLD"
        on
                                       "IN"."PhysicalAssetStatusCategory" = "OLD"."PhysicalAssetStatusCategory" and
                                       "IN"."PhysicalAssetStatusType" = "OLD"."PhysicalAssetStatusType" and
                                       "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
