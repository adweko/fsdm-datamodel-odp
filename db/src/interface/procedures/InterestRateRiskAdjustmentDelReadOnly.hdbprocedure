PROCEDURE "sap.fsdm.procedures::InterestRateRiskAdjustmentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::InterestRateRiskAdjustmentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::InterestRateRiskAdjustmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::InterestRateRiskAdjustmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AccountingChangeSequenceNumber=' || TO_VARCHAR("AccountingChangeSequenceNumber") || ' ' ||
                'IRRCalculationMethod=' || TO_VARCHAR("IRRCalculationMethod") || ' ' ||
                'IndicatorResultBeforeChange=' || TO_VARCHAR("IndicatorResultBeforeChange") || ' ' ||
                '_AccountingSystem.AccountingSystemID=' || TO_VARCHAR("_AccountingSystem.AccountingSystemID") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_InvestmentAccount.FinancialContractID=' || TO_VARCHAR("_InvestmentAccount.FinancialContractID") || ' ' ||
                '_InvestmentAccount.IDSystem=' || TO_VARCHAR("_InvestmentAccount.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AccountingChangeSequenceNumber",
                        "IN"."IRRCalculationMethod",
                        "IN"."IndicatorResultBeforeChange",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InvestmentAccount.FinancialContractID",
                        "IN"."_InvestmentAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AccountingChangeSequenceNumber",
                        "IN"."IRRCalculationMethod",
                        "IN"."IndicatorResultBeforeChange",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InvestmentAccount.FinancialContractID",
                        "IN"."_InvestmentAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AccountingChangeSequenceNumber",
                        "IRRCalculationMethod",
                        "IndicatorResultBeforeChange",
                        "_AccountingSystem.AccountingSystemID",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_InvestmentAccount.FinancialContractID",
                        "_InvestmentAccount.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AccountingChangeSequenceNumber",
                                    "IN"."IRRCalculationMethod",
                                    "IN"."IndicatorResultBeforeChange",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_InvestmentAccount.FinancialContractID",
                                    "IN"."_InvestmentAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AccountingChangeSequenceNumber",
                                    "IN"."IRRCalculationMethod",
                                    "IN"."IndicatorResultBeforeChange",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_InvestmentAccount.FinancialContractID",
                                    "IN"."_InvestmentAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "AccountingChangeSequenceNumber" is null and
            "IRRCalculationMethod" is null and
            "IndicatorResultBeforeChange" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_InvestmentAccount.FinancialContractID" is null and
            "_InvestmentAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "AccountingChangeSequenceNumber",
            "IRRCalculationMethod",
            "IndicatorResultBeforeChange",
            "_AccountingSystem.AccountingSystemID",
            "_FinancialContract.FinancialContractID",
            "_FinancialContract.IDSystem",
            "_FinancialInstrument.FinancialInstrumentID",
            "_InvestmentAccount.FinancialContractID",
            "_InvestmentAccount.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::InterestRateRiskAdjustment" WHERE
            (
            "AccountingChangeSequenceNumber" ,
            "IRRCalculationMethod" ,
            "IndicatorResultBeforeChange" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InvestmentAccount.FinancialContractID" ,
            "_InvestmentAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."AccountingChangeSequenceNumber",
            "OLD"."IRRCalculationMethod",
            "OLD"."IndicatorResultBeforeChange",
            "OLD"."_AccountingSystem.AccountingSystemID",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_InvestmentAccount.FinancialContractID",
            "OLD"."_InvestmentAccount.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::InterestRateRiskAdjustment" as "OLD"
        on
                              "IN"."AccountingChangeSequenceNumber" = "OLD"."AccountingChangeSequenceNumber" and
                              "IN"."IRRCalculationMethod" = "OLD"."IRRCalculationMethod" and
                              "IN"."IndicatorResultBeforeChange" = "OLD"."IndicatorResultBeforeChange" and
                              "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                              "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                              "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                              "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                              "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                              "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "AccountingChangeSequenceNumber",
        "IRRCalculationMethod",
        "IndicatorResultBeforeChange",
        "_AccountingSystem.AccountingSystemID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InvestmentAccount.FinancialContractID",
        "_InvestmentAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AccountingChangeDate",
        "AccountingChangeReason",
        "IRRHedgePaymentCurrency",
        "IRRHedgePositionCurrency",
        "IRRPaymentCurrency",
        "IRRPositionCurrency",
        "IRRiskAdjHedgedInPaymentPositionCurrency",
        "IRRiskAdjHedgedInPositionCurrency",
        "IRRiskAdjUnhedgedInPaymentCurrency",
        "IRRiskAdjUnhedgedInPositionCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_AccountingChangeSequenceNumber" as "AccountingChangeSequenceNumber" ,
            "OLD_IRRCalculationMethod" as "IRRCalculationMethod" ,
            "OLD_IndicatorResultBeforeChange" as "IndicatorResultBeforeChange" ,
            "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__InvestmentAccount.FinancialContractID" as "_InvestmentAccount.FinancialContractID" ,
            "OLD__InvestmentAccount.IDSystem" as "_InvestmentAccount.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AccountingChangeDate" as "AccountingChangeDate" ,
            "OLD_AccountingChangeReason" as "AccountingChangeReason" ,
            "OLD_IRRHedgePaymentCurrency" as "IRRHedgePaymentCurrency" ,
            "OLD_IRRHedgePositionCurrency" as "IRRHedgePositionCurrency" ,
            "OLD_IRRPaymentCurrency" as "IRRPaymentCurrency" ,
            "OLD_IRRPositionCurrency" as "IRRPositionCurrency" ,
            "OLD_IRRiskAdjHedgedInPaymentPositionCurrency" as "IRRiskAdjHedgedInPaymentPositionCurrency" ,
            "OLD_IRRiskAdjHedgedInPositionCurrency" as "IRRiskAdjHedgedInPositionCurrency" ,
            "OLD_IRRiskAdjUnhedgedInPaymentCurrency" as "IRRiskAdjUnhedgedInPaymentCurrency" ,
            "OLD_IRRiskAdjUnhedgedInPositionCurrency" as "IRRiskAdjUnhedgedInPositionCurrency" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."AccountingChangeSequenceNumber",
                        "OLD"."IRRCalculationMethod",
                        "OLD"."IndicatorResultBeforeChange",
                        "OLD"."_AccountingSystem.AccountingSystemID",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_InvestmentAccount.FinancialContractID",
                        "OLD"."_InvestmentAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."AccountingChangeSequenceNumber" AS "OLD_AccountingChangeSequenceNumber" ,
                "OLD"."IRRCalculationMethod" AS "OLD_IRRCalculationMethod" ,
                "OLD"."IndicatorResultBeforeChange" AS "OLD_IndicatorResultBeforeChange" ,
                "OLD"."_AccountingSystem.AccountingSystemID" AS "OLD__AccountingSystem.AccountingSystemID" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" AS "OLD__InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" AS "OLD__InvestmentAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AccountingChangeDate" AS "OLD_AccountingChangeDate" ,
                "OLD"."AccountingChangeReason" AS "OLD_AccountingChangeReason" ,
                "OLD"."IRRHedgePaymentCurrency" AS "OLD_IRRHedgePaymentCurrency" ,
                "OLD"."IRRHedgePositionCurrency" AS "OLD_IRRHedgePositionCurrency" ,
                "OLD"."IRRPaymentCurrency" AS "OLD_IRRPaymentCurrency" ,
                "OLD"."IRRPositionCurrency" AS "OLD_IRRPositionCurrency" ,
                "OLD"."IRRiskAdjHedgedInPaymentPositionCurrency" AS "OLD_IRRiskAdjHedgedInPaymentPositionCurrency" ,
                "OLD"."IRRiskAdjHedgedInPositionCurrency" AS "OLD_IRRiskAdjHedgedInPositionCurrency" ,
                "OLD"."IRRiskAdjUnhedgedInPaymentCurrency" AS "OLD_IRRiskAdjUnhedgedInPaymentCurrency" ,
                "OLD"."IRRiskAdjUnhedgedInPositionCurrency" AS "OLD_IRRiskAdjUnhedgedInPositionCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InterestRateRiskAdjustment" as "OLD"
            on
                                      "IN"."AccountingChangeSequenceNumber" = "OLD"."AccountingChangeSequenceNumber" and
                                      "IN"."IRRCalculationMethod" = "OLD"."IRRCalculationMethod" and
                                      "IN"."IndicatorResultBeforeChange" = "OLD"."IndicatorResultBeforeChange" and
                                      "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                                      "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                      "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                                      "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                                      "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_AccountingChangeSequenceNumber" as "AccountingChangeSequenceNumber",
            "OLD_IRRCalculationMethod" as "IRRCalculationMethod",
            "OLD_IndicatorResultBeforeChange" as "IndicatorResultBeforeChange",
            "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__InvestmentAccount.FinancialContractID" as "_InvestmentAccount.FinancialContractID",
            "OLD__InvestmentAccount.IDSystem" as "_InvestmentAccount.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AccountingChangeDate" as "AccountingChangeDate",
            "OLD_AccountingChangeReason" as "AccountingChangeReason",
            "OLD_IRRHedgePaymentCurrency" as "IRRHedgePaymentCurrency",
            "OLD_IRRHedgePositionCurrency" as "IRRHedgePositionCurrency",
            "OLD_IRRPaymentCurrency" as "IRRPaymentCurrency",
            "OLD_IRRPositionCurrency" as "IRRPositionCurrency",
            "OLD_IRRiskAdjHedgedInPaymentPositionCurrency" as "IRRiskAdjHedgedInPaymentPositionCurrency",
            "OLD_IRRiskAdjHedgedInPositionCurrency" as "IRRiskAdjHedgedInPositionCurrency",
            "OLD_IRRiskAdjUnhedgedInPaymentCurrency" as "IRRiskAdjUnhedgedInPaymentCurrency",
            "OLD_IRRiskAdjUnhedgedInPositionCurrency" as "IRRiskAdjUnhedgedInPositionCurrency",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."AccountingChangeSequenceNumber",
                        "OLD"."IRRCalculationMethod",
                        "OLD"."IndicatorResultBeforeChange",
                        "OLD"."_AccountingSystem.AccountingSystemID",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_InvestmentAccount.FinancialContractID",
                        "OLD"."_InvestmentAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."AccountingChangeSequenceNumber" AS "OLD_AccountingChangeSequenceNumber" ,
                "OLD"."IRRCalculationMethod" AS "OLD_IRRCalculationMethod" ,
                "OLD"."IndicatorResultBeforeChange" AS "OLD_IndicatorResultBeforeChange" ,
                "OLD"."_AccountingSystem.AccountingSystemID" AS "OLD__AccountingSystem.AccountingSystemID" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" AS "OLD__InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" AS "OLD__InvestmentAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AccountingChangeDate" AS "OLD_AccountingChangeDate" ,
                "OLD"."AccountingChangeReason" AS "OLD_AccountingChangeReason" ,
                "OLD"."IRRHedgePaymentCurrency" AS "OLD_IRRHedgePaymentCurrency" ,
                "OLD"."IRRHedgePositionCurrency" AS "OLD_IRRHedgePositionCurrency" ,
                "OLD"."IRRPaymentCurrency" AS "OLD_IRRPaymentCurrency" ,
                "OLD"."IRRPositionCurrency" AS "OLD_IRRPositionCurrency" ,
                "OLD"."IRRiskAdjHedgedInPaymentPositionCurrency" AS "OLD_IRRiskAdjHedgedInPaymentPositionCurrency" ,
                "OLD"."IRRiskAdjHedgedInPositionCurrency" AS "OLD_IRRiskAdjHedgedInPositionCurrency" ,
                "OLD"."IRRiskAdjUnhedgedInPaymentCurrency" AS "OLD_IRRiskAdjUnhedgedInPaymentCurrency" ,
                "OLD"."IRRiskAdjUnhedgedInPositionCurrency" AS "OLD_IRRiskAdjUnhedgedInPositionCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InterestRateRiskAdjustment" as "OLD"
            on
               "IN"."AccountingChangeSequenceNumber" = "OLD"."AccountingChangeSequenceNumber" and
               "IN"."IRRCalculationMethod" = "OLD"."IRRCalculationMethod" and
               "IN"."IndicatorResultBeforeChange" = "OLD"."IndicatorResultBeforeChange" and
               "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
               "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
               "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
               "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
               "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
