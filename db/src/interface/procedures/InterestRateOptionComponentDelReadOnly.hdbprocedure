PROCEDURE "sap.fsdm.procedures::InterestRateOptionComponentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::InterestRateOptionComponentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::InterestRateOptionComponentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::InterestRateOptionComponentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ComponentNumber=' || TO_VARCHAR("ComponentNumber") || ' ' ||
                '_InterestRateOption.FinancialContractID=' || TO_VARCHAR("_InterestRateOption.FinancialContractID") || ' ' ||
                '_InterestRateOption.IDSystem=' || TO_VARCHAR("_InterestRateOption.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ComponentNumber",
                        "IN"."_InterestRateOption.FinancialContractID",
                        "IN"."_InterestRateOption.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ComponentNumber",
                        "IN"."_InterestRateOption.FinancialContractID",
                        "IN"."_InterestRateOption.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ComponentNumber",
                        "_InterestRateOption.FinancialContractID",
                        "_InterestRateOption.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ComponentNumber",
                                    "IN"."_InterestRateOption.FinancialContractID",
                                    "IN"."_InterestRateOption.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ComponentNumber",
                                    "IN"."_InterestRateOption.FinancialContractID",
                                    "IN"."_InterestRateOption.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ComponentNumber" is null and
            "_InterestRateOption.FinancialContractID" is null and
            "_InterestRateOption.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "ComponentNumber",
            "_InterestRateOption.FinancialContractID",
            "_InterestRateOption.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::InterestRateOptionComponent" WHERE
            (
            "ComponentNumber" ,
            "_InterestRateOption.FinancialContractID" ,
            "_InterestRateOption.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."ComponentNumber",
            "OLD"."_InterestRateOption.FinancialContractID",
            "OLD"."_InterestRateOption.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::InterestRateOptionComponent" as "OLD"
        on
                              "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
                              "IN"."_InterestRateOption.FinancialContractID" = "OLD"."_InterestRateOption.FinancialContractID" and
                              "IN"."_InterestRateOption.IDSystem" = "OLD"."_InterestRateOption.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "ComponentNumber",
        "_InterestRateOption.FinancialContractID",
        "_InterestRateOption.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CapOrFloor",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_ComponentNumber" as "ComponentNumber" ,
            "OLD__InterestRateOption.FinancialContractID" as "_InterestRateOption.FinancialContractID" ,
            "OLD__InterestRateOption.IDSystem" as "_InterestRateOption.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CapOrFloor" as "CapOrFloor" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ComponentNumber",
                        "OLD"."_InterestRateOption.FinancialContractID",
                        "OLD"."_InterestRateOption.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."ComponentNumber" AS "OLD_ComponentNumber" ,
                "OLD"."_InterestRateOption.FinancialContractID" AS "OLD__InterestRateOption.FinancialContractID" ,
                "OLD"."_InterestRateOption.IDSystem" AS "OLD__InterestRateOption.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CapOrFloor" AS "OLD_CapOrFloor" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InterestRateOptionComponent" as "OLD"
            on
                                      "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
                                      "IN"."_InterestRateOption.FinancialContractID" = "OLD"."_InterestRateOption.FinancialContractID" and
                                      "IN"."_InterestRateOption.IDSystem" = "OLD"."_InterestRateOption.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_ComponentNumber" as "ComponentNumber",
            "OLD__InterestRateOption.FinancialContractID" as "_InterestRateOption.FinancialContractID",
            "OLD__InterestRateOption.IDSystem" as "_InterestRateOption.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CapOrFloor" as "CapOrFloor",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ComponentNumber",
                        "OLD"."_InterestRateOption.FinancialContractID",
                        "OLD"."_InterestRateOption.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ComponentNumber" AS "OLD_ComponentNumber" ,
                "OLD"."_InterestRateOption.FinancialContractID" AS "OLD__InterestRateOption.FinancialContractID" ,
                "OLD"."_InterestRateOption.IDSystem" AS "OLD__InterestRateOption.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CapOrFloor" AS "OLD_CapOrFloor" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InterestRateOptionComponent" as "OLD"
            on
               "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
               "IN"."_InterestRateOption.FinancialContractID" = "OLD"."_InterestRateOption.FinancialContractID" and
               "IN"."_InterestRateOption.IDSystem" = "OLD"."_InterestRateOption.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
