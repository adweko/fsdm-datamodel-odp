PROCEDURE "sap.fsdm.procedures::SensitivityBasedResultOfStandardizedApproachForMarketRiskLoad" (IN ROW "sap.fsdm.tabletypes::SensitivityBasedResultOfStandardizedApproachForMarketRiskTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'RiskClass=' || TO_VARCHAR("RiskClass") || ' ' ||
                'RiskProvisioningScenario=' || TO_VARCHAR("RiskProvisioningScenario") || ' ' ||
                'RoleOfCurrency=' || TO_VARCHAR("RoleOfCurrency") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_RiskReportingNode.RiskReportingNodeID=' || TO_VARCHAR("_RiskReportingNode.RiskReportingNodeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."RiskClass",
                        "IN"."RiskProvisioningScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."RiskClass",
                        "IN"."RiskProvisioningScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "RiskClass",
                        "RiskProvisioningScenario",
                        "RoleOfCurrency",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_RiskReportingNode.RiskReportingNodeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."RiskClass",
                                    "IN"."RiskProvisioningScenario",
                                    "IN"."RoleOfCurrency",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_RiskReportingNode.RiskReportingNodeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."RiskClass",
                                    "IN"."RiskProvisioningScenario",
                                    "IN"."RoleOfCurrency",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_RiskReportingNode.RiskReportingNodeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::SensitivityBasedResultOfStandardizedApproachForMarketRisk" (
        "RiskClass",
        "RiskProvisioningScenario",
        "RoleOfCurrency",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Currency",
        "HighCorrelationCurvatureRisk",
        "HighCorrelationDeltaRisk",
        "HighCorrelationVegaRisk",
        "LowCorrelationCurvatureRisk",
        "LowCorrelationDeltaRisk",
        "LowCorrelationVegaRisk",
        "MediumCorrelationCurvatureRisk",
        "MediumCorrelationDeltaRisk",
        "MediumCorrelationVegaRisk",
        "NegativeUnweightedDeltaSensitivity",
        "OwnFundsRequirement",
        "PositiveUnweightedDeltaSensitivity",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_RiskClass" as "RiskClass" ,
            "OLD_RiskProvisioningScenario" as "RiskProvisioningScenario" ,
            "OLD_RoleOfCurrency" as "RoleOfCurrency" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "OLD__RiskReportingNode.RiskReportingNodeID" as "_RiskReportingNode.RiskReportingNodeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Currency" as "Currency" ,
            "OLD_HighCorrelationCurvatureRisk" as "HighCorrelationCurvatureRisk" ,
            "OLD_HighCorrelationDeltaRisk" as "HighCorrelationDeltaRisk" ,
            "OLD_HighCorrelationVegaRisk" as "HighCorrelationVegaRisk" ,
            "OLD_LowCorrelationCurvatureRisk" as "LowCorrelationCurvatureRisk" ,
            "OLD_LowCorrelationDeltaRisk" as "LowCorrelationDeltaRisk" ,
            "OLD_LowCorrelationVegaRisk" as "LowCorrelationVegaRisk" ,
            "OLD_MediumCorrelationCurvatureRisk" as "MediumCorrelationCurvatureRisk" ,
            "OLD_MediumCorrelationDeltaRisk" as "MediumCorrelationDeltaRisk" ,
            "OLD_MediumCorrelationVegaRisk" as "MediumCorrelationVegaRisk" ,
            "OLD_NegativeUnweightedDeltaSensitivity" as "NegativeUnweightedDeltaSensitivity" ,
            "OLD_OwnFundsRequirement" as "OwnFundsRequirement" ,
            "OLD_PositiveUnweightedDeltaSensitivity" as "PositiveUnweightedDeltaSensitivity" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."RiskClass",
                        "IN"."RiskProvisioningScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."RiskClass" as "OLD_RiskClass",
                                "OLD"."RiskProvisioningScenario" as "OLD_RiskProvisioningScenario",
                                "OLD"."RoleOfCurrency" as "OLD_RoleOfCurrency",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_RiskReportingNode.RiskReportingNodeID" as "OLD__RiskReportingNode.RiskReportingNodeID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."Currency" as "OLD_Currency",
                                "OLD"."HighCorrelationCurvatureRisk" as "OLD_HighCorrelationCurvatureRisk",
                                "OLD"."HighCorrelationDeltaRisk" as "OLD_HighCorrelationDeltaRisk",
                                "OLD"."HighCorrelationVegaRisk" as "OLD_HighCorrelationVegaRisk",
                                "OLD"."LowCorrelationCurvatureRisk" as "OLD_LowCorrelationCurvatureRisk",
                                "OLD"."LowCorrelationDeltaRisk" as "OLD_LowCorrelationDeltaRisk",
                                "OLD"."LowCorrelationVegaRisk" as "OLD_LowCorrelationVegaRisk",
                                "OLD"."MediumCorrelationCurvatureRisk" as "OLD_MediumCorrelationCurvatureRisk",
                                "OLD"."MediumCorrelationDeltaRisk" as "OLD_MediumCorrelationDeltaRisk",
                                "OLD"."MediumCorrelationVegaRisk" as "OLD_MediumCorrelationVegaRisk",
                                "OLD"."NegativeUnweightedDeltaSensitivity" as "OLD_NegativeUnweightedDeltaSensitivity",
                                "OLD"."OwnFundsRequirement" as "OLD_OwnFundsRequirement",
                                "OLD"."PositiveUnweightedDeltaSensitivity" as "OLD_PositiveUnweightedDeltaSensitivity",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::SensitivityBasedResultOfStandardizedApproachForMarketRisk" as "OLD"
            on
                ifnull( "IN"."RiskClass", '') = "OLD"."RiskClass" and
                ifnull( "IN"."RiskProvisioningScenario", '') = "OLD"."RiskProvisioningScenario" and
                ifnull( "IN"."RoleOfCurrency", '') = "OLD"."RoleOfCurrency" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_RiskReportingNode.RiskReportingNodeID", '') = "OLD"."_RiskReportingNode.RiskReportingNodeID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::SensitivityBasedResultOfStandardizedApproachForMarketRisk" (
        "RiskClass",
        "RiskProvisioningScenario",
        "RoleOfCurrency",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Currency",
        "HighCorrelationCurvatureRisk",
        "HighCorrelationDeltaRisk",
        "HighCorrelationVegaRisk",
        "LowCorrelationCurvatureRisk",
        "LowCorrelationDeltaRisk",
        "LowCorrelationVegaRisk",
        "MediumCorrelationCurvatureRisk",
        "MediumCorrelationDeltaRisk",
        "MediumCorrelationVegaRisk",
        "NegativeUnweightedDeltaSensitivity",
        "OwnFundsRequirement",
        "PositiveUnweightedDeltaSensitivity",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_RiskClass"  as "RiskClass",
            "OLD_RiskProvisioningScenario"  as "RiskProvisioningScenario",
            "OLD_RoleOfCurrency"  as "RoleOfCurrency",
            "OLD__ResultGroup.ResultDataProvider"  as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID"  as "_ResultGroup.ResultGroupID",
            "OLD__RiskReportingNode.RiskReportingNodeID"  as "_RiskReportingNode.RiskReportingNodeID",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_Currency"  as "Currency",
            "OLD_HighCorrelationCurvatureRisk"  as "HighCorrelationCurvatureRisk",
            "OLD_HighCorrelationDeltaRisk"  as "HighCorrelationDeltaRisk",
            "OLD_HighCorrelationVegaRisk"  as "HighCorrelationVegaRisk",
            "OLD_LowCorrelationCurvatureRisk"  as "LowCorrelationCurvatureRisk",
            "OLD_LowCorrelationDeltaRisk"  as "LowCorrelationDeltaRisk",
            "OLD_LowCorrelationVegaRisk"  as "LowCorrelationVegaRisk",
            "OLD_MediumCorrelationCurvatureRisk"  as "MediumCorrelationCurvatureRisk",
            "OLD_MediumCorrelationDeltaRisk"  as "MediumCorrelationDeltaRisk",
            "OLD_MediumCorrelationVegaRisk"  as "MediumCorrelationVegaRisk",
            "OLD_NegativeUnweightedDeltaSensitivity"  as "NegativeUnweightedDeltaSensitivity",
            "OLD_OwnFundsRequirement"  as "OwnFundsRequirement",
            "OLD_PositiveUnweightedDeltaSensitivity"  as "PositiveUnweightedDeltaSensitivity",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."RiskClass",
                        "IN"."RiskProvisioningScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."RiskClass" as "OLD_RiskClass",
                        "OLD"."RiskProvisioningScenario" as "OLD_RiskProvisioningScenario",
                        "OLD"."RoleOfCurrency" as "OLD_RoleOfCurrency",
                        "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                        "OLD"."_RiskReportingNode.RiskReportingNodeID" as "OLD__RiskReportingNode.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."Currency" as "OLD_Currency",
                        "OLD"."HighCorrelationCurvatureRisk" as "OLD_HighCorrelationCurvatureRisk",
                        "OLD"."HighCorrelationDeltaRisk" as "OLD_HighCorrelationDeltaRisk",
                        "OLD"."HighCorrelationVegaRisk" as "OLD_HighCorrelationVegaRisk",
                        "OLD"."LowCorrelationCurvatureRisk" as "OLD_LowCorrelationCurvatureRisk",
                        "OLD"."LowCorrelationDeltaRisk" as "OLD_LowCorrelationDeltaRisk",
                        "OLD"."LowCorrelationVegaRisk" as "OLD_LowCorrelationVegaRisk",
                        "OLD"."MediumCorrelationCurvatureRisk" as "OLD_MediumCorrelationCurvatureRisk",
                        "OLD"."MediumCorrelationDeltaRisk" as "OLD_MediumCorrelationDeltaRisk",
                        "OLD"."MediumCorrelationVegaRisk" as "OLD_MediumCorrelationVegaRisk",
                        "OLD"."NegativeUnweightedDeltaSensitivity" as "OLD_NegativeUnweightedDeltaSensitivity",
                        "OLD"."OwnFundsRequirement" as "OLD_OwnFundsRequirement",
                        "OLD"."PositiveUnweightedDeltaSensitivity" as "OLD_PositiveUnweightedDeltaSensitivity",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::SensitivityBasedResultOfStandardizedApproachForMarketRisk" as "OLD"
            on
                ifnull( "IN"."RiskClass", '' ) = "OLD"."RiskClass" and
                ifnull( "IN"."RiskProvisioningScenario", '' ) = "OLD"."RiskProvisioningScenario" and
                ifnull( "IN"."RoleOfCurrency", '' ) = "OLD"."RoleOfCurrency" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_RiskReportingNode.RiskReportingNodeID", '' ) = "OLD"."_RiskReportingNode.RiskReportingNodeID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::SensitivityBasedResultOfStandardizedApproachForMarketRisk"
    where (
        "RiskClass",
        "RiskProvisioningScenario",
        "RoleOfCurrency",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."RiskClass",
            "OLD"."RiskProvisioningScenario",
            "OLD"."RoleOfCurrency",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_RiskReportingNode.RiskReportingNodeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::SensitivityBasedResultOfStandardizedApproachForMarketRisk" as "OLD"
        on
           ifnull( "IN"."RiskClass", '' ) = "OLD"."RiskClass" and
           ifnull( "IN"."RiskProvisioningScenario", '' ) = "OLD"."RiskProvisioningScenario" and
           ifnull( "IN"."RoleOfCurrency", '' ) = "OLD"."RoleOfCurrency" and
           ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
           ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" and
           ifnull( "IN"."_RiskReportingNode.RiskReportingNodeID", '' ) = "OLD"."_RiskReportingNode.RiskReportingNodeID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::SensitivityBasedResultOfStandardizedApproachForMarketRisk" (
        "RiskClass",
        "RiskProvisioningScenario",
        "RoleOfCurrency",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Currency",
        "HighCorrelationCurvatureRisk",
        "HighCorrelationDeltaRisk",
        "HighCorrelationVegaRisk",
        "LowCorrelationCurvatureRisk",
        "LowCorrelationDeltaRisk",
        "LowCorrelationVegaRisk",
        "MediumCorrelationCurvatureRisk",
        "MediumCorrelationDeltaRisk",
        "MediumCorrelationVegaRisk",
        "NegativeUnweightedDeltaSensitivity",
        "OwnFundsRequirement",
        "PositiveUnweightedDeltaSensitivity",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "RiskClass", '' ) as "RiskClass",
            ifnull( "RiskProvisioningScenario", '' ) as "RiskProvisioningScenario",
            ifnull( "RoleOfCurrency", '' ) as "RoleOfCurrency",
            ifnull( "_ResultGroup.ResultDataProvider", '' ) as "_ResultGroup.ResultDataProvider",
            ifnull( "_ResultGroup.ResultGroupID", '' ) as "_ResultGroup.ResultGroupID",
            ifnull( "_RiskReportingNode.RiskReportingNodeID", '' ) as "_RiskReportingNode.RiskReportingNodeID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "Currency"  ,
            "HighCorrelationCurvatureRisk"  ,
            "HighCorrelationDeltaRisk"  ,
            "HighCorrelationVegaRisk"  ,
            "LowCorrelationCurvatureRisk"  ,
            "LowCorrelationDeltaRisk"  ,
            "LowCorrelationVegaRisk"  ,
            "MediumCorrelationCurvatureRisk"  ,
            "MediumCorrelationDeltaRisk"  ,
            "MediumCorrelationVegaRisk"  ,
            "NegativeUnweightedDeltaSensitivity"  ,
            "OwnFundsRequirement"  ,
            "PositiveUnweightedDeltaSensitivity"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END