PROCEDURE "sap.fsdm.procedures::EligibleCollateralEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::EligibleCollateralTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::EligibleCollateralTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::EligibleCollateralTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Scheme" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_InstrumentClass.InstrumentClass" is null and
            "_InstrumentClass.InstrumentClassificationSystem" is null and
            "_MasterAgreement.FinancialContractID" is null and
            "_MasterAgreement.IDSystem" is null and
            "_Organization.BusinessPartnerID" is null and
            "_ProductCatalogItem.ProductCatalogItem" is null and
            "_ProductCatalogItem._ProductCatalog.CatalogID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Scheme" ,
                "_FinancialContract.FinancialContractID" ,
                "_FinancialContract.IDSystem" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "_InstrumentClass.InstrumentClass" ,
                "_InstrumentClass.InstrumentClassificationSystem" ,
                "_MasterAgreement.FinancialContractID" ,
                "_MasterAgreement.IDSystem" ,
                "_Organization.BusinessPartnerID" ,
                "_ProductCatalogItem.ProductCatalogItem" ,
                "_ProductCatalogItem._ProductCatalog.CatalogID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Scheme" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InstrumentClass.InstrumentClass" ,
                "OLD"."_InstrumentClass.InstrumentClassificationSystem" ,
                "OLD"."_MasterAgreement.FinancialContractID" ,
                "OLD"."_MasterAgreement.IDSystem" ,
                "OLD"."_Organization.BusinessPartnerID" ,
                "OLD"."_ProductCatalogItem.ProductCatalogItem" ,
                "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::EligibleCollateral" "OLD"
            on
                "IN"."Scheme" = "OLD"."Scheme" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_InstrumentClass.InstrumentClass" = "OLD"."_InstrumentClass.InstrumentClass" and
                "IN"."_InstrumentClass.InstrumentClassificationSystem" = "OLD"."_InstrumentClass.InstrumentClassificationSystem" and
                "IN"."_MasterAgreement.FinancialContractID" = "OLD"."_MasterAgreement.FinancialContractID" and
                "IN"."_MasterAgreement.IDSystem" = "OLD"."_MasterAgreement.IDSystem" and
                "IN"."_Organization.BusinessPartnerID" = "OLD"."_Organization.BusinessPartnerID" and
                "IN"."_ProductCatalogItem.ProductCatalogItem" = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
                "IN"."_ProductCatalogItem._ProductCatalog.CatalogID" = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Scheme" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InstrumentClass.InstrumentClass" ,
            "_InstrumentClass.InstrumentClassificationSystem" ,
            "_MasterAgreement.FinancialContractID" ,
            "_MasterAgreement.IDSystem" ,
            "_Organization.BusinessPartnerID" ,
            "_ProductCatalogItem.ProductCatalogItem" ,
            "_ProductCatalogItem._ProductCatalog.CatalogID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Scheme" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InstrumentClass.InstrumentClass" ,
                "OLD"."_InstrumentClass.InstrumentClassificationSystem" ,
                "OLD"."_MasterAgreement.FinancialContractID" ,
                "OLD"."_MasterAgreement.IDSystem" ,
                "OLD"."_Organization.BusinessPartnerID" ,
                "OLD"."_ProductCatalogItem.ProductCatalogItem" ,
                "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::EligibleCollateral_Historical" "OLD"
            on
                "IN"."Scheme" = "OLD"."Scheme" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_InstrumentClass.InstrumentClass" = "OLD"."_InstrumentClass.InstrumentClass" and
                "IN"."_InstrumentClass.InstrumentClassificationSystem" = "OLD"."_InstrumentClass.InstrumentClassificationSystem" and
                "IN"."_MasterAgreement.FinancialContractID" = "OLD"."_MasterAgreement.FinancialContractID" and
                "IN"."_MasterAgreement.IDSystem" = "OLD"."_MasterAgreement.IDSystem" and
                "IN"."_Organization.BusinessPartnerID" = "OLD"."_Organization.BusinessPartnerID" and
                "IN"."_ProductCatalogItem.ProductCatalogItem" = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
                "IN"."_ProductCatalogItem._ProductCatalog.CatalogID" = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
        );

END
