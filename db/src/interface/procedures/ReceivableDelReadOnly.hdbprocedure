PROCEDURE "sap.fsdm.procedures::ReceivableDelReadOnly" (IN ROW "sap.fsdm.tabletypes::ReceivableTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::ReceivableTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::ReceivableTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ReceivableID=' || TO_VARCHAR("ReceivableID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ReceivableID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ReceivableID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ReceivableID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ReceivableID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ReceivableID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ReceivableID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "ReceivableID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::Receivable" WHERE
            (
            "ReceivableID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."ReceivableID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Receivable" as "OLD"
        on
                              "IN"."ReceivableID" = "OLD"."ReceivableID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "ReceivableID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_GuaranteeProvidingEntitlement.FinancialContractID",
        "ASSOC_GuaranteeProvidingEntitlement.IDSystem",
        "ASSOC_OrganizationUnit.IDSystem",
        "ASSOC_OrganizationUnit.OrganizationalUnitID",
        "ASSOC_OrganizationUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "ApplicableLaw",
        "LastDueDate",
        "OriginalReceivableAmount",
        "OriginalReceivableAmountCurrency",
        "OutstandingReceivableAmount",
        "OutstandingReceivableAmountCurrency",
        "PlaceOfJurisdiction",
        "ReceivableType",
        "UnderlyingContractStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_ReceivableID" as "ReceivableID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ASSOC_GuaranteeProvidingEntitlement.FinancialContractID" as "ASSOC_GuaranteeProvidingEntitlement.FinancialContractID" ,
            "OLD_ASSOC_GuaranteeProvidingEntitlement.IDSystem" as "ASSOC_GuaranteeProvidingEntitlement.IDSystem" ,
            "OLD_ASSOC_OrganizationUnit.IDSystem" as "ASSOC_OrganizationUnit.IDSystem" ,
            "OLD_ASSOC_OrganizationUnit.OrganizationalUnitID" as "ASSOC_OrganizationUnit.OrganizationalUnitID" ,
            "OLD_ASSOC_OrganizationUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD_ApplicableLaw" as "ApplicableLaw" ,
            "OLD_LastDueDate" as "LastDueDate" ,
            "OLD_OriginalReceivableAmount" as "OriginalReceivableAmount" ,
            "OLD_OriginalReceivableAmountCurrency" as "OriginalReceivableAmountCurrency" ,
            "OLD_OutstandingReceivableAmount" as "OutstandingReceivableAmount" ,
            "OLD_OutstandingReceivableAmountCurrency" as "OutstandingReceivableAmountCurrency" ,
            "OLD_PlaceOfJurisdiction" as "PlaceOfJurisdiction" ,
            "OLD_ReceivableType" as "ReceivableType" ,
            "OLD_UnderlyingContractStartDate" as "UnderlyingContractStartDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ReceivableID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."ReceivableID" AS "OLD_ReceivableID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_GuaranteeProvidingEntitlement.FinancialContractID" AS "OLD_ASSOC_GuaranteeProvidingEntitlement.FinancialContractID" ,
                "OLD"."ASSOC_GuaranteeProvidingEntitlement.IDSystem" AS "OLD_ASSOC_GuaranteeProvidingEntitlement.IDSystem" ,
                "OLD"."ASSOC_OrganizationUnit.IDSystem" AS "OLD_ASSOC_OrganizationUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationUnit.OrganizationalUnitID" AS "OLD_ASSOC_OrganizationUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_OrganizationUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ApplicableLaw" AS "OLD_ApplicableLaw" ,
                "OLD"."LastDueDate" AS "OLD_LastDueDate" ,
                "OLD"."OriginalReceivableAmount" AS "OLD_OriginalReceivableAmount" ,
                "OLD"."OriginalReceivableAmountCurrency" AS "OLD_OriginalReceivableAmountCurrency" ,
                "OLD"."OutstandingReceivableAmount" AS "OLD_OutstandingReceivableAmount" ,
                "OLD"."OutstandingReceivableAmountCurrency" AS "OLD_OutstandingReceivableAmountCurrency" ,
                "OLD"."PlaceOfJurisdiction" AS "OLD_PlaceOfJurisdiction" ,
                "OLD"."ReceivableType" AS "OLD_ReceivableType" ,
                "OLD"."UnderlyingContractStartDate" AS "OLD_UnderlyingContractStartDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Receivable" as "OLD"
            on
                                      "IN"."ReceivableID" = "OLD"."ReceivableID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_ReceivableID" as "ReceivableID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ASSOC_GuaranteeProvidingEntitlement.FinancialContractID" as "ASSOC_GuaranteeProvidingEntitlement.FinancialContractID",
            "OLD_ASSOC_GuaranteeProvidingEntitlement.IDSystem" as "ASSOC_GuaranteeProvidingEntitlement.IDSystem",
            "OLD_ASSOC_OrganizationUnit.IDSystem" as "ASSOC_OrganizationUnit.IDSystem",
            "OLD_ASSOC_OrganizationUnit.OrganizationalUnitID" as "ASSOC_OrganizationUnit.OrganizationalUnitID",
            "OLD_ASSOC_OrganizationUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD_ApplicableLaw" as "ApplicableLaw",
            "OLD_LastDueDate" as "LastDueDate",
            "OLD_OriginalReceivableAmount" as "OriginalReceivableAmount",
            "OLD_OriginalReceivableAmountCurrency" as "OriginalReceivableAmountCurrency",
            "OLD_OutstandingReceivableAmount" as "OutstandingReceivableAmount",
            "OLD_OutstandingReceivableAmountCurrency" as "OutstandingReceivableAmountCurrency",
            "OLD_PlaceOfJurisdiction" as "PlaceOfJurisdiction",
            "OLD_ReceivableType" as "ReceivableType",
            "OLD_UnderlyingContractStartDate" as "UnderlyingContractStartDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ReceivableID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ReceivableID" AS "OLD_ReceivableID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_GuaranteeProvidingEntitlement.FinancialContractID" AS "OLD_ASSOC_GuaranteeProvidingEntitlement.FinancialContractID" ,
                "OLD"."ASSOC_GuaranteeProvidingEntitlement.IDSystem" AS "OLD_ASSOC_GuaranteeProvidingEntitlement.IDSystem" ,
                "OLD"."ASSOC_OrganizationUnit.IDSystem" AS "OLD_ASSOC_OrganizationUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationUnit.OrganizationalUnitID" AS "OLD_ASSOC_OrganizationUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_OrganizationUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ApplicableLaw" AS "OLD_ApplicableLaw" ,
                "OLD"."LastDueDate" AS "OLD_LastDueDate" ,
                "OLD"."OriginalReceivableAmount" AS "OLD_OriginalReceivableAmount" ,
                "OLD"."OriginalReceivableAmountCurrency" AS "OLD_OriginalReceivableAmountCurrency" ,
                "OLD"."OutstandingReceivableAmount" AS "OLD_OutstandingReceivableAmount" ,
                "OLD"."OutstandingReceivableAmountCurrency" AS "OLD_OutstandingReceivableAmountCurrency" ,
                "OLD"."PlaceOfJurisdiction" AS "OLD_PlaceOfJurisdiction" ,
                "OLD"."ReceivableType" AS "OLD_ReceivableType" ,
                "OLD"."UnderlyingContractStartDate" AS "OLD_UnderlyingContractStartDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Receivable" as "OLD"
            on
               "IN"."ReceivableID" = "OLD"."ReceivableID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
