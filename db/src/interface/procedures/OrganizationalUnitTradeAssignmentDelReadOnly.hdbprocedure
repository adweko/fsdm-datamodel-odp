PROCEDURE "sap.fsdm.procedures::OrganizationalUnitTradeAssignmentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::OrganizationalUnitTradeAssignmentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::OrganizationalUnitTradeAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::OrganizationalUnitTradeAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'RoleOfOrganizationalUnit=' || TO_VARCHAR("RoleOfOrganizationalUnit") || ' ' ||
                '_OrgUnit.IDSystem=' || TO_VARCHAR("_OrgUnit.IDSystem") || ' ' ||
                '_OrgUnit.OrganizationalUnitID=' || TO_VARCHAR("_OrgUnit.OrganizationalUnitID") || ' ' ||
                '_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID=' || TO_VARCHAR("_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID") || ' ' ||
                '_Trade.IDSystem=' || TO_VARCHAR("_Trade.IDSystem") || ' ' ||
                '_Trade.TradeID=' || TO_VARCHAR("_Trade.TradeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."RoleOfOrganizationalUnit",
                        "IN"."_OrgUnit.IDSystem",
                        "IN"."_OrgUnit.OrganizationalUnitID",
                        "IN"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."RoleOfOrganizationalUnit",
                        "IN"."_OrgUnit.IDSystem",
                        "IN"."_OrgUnit.OrganizationalUnitID",
                        "IN"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "RoleOfOrganizationalUnit",
                        "_OrgUnit.IDSystem",
                        "_OrgUnit.OrganizationalUnitID",
                        "_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "_Trade.IDSystem",
                        "_Trade.TradeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."RoleOfOrganizationalUnit",
                                    "IN"."_OrgUnit.IDSystem",
                                    "IN"."_OrgUnit.OrganizationalUnitID",
                                    "IN"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."RoleOfOrganizationalUnit",
                                    "IN"."_OrgUnit.IDSystem",
                                    "IN"."_OrgUnit.OrganizationalUnitID",
                                    "IN"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "RoleOfOrganizationalUnit" is null and
            "_OrgUnit.IDSystem" is null and
            "_OrgUnit.OrganizationalUnitID" is null and
            "_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "_Trade.IDSystem" is null and
            "_Trade.TradeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "RoleOfOrganizationalUnit",
            "_OrgUnit.IDSystem",
            "_OrgUnit.OrganizationalUnitID",
            "_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "_Trade.IDSystem",
            "_Trade.TradeID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::OrganizationalUnitTradeAssignment" WHERE
            (
            "RoleOfOrganizationalUnit" ,
            "_OrgUnit.IDSystem" ,
            "_OrgUnit.OrganizationalUnitID" ,
            "_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_Trade.IDSystem" ,
            "_Trade.TradeID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."RoleOfOrganizationalUnit",
            "OLD"."_OrgUnit.IDSystem",
            "OLD"."_OrgUnit.OrganizationalUnitID",
            "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD"."_Trade.IDSystem",
            "OLD"."_Trade.TradeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::OrganizationalUnitTradeAssignment" as "OLD"
        on
                              "IN"."RoleOfOrganizationalUnit" = "OLD"."RoleOfOrganizationalUnit" and
                              "IN"."_OrgUnit.IDSystem" = "OLD"."_OrgUnit.IDSystem" and
                              "IN"."_OrgUnit.OrganizationalUnitID" = "OLD"."_OrgUnit.OrganizationalUnitID" and
                              "IN"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                              "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                              "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "RoleOfOrganizationalUnit",
        "_OrgUnit.IDSystem",
        "_OrgUnit.OrganizationalUnitID",
        "_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_RoleOfOrganizationalUnit" as "RoleOfOrganizationalUnit" ,
            "OLD__OrgUnit.IDSystem" as "_OrgUnit.IDSystem" ,
            "OLD__OrgUnit.OrganizationalUnitID" as "_OrgUnit.OrganizationalUnitID" ,
            "OLD__OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD__Trade.IDSystem" as "_Trade.IDSystem" ,
            "OLD__Trade.TradeID" as "_Trade.TradeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."RoleOfOrganizationalUnit",
                        "OLD"."_OrgUnit.IDSystem",
                        "OLD"."_OrgUnit.OrganizationalUnitID",
                        "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."RoleOfOrganizationalUnit" AS "OLD_RoleOfOrganizationalUnit" ,
                "OLD"."_OrgUnit.IDSystem" AS "OLD__OrgUnit.IDSystem" ,
                "OLD"."_OrgUnit.OrganizationalUnitID" AS "OLD__OrgUnit.OrganizationalUnitID" ,
                "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD__OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::OrganizationalUnitTradeAssignment" as "OLD"
            on
                                      "IN"."RoleOfOrganizationalUnit" = "OLD"."RoleOfOrganizationalUnit" and
                                      "IN"."_OrgUnit.IDSystem" = "OLD"."_OrgUnit.IDSystem" and
                                      "IN"."_OrgUnit.OrganizationalUnitID" = "OLD"."_OrgUnit.OrganizationalUnitID" and
                                      "IN"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                                      "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                      "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_RoleOfOrganizationalUnit" as "RoleOfOrganizationalUnit",
            "OLD__OrgUnit.IDSystem" as "_OrgUnit.IDSystem",
            "OLD__OrgUnit.OrganizationalUnitID" as "_OrgUnit.OrganizationalUnitID",
            "OLD__OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD__Trade.IDSystem" as "_Trade.IDSystem",
            "OLD__Trade.TradeID" as "_Trade.TradeID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."RoleOfOrganizationalUnit",
                        "OLD"."_OrgUnit.IDSystem",
                        "OLD"."_OrgUnit.OrganizationalUnitID",
                        "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."RoleOfOrganizationalUnit" AS "OLD_RoleOfOrganizationalUnit" ,
                "OLD"."_OrgUnit.IDSystem" AS "OLD__OrgUnit.IDSystem" ,
                "OLD"."_OrgUnit.OrganizationalUnitID" AS "OLD__OrgUnit.OrganizationalUnitID" ,
                "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD__OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::OrganizationalUnitTradeAssignment" as "OLD"
            on
               "IN"."RoleOfOrganizationalUnit" = "OLD"."RoleOfOrganizationalUnit" and
               "IN"."_OrgUnit.IDSystem" = "OLD"."_OrgUnit.IDSystem" and
               "IN"."_OrgUnit.OrganizationalUnitID" = "OLD"."_OrgUnit.OrganizationalUnitID" and
               "IN"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
               "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
               "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
