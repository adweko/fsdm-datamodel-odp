PROCEDURE "sap.fsdm.procedures::BusinessPartnerCreditRiskAssessmentErase" (IN ROW "sap.fsdm.tabletypes::BusinessPartnerCreditRiskAssessmentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "BusinessPartnerCreditRiskAssessmentReason" is null and
            "CreditRiskAssessmentDate" is null and
            "_BusinessPartner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::BusinessPartnerCreditRiskAssessment"
        WHERE
        (            "BusinessPartnerCreditRiskAssessmentReason" ,
            "CreditRiskAssessmentDate" ,
            "_BusinessPartner.BusinessPartnerID" 
        ) in
        (
            select                 "OLD"."BusinessPartnerCreditRiskAssessmentReason" ,
                "OLD"."CreditRiskAssessmentDate" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::BusinessPartnerCreditRiskAssessment" "OLD"
            on
            "IN"."BusinessPartnerCreditRiskAssessmentReason" = "OLD"."BusinessPartnerCreditRiskAssessmentReason" and
            "IN"."CreditRiskAssessmentDate" = "OLD"."CreditRiskAssessmentDate" and
            "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
        );

        --delete data from history table
        delete from "sap.fsdm::BusinessPartnerCreditRiskAssessment_Historical"
        WHERE
        (
            "BusinessPartnerCreditRiskAssessmentReason" ,
            "CreditRiskAssessmentDate" ,
            "_BusinessPartner.BusinessPartnerID" 
        ) in
        (
            select
                "OLD"."BusinessPartnerCreditRiskAssessmentReason" ,
                "OLD"."CreditRiskAssessmentDate" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::BusinessPartnerCreditRiskAssessment_Historical" "OLD"
            on
                "IN"."BusinessPartnerCreditRiskAssessmentReason" = "OLD"."BusinessPartnerCreditRiskAssessmentReason" and
                "IN"."CreditRiskAssessmentDate" = "OLD"."CreditRiskAssessmentDate" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
        );

END
