PROCEDURE "sap.fsdm.procedures::DataReleaseInfoEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::DataReleaseInfoTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::DataReleaseInfoTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::DataReleaseInfoTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "FiscalPeriod" is null and
            "KeyDate" is null and
            "Object" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "FiscalPeriod" ,
                "KeyDate" ,
                "Object" 
        from
        (
            select
                "OLD"."FiscalPeriod" ,
                "OLD"."KeyDate" ,
                "OLD"."Object" 
            from :ROW "IN"
            inner join "sap.fsdm::DataReleaseInfo" "OLD"
            on
                "IN"."FiscalPeriod" = "OLD"."FiscalPeriod" and
                "IN"."KeyDate" = "OLD"."KeyDate" and
                "IN"."Object" = "OLD"."Object" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "FiscalPeriod" ,
            "KeyDate" ,
            "Object" 
        from
        (
            select
                "OLD"."FiscalPeriod" ,
                "OLD"."KeyDate" ,
                "OLD"."Object" 
            from :ROW "IN"
            inner join "sap.fsdm::DataReleaseInfo_Historical" "OLD"
            on
                "IN"."FiscalPeriod" = "OLD"."FiscalPeriod" and
                "IN"."KeyDate" = "OLD"."KeyDate" and
                "IN"."Object" = "OLD"."Object" 
        );

END
