PROCEDURE "sap.fsdm.procedures::TaxExemptStatusDocumentReadOnly" (IN ROW "sap.fsdm.tabletypes::TaxExemptStatusDocumentTT", OUT CURR_DEL "sap.fsdm.tabletypes::TaxExemptStatusDocumentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::TaxExemptStatusDocumentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Category=' || TO_VARCHAR("Category") || ' ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Category",
                        "IN"."_BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Category",
                        "IN"."_BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Category",
                        "_BusinessPartner.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Category",
                                    "IN"."_BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Category",
                                    "IN"."_BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "Category",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::TaxExemptStatusDocument" WHERE
        (            "Category" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."Category",
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::TaxExemptStatusDocument" as "OLD"
            on
               ifnull( "IN"."Category",'' ) = "OLD"."Category" and
               ifnull( "IN"."_BusinessPartner.BusinessPartnerID",'' ) = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "Category",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Organization.BusinessPartnerID",
        "CapitalClaimCompanyIncomeStartDate",
        "ExemptionThreshold",
        "ExemptionThresholdCurrency",
        "FuturesandOptionsIncomefromLettingandLeasingStartDate",
        "FuturesandOptionsInvestmentCompanyIncomeStartDate",
        "IssueDate",
        "SerialNumber",
        "Subtype",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "Category", '' ) as "Category",
                    ifnull( "_BusinessPartner.BusinessPartnerID", '' ) as "_BusinessPartner.BusinessPartnerID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "_Organization.BusinessPartnerID"  ,
                    "CapitalClaimCompanyIncomeStartDate"  ,
                    "ExemptionThreshold"  ,
                    "ExemptionThresholdCurrency"  ,
                    "FuturesandOptionsIncomefromLettingandLeasingStartDate"  ,
                    "FuturesandOptionsInvestmentCompanyIncomeStartDate"  ,
                    "IssueDate"  ,
                    "SerialNumber"  ,
                    "Subtype"  ,
                    "Type"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_Category" as "Category" ,
                    "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD__Organization.BusinessPartnerID" as "_Organization.BusinessPartnerID" ,
                    "OLD_CapitalClaimCompanyIncomeStartDate" as "CapitalClaimCompanyIncomeStartDate" ,
                    "OLD_ExemptionThreshold" as "ExemptionThreshold" ,
                    "OLD_ExemptionThresholdCurrency" as "ExemptionThresholdCurrency" ,
                    "OLD_FuturesandOptionsIncomefromLettingandLeasingStartDate" as "FuturesandOptionsIncomefromLettingandLeasingStartDate" ,
                    "OLD_FuturesandOptionsInvestmentCompanyIncomeStartDate" as "FuturesandOptionsInvestmentCompanyIncomeStartDate" ,
                    "OLD_IssueDate" as "IssueDate" ,
                    "OLD_SerialNumber" as "SerialNumber" ,
                    "OLD_Subtype" as "Subtype" ,
                    "OLD_Type" as "Type" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."Category",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."Category" as "OLD_Category",
                                "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."_Organization.BusinessPartnerID" as "OLD__Organization.BusinessPartnerID",
                                "OLD"."CapitalClaimCompanyIncomeStartDate" as "OLD_CapitalClaimCompanyIncomeStartDate",
                                "OLD"."ExemptionThreshold" as "OLD_ExemptionThreshold",
                                "OLD"."ExemptionThresholdCurrency" as "OLD_ExemptionThresholdCurrency",
                                "OLD"."FuturesandOptionsIncomefromLettingandLeasingStartDate" as "OLD_FuturesandOptionsIncomefromLettingandLeasingStartDate",
                                "OLD"."FuturesandOptionsInvestmentCompanyIncomeStartDate" as "OLD_FuturesandOptionsInvestmentCompanyIncomeStartDate",
                                "OLD"."IssueDate" as "OLD_IssueDate",
                                "OLD"."SerialNumber" as "OLD_SerialNumber",
                                "OLD"."Subtype" as "OLD_Subtype",
                                "OLD"."Type" as "OLD_Type",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::TaxExemptStatusDocument" as "OLD"
            on
                ifnull( "IN"."Category", '') = "OLD"."Category" and
                ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '') = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_Category" as "Category",
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__Organization.BusinessPartnerID" as "_Organization.BusinessPartnerID",
            "OLD_CapitalClaimCompanyIncomeStartDate" as "CapitalClaimCompanyIncomeStartDate",
            "OLD_ExemptionThreshold" as "ExemptionThreshold",
            "OLD_ExemptionThresholdCurrency" as "ExemptionThresholdCurrency",
            "OLD_FuturesandOptionsIncomefromLettingandLeasingStartDate" as "FuturesandOptionsIncomefromLettingandLeasingStartDate",
            "OLD_FuturesandOptionsInvestmentCompanyIncomeStartDate" as "FuturesandOptionsInvestmentCompanyIncomeStartDate",
            "OLD_IssueDate" as "IssueDate",
            "OLD_SerialNumber" as "SerialNumber",
            "OLD_Subtype" as "Subtype",
            "OLD_Type" as "Type",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."Category",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."Category" as "OLD_Category",
                                "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."_Organization.BusinessPartnerID" as "OLD__Organization.BusinessPartnerID",
                                "OLD"."CapitalClaimCompanyIncomeStartDate" as "OLD_CapitalClaimCompanyIncomeStartDate",
                                "OLD"."ExemptionThreshold" as "OLD_ExemptionThreshold",
                                "OLD"."ExemptionThresholdCurrency" as "OLD_ExemptionThresholdCurrency",
                                "OLD"."FuturesandOptionsIncomefromLettingandLeasingStartDate" as "OLD_FuturesandOptionsIncomefromLettingandLeasingStartDate",
                                "OLD"."FuturesandOptionsInvestmentCompanyIncomeStartDate" as "OLD_FuturesandOptionsInvestmentCompanyIncomeStartDate",
                                "OLD"."IssueDate" as "OLD_IssueDate",
                                "OLD"."SerialNumber" as "OLD_SerialNumber",
                                "OLD"."Subtype" as "OLD_Subtype",
                                "OLD"."Type" as "OLD_Type",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::TaxExemptStatusDocument" as "OLD"
            on
                ifnull("IN"."Category", '') = "OLD"."Category" and
                ifnull("IN"."_BusinessPartner.BusinessPartnerID", '') = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
