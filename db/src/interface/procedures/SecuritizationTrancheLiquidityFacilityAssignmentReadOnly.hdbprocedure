PROCEDURE "sap.fsdm.procedures::SecuritizationTrancheLiquidityFacilityAssignmentReadOnly" (IN ROW "sap.fsdm.tabletypes::SecuritizationTrancheLiquidityFacilityAssignmentTT", OUT CURR_DEL "sap.fsdm.tabletypes::SecuritizationTrancheLiquidityFacilityAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::SecuritizationTrancheLiquidityFacilityAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Facility.FinancialContractID=' || TO_VARCHAR("_Facility.FinancialContractID") || ' ' ||
                '_Facility.IDSystem=' || TO_VARCHAR("_Facility.IDSystem") || ' ' ||
                '_SecuritizationTranche.FinancialInstrumentID=' || TO_VARCHAR("_SecuritizationTranche.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Facility.FinancialContractID",
                        "IN"."_Facility.IDSystem",
                        "IN"."_SecuritizationTranche.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Facility.FinancialContractID",
                        "IN"."_Facility.IDSystem",
                        "IN"."_SecuritizationTranche.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Facility.FinancialContractID",
                        "_Facility.IDSystem",
                        "_SecuritizationTranche.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Facility.FinancialContractID",
                                    "IN"."_Facility.IDSystem",
                                    "IN"."_SecuritizationTranche.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Facility.FinancialContractID",
                                    "IN"."_Facility.IDSystem",
                                    "IN"."_SecuritizationTranche.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "_Facility.FinancialContractID",
        "_Facility.IDSystem",
        "_SecuritizationTranche.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::SecuritizationTrancheLiquidityFacilityAssignment" WHERE
        (            "_Facility.FinancialContractID" ,
            "_Facility.IDSystem" ,
            "_SecuritizationTranche.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."_Facility.FinancialContractID",
            "OLD"."_Facility.IDSystem",
            "OLD"."_SecuritizationTranche.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::SecuritizationTrancheLiquidityFacilityAssignment" as "OLD"
            on
               ifnull( "IN"."_Facility.FinancialContractID",'' ) = "OLD"."_Facility.FinancialContractID" and
               ifnull( "IN"."_Facility.IDSystem",'' ) = "OLD"."_Facility.IDSystem" and
               ifnull( "IN"."_SecuritizationTranche.FinancialInstrumentID",'' ) = "OLD"."_SecuritizationTranche.FinancialInstrumentID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "_Facility.FinancialContractID",
        "_Facility.IDSystem",
        "_SecuritizationTranche.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "_Facility.FinancialContractID", '' ) as "_Facility.FinancialContractID",
                    ifnull( "_Facility.IDSystem", '' ) as "_Facility.IDSystem",
                    ifnull( "_SecuritizationTranche.FinancialInstrumentID", '' ) as "_SecuritizationTranche.FinancialInstrumentID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD__Facility.FinancialContractID" as "_Facility.FinancialContractID" ,
                    "OLD__Facility.IDSystem" as "_Facility.IDSystem" ,
                    "OLD__SecuritizationTranche.FinancialInstrumentID" as "_SecuritizationTranche.FinancialInstrumentID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."_Facility.FinancialContractID",
                        "IN"."_Facility.IDSystem",
                        "IN"."_SecuritizationTranche.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."_Facility.FinancialContractID" as "OLD__Facility.FinancialContractID",
                                "OLD"."_Facility.IDSystem" as "OLD__Facility.IDSystem",
                                "OLD"."_SecuritizationTranche.FinancialInstrumentID" as "OLD__SecuritizationTranche.FinancialInstrumentID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::SecuritizationTrancheLiquidityFacilityAssignment" as "OLD"
            on
                ifnull( "IN"."_Facility.FinancialContractID", '') = "OLD"."_Facility.FinancialContractID" and
                ifnull( "IN"."_Facility.IDSystem", '') = "OLD"."_Facility.IDSystem" and
                ifnull( "IN"."_SecuritizationTranche.FinancialInstrumentID", '') = "OLD"."_SecuritizationTranche.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD__Facility.FinancialContractID" as "_Facility.FinancialContractID",
            "OLD__Facility.IDSystem" as "_Facility.IDSystem",
            "OLD__SecuritizationTranche.FinancialInstrumentID" as "_SecuritizationTranche.FinancialInstrumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."_Facility.FinancialContractID",
                        "IN"."_Facility.IDSystem",
                        "IN"."_SecuritizationTranche.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."_Facility.FinancialContractID" as "OLD__Facility.FinancialContractID",
                                "OLD"."_Facility.IDSystem" as "OLD__Facility.IDSystem",
                                "OLD"."_SecuritizationTranche.FinancialInstrumentID" as "OLD__SecuritizationTranche.FinancialInstrumentID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::SecuritizationTrancheLiquidityFacilityAssignment" as "OLD"
            on
                ifnull("IN"."_Facility.FinancialContractID", '') = "OLD"."_Facility.FinancialContractID" and
                ifnull("IN"."_Facility.IDSystem", '') = "OLD"."_Facility.IDSystem" and
                ifnull("IN"."_SecuritizationTranche.FinancialInstrumentID", '') = "OLD"."_SecuritizationTranche.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
