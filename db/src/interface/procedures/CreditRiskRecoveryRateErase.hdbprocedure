PROCEDURE "sap.fsdm.procedures::CreditRiskRecoveryRateErase" (IN ROW "sap.fsdm.tabletypes::CreditRiskRecoveryRateTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "RecoveryRateEstimationMethod" is null and
            "RecoveryRateTimeHorizon" is null and
            "RecoveryRateTimeHorizonUnit" is null and
            "RiskProvisionScenario" is null and
            "_CollateralPortion.PortionNumber" is null and
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" is null and
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" is null and
            "_CombinedCollateralAgreement.FinancialContractID" is null and
            "_CombinedCollateralAgreement.IDSystem" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialContractUsedAsCollateral.FinancialContractID" is null and
            "_FinancialContractUsedAsCollateral.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID" is null and
            "_PhysicalAssetUsedAsCollateral.PhysicalAssetID" is null and
            "_ReceivableUsedAsCollateral.ReceivableID" is null and
            "_SimpleCollateralAgreement.FinancialContractID" is null and
            "_SimpleCollateralAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::CreditRiskRecoveryRate"
        WHERE
        (            "RecoveryRateEstimationMethod" ,
            "RecoveryRateTimeHorizon" ,
            "RecoveryRateTimeHorizonUnit" ,
            "RiskProvisionScenario" ,
            "_CollateralPortion.PortionNumber" ,
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "_CombinedCollateralAgreement.FinancialContractID" ,
            "_CombinedCollateralAgreement.IDSystem" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialContractUsedAsCollateral.FinancialContractID" ,
            "_FinancialContractUsedAsCollateral.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID" ,
            "_PhysicalAssetUsedAsCollateral.PhysicalAssetID" ,
            "_ReceivableUsedAsCollateral.ReceivableID" ,
            "_SimpleCollateralAgreement.FinancialContractID" ,
            "_SimpleCollateralAgreement.IDSystem" 
        ) in
        (
            select                 "OLD"."RecoveryRateEstimationMethod" ,
                "OLD"."RecoveryRateTimeHorizon" ,
                "OLD"."RecoveryRateTimeHorizonUnit" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."_CollateralPortion.PortionNumber" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."_CombinedCollateralAgreement.FinancialContractID" ,
                "OLD"."_CombinedCollateralAgreement.IDSystem" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialContractUsedAsCollateral.FinancialContractID" ,
                "OLD"."_FinancialContractUsedAsCollateral.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID" ,
                "OLD"."_PhysicalAssetUsedAsCollateral.PhysicalAssetID" ,
                "OLD"."_ReceivableUsedAsCollateral.ReceivableID" ,
                "OLD"."_SimpleCollateralAgreement.FinancialContractID" ,
                "OLD"."_SimpleCollateralAgreement.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditRiskRecoveryRate" "OLD"
            on
            "IN"."RecoveryRateEstimationMethod" = "OLD"."RecoveryRateEstimationMethod" and
            "IN"."RecoveryRateTimeHorizon" = "OLD"."RecoveryRateTimeHorizon" and
            "IN"."RecoveryRateTimeHorizonUnit" = "OLD"."RecoveryRateTimeHorizonUnit" and
            "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
            "IN"."_CollateralPortion.PortionNumber" = "OLD"."_CollateralPortion.PortionNumber" and
            "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
            "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
            "IN"."_CombinedCollateralAgreement.FinancialContractID" = "OLD"."_CombinedCollateralAgreement.FinancialContractID" and
            "IN"."_CombinedCollateralAgreement.IDSystem" = "OLD"."_CombinedCollateralAgreement.IDSystem" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
            "IN"."_FinancialContractUsedAsCollateral.FinancialContractID" = "OLD"."_FinancialContractUsedAsCollateral.FinancialContractID" and
            "IN"."_FinancialContractUsedAsCollateral.IDSystem" = "OLD"."_FinancialContractUsedAsCollateral.IDSystem" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
            "IN"."_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID" = "OLD"."_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID" and
            "IN"."_PhysicalAssetUsedAsCollateral.PhysicalAssetID" = "OLD"."_PhysicalAssetUsedAsCollateral.PhysicalAssetID" and
            "IN"."_ReceivableUsedAsCollateral.ReceivableID" = "OLD"."_ReceivableUsedAsCollateral.ReceivableID" and
            "IN"."_SimpleCollateralAgreement.FinancialContractID" = "OLD"."_SimpleCollateralAgreement.FinancialContractID" and
            "IN"."_SimpleCollateralAgreement.IDSystem" = "OLD"."_SimpleCollateralAgreement.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::CreditRiskRecoveryRate_Historical"
        WHERE
        (
            "RecoveryRateEstimationMethod" ,
            "RecoveryRateTimeHorizon" ,
            "RecoveryRateTimeHorizonUnit" ,
            "RiskProvisionScenario" ,
            "_CollateralPortion.PortionNumber" ,
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "_CombinedCollateralAgreement.FinancialContractID" ,
            "_CombinedCollateralAgreement.IDSystem" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialContractUsedAsCollateral.FinancialContractID" ,
            "_FinancialContractUsedAsCollateral.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID" ,
            "_PhysicalAssetUsedAsCollateral.PhysicalAssetID" ,
            "_ReceivableUsedAsCollateral.ReceivableID" ,
            "_SimpleCollateralAgreement.FinancialContractID" ,
            "_SimpleCollateralAgreement.IDSystem" 
        ) in
        (
            select
                "OLD"."RecoveryRateEstimationMethod" ,
                "OLD"."RecoveryRateTimeHorizon" ,
                "OLD"."RecoveryRateTimeHorizonUnit" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."_CollateralPortion.PortionNumber" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."_CombinedCollateralAgreement.FinancialContractID" ,
                "OLD"."_CombinedCollateralAgreement.IDSystem" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialContractUsedAsCollateral.FinancialContractID" ,
                "OLD"."_FinancialContractUsedAsCollateral.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID" ,
                "OLD"."_PhysicalAssetUsedAsCollateral.PhysicalAssetID" ,
                "OLD"."_ReceivableUsedAsCollateral.ReceivableID" ,
                "OLD"."_SimpleCollateralAgreement.FinancialContractID" ,
                "OLD"."_SimpleCollateralAgreement.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditRiskRecoveryRate_Historical" "OLD"
            on
                "IN"."RecoveryRateEstimationMethod" = "OLD"."RecoveryRateEstimationMethod" and
                "IN"."RecoveryRateTimeHorizon" = "OLD"."RecoveryRateTimeHorizon" and
                "IN"."RecoveryRateTimeHorizonUnit" = "OLD"."RecoveryRateTimeHorizonUnit" and
                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                "IN"."_CollateralPortion.PortionNumber" = "OLD"."_CollateralPortion.PortionNumber" and
                "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
                "IN"."_CombinedCollateralAgreement.FinancialContractID" = "OLD"."_CombinedCollateralAgreement.FinancialContractID" and
                "IN"."_CombinedCollateralAgreement.IDSystem" = "OLD"."_CombinedCollateralAgreement.IDSystem" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialContractUsedAsCollateral.FinancialContractID" = "OLD"."_FinancialContractUsedAsCollateral.FinancialContractID" and
                "IN"."_FinancialContractUsedAsCollateral.IDSystem" = "OLD"."_FinancialContractUsedAsCollateral.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID" = "OLD"."_FinancialInstrumentUsedAsCollateral.FinancialInstrumentID" and
                "IN"."_PhysicalAssetUsedAsCollateral.PhysicalAssetID" = "OLD"."_PhysicalAssetUsedAsCollateral.PhysicalAssetID" and
                "IN"."_ReceivableUsedAsCollateral.ReceivableID" = "OLD"."_ReceivableUsedAsCollateral.ReceivableID" and
                "IN"."_SimpleCollateralAgreement.FinancialContractID" = "OLD"."_SimpleCollateralAgreement.FinancialContractID" and
                "IN"."_SimpleCollateralAgreement.IDSystem" = "OLD"."_SimpleCollateralAgreement.IDSystem" 
        );

END
