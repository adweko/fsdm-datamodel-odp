PROCEDURE "sap.fsdm.procedures::OptionRightReferenceRateAssignmentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::OptionRightReferenceRateAssignmentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::OptionRightReferenceRateAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::OptionRightReferenceRateAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_ConditionOptionRight.SequenceNumber=' || TO_VARCHAR("_ConditionOptionRight.SequenceNumber") || ' ' ||
                '_ConditionOptionRight._FinancialContract.FinancialContractID=' || TO_VARCHAR("_ConditionOptionRight._FinancialContract.FinancialContractID") || ' ' ||
                '_ConditionOptionRight._FinancialContract.IDSystem=' || TO_VARCHAR("_ConditionOptionRight._FinancialContract.IDSystem") || ' ' ||
                '_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber=' || TO_VARCHAR("_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber") || ' ' ||
                '_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID=' || TO_VARCHAR("_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID") || ' ' ||
                '_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem=' || TO_VARCHAR("_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem") || ' ' ||
                '_ReferenceRate.ReferenceRateID=' || TO_VARCHAR("_ReferenceRate.ReferenceRateID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_ConditionOptionRight.SequenceNumber",
                        "IN"."_ConditionOptionRight._FinancialContract.FinancialContractID",
                        "IN"."_ConditionOptionRight._FinancialContract.IDSystem",
                        "IN"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
                        "IN"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "IN"."_ReferenceRate.ReferenceRateID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_ConditionOptionRight.SequenceNumber",
                        "IN"."_ConditionOptionRight._FinancialContract.FinancialContractID",
                        "IN"."_ConditionOptionRight._FinancialContract.IDSystem",
                        "IN"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
                        "IN"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "IN"."_ReferenceRate.ReferenceRateID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_ConditionOptionRight.SequenceNumber",
                        "_ConditionOptionRight._FinancialContract.FinancialContractID",
                        "_ConditionOptionRight._FinancialContract.IDSystem",
                        "_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
                        "_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "_ReferenceRate.ReferenceRateID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_ConditionOptionRight.SequenceNumber",
                                    "IN"."_ConditionOptionRight._FinancialContract.FinancialContractID",
                                    "IN"."_ConditionOptionRight._FinancialContract.IDSystem",
                                    "IN"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                                    "IN"."_ReferenceRate.ReferenceRateID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_ConditionOptionRight.SequenceNumber",
                                    "IN"."_ConditionOptionRight._FinancialContract.FinancialContractID",
                                    "IN"."_ConditionOptionRight._FinancialContract.IDSystem",
                                    "IN"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                                    "IN"."_ReferenceRate.ReferenceRateID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_ConditionOptionRight.SequenceNumber" is null and
            "_ConditionOptionRight._FinancialContract.FinancialContractID" is null and
            "_ConditionOptionRight._FinancialContract.IDSystem" is null and
            "_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" is null and
            "_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" is null and
            "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" is null and
            "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" is null and
            "_ReferenceRate.ReferenceRateID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_ConditionOptionRight.SequenceNumber",
            "_ConditionOptionRight._FinancialContract.FinancialContractID",
            "_ConditionOptionRight._FinancialContract.IDSystem",
            "_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
            "_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
            "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "_ReferenceRate.ReferenceRateID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::OptionRightReferenceRateAssignment" WHERE
            (
            "_ConditionOptionRight.SequenceNumber" ,
            "_ConditionOptionRight._FinancialContract.FinancialContractID" ,
            "_ConditionOptionRight._FinancialContract.IDSystem" ,
            "_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" ,
            "_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" ,
            "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "_ReferenceRate.ReferenceRateID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_ConditionOptionRight.SequenceNumber",
            "OLD"."_ConditionOptionRight._FinancialContract.FinancialContractID",
            "OLD"."_ConditionOptionRight._FinancialContract.IDSystem",
            "OLD"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
            "OLD"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
            "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "OLD"."_ReferenceRate.ReferenceRateID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::OptionRightReferenceRateAssignment" as "OLD"
        on
                              "IN"."_ConditionOptionRight.SequenceNumber" = "OLD"."_ConditionOptionRight.SequenceNumber" and
                              "IN"."_ConditionOptionRight._FinancialContract.FinancialContractID" = "OLD"."_ConditionOptionRight._FinancialContract.FinancialContractID" and
                              "IN"."_ConditionOptionRight._FinancialContract.IDSystem" = "OLD"."_ConditionOptionRight._FinancialContract.IDSystem" and
                              "IN"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" = "OLD"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" and
                              "IN"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" and
                              "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                              "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" and
                              "IN"."_ReferenceRate.ReferenceRateID" = "OLD"."_ReferenceRate.ReferenceRateID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_ConditionOptionRight.SequenceNumber",
        "_ConditionOptionRight._FinancialContract.FinancialContractID",
        "_ConditionOptionRight._FinancialContract.IDSystem",
        "_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
        "_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
        "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
        "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
        "_ReferenceRate.ReferenceRateID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "NewSpread",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__ConditionOptionRight.SequenceNumber" as "_ConditionOptionRight.SequenceNumber" ,
            "OLD__ConditionOptionRight._FinancialContract.FinancialContractID" as "_ConditionOptionRight._FinancialContract.FinancialContractID" ,
            "OLD__ConditionOptionRight._FinancialContract.IDSystem" as "_ConditionOptionRight._FinancialContract.IDSystem" ,
            "OLD__ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" as "_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" ,
            "OLD__ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" as "_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" ,
            "OLD__ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "OLD__ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" as "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "OLD__ReferenceRate.ReferenceRateID" as "_ReferenceRate.ReferenceRateID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_NewSpread" as "NewSpread" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_ConditionOptionRight.SequenceNumber",
                        "OLD"."_ConditionOptionRight._FinancialContract.FinancialContractID",
                        "OLD"."_ConditionOptionRight._FinancialContract.IDSystem",
                        "OLD"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."_ReferenceRate.ReferenceRateID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_ConditionOptionRight.SequenceNumber" AS "OLD__ConditionOptionRight.SequenceNumber" ,
                "OLD"."_ConditionOptionRight._FinancialContract.FinancialContractID" AS "OLD__ConditionOptionRight._FinancialContract.FinancialContractID" ,
                "OLD"."_ConditionOptionRight._FinancialContract.IDSystem" AS "OLD__ConditionOptionRight._FinancialContract.IDSystem" ,
                "OLD"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" AS "OLD__ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" AS "OLD__ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" AS "OLD__ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" AS "OLD__ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."_ReferenceRate.ReferenceRateID" AS "OLD__ReferenceRate.ReferenceRateID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."NewSpread" AS "OLD_NewSpread" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::OptionRightReferenceRateAssignment" as "OLD"
            on
                                      "IN"."_ConditionOptionRight.SequenceNumber" = "OLD"."_ConditionOptionRight.SequenceNumber" and
                                      "IN"."_ConditionOptionRight._FinancialContract.FinancialContractID" = "OLD"."_ConditionOptionRight._FinancialContract.FinancialContractID" and
                                      "IN"."_ConditionOptionRight._FinancialContract.IDSystem" = "OLD"."_ConditionOptionRight._FinancialContract.IDSystem" and
                                      "IN"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" = "OLD"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" and
                                      "IN"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" and
                                      "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                                      "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" and
                                      "IN"."_ReferenceRate.ReferenceRateID" = "OLD"."_ReferenceRate.ReferenceRateID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__ConditionOptionRight.SequenceNumber" as "_ConditionOptionRight.SequenceNumber",
            "OLD__ConditionOptionRight._FinancialContract.FinancialContractID" as "_ConditionOptionRight._FinancialContract.FinancialContractID",
            "OLD__ConditionOptionRight._FinancialContract.IDSystem" as "_ConditionOptionRight._FinancialContract.IDSystem",
            "OLD__ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" as "_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
            "OLD__ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" as "_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
            "OLD__ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD__ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" as "_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "OLD__ReferenceRate.ReferenceRateID" as "_ReferenceRate.ReferenceRateID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_NewSpread" as "NewSpread",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_ConditionOptionRight.SequenceNumber",
                        "OLD"."_ConditionOptionRight._FinancialContract.FinancialContractID",
                        "OLD"."_ConditionOptionRight._FinancialContract.IDSystem",
                        "OLD"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."_ReferenceRate.ReferenceRateID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_ConditionOptionRight.SequenceNumber" AS "OLD__ConditionOptionRight.SequenceNumber" ,
                "OLD"."_ConditionOptionRight._FinancialContract.FinancialContractID" AS "OLD__ConditionOptionRight._FinancialContract.FinancialContractID" ,
                "OLD"."_ConditionOptionRight._FinancialContract.IDSystem" AS "OLD__ConditionOptionRight._FinancialContract.IDSystem" ,
                "OLD"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" AS "OLD__ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" AS "OLD__ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" AS "OLD__ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" AS "OLD__ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."_ReferenceRate.ReferenceRateID" AS "OLD__ReferenceRate.ReferenceRateID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."NewSpread" AS "OLD_NewSpread" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::OptionRightReferenceRateAssignment" as "OLD"
            on
               "IN"."_ConditionOptionRight.SequenceNumber" = "OLD"."_ConditionOptionRight.SequenceNumber" and
               "IN"."_ConditionOptionRight._FinancialContract.FinancialContractID" = "OLD"."_ConditionOptionRight._FinancialContract.FinancialContractID" and
               "IN"."_ConditionOptionRight._FinancialContract.IDSystem" = "OLD"."_ConditionOptionRight._FinancialContract.IDSystem" and
               "IN"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" = "OLD"."_ConditionOptionRight._FinancialInstrument.FinancialInstrumentID" and
               "IN"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_ConditionOptionRight._TrancheInSyndication.TrancheSequenceNumber" and
               "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
               "IN"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_ConditionOptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" and
               "IN"."_ReferenceRate.ReferenceRateID" = "OLD"."_ReferenceRate.ReferenceRateID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
