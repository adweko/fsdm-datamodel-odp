PROCEDURE "sap.fsdm.procedures::EnvironmentalObjectiveAssignmentReadOnly" (IN ROW "sap.fsdm.tabletypes::EnvironmentalObjectiveAssignmentTT", OUT CURR_DEL "sap.fsdm.tabletypes::EnvironmentalObjectiveAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::EnvironmentalObjectiveAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'BusinessPartnerKPIType=' || TO_VARCHAR("BusinessPartnerKPIType") || ' ' ||
                'DataProvider=' || TO_VARCHAR("DataProvider") || ' ' ||
                'FulfillmentType=' || TO_VARCHAR("FulfillmentType") || ' ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                '_EconomicActivity.EconomicActivityID=' || TO_VARCHAR("_EconomicActivity.EconomicActivityID") || ' ' ||
                '_EconomicActivity.TaxonomyID=' || TO_VARCHAR("_EconomicActivity.TaxonomyID") || ' ' ||
                '_EnvironmentalObjective.EnvironmentalObjectiveType=' || TO_VARCHAR("_EnvironmentalObjective.EnvironmentalObjectiveType") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_PhysicalAsset.PhysicalAssetID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."BusinessPartnerKPIType",
                        "IN"."DataProvider",
                        "IN"."FulfillmentType",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_EconomicActivity.EconomicActivityID",
                        "IN"."_EconomicActivity.TaxonomyID",
                        "IN"."_EnvironmentalObjective.EnvironmentalObjectiveType",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."BusinessPartnerKPIType",
                        "IN"."DataProvider",
                        "IN"."FulfillmentType",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_EconomicActivity.EconomicActivityID",
                        "IN"."_EconomicActivity.TaxonomyID",
                        "IN"."_EnvironmentalObjective.EnvironmentalObjectiveType",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "BusinessPartnerKPIType",
                        "DataProvider",
                        "FulfillmentType",
                        "_BusinessPartner.BusinessPartnerID",
                        "_EconomicActivity.EconomicActivityID",
                        "_EconomicActivity.TaxonomyID",
                        "_EnvironmentalObjective.EnvironmentalObjectiveType",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_PhysicalAsset.PhysicalAssetID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."BusinessPartnerKPIType",
                                    "IN"."DataProvider",
                                    "IN"."FulfillmentType",
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_EconomicActivity.EconomicActivityID",
                                    "IN"."_EconomicActivity.TaxonomyID",
                                    "IN"."_EnvironmentalObjective.EnvironmentalObjectiveType",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."BusinessPartnerKPIType",
                                    "IN"."DataProvider",
                                    "IN"."FulfillmentType",
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_EconomicActivity.EconomicActivityID",
                                    "IN"."_EconomicActivity.TaxonomyID",
                                    "IN"."_EnvironmentalObjective.EnvironmentalObjectiveType",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "BusinessPartnerKPIType",
        "DataProvider",
        "FulfillmentType",
        "_BusinessPartner.BusinessPartnerID",
        "_EconomicActivity.EconomicActivityID",
        "_EconomicActivity.TaxonomyID",
        "_EnvironmentalObjective.EnvironmentalObjectiveType",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::EnvironmentalObjectiveAssignment" WHERE
        (            "BusinessPartnerKPIType" ,
            "DataProvider" ,
            "FulfillmentType" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_EconomicActivity.EconomicActivityID" ,
            "_EconomicActivity.TaxonomyID" ,
            "_EnvironmentalObjective.EnvironmentalObjectiveType" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_PhysicalAsset.PhysicalAssetID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."BusinessPartnerKPIType",
            "OLD"."DataProvider",
            "OLD"."FulfillmentType",
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."_EconomicActivity.EconomicActivityID",
            "OLD"."_EconomicActivity.TaxonomyID",
            "OLD"."_EnvironmentalObjective.EnvironmentalObjectiveType",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_PhysicalAsset.PhysicalAssetID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::EnvironmentalObjectiveAssignment" as "OLD"
            on
               ifnull( "IN"."BusinessPartnerKPIType",'' ) = "OLD"."BusinessPartnerKPIType" and
               ifnull( "IN"."DataProvider",'' ) = "OLD"."DataProvider" and
               ifnull( "IN"."FulfillmentType",'' ) = "OLD"."FulfillmentType" and
               ifnull( "IN"."_BusinessPartner.BusinessPartnerID",'' ) = "OLD"."_BusinessPartner.BusinessPartnerID" and
               ifnull( "IN"."_EconomicActivity.EconomicActivityID",'' ) = "OLD"."_EconomicActivity.EconomicActivityID" and
               ifnull( "IN"."_EconomicActivity.TaxonomyID",'' ) = "OLD"."_EconomicActivity.TaxonomyID" and
               ifnull( "IN"."_EnvironmentalObjective.EnvironmentalObjectiveType",'' ) = "OLD"."_EnvironmentalObjective.EnvironmentalObjectiveType" and
               ifnull( "IN"."_FinancialContract.FinancialContractID",'' ) = "OLD"."_FinancialContract.FinancialContractID" and
               ifnull( "IN"."_FinancialContract.IDSystem",'' ) = "OLD"."_FinancialContract.IDSystem" and
               ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID",'' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               ifnull( "IN"."_PhysicalAsset.PhysicalAssetID",'' ) = "OLD"."_PhysicalAsset.PhysicalAssetID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "BusinessPartnerKPIType",
        "DataProvider",
        "FulfillmentType",
        "_BusinessPartner.BusinessPartnerID",
        "_EconomicActivity.EconomicActivityID",
        "_EconomicActivity.TaxonomyID",
        "_EnvironmentalObjective.EnvironmentalObjectiveType",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AlignmentRatio",
        "EligibilityRatio",
        "MostRelevantEnvironmentalObjective",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "BusinessPartnerKPIType", '' ) as "BusinessPartnerKPIType",
                    ifnull( "DataProvider", '' ) as "DataProvider",
                    ifnull( "FulfillmentType", '' ) as "FulfillmentType",
                    ifnull( "_BusinessPartner.BusinessPartnerID", '' ) as "_BusinessPartner.BusinessPartnerID",
                    ifnull( "_EconomicActivity.EconomicActivityID", '' ) as "_EconomicActivity.EconomicActivityID",
                    ifnull( "_EconomicActivity.TaxonomyID", '' ) as "_EconomicActivity.TaxonomyID",
                    ifnull( "_EnvironmentalObjective.EnvironmentalObjectiveType", '' ) as "_EnvironmentalObjective.EnvironmentalObjectiveType",
                    ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
                    ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
                    ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
                    ifnull( "_PhysicalAsset.PhysicalAssetID", '' ) as "_PhysicalAsset.PhysicalAssetID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "AlignmentRatio"  ,
                    "EligibilityRatio"  ,
                    "MostRelevantEnvironmentalObjective"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_BusinessPartnerKPIType" as "BusinessPartnerKPIType" ,
                    "OLD_DataProvider" as "DataProvider" ,
                    "OLD_FulfillmentType" as "FulfillmentType" ,
                    "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
                    "OLD__EconomicActivity.EconomicActivityID" as "_EconomicActivity.EconomicActivityID" ,
                    "OLD__EconomicActivity.TaxonomyID" as "_EconomicActivity.TaxonomyID" ,
                    "OLD__EnvironmentalObjective.EnvironmentalObjectiveType" as "_EnvironmentalObjective.EnvironmentalObjectiveType" ,
                    "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
                    "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
                    "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
                    "OLD__PhysicalAsset.PhysicalAssetID" as "_PhysicalAsset.PhysicalAssetID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_AlignmentRatio" as "AlignmentRatio" ,
                    "OLD_EligibilityRatio" as "EligibilityRatio" ,
                    "OLD_MostRelevantEnvironmentalObjective" as "MostRelevantEnvironmentalObjective" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."BusinessPartnerKPIType",
                        "IN"."DataProvider",
                        "IN"."FulfillmentType",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_EconomicActivity.EconomicActivityID",
                        "IN"."_EconomicActivity.TaxonomyID",
                        "IN"."_EnvironmentalObjective.EnvironmentalObjectiveType",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."BusinessPartnerKPIType" as "OLD_BusinessPartnerKPIType",
                                "OLD"."DataProvider" as "OLD_DataProvider",
                                "OLD"."FulfillmentType" as "OLD_FulfillmentType",
                                "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                                "OLD"."_EconomicActivity.EconomicActivityID" as "OLD__EconomicActivity.EconomicActivityID",
                                "OLD"."_EconomicActivity.TaxonomyID" as "OLD__EconomicActivity.TaxonomyID",
                                "OLD"."_EnvironmentalObjective.EnvironmentalObjectiveType" as "OLD__EnvironmentalObjective.EnvironmentalObjectiveType",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_PhysicalAsset.PhysicalAssetID" as "OLD__PhysicalAsset.PhysicalAssetID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AlignmentRatio" as "OLD_AlignmentRatio",
                                "OLD"."EligibilityRatio" as "OLD_EligibilityRatio",
                                "OLD"."MostRelevantEnvironmentalObjective" as "OLD_MostRelevantEnvironmentalObjective",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::EnvironmentalObjectiveAssignment" as "OLD"
            on
                ifnull( "IN"."BusinessPartnerKPIType", '') = "OLD"."BusinessPartnerKPIType" and
                ifnull( "IN"."DataProvider", '') = "OLD"."DataProvider" and
                ifnull( "IN"."FulfillmentType", '') = "OLD"."FulfillmentType" and
                ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '') = "OLD"."_BusinessPartner.BusinessPartnerID" and
                ifnull( "IN"."_EconomicActivity.EconomicActivityID", '') = "OLD"."_EconomicActivity.EconomicActivityID" and
                ifnull( "IN"."_EconomicActivity.TaxonomyID", '') = "OLD"."_EconomicActivity.TaxonomyID" and
                ifnull( "IN"."_EnvironmentalObjective.EnvironmentalObjectiveType", '') = "OLD"."_EnvironmentalObjective.EnvironmentalObjectiveType" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_PhysicalAsset.PhysicalAssetID", '') = "OLD"."_PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_BusinessPartnerKPIType" as "BusinessPartnerKPIType",
            "OLD_DataProvider" as "DataProvider",
            "OLD_FulfillmentType" as "FulfillmentType",
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID",
            "OLD__EconomicActivity.EconomicActivityID" as "_EconomicActivity.EconomicActivityID",
            "OLD__EconomicActivity.TaxonomyID" as "_EconomicActivity.TaxonomyID",
            "OLD__EnvironmentalObjective.EnvironmentalObjectiveType" as "_EnvironmentalObjective.EnvironmentalObjectiveType",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__PhysicalAsset.PhysicalAssetID" as "_PhysicalAsset.PhysicalAssetID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AlignmentRatio" as "AlignmentRatio",
            "OLD_EligibilityRatio" as "EligibilityRatio",
            "OLD_MostRelevantEnvironmentalObjective" as "MostRelevantEnvironmentalObjective",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."BusinessPartnerKPIType",
                        "IN"."DataProvider",
                        "IN"."FulfillmentType",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_EconomicActivity.EconomicActivityID",
                        "IN"."_EconomicActivity.TaxonomyID",
                        "IN"."_EnvironmentalObjective.EnvironmentalObjectiveType",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."BusinessPartnerKPIType" as "OLD_BusinessPartnerKPIType",
                                "OLD"."DataProvider" as "OLD_DataProvider",
                                "OLD"."FulfillmentType" as "OLD_FulfillmentType",
                                "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                                "OLD"."_EconomicActivity.EconomicActivityID" as "OLD__EconomicActivity.EconomicActivityID",
                                "OLD"."_EconomicActivity.TaxonomyID" as "OLD__EconomicActivity.TaxonomyID",
                                "OLD"."_EnvironmentalObjective.EnvironmentalObjectiveType" as "OLD__EnvironmentalObjective.EnvironmentalObjectiveType",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_PhysicalAsset.PhysicalAssetID" as "OLD__PhysicalAsset.PhysicalAssetID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AlignmentRatio" as "OLD_AlignmentRatio",
                                "OLD"."EligibilityRatio" as "OLD_EligibilityRatio",
                                "OLD"."MostRelevantEnvironmentalObjective" as "OLD_MostRelevantEnvironmentalObjective",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::EnvironmentalObjectiveAssignment" as "OLD"
            on
                ifnull("IN"."BusinessPartnerKPIType", '') = "OLD"."BusinessPartnerKPIType" and
                ifnull("IN"."DataProvider", '') = "OLD"."DataProvider" and
                ifnull("IN"."FulfillmentType", '') = "OLD"."FulfillmentType" and
                ifnull("IN"."_BusinessPartner.BusinessPartnerID", '') = "OLD"."_BusinessPartner.BusinessPartnerID" and
                ifnull("IN"."_EconomicActivity.EconomicActivityID", '') = "OLD"."_EconomicActivity.EconomicActivityID" and
                ifnull("IN"."_EconomicActivity.TaxonomyID", '') = "OLD"."_EconomicActivity.TaxonomyID" and
                ifnull("IN"."_EnvironmentalObjective.EnvironmentalObjectiveType", '') = "OLD"."_EnvironmentalObjective.EnvironmentalObjectiveType" and
                ifnull("IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull("IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull("IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull("IN"."_PhysicalAsset.PhysicalAssetID", '') = "OLD"."_PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
