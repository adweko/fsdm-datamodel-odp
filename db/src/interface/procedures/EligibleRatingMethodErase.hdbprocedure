PROCEDURE "sap.fsdm.procedures::EligibleRatingMethodErase" (IN ROW "sap.fsdm.tabletypes::EligibleRatingMethodTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Regulation" is null and
            "_RatingMethod.RatingMethod" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::EligibleRatingMethod"
        WHERE
        (            "Regulation" ,
            "_RatingMethod.RatingMethod" 
        ) in
        (
            select                 "OLD"."Regulation" ,
                "OLD"."_RatingMethod.RatingMethod" 
            from :ROW "IN"
            inner join "sap.fsdm::EligibleRatingMethod" "OLD"
            on
            "IN"."Regulation" = "OLD"."Regulation" and
            "IN"."_RatingMethod.RatingMethod" = "OLD"."_RatingMethod.RatingMethod" 
        );

        --delete data from history table
        delete from "sap.fsdm::EligibleRatingMethod_Historical"
        WHERE
        (
            "Regulation" ,
            "_RatingMethod.RatingMethod" 
        ) in
        (
            select
                "OLD"."Regulation" ,
                "OLD"."_RatingMethod.RatingMethod" 
            from :ROW "IN"
            inner join "sap.fsdm::EligibleRatingMethod_Historical" "OLD"
            on
                "IN"."Regulation" = "OLD"."Regulation" and
                "IN"."_RatingMethod.RatingMethod" = "OLD"."_RatingMethod.RatingMethod" 
        );

END
