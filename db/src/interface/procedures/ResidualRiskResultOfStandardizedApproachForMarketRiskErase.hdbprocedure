PROCEDURE "sap.fsdm.procedures::ResidualRiskResultOfStandardizedApproachForMarketRiskErase" (IN ROW "sap.fsdm.tabletypes::ResidualRiskResultOfStandardizedApproachForMarketRiskTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "RiskClass" is null and
            "RiskProvisioningScenario" is null and
            "RoleOfCurrency" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_RiskReportingNode.RiskReportingNodeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::ResidualRiskResultOfStandardizedApproachForMarketRisk"
        WHERE
        (            "RiskClass" ,
            "RiskProvisioningScenario" ,
            "RoleOfCurrency" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" 
        ) in
        (
            select                 "OLD"."RiskClass" ,
                "OLD"."RiskProvisioningScenario" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" 
            from :ROW "IN"
            inner join "sap.fsdm::ResidualRiskResultOfStandardizedApproachForMarketRisk" "OLD"
            on
            "IN"."RiskClass" = "OLD"."RiskClass" and
            "IN"."RiskProvisioningScenario" = "OLD"."RiskProvisioningScenario" and
            "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
            "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
            "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
            "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" 
        );

        --delete data from history table
        delete from "sap.fsdm::ResidualRiskResultOfStandardizedApproachForMarketRisk_Historical"
        WHERE
        (
            "RiskClass" ,
            "RiskProvisioningScenario" ,
            "RoleOfCurrency" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" 
        ) in
        (
            select
                "OLD"."RiskClass" ,
                "OLD"."RiskProvisioningScenario" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" 
            from :ROW "IN"
            inner join "sap.fsdm::ResidualRiskResultOfStandardizedApproachForMarketRisk_Historical" "OLD"
            on
                "IN"."RiskClass" = "OLD"."RiskClass" and
                "IN"."RiskProvisioningScenario" = "OLD"."RiskProvisioningScenario" and
                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" 
        );

END
