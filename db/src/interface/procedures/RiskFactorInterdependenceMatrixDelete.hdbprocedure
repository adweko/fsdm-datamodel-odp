PROCEDURE "sap.fsdm.procedures::RiskFactorInterdependenceMatrixDelete" (IN ROW "sap.fsdm.tabletypes::RiskFactorInterdependenceMatrixTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ColumnRiskFactorType=' || TO_VARCHAR("ColumnRiskFactorType") || ' ' ||
                'ColumnRiskFactorTypeValue=' || TO_VARCHAR("ColumnRiskFactorTypeValue") || ' ' ||
                'MatrixCategory=' || TO_VARCHAR("MatrixCategory") || ' ' ||
                'MatrixID=' || TO_VARCHAR("MatrixID") || ' ' ||
                'RowRiskFactorType=' || TO_VARCHAR("RowRiskFactorType") || ' ' ||
                'RowRiskFactorTypeValue=' || TO_VARCHAR("RowRiskFactorTypeValue") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ColumnRiskFactorType",
                        "IN"."ColumnRiskFactorTypeValue",
                        "IN"."MatrixCategory",
                        "IN"."MatrixID",
                        "IN"."RowRiskFactorType",
                        "IN"."RowRiskFactorTypeValue"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ColumnRiskFactorType",
                        "IN"."ColumnRiskFactorTypeValue",
                        "IN"."MatrixCategory",
                        "IN"."MatrixID",
                        "IN"."RowRiskFactorType",
                        "IN"."RowRiskFactorTypeValue"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ColumnRiskFactorType",
                        "ColumnRiskFactorTypeValue",
                        "MatrixCategory",
                        "MatrixID",
                        "RowRiskFactorType",
                        "RowRiskFactorTypeValue"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ColumnRiskFactorType",
                                    "IN"."ColumnRiskFactorTypeValue",
                                    "IN"."MatrixCategory",
                                    "IN"."MatrixID",
                                    "IN"."RowRiskFactorType",
                                    "IN"."RowRiskFactorTypeValue"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ColumnRiskFactorType",
                                    "IN"."ColumnRiskFactorTypeValue",
                                    "IN"."MatrixCategory",
                                    "IN"."MatrixID",
                                    "IN"."RowRiskFactorType",
                                    "IN"."RowRiskFactorTypeValue"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ColumnRiskFactorType" is null and
            "ColumnRiskFactorTypeValue" is null and
            "MatrixCategory" is null and
            "MatrixID" is null and
            "RowRiskFactorType" is null and
            "RowRiskFactorTypeValue" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::RiskFactorInterdependenceMatrix" (
        "ColumnRiskFactorType",
        "ColumnRiskFactorTypeValue",
        "MatrixCategory",
        "MatrixID",
        "RowRiskFactorType",
        "RowRiskFactorTypeValue",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Amount",
        "CalculationMethod",
        "DataProvider",
        "Description",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ColumnRiskFactorType" as "ColumnRiskFactorType" ,
            "OLD_ColumnRiskFactorTypeValue" as "ColumnRiskFactorTypeValue" ,
            "OLD_MatrixCategory" as "MatrixCategory" ,
            "OLD_MatrixID" as "MatrixID" ,
            "OLD_RowRiskFactorType" as "RowRiskFactorType" ,
            "OLD_RowRiskFactorTypeValue" as "RowRiskFactorTypeValue" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Amount" as "Amount" ,
            "OLD_CalculationMethod" as "CalculationMethod" ,
            "OLD_DataProvider" as "DataProvider" ,
            "OLD_Description" as "Description" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ColumnRiskFactorType",
                        "OLD"."ColumnRiskFactorTypeValue",
                        "OLD"."MatrixCategory",
                        "OLD"."MatrixID",
                        "OLD"."RowRiskFactorType",
                        "OLD"."RowRiskFactorTypeValue",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."ColumnRiskFactorType" AS "OLD_ColumnRiskFactorType" ,
                "OLD"."ColumnRiskFactorTypeValue" AS "OLD_ColumnRiskFactorTypeValue" ,
                "OLD"."MatrixCategory" AS "OLD_MatrixCategory" ,
                "OLD"."MatrixID" AS "OLD_MatrixID" ,
                "OLD"."RowRiskFactorType" AS "OLD_RowRiskFactorType" ,
                "OLD"."RowRiskFactorTypeValue" AS "OLD_RowRiskFactorTypeValue" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Amount" AS "OLD_Amount" ,
                "OLD"."CalculationMethod" AS "OLD_CalculationMethod" ,
                "OLD"."DataProvider" AS "OLD_DataProvider" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::RiskFactorInterdependenceMatrix" as "OLD"
            on
                      "IN"."ColumnRiskFactorType" = "OLD"."ColumnRiskFactorType" and
                      "IN"."ColumnRiskFactorTypeValue" = "OLD"."ColumnRiskFactorTypeValue" and
                      "IN"."MatrixCategory" = "OLD"."MatrixCategory" and
                      "IN"."MatrixID" = "OLD"."MatrixID" and
                      "IN"."RowRiskFactorType" = "OLD"."RowRiskFactorType" and
                      "IN"."RowRiskFactorTypeValue" = "OLD"."RowRiskFactorTypeValue" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::RiskFactorInterdependenceMatrix" (
        "ColumnRiskFactorType",
        "ColumnRiskFactorTypeValue",
        "MatrixCategory",
        "MatrixID",
        "RowRiskFactorType",
        "RowRiskFactorTypeValue",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Amount",
        "CalculationMethod",
        "DataProvider",
        "Description",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ColumnRiskFactorType" as "ColumnRiskFactorType",
            "OLD_ColumnRiskFactorTypeValue" as "ColumnRiskFactorTypeValue",
            "OLD_MatrixCategory" as "MatrixCategory",
            "OLD_MatrixID" as "MatrixID",
            "OLD_RowRiskFactorType" as "RowRiskFactorType",
            "OLD_RowRiskFactorTypeValue" as "RowRiskFactorTypeValue",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Amount" as "Amount",
            "OLD_CalculationMethod" as "CalculationMethod",
            "OLD_DataProvider" as "DataProvider",
            "OLD_Description" as "Description",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ColumnRiskFactorType",
                        "OLD"."ColumnRiskFactorTypeValue",
                        "OLD"."MatrixCategory",
                        "OLD"."MatrixID",
                        "OLD"."RowRiskFactorType",
                        "OLD"."RowRiskFactorTypeValue",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ColumnRiskFactorType" AS "OLD_ColumnRiskFactorType" ,
                "OLD"."ColumnRiskFactorTypeValue" AS "OLD_ColumnRiskFactorTypeValue" ,
                "OLD"."MatrixCategory" AS "OLD_MatrixCategory" ,
                "OLD"."MatrixID" AS "OLD_MatrixID" ,
                "OLD"."RowRiskFactorType" AS "OLD_RowRiskFactorType" ,
                "OLD"."RowRiskFactorTypeValue" AS "OLD_RowRiskFactorTypeValue" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Amount" AS "OLD_Amount" ,
                "OLD"."CalculationMethod" AS "OLD_CalculationMethod" ,
                "OLD"."DataProvider" AS "OLD_DataProvider" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::RiskFactorInterdependenceMatrix" as "OLD"
            on
                                                "IN"."ColumnRiskFactorType" = "OLD"."ColumnRiskFactorType" and
                                                "IN"."ColumnRiskFactorTypeValue" = "OLD"."ColumnRiskFactorTypeValue" and
                                                "IN"."MatrixCategory" = "OLD"."MatrixCategory" and
                                                "IN"."MatrixID" = "OLD"."MatrixID" and
                                                "IN"."RowRiskFactorType" = "OLD"."RowRiskFactorType" and
                                                "IN"."RowRiskFactorTypeValue" = "OLD"."RowRiskFactorTypeValue" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::RiskFactorInterdependenceMatrix"
    where (
        "ColumnRiskFactorType",
        "ColumnRiskFactorTypeValue",
        "MatrixCategory",
        "MatrixID",
        "RowRiskFactorType",
        "RowRiskFactorTypeValue",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ColumnRiskFactorType",
            "OLD"."ColumnRiskFactorTypeValue",
            "OLD"."MatrixCategory",
            "OLD"."MatrixID",
            "OLD"."RowRiskFactorType",
            "OLD"."RowRiskFactorTypeValue",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::RiskFactorInterdependenceMatrix" as "OLD"
        on
                                       "IN"."ColumnRiskFactorType" = "OLD"."ColumnRiskFactorType" and
                                       "IN"."ColumnRiskFactorTypeValue" = "OLD"."ColumnRiskFactorTypeValue" and
                                       "IN"."MatrixCategory" = "OLD"."MatrixCategory" and
                                       "IN"."MatrixID" = "OLD"."MatrixID" and
                                       "IN"."RowRiskFactorType" = "OLD"."RowRiskFactorType" and
                                       "IN"."RowRiskFactorTypeValue" = "OLD"."RowRiskFactorTypeValue" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
