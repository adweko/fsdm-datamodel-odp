PROCEDURE "sap.fsdm.procedures::CollectionLoad" (IN ROW "sap.fsdm.tabletypes::CollectionTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CollectionID=' || TO_VARCHAR("CollectionID") || ' ' ||
                'IDSystem=' || TO_VARCHAR("IDSystem") || ' ' ||
                '_Client.BusinessPartnerID=' || TO_VARCHAR("_Client.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CollectionID",
                        "IN"."IDSystem",
                        "IN"."_Client.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CollectionID",
                        "IN"."IDSystem",
                        "IN"."_Client.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CollectionID",
                        "IDSystem",
                        "_Client.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CollectionID",
                                    "IN"."IDSystem",
                                    "IN"."_Client.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CollectionID",
                                    "IN"."IDSystem",
                                    "IN"."_Client.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::Collection" (
        "CollectionID",
        "IDSystem",
        "_Client.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_AccountingSystem.AccountingSystemID",
        "_CostCenter.IDSystem",
        "_CostCenter.OrganizationalUnitID",
        "_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "_EmployeeResponsibleForPortfolio.BusinessPartnerID",
        "_OrganizationalUnit.IDSystem",
        "_OrganizationalUnit.OrganizationalUnitID",
        "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "_ProfitCenter.IDSystem",
        "_ProfitCenter.OrganizationalUnitID",
        "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "CollateralPoolCategory",
        "CollateralPoolCoveringType",
        "CollateralPoolType",
        "CollectionCategory",
        "CollectionName",
        "CountryOfCollateralPoolCovering",
        "DerivativeStrategyType",
        "Distributed",
        "FactorLagLength",
        "FactorLagTimeUnit",
        "FinancialInstrumentPortfolioType",
        "HedgeRelationshipStatus",
        "HedgeStrategyType",
        "InvestmentObjective",
        "ManagingUnit",
        "OriginalExposureAtDefault",
        "OriginalExposureAtDefaultCurrency",
        "OriginalRegulatoryCapitalRequirementRate",
        "PoolAdministrationType",
        "PortfolioCategory",
        "SizeOfTradingBucket",
        "TradeBookType",
        "TradingDeskType",
        "UnderlyingExposureType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CollectionID" as "CollectionID" ,
            "OLD_IDSystem" as "IDSystem" ,
            "OLD__Client.BusinessPartnerID" as "_Client.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID" ,
            "OLD__CostCenter.IDSystem" as "_CostCenter.IDSystem" ,
            "OLD__CostCenter.OrganizationalUnitID" as "_CostCenter.OrganizationalUnitID" ,
            "OLD__CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD__EmployeeResponsibleForPortfolio.BusinessPartnerID" as "_EmployeeResponsibleForPortfolio.BusinessPartnerID" ,
            "OLD__OrganizationalUnit.IDSystem" as "_OrganizationalUnit.IDSystem" ,
            "OLD__OrganizationalUnit.OrganizationalUnitID" as "_OrganizationalUnit.OrganizationalUnitID" ,
            "OLD__OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD__ProfitCenter.IDSystem" as "_ProfitCenter.IDSystem" ,
            "OLD__ProfitCenter.OrganizationalUnitID" as "_ProfitCenter.OrganizationalUnitID" ,
            "OLD__ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD_CollateralPoolCategory" as "CollateralPoolCategory" ,
            "OLD_CollateralPoolCoveringType" as "CollateralPoolCoveringType" ,
            "OLD_CollateralPoolType" as "CollateralPoolType" ,
            "OLD_CollectionCategory" as "CollectionCategory" ,
            "OLD_CollectionName" as "CollectionName" ,
            "OLD_CountryOfCollateralPoolCovering" as "CountryOfCollateralPoolCovering" ,
            "OLD_DerivativeStrategyType" as "DerivativeStrategyType" ,
            "OLD_Distributed" as "Distributed" ,
            "OLD_FactorLagLength" as "FactorLagLength" ,
            "OLD_FactorLagTimeUnit" as "FactorLagTimeUnit" ,
            "OLD_FinancialInstrumentPortfolioType" as "FinancialInstrumentPortfolioType" ,
            "OLD_HedgeRelationshipStatus" as "HedgeRelationshipStatus" ,
            "OLD_HedgeStrategyType" as "HedgeStrategyType" ,
            "OLD_InvestmentObjective" as "InvestmentObjective" ,
            "OLD_ManagingUnit" as "ManagingUnit" ,
            "OLD_OriginalExposureAtDefault" as "OriginalExposureAtDefault" ,
            "OLD_OriginalExposureAtDefaultCurrency" as "OriginalExposureAtDefaultCurrency" ,
            "OLD_OriginalRegulatoryCapitalRequirementRate" as "OriginalRegulatoryCapitalRequirementRate" ,
            "OLD_PoolAdministrationType" as "PoolAdministrationType" ,
            "OLD_PortfolioCategory" as "PortfolioCategory" ,
            "OLD_SizeOfTradingBucket" as "SizeOfTradingBucket" ,
            "OLD_TradeBookType" as "TradeBookType" ,
            "OLD_TradingDeskType" as "TradingDeskType" ,
            "OLD_UnderlyingExposureType" as "UnderlyingExposureType" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."CollectionID",
                        "IN"."IDSystem",
                        "IN"."_Client.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."CollectionID" as "OLD_CollectionID",
                                "OLD"."IDSystem" as "OLD_IDSystem",
                                "OLD"."_Client.BusinessPartnerID" as "OLD__Client.BusinessPartnerID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."_AccountingSystem.AccountingSystemID" as "OLD__AccountingSystem.AccountingSystemID",
                                "OLD"."_CostCenter.IDSystem" as "OLD__CostCenter.IDSystem",
                                "OLD"."_CostCenter.OrganizationalUnitID" as "OLD__CostCenter.OrganizationalUnitID",
                                "OLD"."_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "OLD__CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                "OLD"."_EmployeeResponsibleForPortfolio.BusinessPartnerID" as "OLD__EmployeeResponsibleForPortfolio.BusinessPartnerID",
                                "OLD"."_OrganizationalUnit.IDSystem" as "OLD__OrganizationalUnit.IDSystem",
                                "OLD"."_OrganizationalUnit.OrganizationalUnitID" as "OLD__OrganizationalUnit.OrganizationalUnitID",
                                "OLD"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "OLD__OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                "OLD"."_ProfitCenter.IDSystem" as "OLD__ProfitCenter.IDSystem",
                                "OLD"."_ProfitCenter.OrganizationalUnitID" as "OLD__ProfitCenter.OrganizationalUnitID",
                                "OLD"."_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "OLD__ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                "OLD"."CollateralPoolCategory" as "OLD_CollateralPoolCategory",
                                "OLD"."CollateralPoolCoveringType" as "OLD_CollateralPoolCoveringType",
                                "OLD"."CollateralPoolType" as "OLD_CollateralPoolType",
                                "OLD"."CollectionCategory" as "OLD_CollectionCategory",
                                "OLD"."CollectionName" as "OLD_CollectionName",
                                "OLD"."CountryOfCollateralPoolCovering" as "OLD_CountryOfCollateralPoolCovering",
                                "OLD"."DerivativeStrategyType" as "OLD_DerivativeStrategyType",
                                "OLD"."Distributed" as "OLD_Distributed",
                                "OLD"."FactorLagLength" as "OLD_FactorLagLength",
                                "OLD"."FactorLagTimeUnit" as "OLD_FactorLagTimeUnit",
                                "OLD"."FinancialInstrumentPortfolioType" as "OLD_FinancialInstrumentPortfolioType",
                                "OLD"."HedgeRelationshipStatus" as "OLD_HedgeRelationshipStatus",
                                "OLD"."HedgeStrategyType" as "OLD_HedgeStrategyType",
                                "OLD"."InvestmentObjective" as "OLD_InvestmentObjective",
                                "OLD"."ManagingUnit" as "OLD_ManagingUnit",
                                "OLD"."OriginalExposureAtDefault" as "OLD_OriginalExposureAtDefault",
                                "OLD"."OriginalExposureAtDefaultCurrency" as "OLD_OriginalExposureAtDefaultCurrency",
                                "OLD"."OriginalRegulatoryCapitalRequirementRate" as "OLD_OriginalRegulatoryCapitalRequirementRate",
                                "OLD"."PoolAdministrationType" as "OLD_PoolAdministrationType",
                                "OLD"."PortfolioCategory" as "OLD_PortfolioCategory",
                                "OLD"."SizeOfTradingBucket" as "OLD_SizeOfTradingBucket",
                                "OLD"."TradeBookType" as "OLD_TradeBookType",
                                "OLD"."TradingDeskType" as "OLD_TradingDeskType",
                                "OLD"."UnderlyingExposureType" as "OLD_UnderlyingExposureType",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::Collection" as "OLD"
            on
                ifnull( "IN"."CollectionID", '') = "OLD"."CollectionID" and
                ifnull( "IN"."IDSystem", '') = "OLD"."IDSystem" and
                ifnull( "IN"."_Client.BusinessPartnerID", '') = "OLD"."_Client.BusinessPartnerID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::Collection" (
        "CollectionID",
        "IDSystem",
        "_Client.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_AccountingSystem.AccountingSystemID",
        "_CostCenter.IDSystem",
        "_CostCenter.OrganizationalUnitID",
        "_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "_EmployeeResponsibleForPortfolio.BusinessPartnerID",
        "_OrganizationalUnit.IDSystem",
        "_OrganizationalUnit.OrganizationalUnitID",
        "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "_ProfitCenter.IDSystem",
        "_ProfitCenter.OrganizationalUnitID",
        "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "CollateralPoolCategory",
        "CollateralPoolCoveringType",
        "CollateralPoolType",
        "CollectionCategory",
        "CollectionName",
        "CountryOfCollateralPoolCovering",
        "DerivativeStrategyType",
        "Distributed",
        "FactorLagLength",
        "FactorLagTimeUnit",
        "FinancialInstrumentPortfolioType",
        "HedgeRelationshipStatus",
        "HedgeStrategyType",
        "InvestmentObjective",
        "ManagingUnit",
        "OriginalExposureAtDefault",
        "OriginalExposureAtDefaultCurrency",
        "OriginalRegulatoryCapitalRequirementRate",
        "PoolAdministrationType",
        "PortfolioCategory",
        "SizeOfTradingBucket",
        "TradeBookType",
        "TradingDeskType",
        "UnderlyingExposureType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CollectionID"  as "CollectionID",
            "OLD_IDSystem"  as "IDSystem",
            "OLD__Client.BusinessPartnerID"  as "_Client.BusinessPartnerID",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD__AccountingSystem.AccountingSystemID"  as "_AccountingSystem.AccountingSystemID",
            "OLD__CostCenter.IDSystem"  as "_CostCenter.IDSystem",
            "OLD__CostCenter.OrganizationalUnitID"  as "_CostCenter.OrganizationalUnitID",
            "OLD__CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"  as "_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD__EmployeeResponsibleForPortfolio.BusinessPartnerID"  as "_EmployeeResponsibleForPortfolio.BusinessPartnerID",
            "OLD__OrganizationalUnit.IDSystem"  as "_OrganizationalUnit.IDSystem",
            "OLD__OrganizationalUnit.OrganizationalUnitID"  as "_OrganizationalUnit.OrganizationalUnitID",
            "OLD__OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"  as "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD__ProfitCenter.IDSystem"  as "_ProfitCenter.IDSystem",
            "OLD__ProfitCenter.OrganizationalUnitID"  as "_ProfitCenter.OrganizationalUnitID",
            "OLD__ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"  as "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD_CollateralPoolCategory"  as "CollateralPoolCategory",
            "OLD_CollateralPoolCoveringType"  as "CollateralPoolCoveringType",
            "OLD_CollateralPoolType"  as "CollateralPoolType",
            "OLD_CollectionCategory"  as "CollectionCategory",
            "OLD_CollectionName"  as "CollectionName",
            "OLD_CountryOfCollateralPoolCovering"  as "CountryOfCollateralPoolCovering",
            "OLD_DerivativeStrategyType"  as "DerivativeStrategyType",
            "OLD_Distributed"  as "Distributed",
            "OLD_FactorLagLength"  as "FactorLagLength",
            "OLD_FactorLagTimeUnit"  as "FactorLagTimeUnit",
            "OLD_FinancialInstrumentPortfolioType"  as "FinancialInstrumentPortfolioType",
            "OLD_HedgeRelationshipStatus"  as "HedgeRelationshipStatus",
            "OLD_HedgeStrategyType"  as "HedgeStrategyType",
            "OLD_InvestmentObjective"  as "InvestmentObjective",
            "OLD_ManagingUnit"  as "ManagingUnit",
            "OLD_OriginalExposureAtDefault"  as "OriginalExposureAtDefault",
            "OLD_OriginalExposureAtDefaultCurrency"  as "OriginalExposureAtDefaultCurrency",
            "OLD_OriginalRegulatoryCapitalRequirementRate"  as "OriginalRegulatoryCapitalRequirementRate",
            "OLD_PoolAdministrationType"  as "PoolAdministrationType",
            "OLD_PortfolioCategory"  as "PortfolioCategory",
            "OLD_SizeOfTradingBucket"  as "SizeOfTradingBucket",
            "OLD_TradeBookType"  as "TradeBookType",
            "OLD_TradingDeskType"  as "TradingDeskType",
            "OLD_UnderlyingExposureType"  as "UnderlyingExposureType",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."CollectionID",
                        "IN"."IDSystem",
                        "IN"."_Client.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."CollectionID" as "OLD_CollectionID",
                        "OLD"."IDSystem" as "OLD_IDSystem",
                        "OLD"."_Client.BusinessPartnerID" as "OLD__Client.BusinessPartnerID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."_AccountingSystem.AccountingSystemID" as "OLD__AccountingSystem.AccountingSystemID",
                        "OLD"."_CostCenter.IDSystem" as "OLD__CostCenter.IDSystem",
                        "OLD"."_CostCenter.OrganizationalUnitID" as "OLD__CostCenter.OrganizationalUnitID",
                        "OLD"."_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "OLD__CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."_EmployeeResponsibleForPortfolio.BusinessPartnerID" as "OLD__EmployeeResponsibleForPortfolio.BusinessPartnerID",
                        "OLD"."_OrganizationalUnit.IDSystem" as "OLD__OrganizationalUnit.IDSystem",
                        "OLD"."_OrganizationalUnit.OrganizationalUnitID" as "OLD__OrganizationalUnit.OrganizationalUnitID",
                        "OLD"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "OLD__OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."_ProfitCenter.IDSystem" as "OLD__ProfitCenter.IDSystem",
                        "OLD"."_ProfitCenter.OrganizationalUnitID" as "OLD__ProfitCenter.OrganizationalUnitID",
                        "OLD"."_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "OLD__ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."CollateralPoolCategory" as "OLD_CollateralPoolCategory",
                        "OLD"."CollateralPoolCoveringType" as "OLD_CollateralPoolCoveringType",
                        "OLD"."CollateralPoolType" as "OLD_CollateralPoolType",
                        "OLD"."CollectionCategory" as "OLD_CollectionCategory",
                        "OLD"."CollectionName" as "OLD_CollectionName",
                        "OLD"."CountryOfCollateralPoolCovering" as "OLD_CountryOfCollateralPoolCovering",
                        "OLD"."DerivativeStrategyType" as "OLD_DerivativeStrategyType",
                        "OLD"."Distributed" as "OLD_Distributed",
                        "OLD"."FactorLagLength" as "OLD_FactorLagLength",
                        "OLD"."FactorLagTimeUnit" as "OLD_FactorLagTimeUnit",
                        "OLD"."FinancialInstrumentPortfolioType" as "OLD_FinancialInstrumentPortfolioType",
                        "OLD"."HedgeRelationshipStatus" as "OLD_HedgeRelationshipStatus",
                        "OLD"."HedgeStrategyType" as "OLD_HedgeStrategyType",
                        "OLD"."InvestmentObjective" as "OLD_InvestmentObjective",
                        "OLD"."ManagingUnit" as "OLD_ManagingUnit",
                        "OLD"."OriginalExposureAtDefault" as "OLD_OriginalExposureAtDefault",
                        "OLD"."OriginalExposureAtDefaultCurrency" as "OLD_OriginalExposureAtDefaultCurrency",
                        "OLD"."OriginalRegulatoryCapitalRequirementRate" as "OLD_OriginalRegulatoryCapitalRequirementRate",
                        "OLD"."PoolAdministrationType" as "OLD_PoolAdministrationType",
                        "OLD"."PortfolioCategory" as "OLD_PortfolioCategory",
                        "OLD"."SizeOfTradingBucket" as "OLD_SizeOfTradingBucket",
                        "OLD"."TradeBookType" as "OLD_TradeBookType",
                        "OLD"."TradingDeskType" as "OLD_TradingDeskType",
                        "OLD"."UnderlyingExposureType" as "OLD_UnderlyingExposureType",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::Collection" as "OLD"
            on
                ifnull( "IN"."CollectionID", '' ) = "OLD"."CollectionID" and
                ifnull( "IN"."IDSystem", '' ) = "OLD"."IDSystem" and
                ifnull( "IN"."_Client.BusinessPartnerID", '' ) = "OLD"."_Client.BusinessPartnerID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::Collection"
    where (
        "CollectionID",
        "IDSystem",
        "_Client.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."CollectionID",
            "OLD"."IDSystem",
            "OLD"."_Client.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Collection" as "OLD"
        on
           ifnull( "IN"."CollectionID", '' ) = "OLD"."CollectionID" and
           ifnull( "IN"."IDSystem", '' ) = "OLD"."IDSystem" and
           ifnull( "IN"."_Client.BusinessPartnerID", '' ) = "OLD"."_Client.BusinessPartnerID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::Collection" (
        "CollectionID",
        "IDSystem",
        "_Client.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_AccountingSystem.AccountingSystemID",
        "_CostCenter.IDSystem",
        "_CostCenter.OrganizationalUnitID",
        "_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "_EmployeeResponsibleForPortfolio.BusinessPartnerID",
        "_OrganizationalUnit.IDSystem",
        "_OrganizationalUnit.OrganizationalUnitID",
        "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "_ProfitCenter.IDSystem",
        "_ProfitCenter.OrganizationalUnitID",
        "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "CollateralPoolCategory",
        "CollateralPoolCoveringType",
        "CollateralPoolType",
        "CollectionCategory",
        "CollectionName",
        "CountryOfCollateralPoolCovering",
        "DerivativeStrategyType",
        "Distributed",
        "FactorLagLength",
        "FactorLagTimeUnit",
        "FinancialInstrumentPortfolioType",
        "HedgeRelationshipStatus",
        "HedgeStrategyType",
        "InvestmentObjective",
        "ManagingUnit",
        "OriginalExposureAtDefault",
        "OriginalExposureAtDefaultCurrency",
        "OriginalRegulatoryCapitalRequirementRate",
        "PoolAdministrationType",
        "PortfolioCategory",
        "SizeOfTradingBucket",
        "TradeBookType",
        "TradingDeskType",
        "UnderlyingExposureType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "CollectionID", '' ) as "CollectionID",
            ifnull( "IDSystem", '' ) as "IDSystem",
            ifnull( "_Client.BusinessPartnerID", '' ) as "_Client.BusinessPartnerID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "_AccountingSystem.AccountingSystemID"  ,
            "_CostCenter.IDSystem"  ,
            "_CostCenter.OrganizationalUnitID"  ,
            "_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"  ,
            "_EmployeeResponsibleForPortfolio.BusinessPartnerID"  ,
            "_OrganizationalUnit.IDSystem"  ,
            "_OrganizationalUnit.OrganizationalUnitID"  ,
            "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"  ,
            "_ProfitCenter.IDSystem"  ,
            "_ProfitCenter.OrganizationalUnitID"  ,
            "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"  ,
            "CollateralPoolCategory"  ,
            "CollateralPoolCoveringType"  ,
            "CollateralPoolType"  ,
            "CollectionCategory"  ,
            "CollectionName"  ,
            "CountryOfCollateralPoolCovering"  ,
            "DerivativeStrategyType"  ,
            "Distributed"  ,
            "FactorLagLength"  ,
            "FactorLagTimeUnit"  ,
            "FinancialInstrumentPortfolioType"  ,
            "HedgeRelationshipStatus"  ,
            "HedgeStrategyType"  ,
            "InvestmentObjective"  ,
            "ManagingUnit"  ,
            "OriginalExposureAtDefault"  ,
            "OriginalExposureAtDefaultCurrency"  ,
            "OriginalRegulatoryCapitalRequirementRate"  ,
            "PoolAdministrationType"  ,
            "PortfolioCategory"  ,
            "SizeOfTradingBucket"  ,
            "TradeBookType"  ,
            "TradingDeskType"  ,
            "UnderlyingExposureType"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END