PROCEDURE "sap.fsdm.procedures::CommodityExposureReadOnly" (IN ROW "sap.fsdm.tabletypes::CommodityExposureTT", OUT CURR_DEL "sap.fsdm.tabletypes::CommodityExposureTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CommodityExposureTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'MarketRiskAnalysisType=' || TO_VARCHAR("MarketRiskAnalysisType") || ' ' ||
                'MarketRiskSplitPartType=' || TO_VARCHAR("MarketRiskSplitPartType") || ' ' ||
                'RiskProvisionScenario=' || TO_VARCHAR("RiskProvisionScenario") || ' ' ||
                'RoleOfCurrency=' || TO_VARCHAR("RoleOfCurrency") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_PositionCurrencyOfMultiCurrencyContract.PositionCurrency=' || TO_VARCHAR("_PositionCurrencyOfMultiCurrencyContract.PositionCurrency") || ' ' ||
                '_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID=' || TO_VARCHAR("_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID") || ' ' ||
                '_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem=' || TO_VARCHAR("_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_RiskReportingNode.RiskReportingNodeID=' || TO_VARCHAR("_RiskReportingNode.RiskReportingNodeID") || ' ' ||
                '_SecuritiesAccount.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccount.FinancialContractID") || ' ' ||
                '_SecuritiesAccount.IDSystem=' || TO_VARCHAR("_SecuritiesAccount.IDSystem") || ' ' ||
                '_TimeBucket.MaturityBandID=' || TO_VARCHAR("_TimeBucket.MaturityBandID") || ' ' ||
                '_TimeBucket.TimeBucketID=' || TO_VARCHAR("_TimeBucket.TimeBucketID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."MarketRiskAnalysisType",
                        "IN"."MarketRiskSplitPartType",
                        "IN"."RiskProvisionScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."MarketRiskAnalysisType",
                        "IN"."MarketRiskSplitPartType",
                        "IN"."RiskProvisionScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "MarketRiskAnalysisType",
                        "MarketRiskSplitPartType",
                        "RiskProvisionScenario",
                        "RoleOfCurrency",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_RiskReportingNode.RiskReportingNodeID",
                        "_SecuritiesAccount.FinancialContractID",
                        "_SecuritiesAccount.IDSystem",
                        "_TimeBucket.MaturityBandID",
                        "_TimeBucket.TimeBucketID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."MarketRiskAnalysisType",
                                    "IN"."MarketRiskSplitPartType",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."RoleOfCurrency",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                                    "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                                    "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_RiskReportingNode.RiskReportingNodeID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."MarketRiskAnalysisType",
                                    "IN"."MarketRiskSplitPartType",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."RoleOfCurrency",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                                    "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                                    "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_RiskReportingNode.RiskReportingNodeID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "MarketRiskAnalysisType",
        "MarketRiskSplitPartType",
        "RiskProvisionScenario",
        "RoleOfCurrency",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
        "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
        "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::CommodityExposure" WHERE
        (            "MarketRiskAnalysisType" ,
            "MarketRiskSplitPartType" ,
            "RiskProvisionScenario" ,
            "RoleOfCurrency" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
            "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "_TimeBucket.MaturityBandID" ,
            "_TimeBucket.TimeBucketID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."MarketRiskAnalysisType",
            "OLD"."MarketRiskSplitPartType",
            "OLD"."RiskProvisionScenario",
            "OLD"."RoleOfCurrency",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
            "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
            "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_RiskReportingNode.RiskReportingNodeID",
            "OLD"."_SecuritiesAccount.FinancialContractID",
            "OLD"."_SecuritiesAccount.IDSystem",
            "OLD"."_TimeBucket.MaturityBandID",
            "OLD"."_TimeBucket.TimeBucketID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::CommodityExposure" as "OLD"
            on
               ifnull( "IN"."MarketRiskAnalysisType",'' ) = "OLD"."MarketRiskAnalysisType" and
               ifnull( "IN"."MarketRiskSplitPartType",'' ) = "OLD"."MarketRiskSplitPartType" and
               ifnull( "IN"."RiskProvisionScenario",'' ) = "OLD"."RiskProvisionScenario" and
               ifnull( "IN"."RoleOfCurrency",'' ) = "OLD"."RoleOfCurrency" and
               ifnull( "IN"."_FinancialContract.FinancialContractID",'' ) = "OLD"."_FinancialContract.FinancialContractID" and
               ifnull( "IN"."_FinancialContract.IDSystem",'' ) = "OLD"."_FinancialContract.IDSystem" and
               ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID",'' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               ifnull( "IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",'' ) = "OLD"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
               ifnull( "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",'' ) = "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
               ifnull( "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",'' ) = "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
               ifnull( "IN"."_ResultGroup.ResultDataProvider",'' ) = "OLD"."_ResultGroup.ResultDataProvider" and
               ifnull( "IN"."_ResultGroup.ResultGroupID",'' ) = "OLD"."_ResultGroup.ResultGroupID" and
               ifnull( "IN"."_RiskReportingNode.RiskReportingNodeID",'' ) = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
               ifnull( "IN"."_SecuritiesAccount.FinancialContractID",'' ) = "OLD"."_SecuritiesAccount.FinancialContractID" and
               ifnull( "IN"."_SecuritiesAccount.IDSystem",'' ) = "OLD"."_SecuritiesAccount.IDSystem" and
               ifnull( "IN"."_TimeBucket.MaturityBandID",'' ) = "OLD"."_TimeBucket.MaturityBandID" and
               ifnull( "IN"."_TimeBucket.TimeBucketID",'' ) = "OLD"."_TimeBucket.TimeBucketID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "MarketRiskAnalysisType",
        "MarketRiskSplitPartType",
        "RiskProvisionScenario",
        "RoleOfCurrency",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
        "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
        "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Currency",
        "ExposureValue",
        "NetPresentValue",
        "TimeBucketEndDate",
        "TimeBucketStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "MarketRiskAnalysisType", '' ) as "MarketRiskAnalysisType",
                    ifnull( "MarketRiskSplitPartType", '' ) as "MarketRiskSplitPartType",
                    ifnull( "RiskProvisionScenario", '' ) as "RiskProvisionScenario",
                    ifnull( "RoleOfCurrency", '' ) as "RoleOfCurrency",
                    ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
                    ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
                    ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
                    ifnull( "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency", '' ) as "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                    ifnull( "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID", '' ) as "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                    ifnull( "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem", '' ) as "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                    ifnull( "_ResultGroup.ResultDataProvider", '' ) as "_ResultGroup.ResultDataProvider",
                    ifnull( "_ResultGroup.ResultGroupID", '' ) as "_ResultGroup.ResultGroupID",
                    ifnull( "_RiskReportingNode.RiskReportingNodeID", '' ) as "_RiskReportingNode.RiskReportingNodeID",
                    ifnull( "_SecuritiesAccount.FinancialContractID", '' ) as "_SecuritiesAccount.FinancialContractID",
                    ifnull( "_SecuritiesAccount.IDSystem", '' ) as "_SecuritiesAccount.IDSystem",
                    ifnull( "_TimeBucket.MaturityBandID", '' ) as "_TimeBucket.MaturityBandID",
                    ifnull( "_TimeBucket.TimeBucketID", '' ) as "_TimeBucket.TimeBucketID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "Currency"  ,
                    "ExposureValue"  ,
                    "NetPresentValue"  ,
                    "TimeBucketEndDate"  ,
                    "TimeBucketStartDate"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_MarketRiskAnalysisType" as "MarketRiskAnalysisType" ,
                    "OLD_MarketRiskSplitPartType" as "MarketRiskSplitPartType" ,
                    "OLD_RiskProvisionScenario" as "RiskProvisionScenario" ,
                    "OLD_RoleOfCurrency" as "RoleOfCurrency" ,
                    "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
                    "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
                    "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
                    "OLD__PositionCurrencyOfMultiCurrencyContract.PositionCurrency" as "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
                    "OLD__PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" as "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                    "OLD__PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" as "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                    "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
                    "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
                    "OLD__RiskReportingNode.RiskReportingNodeID" as "_RiskReportingNode.RiskReportingNodeID" ,
                    "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID" ,
                    "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem" ,
                    "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID" ,
                    "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_Currency" as "Currency" ,
                    "OLD_ExposureValue" as "ExposureValue" ,
                    "OLD_NetPresentValue" as "NetPresentValue" ,
                    "OLD_TimeBucketEndDate" as "TimeBucketEndDate" ,
                    "OLD_TimeBucketStartDate" as "TimeBucketStartDate" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."MarketRiskAnalysisType",
                        "IN"."MarketRiskSplitPartType",
                        "IN"."RiskProvisionScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."MarketRiskAnalysisType" as "OLD_MarketRiskAnalysisType",
                                "OLD"."MarketRiskSplitPartType" as "OLD_MarketRiskSplitPartType",
                                "OLD"."RiskProvisionScenario" as "OLD_RiskProvisionScenario",
                                "OLD"."RoleOfCurrency" as "OLD_RoleOfCurrency",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" as "OLD__PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                                "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" as "OLD__PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                                "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" as "OLD__PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_RiskReportingNode.RiskReportingNodeID" as "OLD__RiskReportingNode.RiskReportingNodeID",
                                "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                                "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                                "OLD"."_TimeBucket.MaturityBandID" as "OLD__TimeBucket.MaturityBandID",
                                "OLD"."_TimeBucket.TimeBucketID" as "OLD__TimeBucket.TimeBucketID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."Currency" as "OLD_Currency",
                                "OLD"."ExposureValue" as "OLD_ExposureValue",
                                "OLD"."NetPresentValue" as "OLD_NetPresentValue",
                                "OLD"."TimeBucketEndDate" as "OLD_TimeBucketEndDate",
                                "OLD"."TimeBucketStartDate" as "OLD_TimeBucketStartDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CommodityExposure" as "OLD"
            on
                ifnull( "IN"."MarketRiskAnalysisType", '') = "OLD"."MarketRiskAnalysisType" and
                ifnull( "IN"."MarketRiskSplitPartType", '') = "OLD"."MarketRiskSplitPartType" and
                ifnull( "IN"."RiskProvisionScenario", '') = "OLD"."RiskProvisionScenario" and
                ifnull( "IN"."RoleOfCurrency", '') = "OLD"."RoleOfCurrency" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency", '') = "OLD"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
                ifnull( "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID", '') = "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
                ifnull( "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem", '') = "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_RiskReportingNode.RiskReportingNodeID", '') = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" and
                ifnull( "IN"."_TimeBucket.MaturityBandID", '') = "OLD"."_TimeBucket.MaturityBandID" and
                ifnull( "IN"."_TimeBucket.TimeBucketID", '') = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_MarketRiskAnalysisType" as "MarketRiskAnalysisType",
            "OLD_MarketRiskSplitPartType" as "MarketRiskSplitPartType",
            "OLD_RiskProvisionScenario" as "RiskProvisionScenario",
            "OLD_RoleOfCurrency" as "RoleOfCurrency",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__PositionCurrencyOfMultiCurrencyContract.PositionCurrency" as "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
            "OLD__PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" as "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
            "OLD__PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" as "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__RiskReportingNode.RiskReportingNodeID" as "_RiskReportingNode.RiskReportingNodeID",
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID",
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem",
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID",
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Currency" as "Currency",
            "OLD_ExposureValue" as "ExposureValue",
            "OLD_NetPresentValue" as "NetPresentValue",
            "OLD_TimeBucketEndDate" as "TimeBucketEndDate",
            "OLD_TimeBucketStartDate" as "TimeBucketStartDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."MarketRiskAnalysisType",
                        "IN"."MarketRiskSplitPartType",
                        "IN"."RiskProvisionScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."MarketRiskAnalysisType" as "OLD_MarketRiskAnalysisType",
                                "OLD"."MarketRiskSplitPartType" as "OLD_MarketRiskSplitPartType",
                                "OLD"."RiskProvisionScenario" as "OLD_RiskProvisionScenario",
                                "OLD"."RoleOfCurrency" as "OLD_RoleOfCurrency",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" as "OLD__PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                                "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" as "OLD__PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                                "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" as "OLD__PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_RiskReportingNode.RiskReportingNodeID" as "OLD__RiskReportingNode.RiskReportingNodeID",
                                "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                                "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                                "OLD"."_TimeBucket.MaturityBandID" as "OLD__TimeBucket.MaturityBandID",
                                "OLD"."_TimeBucket.TimeBucketID" as "OLD__TimeBucket.TimeBucketID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."Currency" as "OLD_Currency",
                                "OLD"."ExposureValue" as "OLD_ExposureValue",
                                "OLD"."NetPresentValue" as "OLD_NetPresentValue",
                                "OLD"."TimeBucketEndDate" as "OLD_TimeBucketEndDate",
                                "OLD"."TimeBucketStartDate" as "OLD_TimeBucketStartDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CommodityExposure" as "OLD"
            on
                ifnull("IN"."MarketRiskAnalysisType", '') = "OLD"."MarketRiskAnalysisType" and
                ifnull("IN"."MarketRiskSplitPartType", '') = "OLD"."MarketRiskSplitPartType" and
                ifnull("IN"."RiskProvisionScenario", '') = "OLD"."RiskProvisionScenario" and
                ifnull("IN"."RoleOfCurrency", '') = "OLD"."RoleOfCurrency" and
                ifnull("IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull("IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull("IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull("IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency", '') = "OLD"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
                ifnull("IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID", '') = "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
                ifnull("IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem", '') = "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
                ifnull("IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull("IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull("IN"."_RiskReportingNode.RiskReportingNodeID", '') = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                ifnull("IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull("IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" and
                ifnull("IN"."_TimeBucket.MaturityBandID", '') = "OLD"."_TimeBucket.MaturityBandID" and
                ifnull("IN"."_TimeBucket.TimeBucketID", '') = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
