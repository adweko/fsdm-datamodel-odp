PROCEDURE "sap.fsdm.procedures::InsuredPersonEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::InsuredPersonTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::InsuredPersonTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::InsuredPersonTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceOrdinalNumber" is null and
            "_IndividualPerson.BusinessPartnerID" is null and
            "_InsuranceCoverage.ID" is null and
            "_InsuranceCoverage._InsuranceContract.FinancialContractID" is null and
            "_InsuranceCoverage._InsuranceContract.IDSystem" is null and
            "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" is null and
            "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" is null and
            "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "SequenceOrdinalNumber" ,
                "_IndividualPerson.BusinessPartnerID" ,
                "_InsuranceCoverage.ID" ,
                "_InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "_InsuranceCoverage._InsuranceContract.IDSystem" ,
                "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceOrdinalNumber" ,
                "OLD"."_IndividualPerson.BusinessPartnerID" ,
                "OLD"."_InsuranceCoverage.ID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InsuredPerson" "OLD"
            on
                "IN"."SequenceOrdinalNumber" = "OLD"."SequenceOrdinalNumber" and
                "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" and
                "IN"."_InsuranceCoverage.ID" = "OLD"."_InsuranceCoverage.ID" and
                "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" and
                "IN"."_InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" and
                "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "SequenceOrdinalNumber" ,
            "_IndividualPerson.BusinessPartnerID" ,
            "_InsuranceCoverage.ID" ,
            "_InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "_InsuranceCoverage._InsuranceContract.IDSystem" ,
            "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceOrdinalNumber" ,
                "OLD"."_IndividualPerson.BusinessPartnerID" ,
                "OLD"."_InsuranceCoverage.ID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InsuredPerson_Historical" "OLD"
            on
                "IN"."SequenceOrdinalNumber" = "OLD"."SequenceOrdinalNumber" and
                "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" and
                "IN"."_InsuranceCoverage.ID" = "OLD"."_InsuranceCoverage.ID" and
                "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" and
                "IN"."_InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" and
                "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

END
