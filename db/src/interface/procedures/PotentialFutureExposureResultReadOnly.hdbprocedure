PROCEDURE "sap.fsdm.procedures::PotentialFutureExposureResultReadOnly" (IN ROW "sap.fsdm.tabletypes::PotentialFutureExposureResultTT", OUT CURR_DEL "sap.fsdm.tabletypes::PotentialFutureExposureResultTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::PotentialFutureExposureResultTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ConfidenceLevel=' || TO_VARCHAR("ConfidenceLevel") || ' ' ||
                'RiskProvisionScenario=' || TO_VARCHAR("RiskProvisionScenario") || ' ' ||
                'RoleOfCurrency=' || TO_VARCHAR("RoleOfCurrency") || ' ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_RiskReportingNode.RiskReportingNodeID=' || TO_VARCHAR("_RiskReportingNode.RiskReportingNodeID") || ' ' ||
                '_SecuritiesAccount.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccount.FinancialContractID") || ' ' ||
                '_SecuritiesAccount.IDSystem=' || TO_VARCHAR("_SecuritiesAccount.IDSystem") || ' ' ||
                '_TimeBucket.MaturityBandID=' || TO_VARCHAR("_TimeBucket.MaturityBandID") || ' ' ||
                '_TimeBucket.TimeBucketID=' || TO_VARCHAR("_TimeBucket.TimeBucketID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ConfidenceLevel",
                        "IN"."RiskProvisionScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ConfidenceLevel",
                        "IN"."RiskProvisionScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ConfidenceLevel",
                        "RiskProvisionScenario",
                        "RoleOfCurrency",
                        "_BusinessPartner.BusinessPartnerID",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_RiskReportingNode.RiskReportingNodeID",
                        "_SecuritiesAccount.FinancialContractID",
                        "_SecuritiesAccount.IDSystem",
                        "_TimeBucket.MaturityBandID",
                        "_TimeBucket.TimeBucketID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ConfidenceLevel",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."RoleOfCurrency",
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_RiskReportingNode.RiskReportingNodeID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ConfidenceLevel",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."RoleOfCurrency",
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_RiskReportingNode.RiskReportingNodeID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "ConfidenceLevel",
        "RiskProvisionScenario",
        "RoleOfCurrency",
        "_BusinessPartner.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::PotentialFutureExposureResult" WHERE
        (            "ConfidenceLevel" ,
            "RiskProvisionScenario" ,
            "RoleOfCurrency" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "_TimeBucket.MaturityBandID" ,
            "_TimeBucket.TimeBucketID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."ConfidenceLevel",
            "OLD"."RiskProvisionScenario",
            "OLD"."RoleOfCurrency",
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_RiskReportingNode.RiskReportingNodeID",
            "OLD"."_SecuritiesAccount.FinancialContractID",
            "OLD"."_SecuritiesAccount.IDSystem",
            "OLD"."_TimeBucket.MaturityBandID",
            "OLD"."_TimeBucket.TimeBucketID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::PotentialFutureExposureResult" as "OLD"
            on
               ifnull( "IN"."ConfidenceLevel",0 ) = "OLD"."ConfidenceLevel" and
               ifnull( "IN"."RiskProvisionScenario",'' ) = "OLD"."RiskProvisionScenario" and
               ifnull( "IN"."RoleOfCurrency",'' ) = "OLD"."RoleOfCurrency" and
               ifnull( "IN"."_BusinessPartner.BusinessPartnerID",'' ) = "OLD"."_BusinessPartner.BusinessPartnerID" and
               ifnull( "IN"."_FinancialContract.FinancialContractID",'' ) = "OLD"."_FinancialContract.FinancialContractID" and
               ifnull( "IN"."_FinancialContract.IDSystem",'' ) = "OLD"."_FinancialContract.IDSystem" and
               ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID",'' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               ifnull( "IN"."_ResultGroup.ResultDataProvider",'' ) = "OLD"."_ResultGroup.ResultDataProvider" and
               ifnull( "IN"."_ResultGroup.ResultGroupID",'' ) = "OLD"."_ResultGroup.ResultGroupID" and
               ifnull( "IN"."_RiskReportingNode.RiskReportingNodeID",'' ) = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
               ifnull( "IN"."_SecuritiesAccount.FinancialContractID",'' ) = "OLD"."_SecuritiesAccount.FinancialContractID" and
               ifnull( "IN"."_SecuritiesAccount.IDSystem",'' ) = "OLD"."_SecuritiesAccount.IDSystem" and
               ifnull( "IN"."_TimeBucket.MaturityBandID",'' ) = "OLD"."_TimeBucket.MaturityBandID" and
               ifnull( "IN"."_TimeBucket.TimeBucketID",'' ) = "OLD"."_TimeBucket.TimeBucketID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "ConfidenceLevel",
        "RiskProvisionScenario",
        "RoleOfCurrency",
        "_BusinessPartner.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AverageEffectiveExpectedExposureAmount",
        "Currency",
        "EffectiveExpectedExposureAmount",
        "ExpectedExposureAmount",
        "ExpectedNegativeExposureAmount",
        "ExpectedPositiveExposureAmount",
        "PeakExposureAmount",
        "TimeBucketEndDate",
        "TimeBucketStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "ConfidenceLevel", 0 ) as "ConfidenceLevel",
                    ifnull( "RiskProvisionScenario", '' ) as "RiskProvisionScenario",
                    ifnull( "RoleOfCurrency", '' ) as "RoleOfCurrency",
                    ifnull( "_BusinessPartner.BusinessPartnerID", '' ) as "_BusinessPartner.BusinessPartnerID",
                    ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
                    ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
                    ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
                    ifnull( "_ResultGroup.ResultDataProvider", '' ) as "_ResultGroup.ResultDataProvider",
                    ifnull( "_ResultGroup.ResultGroupID", '' ) as "_ResultGroup.ResultGroupID",
                    ifnull( "_RiskReportingNode.RiskReportingNodeID", '' ) as "_RiskReportingNode.RiskReportingNodeID",
                    ifnull( "_SecuritiesAccount.FinancialContractID", '' ) as "_SecuritiesAccount.FinancialContractID",
                    ifnull( "_SecuritiesAccount.IDSystem", '' ) as "_SecuritiesAccount.IDSystem",
                    ifnull( "_TimeBucket.MaturityBandID", '' ) as "_TimeBucket.MaturityBandID",
                    ifnull( "_TimeBucket.TimeBucketID", '' ) as "_TimeBucket.TimeBucketID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "AverageEffectiveExpectedExposureAmount"  ,
                    "Currency"  ,
                    "EffectiveExpectedExposureAmount"  ,
                    "ExpectedExposureAmount"  ,
                    "ExpectedNegativeExposureAmount"  ,
                    "ExpectedPositiveExposureAmount"  ,
                    "PeakExposureAmount"  ,
                    "TimeBucketEndDate"  ,
                    "TimeBucketStartDate"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_ConfidenceLevel" as "ConfidenceLevel" ,
                    "OLD_RiskProvisionScenario" as "RiskProvisionScenario" ,
                    "OLD_RoleOfCurrency" as "RoleOfCurrency" ,
                    "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
                    "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
                    "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
                    "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
                    "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
                    "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
                    "OLD__RiskReportingNode.RiskReportingNodeID" as "_RiskReportingNode.RiskReportingNodeID" ,
                    "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID" ,
                    "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem" ,
                    "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID" ,
                    "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_AverageEffectiveExpectedExposureAmount" as "AverageEffectiveExpectedExposureAmount" ,
                    "OLD_Currency" as "Currency" ,
                    "OLD_EffectiveExpectedExposureAmount" as "EffectiveExpectedExposureAmount" ,
                    "OLD_ExpectedExposureAmount" as "ExpectedExposureAmount" ,
                    "OLD_ExpectedNegativeExposureAmount" as "ExpectedNegativeExposureAmount" ,
                    "OLD_ExpectedPositiveExposureAmount" as "ExpectedPositiveExposureAmount" ,
                    "OLD_PeakExposureAmount" as "PeakExposureAmount" ,
                    "OLD_TimeBucketEndDate" as "TimeBucketEndDate" ,
                    "OLD_TimeBucketStartDate" as "TimeBucketStartDate" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."ConfidenceLevel",
                        "IN"."RiskProvisionScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."ConfidenceLevel" as "OLD_ConfidenceLevel",
                                "OLD"."RiskProvisionScenario" as "OLD_RiskProvisionScenario",
                                "OLD"."RoleOfCurrency" as "OLD_RoleOfCurrency",
                                "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_RiskReportingNode.RiskReportingNodeID" as "OLD__RiskReportingNode.RiskReportingNodeID",
                                "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                                "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                                "OLD"."_TimeBucket.MaturityBandID" as "OLD__TimeBucket.MaturityBandID",
                                "OLD"."_TimeBucket.TimeBucketID" as "OLD__TimeBucket.TimeBucketID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AverageEffectiveExpectedExposureAmount" as "OLD_AverageEffectiveExpectedExposureAmount",
                                "OLD"."Currency" as "OLD_Currency",
                                "OLD"."EffectiveExpectedExposureAmount" as "OLD_EffectiveExpectedExposureAmount",
                                "OLD"."ExpectedExposureAmount" as "OLD_ExpectedExposureAmount",
                                "OLD"."ExpectedNegativeExposureAmount" as "OLD_ExpectedNegativeExposureAmount",
                                "OLD"."ExpectedPositiveExposureAmount" as "OLD_ExpectedPositiveExposureAmount",
                                "OLD"."PeakExposureAmount" as "OLD_PeakExposureAmount",
                                "OLD"."TimeBucketEndDate" as "OLD_TimeBucketEndDate",
                                "OLD"."TimeBucketStartDate" as "OLD_TimeBucketStartDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::PotentialFutureExposureResult" as "OLD"
            on
                ifnull( "IN"."ConfidenceLevel", 0) = "OLD"."ConfidenceLevel" and
                ifnull( "IN"."RiskProvisionScenario", '') = "OLD"."RiskProvisionScenario" and
                ifnull( "IN"."RoleOfCurrency", '') = "OLD"."RoleOfCurrency" and
                ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '') = "OLD"."_BusinessPartner.BusinessPartnerID" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_RiskReportingNode.RiskReportingNodeID", '') = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" and
                ifnull( "IN"."_TimeBucket.MaturityBandID", '') = "OLD"."_TimeBucket.MaturityBandID" and
                ifnull( "IN"."_TimeBucket.TimeBucketID", '') = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_ConfidenceLevel" as "ConfidenceLevel",
            "OLD_RiskProvisionScenario" as "RiskProvisionScenario",
            "OLD_RoleOfCurrency" as "RoleOfCurrency",
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__RiskReportingNode.RiskReportingNodeID" as "_RiskReportingNode.RiskReportingNodeID",
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID",
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem",
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID",
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AverageEffectiveExpectedExposureAmount" as "AverageEffectiveExpectedExposureAmount",
            "OLD_Currency" as "Currency",
            "OLD_EffectiveExpectedExposureAmount" as "EffectiveExpectedExposureAmount",
            "OLD_ExpectedExposureAmount" as "ExpectedExposureAmount",
            "OLD_ExpectedNegativeExposureAmount" as "ExpectedNegativeExposureAmount",
            "OLD_ExpectedPositiveExposureAmount" as "ExpectedPositiveExposureAmount",
            "OLD_PeakExposureAmount" as "PeakExposureAmount",
            "OLD_TimeBucketEndDate" as "TimeBucketEndDate",
            "OLD_TimeBucketStartDate" as "TimeBucketStartDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."ConfidenceLevel",
                        "IN"."RiskProvisionScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."ConfidenceLevel" as "OLD_ConfidenceLevel",
                                "OLD"."RiskProvisionScenario" as "OLD_RiskProvisionScenario",
                                "OLD"."RoleOfCurrency" as "OLD_RoleOfCurrency",
                                "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_RiskReportingNode.RiskReportingNodeID" as "OLD__RiskReportingNode.RiskReportingNodeID",
                                "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                                "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                                "OLD"."_TimeBucket.MaturityBandID" as "OLD__TimeBucket.MaturityBandID",
                                "OLD"."_TimeBucket.TimeBucketID" as "OLD__TimeBucket.TimeBucketID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AverageEffectiveExpectedExposureAmount" as "OLD_AverageEffectiveExpectedExposureAmount",
                                "OLD"."Currency" as "OLD_Currency",
                                "OLD"."EffectiveExpectedExposureAmount" as "OLD_EffectiveExpectedExposureAmount",
                                "OLD"."ExpectedExposureAmount" as "OLD_ExpectedExposureAmount",
                                "OLD"."ExpectedNegativeExposureAmount" as "OLD_ExpectedNegativeExposureAmount",
                                "OLD"."ExpectedPositiveExposureAmount" as "OLD_ExpectedPositiveExposureAmount",
                                "OLD"."PeakExposureAmount" as "OLD_PeakExposureAmount",
                                "OLD"."TimeBucketEndDate" as "OLD_TimeBucketEndDate",
                                "OLD"."TimeBucketStartDate" as "OLD_TimeBucketStartDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::PotentialFutureExposureResult" as "OLD"
            on
                ifnull("IN"."ConfidenceLevel", 0) = "OLD"."ConfidenceLevel" and
                ifnull("IN"."RiskProvisionScenario", '') = "OLD"."RiskProvisionScenario" and
                ifnull("IN"."RoleOfCurrency", '') = "OLD"."RoleOfCurrency" and
                ifnull("IN"."_BusinessPartner.BusinessPartnerID", '') = "OLD"."_BusinessPartner.BusinessPartnerID" and
                ifnull("IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull("IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull("IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull("IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull("IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull("IN"."_RiskReportingNode.RiskReportingNodeID", '') = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                ifnull("IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull("IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" and
                ifnull("IN"."_TimeBucket.MaturityBandID", '') = "OLD"."_TimeBucket.MaturityBandID" and
                ifnull("IN"."_TimeBucket.TimeBucketID", '') = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
