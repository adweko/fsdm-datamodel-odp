PROCEDURE "sap.fsdm.procedures::RegulatoryIndicatorForFinancialInstrumentEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::RegulatoryIndicatorForFinancialInstrumentTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::RegulatoryIndicatorForFinancialInstrumentTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::RegulatoryIndicatorForFinancialInstrumentTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Criterion" is null and
            "LotID" is null and
            "Regulation" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Criterion" ,
                "LotID" ,
                "Regulation" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "_SecuritiesAccount.FinancialContractID" ,
                "_SecuritiesAccount.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Criterion" ,
                "OLD"."LotID" ,
                "OLD"."Regulation" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RegulatoryIndicatorForFinancialInstrument" "OLD"
            on
                "IN"."Criterion" = "OLD"."Criterion" and
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."Regulation" = "OLD"."Regulation" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Criterion" ,
            "LotID" ,
            "Regulation" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Criterion" ,
                "OLD"."LotID" ,
                "OLD"."Regulation" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RegulatoryIndicatorForFinancialInstrument_Historical" "OLD"
            on
                "IN"."Criterion" = "OLD"."Criterion" and
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."Regulation" = "OLD"."Regulation" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        );

END
