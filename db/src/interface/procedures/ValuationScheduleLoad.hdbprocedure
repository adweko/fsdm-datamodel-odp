PROCEDURE "sap.fsdm.procedures::ValuationScheduleLoad" (IN ROW "sap.fsdm.tabletypes::ValuationScheduleTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                '_EquitySwapEquityLeg.RoleOfPayer=' || TO_VARCHAR("_EquitySwapEquityLeg.RoleOfPayer") || ' ' ||
                '_EquitySwapEquityLeg._EquitySwap.FinancialContractID=' || TO_VARCHAR("_EquitySwapEquityLeg._EquitySwap.FinancialContractID") || ' ' ||
                '_EquitySwapEquityLeg._EquitySwap.IDSystem=' || TO_VARCHAR("_EquitySwapEquityLeg._EquitySwap.IDSystem") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_PhysicalAsset.PhysicalAssetID") || ' ' ||
                '_TotalReturnSwapReturnLeg._Swap.FinancialContractID=' || TO_VARCHAR("_TotalReturnSwapReturnLeg._Swap.FinancialContractID") || ' ' ||
                '_TotalReturnSwapReturnLeg._Swap.IDSystem=' || TO_VARCHAR("_TotalReturnSwapReturnLeg._Swap.IDSystem") || ' ' ||
                '_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID=' || TO_VARCHAR("_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID") || ' ' ||
                '_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID=' || TO_VARCHAR("_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."_EquitySwapEquityLeg.RoleOfPayer",
                        "IN"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
                        "IN"."_EquitySwapEquityLeg._EquitySwap.IDSystem",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PhysicalAsset.PhysicalAssetID",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."_EquitySwapEquityLeg.RoleOfPayer",
                        "IN"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
                        "IN"."_EquitySwapEquityLeg._EquitySwap.IDSystem",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PhysicalAsset.PhysicalAssetID",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "_EquitySwapEquityLeg.RoleOfPayer",
                        "_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
                        "_EquitySwapEquityLeg._EquitySwap.IDSystem",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_PhysicalAsset.PhysicalAssetID",
                        "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "_TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."_EquitySwapEquityLeg.RoleOfPayer",
                                    "IN"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
                                    "IN"."_EquitySwapEquityLeg._EquitySwap.IDSystem",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_PhysicalAsset.PhysicalAssetID",
                                    "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                                    "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                                    "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                                    "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."_EquitySwapEquityLeg.RoleOfPayer",
                                    "IN"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
                                    "IN"."_EquitySwapEquityLeg._EquitySwap.IDSystem",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_PhysicalAsset.PhysicalAssetID",
                                    "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                                    "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                                    "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                                    "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::ValuationSchedule" (
        "SequenceNumber",
        "_EquitySwapEquityLeg.RoleOfPayer",
        "_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
        "_EquitySwapEquityLeg._EquitySwap.IDSystem",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_PhysicalAsset.PhysicalAssetID",
        "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
        "_TotalReturnSwapReturnLeg._Swap.IDSystem",
        "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
        "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "FirstValuationDate",
        "LastValuationDate",
        "ValuationDate",
        "ValuationPeriodLength",
        "ValuationPeriodUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD__EquitySwapEquityLeg.RoleOfPayer" as "_EquitySwapEquityLeg.RoleOfPayer" ,
            "OLD__EquitySwapEquityLeg._EquitySwap.FinancialContractID" as "_EquitySwapEquityLeg._EquitySwap.FinancialContractID" ,
            "OLD__EquitySwapEquityLeg._EquitySwap.IDSystem" as "_EquitySwapEquityLeg._EquitySwap.IDSystem" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__PhysicalAsset.PhysicalAssetID" as "_PhysicalAsset.PhysicalAssetID" ,
            "OLD__TotalReturnSwapReturnLeg._Swap.FinancialContractID" as "_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
            "OLD__TotalReturnSwapReturnLeg._Swap.IDSystem" as "_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
            "OLD__TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" as "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
            "OLD__TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" as "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_FirstValuationDate" as "FirstValuationDate" ,
            "OLD_LastValuationDate" as "LastValuationDate" ,
            "OLD_ValuationDate" as "ValuationDate" ,
            "OLD_ValuationPeriodLength" as "ValuationPeriodLength" ,
            "OLD_ValuationPeriodUnit" as "ValuationPeriodUnit" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."_EquitySwapEquityLeg.RoleOfPayer",
                        "IN"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
                        "IN"."_EquitySwapEquityLeg._EquitySwap.IDSystem",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PhysicalAsset.PhysicalAssetID",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."SequenceNumber" as "OLD_SequenceNumber",
                                "OLD"."_EquitySwapEquityLeg.RoleOfPayer" as "OLD__EquitySwapEquityLeg.RoleOfPayer",
                                "OLD"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID" as "OLD__EquitySwapEquityLeg._EquitySwap.FinancialContractID",
                                "OLD"."_EquitySwapEquityLeg._EquitySwap.IDSystem" as "OLD__EquitySwapEquityLeg._EquitySwap.IDSystem",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_PhysicalAsset.PhysicalAssetID" as "OLD__PhysicalAsset.PhysicalAssetID",
                                "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" as "OLD__TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                                "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" as "OLD__TotalReturnSwapReturnLeg._Swap.IDSystem",
                                "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" as "OLD__TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                                "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" as "OLD__TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."BusinessCalendar" as "OLD_BusinessCalendar",
                                "OLD"."BusinessDayConvention" as "OLD_BusinessDayConvention",
                                "OLD"."FirstValuationDate" as "OLD_FirstValuationDate",
                                "OLD"."LastValuationDate" as "OLD_LastValuationDate",
                                "OLD"."ValuationDate" as "OLD_ValuationDate",
                                "OLD"."ValuationPeriodLength" as "OLD_ValuationPeriodLength",
                                "OLD"."ValuationPeriodUnit" as "OLD_ValuationPeriodUnit",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::ValuationSchedule" as "OLD"
            on
                ifnull( "IN"."SequenceNumber", -1) = "OLD"."SequenceNumber" and
                ifnull( "IN"."_EquitySwapEquityLeg.RoleOfPayer", '') = "OLD"."_EquitySwapEquityLeg.RoleOfPayer" and
                ifnull( "IN"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID", '') = "OLD"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID" and
                ifnull( "IN"."_EquitySwapEquityLeg._EquitySwap.IDSystem", '') = "OLD"."_EquitySwapEquityLeg._EquitySwap.IDSystem" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_PhysicalAsset.PhysicalAssetID", '') = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                ifnull( "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID", '') = "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" and
                ifnull( "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem", '') = "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" and
                ifnull( "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID", '') = "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" and
                ifnull( "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID", '') = "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::ValuationSchedule" (
        "SequenceNumber",
        "_EquitySwapEquityLeg.RoleOfPayer",
        "_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
        "_EquitySwapEquityLeg._EquitySwap.IDSystem",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_PhysicalAsset.PhysicalAssetID",
        "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
        "_TotalReturnSwapReturnLeg._Swap.IDSystem",
        "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
        "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "FirstValuationDate",
        "LastValuationDate",
        "ValuationDate",
        "ValuationPeriodLength",
        "ValuationPeriodUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber"  as "SequenceNumber",
            "OLD__EquitySwapEquityLeg.RoleOfPayer"  as "_EquitySwapEquityLeg.RoleOfPayer",
            "OLD__EquitySwapEquityLeg._EquitySwap.FinancialContractID"  as "_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
            "OLD__EquitySwapEquityLeg._EquitySwap.IDSystem"  as "_EquitySwapEquityLeg._EquitySwap.IDSystem",
            "OLD__FinancialContract.FinancialContractID"  as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem"  as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID"  as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__PhysicalAsset.PhysicalAssetID"  as "_PhysicalAsset.PhysicalAssetID",
            "OLD__TotalReturnSwapReturnLeg._Swap.FinancialContractID"  as "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
            "OLD__TotalReturnSwapReturnLeg._Swap.IDSystem"  as "_TotalReturnSwapReturnLeg._Swap.IDSystem",
            "OLD__TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID"  as "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
            "OLD__TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"  as "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_BusinessCalendar"  as "BusinessCalendar",
            "OLD_BusinessDayConvention"  as "BusinessDayConvention",
            "OLD_FirstValuationDate"  as "FirstValuationDate",
            "OLD_LastValuationDate"  as "LastValuationDate",
            "OLD_ValuationDate"  as "ValuationDate",
            "OLD_ValuationPeriodLength"  as "ValuationPeriodLength",
            "OLD_ValuationPeriodUnit"  as "ValuationPeriodUnit",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."_EquitySwapEquityLeg.RoleOfPayer",
                        "IN"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
                        "IN"."_EquitySwapEquityLeg._EquitySwap.IDSystem",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_PhysicalAsset.PhysicalAssetID",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."SequenceNumber" as "OLD_SequenceNumber",
                        "OLD"."_EquitySwapEquityLeg.RoleOfPayer" as "OLD__EquitySwapEquityLeg.RoleOfPayer",
                        "OLD"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID" as "OLD__EquitySwapEquityLeg._EquitySwap.FinancialContractID",
                        "OLD"."_EquitySwapEquityLeg._EquitySwap.IDSystem" as "OLD__EquitySwapEquityLeg._EquitySwap.IDSystem",
                        "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_PhysicalAsset.PhysicalAssetID" as "OLD__PhysicalAsset.PhysicalAssetID",
                        "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" as "OLD__TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" as "OLD__TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" as "OLD__TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" as "OLD__TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."BusinessCalendar" as "OLD_BusinessCalendar",
                        "OLD"."BusinessDayConvention" as "OLD_BusinessDayConvention",
                        "OLD"."FirstValuationDate" as "OLD_FirstValuationDate",
                        "OLD"."LastValuationDate" as "OLD_LastValuationDate",
                        "OLD"."ValuationDate" as "OLD_ValuationDate",
                        "OLD"."ValuationPeriodLength" as "OLD_ValuationPeriodLength",
                        "OLD"."ValuationPeriodUnit" as "OLD_ValuationPeriodUnit",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::ValuationSchedule" as "OLD"
            on
                ifnull( "IN"."SequenceNumber", -1 ) = "OLD"."SequenceNumber" and
                ifnull( "IN"."_EquitySwapEquityLeg.RoleOfPayer", '' ) = "OLD"."_EquitySwapEquityLeg.RoleOfPayer" and
                ifnull( "IN"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID", '' ) = "OLD"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID" and
                ifnull( "IN"."_EquitySwapEquityLeg._EquitySwap.IDSystem", '' ) = "OLD"."_EquitySwapEquityLeg._EquitySwap.IDSystem" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_PhysicalAsset.PhysicalAssetID", '' ) = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                ifnull( "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID", '' ) = "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" and
                ifnull( "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem", '' ) = "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" and
                ifnull( "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID", '' ) = "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" and
                ifnull( "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID", '' ) = "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::ValuationSchedule"
    where (
        "SequenceNumber",
        "_EquitySwapEquityLeg.RoleOfPayer",
        "_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
        "_EquitySwapEquityLeg._EquitySwap.IDSystem",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_PhysicalAsset.PhysicalAssetID",
        "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
        "_TotalReturnSwapReturnLeg._Swap.IDSystem",
        "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
        "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."SequenceNumber",
            "OLD"."_EquitySwapEquityLeg.RoleOfPayer",
            "OLD"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
            "OLD"."_EquitySwapEquityLeg._EquitySwap.IDSystem",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_PhysicalAsset.PhysicalAssetID",
            "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
            "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
            "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
            "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ValuationSchedule" as "OLD"
        on
           ifnull( "IN"."SequenceNumber", -1 ) = "OLD"."SequenceNumber" and
           ifnull( "IN"."_EquitySwapEquityLeg.RoleOfPayer", '' ) = "OLD"."_EquitySwapEquityLeg.RoleOfPayer" and
           ifnull( "IN"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID", '' ) = "OLD"."_EquitySwapEquityLeg._EquitySwap.FinancialContractID" and
           ifnull( "IN"."_EquitySwapEquityLeg._EquitySwap.IDSystem", '' ) = "OLD"."_EquitySwapEquityLeg._EquitySwap.IDSystem" and
           ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
           ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" and
           ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
           ifnull( "IN"."_PhysicalAsset.PhysicalAssetID", '' ) = "OLD"."_PhysicalAsset.PhysicalAssetID" and
           ifnull( "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID", '' ) = "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" and
           ifnull( "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem", '' ) = "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" and
           ifnull( "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID", '' ) = "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" and
           ifnull( "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID", '' ) = "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::ValuationSchedule" (
        "SequenceNumber",
        "_EquitySwapEquityLeg.RoleOfPayer",
        "_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
        "_EquitySwapEquityLeg._EquitySwap.IDSystem",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_PhysicalAsset.PhysicalAssetID",
        "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
        "_TotalReturnSwapReturnLeg._Swap.IDSystem",
        "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
        "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "FirstValuationDate",
        "LastValuationDate",
        "ValuationDate",
        "ValuationPeriodLength",
        "ValuationPeriodUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "SequenceNumber", -1 ) as "SequenceNumber",
            ifnull( "_EquitySwapEquityLeg.RoleOfPayer", '' ) as "_EquitySwapEquityLeg.RoleOfPayer",
            ifnull( "_EquitySwapEquityLeg._EquitySwap.FinancialContractID", '' ) as "_EquitySwapEquityLeg._EquitySwap.FinancialContractID",
            ifnull( "_EquitySwapEquityLeg._EquitySwap.IDSystem", '' ) as "_EquitySwapEquityLeg._EquitySwap.IDSystem",
            ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
            ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
            ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
            ifnull( "_PhysicalAsset.PhysicalAssetID", '' ) as "_PhysicalAsset.PhysicalAssetID",
            ifnull( "_TotalReturnSwapReturnLeg._Swap.FinancialContractID", '' ) as "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
            ifnull( "_TotalReturnSwapReturnLeg._Swap.IDSystem", '' ) as "_TotalReturnSwapReturnLeg._Swap.IDSystem",
            ifnull( "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID", '' ) as "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
            ifnull( "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID", '' ) as "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "BusinessCalendar"  ,
            "BusinessDayConvention"  ,
            "FirstValuationDate"  ,
            "LastValuationDate"  ,
            "ValuationDate"  ,
            "ValuationPeriodLength"  ,
            "ValuationPeriodUnit"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END