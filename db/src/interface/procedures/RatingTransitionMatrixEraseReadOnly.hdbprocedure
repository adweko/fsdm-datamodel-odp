PROCEDURE "sap.fsdm.procedures::RatingTransitionMatrixEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::RatingTransitionMatrixTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::RatingTransitionMatrixTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::RatingTransitionMatrixTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "MatrixID" is null and
            "RatingAgency" is null and
            "RatingCategory" is null and
            "RatingMethod" is null and
            "RatingTimeHorizon" is null and
            "SourceRating" is null and
            "TargetRating" is null and
            "TransitionTimeHorizon" is null and
            "TransitionTimeHorizonUnit" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "MatrixID" ,
                "RatingAgency" ,
                "RatingCategory" ,
                "RatingMethod" ,
                "RatingTimeHorizon" ,
                "SourceRating" ,
                "TargetRating" ,
                "TransitionTimeHorizon" ,
                "TransitionTimeHorizonUnit" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."MatrixID" ,
                "OLD"."RatingAgency" ,
                "OLD"."RatingCategory" ,
                "OLD"."RatingMethod" ,
                "OLD"."RatingTimeHorizon" ,
                "OLD"."SourceRating" ,
                "OLD"."TargetRating" ,
                "OLD"."TransitionTimeHorizon" ,
                "OLD"."TransitionTimeHorizonUnit" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RatingTransitionMatrix" "OLD"
            on
                "IN"."MatrixID" = "OLD"."MatrixID" and
                "IN"."RatingAgency" = "OLD"."RatingAgency" and
                "IN"."RatingCategory" = "OLD"."RatingCategory" and
                "IN"."RatingMethod" = "OLD"."RatingMethod" and
                "IN"."RatingTimeHorizon" = "OLD"."RatingTimeHorizon" and
                "IN"."SourceRating" = "OLD"."SourceRating" and
                "IN"."TargetRating" = "OLD"."TargetRating" and
                "IN"."TransitionTimeHorizon" = "OLD"."TransitionTimeHorizon" and
                "IN"."TransitionTimeHorizonUnit" = "OLD"."TransitionTimeHorizonUnit" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "MatrixID" ,
            "RatingAgency" ,
            "RatingCategory" ,
            "RatingMethod" ,
            "RatingTimeHorizon" ,
            "SourceRating" ,
            "TargetRating" ,
            "TransitionTimeHorizon" ,
            "TransitionTimeHorizonUnit" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."MatrixID" ,
                "OLD"."RatingAgency" ,
                "OLD"."RatingCategory" ,
                "OLD"."RatingMethod" ,
                "OLD"."RatingTimeHorizon" ,
                "OLD"."SourceRating" ,
                "OLD"."TargetRating" ,
                "OLD"."TransitionTimeHorizon" ,
                "OLD"."TransitionTimeHorizonUnit" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RatingTransitionMatrix_Historical" "OLD"
            on
                "IN"."MatrixID" = "OLD"."MatrixID" and
                "IN"."RatingAgency" = "OLD"."RatingAgency" and
                "IN"."RatingCategory" = "OLD"."RatingCategory" and
                "IN"."RatingMethod" = "OLD"."RatingMethod" and
                "IN"."RatingTimeHorizon" = "OLD"."RatingTimeHorizon" and
                "IN"."SourceRating" = "OLD"."SourceRating" and
                "IN"."TargetRating" = "OLD"."TargetRating" and
                "IN"."TransitionTimeHorizon" = "OLD"."TransitionTimeHorizon" and
                "IN"."TransitionTimeHorizonUnit" = "OLD"."TransitionTimeHorizonUnit" 
        );

END
