PROCEDURE "sap.fsdm.procedures::PhysicalHazardOfGeographicalUnitLoad" (IN ROW "sap.fsdm.tabletypes::PhysicalHazardOfGeographicalUnitTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'DataProvider=' || TO_VARCHAR("DataProvider") || ' ' ||
                'PhysicalHazardType=' || TO_VARCHAR("PhysicalHazardType") || ' ' ||
                '_GeographicalUnit.GeographicalStructureID=' || TO_VARCHAR("_GeographicalUnit.GeographicalStructureID") || ' ' ||
                '_GeographicalUnit.GeographicalUnitID=' || TO_VARCHAR("_GeographicalUnit.GeographicalUnitID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."DataProvider",
                        "IN"."PhysicalHazardType",
                        "IN"."_GeographicalUnit.GeographicalStructureID",
                        "IN"."_GeographicalUnit.GeographicalUnitID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."DataProvider",
                        "IN"."PhysicalHazardType",
                        "IN"."_GeographicalUnit.GeographicalStructureID",
                        "IN"."_GeographicalUnit.GeographicalUnitID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "DataProvider",
                        "PhysicalHazardType",
                        "_GeographicalUnit.GeographicalStructureID",
                        "_GeographicalUnit.GeographicalUnitID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."DataProvider",
                                    "IN"."PhysicalHazardType",
                                    "IN"."_GeographicalUnit.GeographicalStructureID",
                                    "IN"."_GeographicalUnit.GeographicalUnitID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."DataProvider",
                                    "IN"."PhysicalHazardType",
                                    "IN"."_GeographicalUnit.GeographicalStructureID",
                                    "IN"."_GeographicalUnit.GeographicalUnitID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::PhysicalHazardOfGeographicalUnit" (
        "DataProvider",
        "PhysicalHazardType",
        "_GeographicalUnit.GeographicalStructureID",
        "_GeographicalUnit.GeographicalUnitID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ClimateRelatedPhysicalHazard",
        "PhysicalHazardCategory",
        "PhysicalRiskType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_DataProvider" as "DataProvider" ,
            "OLD_PhysicalHazardType" as "PhysicalHazardType" ,
            "OLD__GeographicalUnit.GeographicalStructureID" as "_GeographicalUnit.GeographicalStructureID" ,
            "OLD__GeographicalUnit.GeographicalUnitID" as "_GeographicalUnit.GeographicalUnitID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ClimateRelatedPhysicalHazard" as "ClimateRelatedPhysicalHazard" ,
            "OLD_PhysicalHazardCategory" as "PhysicalHazardCategory" ,
            "OLD_PhysicalRiskType" as "PhysicalRiskType" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."DataProvider",
                        "IN"."PhysicalHazardType",
                        "IN"."_GeographicalUnit.GeographicalStructureID",
                        "IN"."_GeographicalUnit.GeographicalUnitID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."DataProvider" as "OLD_DataProvider",
                                "OLD"."PhysicalHazardType" as "OLD_PhysicalHazardType",
                                "OLD"."_GeographicalUnit.GeographicalStructureID" as "OLD__GeographicalUnit.GeographicalStructureID",
                                "OLD"."_GeographicalUnit.GeographicalUnitID" as "OLD__GeographicalUnit.GeographicalUnitID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."ClimateRelatedPhysicalHazard" as "OLD_ClimateRelatedPhysicalHazard",
                                "OLD"."PhysicalHazardCategory" as "OLD_PhysicalHazardCategory",
                                "OLD"."PhysicalRiskType" as "OLD_PhysicalRiskType",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::PhysicalHazardOfGeographicalUnit" as "OLD"
            on
                ifnull( "IN"."DataProvider", '') = "OLD"."DataProvider" and
                ifnull( "IN"."PhysicalHazardType", '') = "OLD"."PhysicalHazardType" and
                ifnull( "IN"."_GeographicalUnit.GeographicalStructureID", '') = "OLD"."_GeographicalUnit.GeographicalStructureID" and
                ifnull( "IN"."_GeographicalUnit.GeographicalUnitID", '') = "OLD"."_GeographicalUnit.GeographicalUnitID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::PhysicalHazardOfGeographicalUnit" (
        "DataProvider",
        "PhysicalHazardType",
        "_GeographicalUnit.GeographicalStructureID",
        "_GeographicalUnit.GeographicalUnitID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ClimateRelatedPhysicalHazard",
        "PhysicalHazardCategory",
        "PhysicalRiskType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_DataProvider"  as "DataProvider",
            "OLD_PhysicalHazardType"  as "PhysicalHazardType",
            "OLD__GeographicalUnit.GeographicalStructureID"  as "_GeographicalUnit.GeographicalStructureID",
            "OLD__GeographicalUnit.GeographicalUnitID"  as "_GeographicalUnit.GeographicalUnitID",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_ClimateRelatedPhysicalHazard"  as "ClimateRelatedPhysicalHazard",
            "OLD_PhysicalHazardCategory"  as "PhysicalHazardCategory",
            "OLD_PhysicalRiskType"  as "PhysicalRiskType",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."DataProvider",
                        "IN"."PhysicalHazardType",
                        "IN"."_GeographicalUnit.GeographicalStructureID",
                        "IN"."_GeographicalUnit.GeographicalUnitID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."DataProvider" as "OLD_DataProvider",
                        "OLD"."PhysicalHazardType" as "OLD_PhysicalHazardType",
                        "OLD"."_GeographicalUnit.GeographicalStructureID" as "OLD__GeographicalUnit.GeographicalStructureID",
                        "OLD"."_GeographicalUnit.GeographicalUnitID" as "OLD__GeographicalUnit.GeographicalUnitID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."ClimateRelatedPhysicalHazard" as "OLD_ClimateRelatedPhysicalHazard",
                        "OLD"."PhysicalHazardCategory" as "OLD_PhysicalHazardCategory",
                        "OLD"."PhysicalRiskType" as "OLD_PhysicalRiskType",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::PhysicalHazardOfGeographicalUnit" as "OLD"
            on
                ifnull( "IN"."DataProvider", '' ) = "OLD"."DataProvider" and
                ifnull( "IN"."PhysicalHazardType", '' ) = "OLD"."PhysicalHazardType" and
                ifnull( "IN"."_GeographicalUnit.GeographicalStructureID", '' ) = "OLD"."_GeographicalUnit.GeographicalStructureID" and
                ifnull( "IN"."_GeographicalUnit.GeographicalUnitID", '' ) = "OLD"."_GeographicalUnit.GeographicalUnitID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::PhysicalHazardOfGeographicalUnit"
    where (
        "DataProvider",
        "PhysicalHazardType",
        "_GeographicalUnit.GeographicalStructureID",
        "_GeographicalUnit.GeographicalUnitID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."DataProvider",
            "OLD"."PhysicalHazardType",
            "OLD"."_GeographicalUnit.GeographicalStructureID",
            "OLD"."_GeographicalUnit.GeographicalUnitID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PhysicalHazardOfGeographicalUnit" as "OLD"
        on
           ifnull( "IN"."DataProvider", '' ) = "OLD"."DataProvider" and
           ifnull( "IN"."PhysicalHazardType", '' ) = "OLD"."PhysicalHazardType" and
           ifnull( "IN"."_GeographicalUnit.GeographicalStructureID", '' ) = "OLD"."_GeographicalUnit.GeographicalStructureID" and
           ifnull( "IN"."_GeographicalUnit.GeographicalUnitID", '' ) = "OLD"."_GeographicalUnit.GeographicalUnitID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::PhysicalHazardOfGeographicalUnit" (
        "DataProvider",
        "PhysicalHazardType",
        "_GeographicalUnit.GeographicalStructureID",
        "_GeographicalUnit.GeographicalUnitID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ClimateRelatedPhysicalHazard",
        "PhysicalHazardCategory",
        "PhysicalRiskType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "DataProvider", '' ) as "DataProvider",
            ifnull( "PhysicalHazardType", '' ) as "PhysicalHazardType",
            ifnull( "_GeographicalUnit.GeographicalStructureID", '' ) as "_GeographicalUnit.GeographicalStructureID",
            ifnull( "_GeographicalUnit.GeographicalUnitID", '' ) as "_GeographicalUnit.GeographicalUnitID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "ClimateRelatedPhysicalHazard"  ,
            "PhysicalHazardCategory"  ,
            "PhysicalRiskType"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END