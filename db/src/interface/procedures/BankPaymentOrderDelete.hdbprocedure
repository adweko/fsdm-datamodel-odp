PROCEDURE "sap.fsdm.procedures::BankPaymentOrderDelete" (IN ROW "sap.fsdm.tabletypes::BankPaymentOrderTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'BankPaymentOrderID=' || TO_VARCHAR("BankPaymentOrderID") || ' ' ||
                'IDSystem=' || TO_VARCHAR("IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."BankPaymentOrderID",
                        "IN"."IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."BankPaymentOrderID",
                        "IN"."IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "BankPaymentOrderID",
                        "IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."BankPaymentOrderID",
                                    "IN"."IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."BankPaymentOrderID",
                                    "IN"."IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "BankPaymentOrderID" is null and
            "IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::BankPaymentOrder" (
        "BankPaymentOrderID",
        "IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_BankingChannel.BankingChannelID",
        "_BusinessEventDataOwner.BusinessPartnerID",
        "BankPaymentOrderCategory",
        "Channel",
        "ClearingDate",
        "CollectorDate",
        "CollectorID",
        "DirectDebitMandateType",
        "ExecutionDate",
        "ExternalPaymentOrderID",
        "FileName",
        "Format",
        "InitiatorType",
        "IsDerivedFromQueue",
        "IsSepaCompatible",
        "MeansOfPaymentType",
        "Medium",
        "NumberOfItems",
        "PISPTriggerType",
        "PaperBasedPayment",
        "Priority",
        "ReceiptDate",
        "RemoteAccess",
        "SettlementDate",
        "StrongCustomerAuthentication",
        "StrongCustomerAuthenticationReason",
        "TotalSum",
        "TotalSumCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_BankPaymentOrderID" as "BankPaymentOrderID" ,
            "OLD_IDSystem" as "IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__BankingChannel.BankingChannelID" as "_BankingChannel.BankingChannelID" ,
            "OLD__BusinessEventDataOwner.BusinessPartnerID" as "_BusinessEventDataOwner.BusinessPartnerID" ,
            "OLD_BankPaymentOrderCategory" as "BankPaymentOrderCategory" ,
            "OLD_Channel" as "Channel" ,
            "OLD_ClearingDate" as "ClearingDate" ,
            "OLD_CollectorDate" as "CollectorDate" ,
            "OLD_CollectorID" as "CollectorID" ,
            "OLD_DirectDebitMandateType" as "DirectDebitMandateType" ,
            "OLD_ExecutionDate" as "ExecutionDate" ,
            "OLD_ExternalPaymentOrderID" as "ExternalPaymentOrderID" ,
            "OLD_FileName" as "FileName" ,
            "OLD_Format" as "Format" ,
            "OLD_InitiatorType" as "InitiatorType" ,
            "OLD_IsDerivedFromQueue" as "IsDerivedFromQueue" ,
            "OLD_IsSepaCompatible" as "IsSepaCompatible" ,
            "OLD_MeansOfPaymentType" as "MeansOfPaymentType" ,
            "OLD_Medium" as "Medium" ,
            "OLD_NumberOfItems" as "NumberOfItems" ,
            "OLD_PISPTriggerType" as "PISPTriggerType" ,
            "OLD_PaperBasedPayment" as "PaperBasedPayment" ,
            "OLD_Priority" as "Priority" ,
            "OLD_ReceiptDate" as "ReceiptDate" ,
            "OLD_RemoteAccess" as "RemoteAccess" ,
            "OLD_SettlementDate" as "SettlementDate" ,
            "OLD_StrongCustomerAuthentication" as "StrongCustomerAuthentication" ,
            "OLD_StrongCustomerAuthenticationReason" as "StrongCustomerAuthenticationReason" ,
            "OLD_TotalSum" as "TotalSum" ,
            "OLD_TotalSumCurrency" as "TotalSumCurrency" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."BankPaymentOrderID",
                        "OLD"."IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."BankPaymentOrderID" AS "OLD_BankPaymentOrderID" ,
                "OLD"."IDSystem" AS "OLD_IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_BankingChannel.BankingChannelID" AS "OLD__BankingChannel.BankingChannelID" ,
                "OLD"."_BusinessEventDataOwner.BusinessPartnerID" AS "OLD__BusinessEventDataOwner.BusinessPartnerID" ,
                "OLD"."BankPaymentOrderCategory" AS "OLD_BankPaymentOrderCategory" ,
                "OLD"."Channel" AS "OLD_Channel" ,
                "OLD"."ClearingDate" AS "OLD_ClearingDate" ,
                "OLD"."CollectorDate" AS "OLD_CollectorDate" ,
                "OLD"."CollectorID" AS "OLD_CollectorID" ,
                "OLD"."DirectDebitMandateType" AS "OLD_DirectDebitMandateType" ,
                "OLD"."ExecutionDate" AS "OLD_ExecutionDate" ,
                "OLD"."ExternalPaymentOrderID" AS "OLD_ExternalPaymentOrderID" ,
                "OLD"."FileName" AS "OLD_FileName" ,
                "OLD"."Format" AS "OLD_Format" ,
                "OLD"."InitiatorType" AS "OLD_InitiatorType" ,
                "OLD"."IsDerivedFromQueue" AS "OLD_IsDerivedFromQueue" ,
                "OLD"."IsSepaCompatible" AS "OLD_IsSepaCompatible" ,
                "OLD"."MeansOfPaymentType" AS "OLD_MeansOfPaymentType" ,
                "OLD"."Medium" AS "OLD_Medium" ,
                "OLD"."NumberOfItems" AS "OLD_NumberOfItems" ,
                "OLD"."PISPTriggerType" AS "OLD_PISPTriggerType" ,
                "OLD"."PaperBasedPayment" AS "OLD_PaperBasedPayment" ,
                "OLD"."Priority" AS "OLD_Priority" ,
                "OLD"."ReceiptDate" AS "OLD_ReceiptDate" ,
                "OLD"."RemoteAccess" AS "OLD_RemoteAccess" ,
                "OLD"."SettlementDate" AS "OLD_SettlementDate" ,
                "OLD"."StrongCustomerAuthentication" AS "OLD_StrongCustomerAuthentication" ,
                "OLD"."StrongCustomerAuthenticationReason" AS "OLD_StrongCustomerAuthenticationReason" ,
                "OLD"."TotalSum" AS "OLD_TotalSum" ,
                "OLD"."TotalSumCurrency" AS "OLD_TotalSumCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BankPaymentOrder" as "OLD"
            on
                      "IN"."BankPaymentOrderID" = "OLD"."BankPaymentOrderID" and
                      "IN"."IDSystem" = "OLD"."IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::BankPaymentOrder" (
        "BankPaymentOrderID",
        "IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_BankingChannel.BankingChannelID",
        "_BusinessEventDataOwner.BusinessPartnerID",
        "BankPaymentOrderCategory",
        "Channel",
        "ClearingDate",
        "CollectorDate",
        "CollectorID",
        "DirectDebitMandateType",
        "ExecutionDate",
        "ExternalPaymentOrderID",
        "FileName",
        "Format",
        "InitiatorType",
        "IsDerivedFromQueue",
        "IsSepaCompatible",
        "MeansOfPaymentType",
        "Medium",
        "NumberOfItems",
        "PISPTriggerType",
        "PaperBasedPayment",
        "Priority",
        "ReceiptDate",
        "RemoteAccess",
        "SettlementDate",
        "StrongCustomerAuthentication",
        "StrongCustomerAuthenticationReason",
        "TotalSum",
        "TotalSumCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_BankPaymentOrderID" as "BankPaymentOrderID",
            "OLD_IDSystem" as "IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__BankingChannel.BankingChannelID" as "_BankingChannel.BankingChannelID",
            "OLD__BusinessEventDataOwner.BusinessPartnerID" as "_BusinessEventDataOwner.BusinessPartnerID",
            "OLD_BankPaymentOrderCategory" as "BankPaymentOrderCategory",
            "OLD_Channel" as "Channel",
            "OLD_ClearingDate" as "ClearingDate",
            "OLD_CollectorDate" as "CollectorDate",
            "OLD_CollectorID" as "CollectorID",
            "OLD_DirectDebitMandateType" as "DirectDebitMandateType",
            "OLD_ExecutionDate" as "ExecutionDate",
            "OLD_ExternalPaymentOrderID" as "ExternalPaymentOrderID",
            "OLD_FileName" as "FileName",
            "OLD_Format" as "Format",
            "OLD_InitiatorType" as "InitiatorType",
            "OLD_IsDerivedFromQueue" as "IsDerivedFromQueue",
            "OLD_IsSepaCompatible" as "IsSepaCompatible",
            "OLD_MeansOfPaymentType" as "MeansOfPaymentType",
            "OLD_Medium" as "Medium",
            "OLD_NumberOfItems" as "NumberOfItems",
            "OLD_PISPTriggerType" as "PISPTriggerType",
            "OLD_PaperBasedPayment" as "PaperBasedPayment",
            "OLD_Priority" as "Priority",
            "OLD_ReceiptDate" as "ReceiptDate",
            "OLD_RemoteAccess" as "RemoteAccess",
            "OLD_SettlementDate" as "SettlementDate",
            "OLD_StrongCustomerAuthentication" as "StrongCustomerAuthentication",
            "OLD_StrongCustomerAuthenticationReason" as "StrongCustomerAuthenticationReason",
            "OLD_TotalSum" as "TotalSum",
            "OLD_TotalSumCurrency" as "TotalSumCurrency",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."BankPaymentOrderID",
                        "OLD"."IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."BankPaymentOrderID" AS "OLD_BankPaymentOrderID" ,
                "OLD"."IDSystem" AS "OLD_IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_BankingChannel.BankingChannelID" AS "OLD__BankingChannel.BankingChannelID" ,
                "OLD"."_BusinessEventDataOwner.BusinessPartnerID" AS "OLD__BusinessEventDataOwner.BusinessPartnerID" ,
                "OLD"."BankPaymentOrderCategory" AS "OLD_BankPaymentOrderCategory" ,
                "OLD"."Channel" AS "OLD_Channel" ,
                "OLD"."ClearingDate" AS "OLD_ClearingDate" ,
                "OLD"."CollectorDate" AS "OLD_CollectorDate" ,
                "OLD"."CollectorID" AS "OLD_CollectorID" ,
                "OLD"."DirectDebitMandateType" AS "OLD_DirectDebitMandateType" ,
                "OLD"."ExecutionDate" AS "OLD_ExecutionDate" ,
                "OLD"."ExternalPaymentOrderID" AS "OLD_ExternalPaymentOrderID" ,
                "OLD"."FileName" AS "OLD_FileName" ,
                "OLD"."Format" AS "OLD_Format" ,
                "OLD"."InitiatorType" AS "OLD_InitiatorType" ,
                "OLD"."IsDerivedFromQueue" AS "OLD_IsDerivedFromQueue" ,
                "OLD"."IsSepaCompatible" AS "OLD_IsSepaCompatible" ,
                "OLD"."MeansOfPaymentType" AS "OLD_MeansOfPaymentType" ,
                "OLD"."Medium" AS "OLD_Medium" ,
                "OLD"."NumberOfItems" AS "OLD_NumberOfItems" ,
                "OLD"."PISPTriggerType" AS "OLD_PISPTriggerType" ,
                "OLD"."PaperBasedPayment" AS "OLD_PaperBasedPayment" ,
                "OLD"."Priority" AS "OLD_Priority" ,
                "OLD"."ReceiptDate" AS "OLD_ReceiptDate" ,
                "OLD"."RemoteAccess" AS "OLD_RemoteAccess" ,
                "OLD"."SettlementDate" AS "OLD_SettlementDate" ,
                "OLD"."StrongCustomerAuthentication" AS "OLD_StrongCustomerAuthentication" ,
                "OLD"."StrongCustomerAuthenticationReason" AS "OLD_StrongCustomerAuthenticationReason" ,
                "OLD"."TotalSum" AS "OLD_TotalSum" ,
                "OLD"."TotalSumCurrency" AS "OLD_TotalSumCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BankPaymentOrder" as "OLD"
            on
                                                "IN"."BankPaymentOrderID" = "OLD"."BankPaymentOrderID" and
                                                "IN"."IDSystem" = "OLD"."IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::BankPaymentOrder"
    where (
        "BankPaymentOrderID",
        "IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."BankPaymentOrderID",
            "OLD"."IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::BankPaymentOrder" as "OLD"
        on
                                       "IN"."BankPaymentOrderID" = "OLD"."BankPaymentOrderID" and
                                       "IN"."IDSystem" = "OLD"."IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
