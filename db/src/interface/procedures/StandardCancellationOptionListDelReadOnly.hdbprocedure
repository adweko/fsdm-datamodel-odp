PROCEDURE "sap.fsdm.procedures::StandardCancellationOptionListDelReadOnly" (IN ROW "sap.fsdm.tabletypes::StandardCancellationOptionTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::StandardCancellationOptionTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::StandardCancellationOptionTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_StandardProduct.FinancialStandardProductID=' || TO_VARCHAR("ASSOC_StandardProduct.FinancialStandardProductID") || ' ' ||
                'ASSOC_StandardProduct.IDSystem=' || TO_VARCHAR("ASSOC_StandardProduct.IDSystem") || ' ' ||
                'ASSOC_StandardProduct.PricingScheme=' || TO_VARCHAR("ASSOC_StandardProduct.PricingScheme") || ' ' ||
                'ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID=' || TO_VARCHAR("ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_StandardProduct.FinancialStandardProductID",
                        "IN"."ASSOC_StandardProduct.IDSystem",
                        "IN"."ASSOC_StandardProduct.PricingScheme",
                        "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_StandardProduct.FinancialStandardProductID",
                        "IN"."ASSOC_StandardProduct.IDSystem",
                        "IN"."ASSOC_StandardProduct.PricingScheme",
                        "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "ASSOC_StandardProduct.FinancialStandardProductID",
                        "ASSOC_StandardProduct.IDSystem",
                        "ASSOC_StandardProduct.PricingScheme",
                        "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_StandardProduct.FinancialStandardProductID",
                                    "IN"."ASSOC_StandardProduct.IDSystem",
                                    "IN"."ASSOC_StandardProduct.PricingScheme",
                                    "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_StandardProduct.FinancialStandardProductID",
                                    "IN"."ASSOC_StandardProduct.IDSystem",
                                    "IN"."ASSOC_StandardProduct.PricingScheme",
                                    "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" is null and
            "ASSOC_StandardProduct.IDSystem" is null and
            "ASSOC_StandardProduct.FinancialStandardProductID" is null and
            "ASSOC_StandardProduct.PricingScheme" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "SequenceNumber",
            "ASSOC_StandardProduct.FinancialStandardProductID",
            "ASSOC_StandardProduct.IDSystem",
            "ASSOC_StandardProduct.PricingScheme",
            "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::StandardCancellationOption" WHERE
            (
            "SequenceNumber" ,
            "ASSOC_StandardProduct.FinancialStandardProductID" ,
            "ASSOC_StandardProduct.IDSystem" ,
            "ASSOC_StandardProduct.PricingScheme" ,
            "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_StandardProduct.FinancialStandardProductID",
            "OLD"."ASSOC_StandardProduct.IDSystem",
            "OLD"."ASSOC_StandardProduct.PricingScheme",
            "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::StandardCancellationOption" as "OLD"
        on
                              "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" and
                              "IN"."ASSOC_StandardProduct.IDSystem" = "OLD"."ASSOC_StandardProduct.IDSystem" and
                              "IN"."ASSOC_StandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardProduct.FinancialStandardProductID" and
                              "IN"."ASSOC_StandardProduct.PricingScheme" = "OLD"."ASSOC_StandardProduct.PricingScheme" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "SequenceNumber",
        "ASSOC_StandardProduct.FinancialStandardProductID",
        "ASSOC_StandardProduct.IDSystem",
        "ASSOC_StandardProduct.PricingScheme",
        "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "CancellationOptionType",
        "NoticePeriodLength",
        "NoticePeriodTimeUnit",
        "OpportunityCostCompensation",
        "OptionHolder",
        "PartialCancellationAllowed",
        "PeriodUntilExercisePeriodEnd",
        "PeriodUntilExercisePeriodStart",
        "StandardCancellationOptionCategory",
        "TimeUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_StandardProduct.FinancialStandardProductID" as "ASSOC_StandardProduct.FinancialStandardProductID" ,
            "OLD_ASSOC_StandardProduct.IDSystem" as "ASSOC_StandardProduct.IDSystem" ,
            "OLD_ASSOC_StandardProduct.PricingScheme" as "ASSOC_StandardProduct.PricingScheme" ,
            "OLD_ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" as "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_CancellationOptionType" as "CancellationOptionType" ,
            "OLD_NoticePeriodLength" as "NoticePeriodLength" ,
            "OLD_NoticePeriodTimeUnit" as "NoticePeriodTimeUnit" ,
            "OLD_OpportunityCostCompensation" as "OpportunityCostCompensation" ,
            "OLD_OptionHolder" as "OptionHolder" ,
            "OLD_PartialCancellationAllowed" as "PartialCancellationAllowed" ,
            "OLD_PeriodUntilExercisePeriodEnd" as "PeriodUntilExercisePeriodEnd" ,
            "OLD_PeriodUntilExercisePeriodStart" as "PeriodUntilExercisePeriodStart" ,
            "OLD_StandardCancellationOptionCategory" as "StandardCancellationOptionCategory" ,
            "OLD_TimeUnit" as "TimeUnit" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
                        "OLD"."ASSOC_StandardProduct.IDSystem",
                        "OLD"."ASSOC_StandardProduct.FinancialStandardProductID",
                        "OLD"."ASSOC_StandardProduct.PricingScheme",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_StandardProduct.FinancialStandardProductID" AS "OLD_ASSOC_StandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardProduct.IDSystem" AS "OLD_ASSOC_StandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardProduct.PricingScheme" AS "OLD_ASSOC_StandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" AS "OLD_ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."CancellationOptionType" AS "OLD_CancellationOptionType" ,
                "OLD"."NoticePeriodLength" AS "OLD_NoticePeriodLength" ,
                "OLD"."NoticePeriodTimeUnit" AS "OLD_NoticePeriodTimeUnit" ,
                "OLD"."OpportunityCostCompensation" AS "OLD_OpportunityCostCompensation" ,
                "OLD"."OptionHolder" AS "OLD_OptionHolder" ,
                "OLD"."PartialCancellationAllowed" AS "OLD_PartialCancellationAllowed" ,
                "OLD"."PeriodUntilExercisePeriodEnd" AS "OLD_PeriodUntilExercisePeriodEnd" ,
                "OLD"."PeriodUntilExercisePeriodStart" AS "OLD_PeriodUntilExercisePeriodStart" ,
                "OLD"."StandardCancellationOptionCategory" AS "OLD_StandardCancellationOptionCategory" ,
                "OLD"."TimeUnit" AS "OLD_TimeUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::StandardCancellationOption" as "OLD"
            on
                                      "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" and
                                      "IN"."ASSOC_StandardProduct.IDSystem" = "OLD"."ASSOC_StandardProduct.IDSystem" and
                                      "IN"."ASSOC_StandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardProduct.FinancialStandardProductID" and
                                      "IN"."ASSOC_StandardProduct.PricingScheme" = "OLD"."ASSOC_StandardProduct.PricingScheme" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_StandardProduct.FinancialStandardProductID" as "ASSOC_StandardProduct.FinancialStandardProductID",
            "OLD_ASSOC_StandardProduct.IDSystem" as "ASSOC_StandardProduct.IDSystem",
            "OLD_ASSOC_StandardProduct.PricingScheme" as "ASSOC_StandardProduct.PricingScheme",
            "OLD_ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" as "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_CancellationOptionType" as "CancellationOptionType",
            "OLD_NoticePeriodLength" as "NoticePeriodLength",
            "OLD_NoticePeriodTimeUnit" as "NoticePeriodTimeUnit",
            "OLD_OpportunityCostCompensation" as "OpportunityCostCompensation",
            "OLD_OptionHolder" as "OptionHolder",
            "OLD_PartialCancellationAllowed" as "PartialCancellationAllowed",
            "OLD_PeriodUntilExercisePeriodEnd" as "PeriodUntilExercisePeriodEnd",
            "OLD_PeriodUntilExercisePeriodStart" as "PeriodUntilExercisePeriodStart",
            "OLD_StandardCancellationOptionCategory" as "StandardCancellationOptionCategory",
            "OLD_TimeUnit" as "TimeUnit",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
                        "OLD"."ASSOC_StandardProduct.IDSystem",
                        "OLD"."ASSOC_StandardProduct.FinancialStandardProductID",
                        "OLD"."ASSOC_StandardProduct.PricingScheme",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_StandardProduct.FinancialStandardProductID" AS "OLD_ASSOC_StandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardProduct.IDSystem" AS "OLD_ASSOC_StandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardProduct.PricingScheme" AS "OLD_ASSOC_StandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" AS "OLD_ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."CancellationOptionType" AS "OLD_CancellationOptionType" ,
                "OLD"."NoticePeriodLength" AS "OLD_NoticePeriodLength" ,
                "OLD"."NoticePeriodTimeUnit" AS "OLD_NoticePeriodTimeUnit" ,
                "OLD"."OpportunityCostCompensation" AS "OLD_OpportunityCostCompensation" ,
                "OLD"."OptionHolder" AS "OLD_OptionHolder" ,
                "OLD"."PartialCancellationAllowed" AS "OLD_PartialCancellationAllowed" ,
                "OLD"."PeriodUntilExercisePeriodEnd" AS "OLD_PeriodUntilExercisePeriodEnd" ,
                "OLD"."PeriodUntilExercisePeriodStart" AS "OLD_PeriodUntilExercisePeriodStart" ,
                "OLD"."StandardCancellationOptionCategory" AS "OLD_StandardCancellationOptionCategory" ,
                "OLD"."TimeUnit" AS "OLD_TimeUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::StandardCancellationOption" as "OLD"
            on
               "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" and
               "IN"."ASSOC_StandardProduct.IDSystem" = "OLD"."ASSOC_StandardProduct.IDSystem" and
               "IN"."ASSOC_StandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardProduct.FinancialStandardProductID" and
               "IN"."ASSOC_StandardProduct.PricingScheme" = "OLD"."ASSOC_StandardProduct.PricingScheme" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
