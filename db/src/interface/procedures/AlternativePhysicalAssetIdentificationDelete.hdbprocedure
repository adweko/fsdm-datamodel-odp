PROCEDURE "sap.fsdm.procedures::AlternativePhysicalAssetIdentificationDelete" (IN ROW "sap.fsdm.tabletypes::AlternativePhysicalAssetIdentificationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'IDSystem=' || TO_VARCHAR("IDSystem") || ' ' ||
                'ASSOC_PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("ASSOC_PhysicalAsset.PhysicalAssetID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."IDSystem",
                        "IN"."ASSOC_PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."IDSystem",
                        "IN"."ASSOC_PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "IDSystem",
                        "ASSOC_PhysicalAsset.PhysicalAssetID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."IDSystem",
                                    "IN"."ASSOC_PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."IDSystem",
                                    "IN"."ASSOC_PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "IDSystem" is null and
            "ASSOC_PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::AlternativePhysicalAssetIdentification" (
        "IDSystem",
        "ASSOC_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PhysicalAssetIdentifier",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_IDSystem" as "IDSystem" ,
            "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" as "ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_PhysicalAssetIdentifier" as "PhysicalAssetIdentifier" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."IDSystem",
                        "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."IDSystem" AS "OLD_IDSystem" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PhysicalAssetIdentifier" AS "OLD_PhysicalAssetIdentifier" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::AlternativePhysicalAssetIdentification" as "OLD"
            on
                      "IN"."IDSystem" = "OLD"."IDSystem" and
                      "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::AlternativePhysicalAssetIdentification" (
        "IDSystem",
        "ASSOC_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PhysicalAssetIdentifier",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_IDSystem" as "IDSystem",
            "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" as "ASSOC_PhysicalAsset.PhysicalAssetID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_PhysicalAssetIdentifier" as "PhysicalAssetIdentifier",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."IDSystem",
                        "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."IDSystem" AS "OLD_IDSystem" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD_ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PhysicalAssetIdentifier" AS "OLD_PhysicalAssetIdentifier" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::AlternativePhysicalAssetIdentification" as "OLD"
            on
                                                "IN"."IDSystem" = "OLD"."IDSystem" and
                                                "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::AlternativePhysicalAssetIdentification"
    where (
        "IDSystem",
        "ASSOC_PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."IDSystem",
            "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::AlternativePhysicalAssetIdentification" as "OLD"
        on
                                       "IN"."IDSystem" = "OLD"."IDSystem" and
                                       "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
