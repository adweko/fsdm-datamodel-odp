PROCEDURE "sap.fsdm.procedures::OrganizationalUnitLoad" (IN ROW "sap.fsdm.tabletypes::OrganizationalUnitTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'IDSystem=' || TO_VARCHAR("IDSystem") || ' ' ||
                'OrganizationalUnitID=' || TO_VARCHAR("OrganizationalUnitID") || ' ' ||
                'ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID=' || TO_VARCHAR("ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."IDSystem",
                        "IN"."OrganizationalUnitID",
                        "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."IDSystem",
                        "IN"."OrganizationalUnitID",
                        "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "IDSystem",
                        "OrganizationalUnitID",
                        "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."IDSystem",
                                    "IN"."OrganizationalUnitID",
                                    "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."IDSystem",
                                    "IN"."OrganizationalUnitID",
                                    "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::OrganizationalUnit" (
        "IDSystem",
        "OrganizationalUnitID",
        "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_CompanyCode.CompanyCode",
        "_CompanyCode.ASSOC_Company.BusinessPartnerID",
        "OrganisationalUnitType",
        "OrganizationalUnitCategory",
        "OrganizationalUnitName",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_IDSystem" as "IDSystem" ,
            "OLD_OrganizationalUnitID" as "OrganizationalUnitID" ,
            "OLD_ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__CompanyCode.CompanyCode" as "_CompanyCode.CompanyCode" ,
            "OLD__CompanyCode.ASSOC_Company.BusinessPartnerID" as "_CompanyCode.ASSOC_Company.BusinessPartnerID" ,
            "OLD_OrganisationalUnitType" as "OrganisationalUnitType" ,
            "OLD_OrganizationalUnitCategory" as "OrganizationalUnitCategory" ,
            "OLD_OrganizationalUnitName" as "OrganizationalUnitName" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."IDSystem",
                        "IN"."OrganizationalUnitID",
                        "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."IDSystem" as "OLD_IDSystem",
                                "OLD"."OrganizationalUnitID" as "OLD_OrganizationalUnitID",
                                "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "OLD_ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."_CompanyCode.CompanyCode" as "OLD__CompanyCode.CompanyCode",
                                "OLD"."_CompanyCode.ASSOC_Company.BusinessPartnerID" as "OLD__CompanyCode.ASSOC_Company.BusinessPartnerID",
                                "OLD"."OrganisationalUnitType" as "OLD_OrganisationalUnitType",
                                "OLD"."OrganizationalUnitCategory" as "OLD_OrganizationalUnitCategory",
                                "OLD"."OrganizationalUnitName" as "OLD_OrganizationalUnitName",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::OrganizationalUnit" as "OLD"
            on
                ifnull( "IN"."IDSystem", '') = "OLD"."IDSystem" and
                ifnull( "IN"."OrganizationalUnitID", '') = "OLD"."OrganizationalUnitID" and
                ifnull( "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID", '') = "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::OrganizationalUnit" (
        "IDSystem",
        "OrganizationalUnitID",
        "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_CompanyCode.CompanyCode",
        "_CompanyCode.ASSOC_Company.BusinessPartnerID",
        "OrganisationalUnitType",
        "OrganizationalUnitCategory",
        "OrganizationalUnitName",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_IDSystem"  as "IDSystem",
            "OLD_OrganizationalUnitID"  as "OrganizationalUnitID",
            "OLD_ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"  as "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD__CompanyCode.CompanyCode"  as "_CompanyCode.CompanyCode",
            "OLD__CompanyCode.ASSOC_Company.BusinessPartnerID"  as "_CompanyCode.ASSOC_Company.BusinessPartnerID",
            "OLD_OrganisationalUnitType"  as "OrganisationalUnitType",
            "OLD_OrganizationalUnitCategory"  as "OrganizationalUnitCategory",
            "OLD_OrganizationalUnitName"  as "OrganizationalUnitName",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."IDSystem",
                        "IN"."OrganizationalUnitID",
                        "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."IDSystem" as "OLD_IDSystem",
                        "OLD"."OrganizationalUnitID" as "OLD_OrganizationalUnitID",
                        "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "OLD_ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."_CompanyCode.CompanyCode" as "OLD__CompanyCode.CompanyCode",
                        "OLD"."_CompanyCode.ASSOC_Company.BusinessPartnerID" as "OLD__CompanyCode.ASSOC_Company.BusinessPartnerID",
                        "OLD"."OrganisationalUnitType" as "OLD_OrganisationalUnitType",
                        "OLD"."OrganizationalUnitCategory" as "OLD_OrganizationalUnitCategory",
                        "OLD"."OrganizationalUnitName" as "OLD_OrganizationalUnitName",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::OrganizationalUnit" as "OLD"
            on
                ifnull( "IN"."IDSystem", '' ) = "OLD"."IDSystem" and
                ifnull( "IN"."OrganizationalUnitID", '' ) = "OLD"."OrganizationalUnitID" and
                ifnull( "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID", '' ) = "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::OrganizationalUnit"
    where (
        "IDSystem",
        "OrganizationalUnitID",
        "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."IDSystem",
            "OLD"."OrganizationalUnitID",
            "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::OrganizationalUnit" as "OLD"
        on
           ifnull( "IN"."IDSystem", '' ) = "OLD"."IDSystem" and
           ifnull( "IN"."OrganizationalUnitID", '' ) = "OLD"."OrganizationalUnitID" and
           ifnull( "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID", '' ) = "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::OrganizationalUnit" (
        "IDSystem",
        "OrganizationalUnitID",
        "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_CompanyCode.CompanyCode",
        "_CompanyCode.ASSOC_Company.BusinessPartnerID",
        "OrganisationalUnitType",
        "OrganizationalUnitCategory",
        "OrganizationalUnitName",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "IDSystem", '' ) as "IDSystem",
            ifnull( "OrganizationalUnitID", '' ) as "OrganizationalUnitID",
            ifnull( "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID", '' ) as "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "_CompanyCode.CompanyCode"  ,
            "_CompanyCode.ASSOC_Company.BusinessPartnerID"  ,
            "OrganisationalUnitType"  ,
            "OrganizationalUnitCategory"  ,
            "OrganizationalUnitName"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END