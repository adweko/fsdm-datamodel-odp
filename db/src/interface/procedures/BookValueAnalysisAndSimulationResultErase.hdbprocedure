PROCEDURE "sap.fsdm.procedures::BookValueAnalysisAndSimulationResultErase" (IN ROW "sap.fsdm.tabletypes::BookValueAnalysisAndSimulationResultTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "BookValueAnalysisSplitPartType" is null and
            "BookValueAnalysisType" is null and
            "RiskProvisionScenario" is null and
            "RoleOfCurrency" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_DynamicTimeBucket.MaturityBandID" is null and
            "_DynamicTimeBucket.TimeBucketID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" is null and
            "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" is null and
            "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_RiskReportingNode.RiskReportingNodeID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null and
            "_TimeBucket.MaturityBandID" is null and
            "_TimeBucket.TimeBucketID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::BookValueAnalysisAndSimulationResult"
        WHERE
        (            "BookValueAnalysisSplitPartType" ,
            "BookValueAnalysisType" ,
            "RiskProvisionScenario" ,
            "RoleOfCurrency" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_DynamicTimeBucket.MaturityBandID" ,
            "_DynamicTimeBucket.TimeBucketID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
            "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "_TimeBucket.MaturityBandID" ,
            "_TimeBucket.TimeBucketID" 
        ) in
        (
            select                 "OLD"."BookValueAnalysisSplitPartType" ,
                "OLD"."BookValueAnalysisType" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_DynamicTimeBucket.MaturityBandID" ,
                "OLD"."_DynamicTimeBucket.TimeBucketID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
                "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."_TimeBucket.MaturityBandID" ,
                "OLD"."_TimeBucket.TimeBucketID" 
            from :ROW "IN"
            inner join "sap.fsdm::BookValueAnalysisAndSimulationResult" "OLD"
            on
            "IN"."BookValueAnalysisSplitPartType" = "OLD"."BookValueAnalysisSplitPartType" and
            "IN"."BookValueAnalysisType" = "OLD"."BookValueAnalysisType" and
            "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
            "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
            "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
            "IN"."_DynamicTimeBucket.MaturityBandID" = "OLD"."_DynamicTimeBucket.MaturityBandID" and
            "IN"."_DynamicTimeBucket.TimeBucketID" = "OLD"."_DynamicTimeBucket.TimeBucketID" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
            "IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" = "OLD"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
            "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
            "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
            "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
            "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
            "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
            "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
            "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
            "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
            "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
        );

        --delete data from history table
        delete from "sap.fsdm::BookValueAnalysisAndSimulationResult_Historical"
        WHERE
        (
            "BookValueAnalysisSplitPartType" ,
            "BookValueAnalysisType" ,
            "RiskProvisionScenario" ,
            "RoleOfCurrency" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_DynamicTimeBucket.MaturityBandID" ,
            "_DynamicTimeBucket.TimeBucketID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
            "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "_TimeBucket.MaturityBandID" ,
            "_TimeBucket.TimeBucketID" 
        ) in
        (
            select
                "OLD"."BookValueAnalysisSplitPartType" ,
                "OLD"."BookValueAnalysisType" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_DynamicTimeBucket.MaturityBandID" ,
                "OLD"."_DynamicTimeBucket.TimeBucketID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
                "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."_TimeBucket.MaturityBandID" ,
                "OLD"."_TimeBucket.TimeBucketID" 
            from :ROW "IN"
            inner join "sap.fsdm::BookValueAnalysisAndSimulationResult_Historical" "OLD"
            on
                "IN"."BookValueAnalysisSplitPartType" = "OLD"."BookValueAnalysisSplitPartType" and
                "IN"."BookValueAnalysisType" = "OLD"."BookValueAnalysisType" and
                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_DynamicTimeBucket.MaturityBandID" = "OLD"."_DynamicTimeBucket.MaturityBandID" and
                "IN"."_DynamicTimeBucket.TimeBucketID" = "OLD"."_DynamicTimeBucket.TimeBucketID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" = "OLD"."_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
                "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
                "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
                "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
        );

END
