PROCEDURE "sap.fsdm.procedures::CovenantBoundaryCheckEventDelReadOnly" (IN ROW "sap.fsdm.tabletypes::CovenantBoundaryCheckEventTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::CovenantBoundaryCheckEventTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CovenantBoundaryCheckEventTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CovenantBoundaryValuationDate=' || TO_VARCHAR("CovenantBoundaryValuationDate") || ' ' ||
                '_FinancialCovenant.SequenceNumber=' || TO_VARCHAR("_FinancialCovenant.SequenceNumber") || ' ' ||
                '_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialCovenant.ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialCovenant.ASSOC_FinancialContract.IDSystem") || ' ' ||
                '_FinancialCovenant._BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_FinancialCovenant._BusinessPartner.BusinessPartnerID") || ' ' ||
                '_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID") || ' ' ||
                '_FinancialCovenant._ContractBundle.ContractBundleID=' || TO_VARCHAR("_FinancialCovenant._ContractBundle.ContractBundleID") || ' ' ||
                '_FinancialCovenant._PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_FinancialCovenant._PhysicalAsset.PhysicalAssetID") || ' ' ||
                '_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber=' || TO_VARCHAR("_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber") || ' ' ||
                '_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID=' || TO_VARCHAR("_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID") || ' ' ||
                '_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem=' || TO_VARCHAR("_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CovenantBoundaryValuationDate",
                        "IN"."_FinancialCovenant.SequenceNumber",
                        "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                        "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                        "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                        "IN"."_FinancialCovenant._ContractBundle.ContractBundleID",
                        "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                        "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CovenantBoundaryValuationDate",
                        "IN"."_FinancialCovenant.SequenceNumber",
                        "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                        "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                        "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                        "IN"."_FinancialCovenant._ContractBundle.ContractBundleID",
                        "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                        "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CovenantBoundaryValuationDate",
                        "_FinancialCovenant.SequenceNumber",
                        "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                        "_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                        "_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                        "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                        "_FinancialCovenant._ContractBundle.ContractBundleID",
                        "_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                        "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                        "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CovenantBoundaryValuationDate",
                                    "IN"."_FinancialCovenant.SequenceNumber",
                                    "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                                    "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                                    "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                                    "IN"."_FinancialCovenant._ContractBundle.ContractBundleID",
                                    "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                                    "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CovenantBoundaryValuationDate",
                                    "IN"."_FinancialCovenant.SequenceNumber",
                                    "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                                    "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                                    "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                                    "IN"."_FinancialCovenant._ContractBundle.ContractBundleID",
                                    "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                                    "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "CovenantBoundaryValuationDate" is null and
            "_FinancialCovenant.SequenceNumber" is null and
            "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" is null and
            "_FinancialCovenant.ASSOC_FinancialContract.IDSystem" is null and
            "_FinancialCovenant._BusinessPartner.BusinessPartnerID" is null and
            "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" is null and
            "_FinancialCovenant._ContractBundle.ContractBundleID" is null and
            "_FinancialCovenant._PhysicalAsset.PhysicalAssetID" is null and
            "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" is null and
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" is null and
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "CovenantBoundaryValuationDate",
            "_FinancialCovenant.SequenceNumber",
            "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
            "_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
            "_FinancialCovenant._BusinessPartner.BusinessPartnerID",
            "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
            "_FinancialCovenant._ContractBundle.ContractBundleID",
            "_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
            "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::CovenantBoundaryCheckEvent" WHERE
            (
            "CovenantBoundaryValuationDate" ,
            "_FinancialCovenant.SequenceNumber" ,
            "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" ,
            "_FinancialCovenant.ASSOC_FinancialContract.IDSystem" ,
            "_FinancialCovenant._BusinessPartner.BusinessPartnerID" ,
            "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" ,
            "_FinancialCovenant._ContractBundle.ContractBundleID" ,
            "_FinancialCovenant._PhysicalAsset.PhysicalAssetID" ,
            "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" ,
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."CovenantBoundaryValuationDate",
            "OLD"."_FinancialCovenant.SequenceNumber",
            "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
            "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
            "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
            "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
            "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID",
            "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
            "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
            "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CovenantBoundaryCheckEvent" as "OLD"
        on
                              "IN"."CovenantBoundaryValuationDate" = "OLD"."CovenantBoundaryValuationDate" and
                              "IN"."_FinancialCovenant.SequenceNumber" = "OLD"."_FinancialCovenant.SequenceNumber" and
                              "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" and
                              "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" and
                              "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" = "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" and
                              "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" = "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" and
                              "IN"."_FinancialCovenant._ContractBundle.ContractBundleID" = "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID" and
                              "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" = "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" and
                              "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" and
                              "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                              "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "CovenantBoundaryValuationDate",
        "_FinancialCovenant.SequenceNumber",
        "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
        "_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
        "_FinancialCovenant._BusinessPartner.BusinessPartnerID",
        "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
        "_FinancialCovenant._ContractBundle.ContractBundleID",
        "_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
        "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
        "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
        "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_BankingChannel.BankingChannelID",
        "_BusinessEventDataOwner.BusinessPartnerID",
        "CriterionUnit",
        "CriterionValueCount",
        "CriterionValueDecimal",
        "CriterionValueNonNumerical",
        "CriterionValuePercentage",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_CovenantBoundaryValuationDate" as "CovenantBoundaryValuationDate" ,
            "OLD__FinancialCovenant.SequenceNumber" as "_FinancialCovenant.SequenceNumber" ,
            "OLD__FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" as "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" ,
            "OLD__FinancialCovenant.ASSOC_FinancialContract.IDSystem" as "_FinancialCovenant.ASSOC_FinancialContract.IDSystem" ,
            "OLD__FinancialCovenant._BusinessPartner.BusinessPartnerID" as "_FinancialCovenant._BusinessPartner.BusinessPartnerID" ,
            "OLD__FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" as "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" ,
            "OLD__FinancialCovenant._ContractBundle.ContractBundleID" as "_FinancialCovenant._ContractBundle.ContractBundleID" ,
            "OLD__FinancialCovenant._PhysicalAsset.PhysicalAssetID" as "_FinancialCovenant._PhysicalAsset.PhysicalAssetID" ,
            "OLD__FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" as "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" ,
            "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" as "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__BankingChannel.BankingChannelID" as "_BankingChannel.BankingChannelID" ,
            "OLD__BusinessEventDataOwner.BusinessPartnerID" as "_BusinessEventDataOwner.BusinessPartnerID" ,
            "OLD_CriterionUnit" as "CriterionUnit" ,
            "OLD_CriterionValueCount" as "CriterionValueCount" ,
            "OLD_CriterionValueDecimal" as "CriterionValueDecimal" ,
            "OLD_CriterionValueNonNumerical" as "CriterionValueNonNumerical" ,
            "OLD_CriterionValuePercentage" as "CriterionValuePercentage" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."CovenantBoundaryValuationDate",
                        "OLD"."_FinancialCovenant.SequenceNumber",
                        "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                        "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                        "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                        "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID",
                        "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                        "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                        "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."CovenantBoundaryValuationDate" AS "OLD_CovenantBoundaryValuationDate" ,
                "OLD"."_FinancialCovenant.SequenceNumber" AS "OLD__FinancialCovenant.SequenceNumber" ,
                "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" AS "OLD__FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" AS "OLD__FinancialCovenant.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" AS "OLD__FinancialCovenant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" AS "OLD__FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID" AS "OLD__FinancialCovenant._ContractBundle.ContractBundleID" ,
                "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" AS "OLD__FinancialCovenant._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" AS "OLD__FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" AS "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" AS "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_BankingChannel.BankingChannelID" AS "OLD__BankingChannel.BankingChannelID" ,
                "OLD"."_BusinessEventDataOwner.BusinessPartnerID" AS "OLD__BusinessEventDataOwner.BusinessPartnerID" ,
                "OLD"."CriterionUnit" AS "OLD_CriterionUnit" ,
                "OLD"."CriterionValueCount" AS "OLD_CriterionValueCount" ,
                "OLD"."CriterionValueDecimal" AS "OLD_CriterionValueDecimal" ,
                "OLD"."CriterionValueNonNumerical" AS "OLD_CriterionValueNonNumerical" ,
                "OLD"."CriterionValuePercentage" AS "OLD_CriterionValuePercentage" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CovenantBoundaryCheckEvent" as "OLD"
            on
                                      "IN"."CovenantBoundaryValuationDate" = "OLD"."CovenantBoundaryValuationDate" and
                                      "IN"."_FinancialCovenant.SequenceNumber" = "OLD"."_FinancialCovenant.SequenceNumber" and
                                      "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" and
                                      "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" and
                                      "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" = "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" and
                                      "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" = "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" and
                                      "IN"."_FinancialCovenant._ContractBundle.ContractBundleID" = "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID" and
                                      "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" = "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" and
                                      "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" and
                                      "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                                      "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_CovenantBoundaryValuationDate" as "CovenantBoundaryValuationDate",
            "OLD__FinancialCovenant.SequenceNumber" as "_FinancialCovenant.SequenceNumber",
            "OLD__FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" as "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
            "OLD__FinancialCovenant.ASSOC_FinancialContract.IDSystem" as "_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
            "OLD__FinancialCovenant._BusinessPartner.BusinessPartnerID" as "_FinancialCovenant._BusinessPartner.BusinessPartnerID",
            "OLD__FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" as "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
            "OLD__FinancialCovenant._ContractBundle.ContractBundleID" as "_FinancialCovenant._ContractBundle.ContractBundleID",
            "OLD__FinancialCovenant._PhysicalAsset.PhysicalAssetID" as "_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
            "OLD__FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" as "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
            "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" as "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__BankingChannel.BankingChannelID" as "_BankingChannel.BankingChannelID",
            "OLD__BusinessEventDataOwner.BusinessPartnerID" as "_BusinessEventDataOwner.BusinessPartnerID",
            "OLD_CriterionUnit" as "CriterionUnit",
            "OLD_CriterionValueCount" as "CriterionValueCount",
            "OLD_CriterionValueDecimal" as "CriterionValueDecimal",
            "OLD_CriterionValueNonNumerical" as "CriterionValueNonNumerical",
            "OLD_CriterionValuePercentage" as "CriterionValuePercentage",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."CovenantBoundaryValuationDate",
                        "OLD"."_FinancialCovenant.SequenceNumber",
                        "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                        "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                        "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                        "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID",
                        "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                        "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                        "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."CovenantBoundaryValuationDate" AS "OLD_CovenantBoundaryValuationDate" ,
                "OLD"."_FinancialCovenant.SequenceNumber" AS "OLD__FinancialCovenant.SequenceNumber" ,
                "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" AS "OLD__FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" AS "OLD__FinancialCovenant.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" AS "OLD__FinancialCovenant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" AS "OLD__FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID" AS "OLD__FinancialCovenant._ContractBundle.ContractBundleID" ,
                "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" AS "OLD__FinancialCovenant._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" AS "OLD__FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" AS "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" AS "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_BankingChannel.BankingChannelID" AS "OLD__BankingChannel.BankingChannelID" ,
                "OLD"."_BusinessEventDataOwner.BusinessPartnerID" AS "OLD__BusinessEventDataOwner.BusinessPartnerID" ,
                "OLD"."CriterionUnit" AS "OLD_CriterionUnit" ,
                "OLD"."CriterionValueCount" AS "OLD_CriterionValueCount" ,
                "OLD"."CriterionValueDecimal" AS "OLD_CriterionValueDecimal" ,
                "OLD"."CriterionValueNonNumerical" AS "OLD_CriterionValueNonNumerical" ,
                "OLD"."CriterionValuePercentage" AS "OLD_CriterionValuePercentage" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CovenantBoundaryCheckEvent" as "OLD"
            on
               "IN"."CovenantBoundaryValuationDate" = "OLD"."CovenantBoundaryValuationDate" and
               "IN"."_FinancialCovenant.SequenceNumber" = "OLD"."_FinancialCovenant.SequenceNumber" and
               "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" and
               "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" and
               "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" = "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" and
               "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" = "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" and
               "IN"."_FinancialCovenant._ContractBundle.ContractBundleID" = "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID" and
               "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" = "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" and
               "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" and
               "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
               "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
