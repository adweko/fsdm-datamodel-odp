PROCEDURE "sap.fsdm.procedures::ForbearanceMeasureLoad" (IN ROW "sap.fsdm.tabletypes::ForbearanceMeasureTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ForbearanceCategory=' || TO_VARCHAR("ForbearanceCategory") || ' ' ||
                'MeasureInForceStartDate=' || TO_VARCHAR("MeasureInForceStartDate") || ' ' ||
                '_ContractStatus.ContractStatusCategory=' || TO_VARCHAR("_ContractStatus.ContractStatusCategory") || ' ' ||
                '_ContractStatus.ContractStatusType=' || TO_VARCHAR("_ContractStatus.ContractStatusType") || ' ' ||
                '_ContractStatus.ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("_ContractStatus.ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                '_ContractStatus.ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("_ContractStatus.ASSOC_FinancialContract.IDSystem") || ' ' ||
                '_InstrumentStatus.InstrumentStatusCategory=' || TO_VARCHAR("_InstrumentStatus.InstrumentStatusCategory") || ' ' ||
                '_InstrumentStatus.InstrumentStatusType=' || TO_VARCHAR("_InstrumentStatus.InstrumentStatusType") || ' ' ||
                '_InstrumentStatus._Instrument.FinancialInstrumentID=' || TO_VARCHAR("_InstrumentStatus._Instrument.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ForbearanceCategory",
                        "IN"."MeasureInForceStartDate",
                        "IN"."_ContractStatus.ContractStatusCategory",
                        "IN"."_ContractStatus.ContractStatusType",
                        "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
                        "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem",
                        "IN"."_InstrumentStatus.InstrumentStatusCategory",
                        "IN"."_InstrumentStatus.InstrumentStatusType",
                        "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ForbearanceCategory",
                        "IN"."MeasureInForceStartDate",
                        "IN"."_ContractStatus.ContractStatusCategory",
                        "IN"."_ContractStatus.ContractStatusType",
                        "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
                        "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem",
                        "IN"."_InstrumentStatus.InstrumentStatusCategory",
                        "IN"."_InstrumentStatus.InstrumentStatusType",
                        "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ForbearanceCategory",
                        "MeasureInForceStartDate",
                        "_ContractStatus.ContractStatusCategory",
                        "_ContractStatus.ContractStatusType",
                        "_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
                        "_ContractStatus.ASSOC_FinancialContract.IDSystem",
                        "_InstrumentStatus.InstrumentStatusCategory",
                        "_InstrumentStatus.InstrumentStatusType",
                        "_InstrumentStatus._Instrument.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ForbearanceCategory",
                                    "IN"."MeasureInForceStartDate",
                                    "IN"."_ContractStatus.ContractStatusCategory",
                                    "IN"."_ContractStatus.ContractStatusType",
                                    "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem",
                                    "IN"."_InstrumentStatus.InstrumentStatusCategory",
                                    "IN"."_InstrumentStatus.InstrumentStatusType",
                                    "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ForbearanceCategory",
                                    "IN"."MeasureInForceStartDate",
                                    "IN"."_ContractStatus.ContractStatusCategory",
                                    "IN"."_ContractStatus.ContractStatusType",
                                    "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem",
                                    "IN"."_InstrumentStatus.InstrumentStatusCategory",
                                    "IN"."_InstrumentStatus.InstrumentStatusType",
                                    "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::ForbearanceMeasure" (
        "ForbearanceCategory",
        "MeasureInForceStartDate",
        "_ContractStatus.ContractStatusCategory",
        "_ContractStatus.ContractStatusType",
        "_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
        "_ContractStatus.ASSOC_FinancialContract.IDSystem",
        "_InstrumentStatus.InstrumentStatusCategory",
        "_InstrumentStatus.InstrumentStatusType",
        "_InstrumentStatus._Instrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "FinancialDifficulty",
        "ForbearanceType",
        "InterestBelowMarketRate",
        "LifecycleStatus",
        "LifecycleStatusChangeDate",
        "LifecycleStatusReason",
        "LongTermMeasure",
        "MainMeasureReporting",
        "MainMeasureReportingDate",
        "MeasureGroupID",
        "MeasureInForceEndDate",
        "MeasureIsSustainable",
        "MeasureWorking",
        "MeasureWorkingDate",
        "MeasureWorkingDetails",
        "PresentValueEffect",
        "RegularPaymentStartDate",
        "SpecialTreatmentEndDate",
        "SpecialTreatmentReason",
        "SpecialTreatmentStartDate",
        "SubstantialContribution",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ForbearanceCategory" as "ForbearanceCategory" ,
            "OLD_MeasureInForceStartDate" as "MeasureInForceStartDate" ,
            "OLD__ContractStatus.ContractStatusCategory" as "_ContractStatus.ContractStatusCategory" ,
            "OLD__ContractStatus.ContractStatusType" as "_ContractStatus.ContractStatusType" ,
            "OLD__ContractStatus.ASSOC_FinancialContract.FinancialContractID" as "_ContractStatus.ASSOC_FinancialContract.FinancialContractID" ,
            "OLD__ContractStatus.ASSOC_FinancialContract.IDSystem" as "_ContractStatus.ASSOC_FinancialContract.IDSystem" ,
            "OLD__InstrumentStatus.InstrumentStatusCategory" as "_InstrumentStatus.InstrumentStatusCategory" ,
            "OLD__InstrumentStatus.InstrumentStatusType" as "_InstrumentStatus.InstrumentStatusType" ,
            "OLD__InstrumentStatus._Instrument.FinancialInstrumentID" as "_InstrumentStatus._Instrument.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_FinancialDifficulty" as "FinancialDifficulty" ,
            "OLD_ForbearanceType" as "ForbearanceType" ,
            "OLD_InterestBelowMarketRate" as "InterestBelowMarketRate" ,
            "OLD_LifecycleStatus" as "LifecycleStatus" ,
            "OLD_LifecycleStatusChangeDate" as "LifecycleStatusChangeDate" ,
            "OLD_LifecycleStatusReason" as "LifecycleStatusReason" ,
            "OLD_LongTermMeasure" as "LongTermMeasure" ,
            "OLD_MainMeasureReporting" as "MainMeasureReporting" ,
            "OLD_MainMeasureReportingDate" as "MainMeasureReportingDate" ,
            "OLD_MeasureGroupID" as "MeasureGroupID" ,
            "OLD_MeasureInForceEndDate" as "MeasureInForceEndDate" ,
            "OLD_MeasureIsSustainable" as "MeasureIsSustainable" ,
            "OLD_MeasureWorking" as "MeasureWorking" ,
            "OLD_MeasureWorkingDate" as "MeasureWorkingDate" ,
            "OLD_MeasureWorkingDetails" as "MeasureWorkingDetails" ,
            "OLD_PresentValueEffect" as "PresentValueEffect" ,
            "OLD_RegularPaymentStartDate" as "RegularPaymentStartDate" ,
            "OLD_SpecialTreatmentEndDate" as "SpecialTreatmentEndDate" ,
            "OLD_SpecialTreatmentReason" as "SpecialTreatmentReason" ,
            "OLD_SpecialTreatmentStartDate" as "SpecialTreatmentStartDate" ,
            "OLD_SubstantialContribution" as "SubstantialContribution" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."ForbearanceCategory",
                        "IN"."MeasureInForceStartDate",
                        "IN"."_ContractStatus.ContractStatusCategory",
                        "IN"."_ContractStatus.ContractStatusType",
                        "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
                        "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem",
                        "IN"."_InstrumentStatus.InstrumentStatusCategory",
                        "IN"."_InstrumentStatus.InstrumentStatusType",
                        "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."ForbearanceCategory" as "OLD_ForbearanceCategory",
                                "OLD"."MeasureInForceStartDate" as "OLD_MeasureInForceStartDate",
                                "OLD"."_ContractStatus.ContractStatusCategory" as "OLD__ContractStatus.ContractStatusCategory",
                                "OLD"."_ContractStatus.ContractStatusType" as "OLD__ContractStatus.ContractStatusType",
                                "OLD"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" as "OLD__ContractStatus.ASSOC_FinancialContract.FinancialContractID",
                                "OLD"."_ContractStatus.ASSOC_FinancialContract.IDSystem" as "OLD__ContractStatus.ASSOC_FinancialContract.IDSystem",
                                "OLD"."_InstrumentStatus.InstrumentStatusCategory" as "OLD__InstrumentStatus.InstrumentStatusCategory",
                                "OLD"."_InstrumentStatus.InstrumentStatusType" as "OLD__InstrumentStatus.InstrumentStatusType",
                                "OLD"."_InstrumentStatus._Instrument.FinancialInstrumentID" as "OLD__InstrumentStatus._Instrument.FinancialInstrumentID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."FinancialDifficulty" as "OLD_FinancialDifficulty",
                                "OLD"."ForbearanceType" as "OLD_ForbearanceType",
                                "OLD"."InterestBelowMarketRate" as "OLD_InterestBelowMarketRate",
                                "OLD"."LifecycleStatus" as "OLD_LifecycleStatus",
                                "OLD"."LifecycleStatusChangeDate" as "OLD_LifecycleStatusChangeDate",
                                "OLD"."LifecycleStatusReason" as "OLD_LifecycleStatusReason",
                                "OLD"."LongTermMeasure" as "OLD_LongTermMeasure",
                                "OLD"."MainMeasureReporting" as "OLD_MainMeasureReporting",
                                "OLD"."MainMeasureReportingDate" as "OLD_MainMeasureReportingDate",
                                "OLD"."MeasureGroupID" as "OLD_MeasureGroupID",
                                "OLD"."MeasureInForceEndDate" as "OLD_MeasureInForceEndDate",
                                "OLD"."MeasureIsSustainable" as "OLD_MeasureIsSustainable",
                                "OLD"."MeasureWorking" as "OLD_MeasureWorking",
                                "OLD"."MeasureWorkingDate" as "OLD_MeasureWorkingDate",
                                "OLD"."MeasureWorkingDetails" as "OLD_MeasureWorkingDetails",
                                "OLD"."PresentValueEffect" as "OLD_PresentValueEffect",
                                "OLD"."RegularPaymentStartDate" as "OLD_RegularPaymentStartDate",
                                "OLD"."SpecialTreatmentEndDate" as "OLD_SpecialTreatmentEndDate",
                                "OLD"."SpecialTreatmentReason" as "OLD_SpecialTreatmentReason",
                                "OLD"."SpecialTreatmentStartDate" as "OLD_SpecialTreatmentStartDate",
                                "OLD"."SubstantialContribution" as "OLD_SubstantialContribution",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::ForbearanceMeasure" as "OLD"
            on
                ifnull( "IN"."ForbearanceCategory", '') = "OLD"."ForbearanceCategory" and
                ifnull( "IN"."MeasureInForceStartDate", '0001-01-01') = "OLD"."MeasureInForceStartDate" and
                ifnull( "IN"."_ContractStatus.ContractStatusCategory", '') = "OLD"."_ContractStatus.ContractStatusCategory" and
                ifnull( "IN"."_ContractStatus.ContractStatusType", '') = "OLD"."_ContractStatus.ContractStatusType" and
                ifnull( "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID", '') = "OLD"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem", '') = "OLD"."_ContractStatus.ASSOC_FinancialContract.IDSystem" and
                ifnull( "IN"."_InstrumentStatus.InstrumentStatusCategory", '') = "OLD"."_InstrumentStatus.InstrumentStatusCategory" and
                ifnull( "IN"."_InstrumentStatus.InstrumentStatusType", '') = "OLD"."_InstrumentStatus.InstrumentStatusType" and
                ifnull( "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID", '') = "OLD"."_InstrumentStatus._Instrument.FinancialInstrumentID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::ForbearanceMeasure" (
        "ForbearanceCategory",
        "MeasureInForceStartDate",
        "_ContractStatus.ContractStatusCategory",
        "_ContractStatus.ContractStatusType",
        "_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
        "_ContractStatus.ASSOC_FinancialContract.IDSystem",
        "_InstrumentStatus.InstrumentStatusCategory",
        "_InstrumentStatus.InstrumentStatusType",
        "_InstrumentStatus._Instrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "FinancialDifficulty",
        "ForbearanceType",
        "InterestBelowMarketRate",
        "LifecycleStatus",
        "LifecycleStatusChangeDate",
        "LifecycleStatusReason",
        "LongTermMeasure",
        "MainMeasureReporting",
        "MainMeasureReportingDate",
        "MeasureGroupID",
        "MeasureInForceEndDate",
        "MeasureIsSustainable",
        "MeasureWorking",
        "MeasureWorkingDate",
        "MeasureWorkingDetails",
        "PresentValueEffect",
        "RegularPaymentStartDate",
        "SpecialTreatmentEndDate",
        "SpecialTreatmentReason",
        "SpecialTreatmentStartDate",
        "SubstantialContribution",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ForbearanceCategory"  as "ForbearanceCategory",
            "OLD_MeasureInForceStartDate"  as "MeasureInForceStartDate",
            "OLD__ContractStatus.ContractStatusCategory"  as "_ContractStatus.ContractStatusCategory",
            "OLD__ContractStatus.ContractStatusType"  as "_ContractStatus.ContractStatusType",
            "OLD__ContractStatus.ASSOC_FinancialContract.FinancialContractID"  as "_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
            "OLD__ContractStatus.ASSOC_FinancialContract.IDSystem"  as "_ContractStatus.ASSOC_FinancialContract.IDSystem",
            "OLD__InstrumentStatus.InstrumentStatusCategory"  as "_InstrumentStatus.InstrumentStatusCategory",
            "OLD__InstrumentStatus.InstrumentStatusType"  as "_InstrumentStatus.InstrumentStatusType",
            "OLD__InstrumentStatus._Instrument.FinancialInstrumentID"  as "_InstrumentStatus._Instrument.FinancialInstrumentID",
            "BusinessValidTo"  as "BusinessValidFrom",
            "OLD_BusinessValidTo"  as "BusinessValidTo",
            "OLD_FinancialDifficulty"  as "FinancialDifficulty",
            "OLD_ForbearanceType"  as "ForbearanceType",
            "OLD_InterestBelowMarketRate"  as "InterestBelowMarketRate",
            "OLD_LifecycleStatus"  as "LifecycleStatus",
            "OLD_LifecycleStatusChangeDate"  as "LifecycleStatusChangeDate",
            "OLD_LifecycleStatusReason"  as "LifecycleStatusReason",
            "OLD_LongTermMeasure"  as "LongTermMeasure",
            "OLD_MainMeasureReporting"  as "MainMeasureReporting",
            "OLD_MainMeasureReportingDate"  as "MainMeasureReportingDate",
            "OLD_MeasureGroupID"  as "MeasureGroupID",
            "OLD_MeasureInForceEndDate"  as "MeasureInForceEndDate",
            "OLD_MeasureIsSustainable"  as "MeasureIsSustainable",
            "OLD_MeasureWorking"  as "MeasureWorking",
            "OLD_MeasureWorkingDate"  as "MeasureWorkingDate",
            "OLD_MeasureWorkingDetails"  as "MeasureWorkingDetails",
            "OLD_PresentValueEffect"  as "PresentValueEffect",
            "OLD_RegularPaymentStartDate"  as "RegularPaymentStartDate",
            "OLD_SpecialTreatmentEndDate"  as "SpecialTreatmentEndDate",
            "OLD_SpecialTreatmentReason"  as "SpecialTreatmentReason",
            "OLD_SpecialTreatmentStartDate"  as "SpecialTreatmentStartDate",
            "OLD_SubstantialContribution"  as "SubstantialContribution",
            "OLD_SourceSystemID"  as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem"  as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem"  as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType"  as "ChangingProcessType",
            "OLD_ChangingProcessID"  as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."ForbearanceCategory",
                        "IN"."MeasureInForceStartDate",
                        "IN"."_ContractStatus.ContractStatusCategory",
                        "IN"."_ContractStatus.ContractStatusType",
                        "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
                        "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem",
                        "IN"."_InstrumentStatus.InstrumentStatusCategory",
                        "IN"."_InstrumentStatus.InstrumentStatusType",
                        "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."ForbearanceCategory" as "OLD_ForbearanceCategory",
                        "OLD"."MeasureInForceStartDate" as "OLD_MeasureInForceStartDate",
                        "OLD"."_ContractStatus.ContractStatusCategory" as "OLD__ContractStatus.ContractStatusCategory",
                        "OLD"."_ContractStatus.ContractStatusType" as "OLD__ContractStatus.ContractStatusType",
                        "OLD"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" as "OLD__ContractStatus.ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."_ContractStatus.ASSOC_FinancialContract.IDSystem" as "OLD__ContractStatus.ASSOC_FinancialContract.IDSystem",
                        "OLD"."_InstrumentStatus.InstrumentStatusCategory" as "OLD__InstrumentStatus.InstrumentStatusCategory",
                        "OLD"."_InstrumentStatus.InstrumentStatusType" as "OLD__InstrumentStatus.InstrumentStatusType",
                        "OLD"."_InstrumentStatus._Instrument.FinancialInstrumentID" as "OLD__InstrumentStatus._Instrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."FinancialDifficulty" as "OLD_FinancialDifficulty",
                        "OLD"."ForbearanceType" as "OLD_ForbearanceType",
                        "OLD"."InterestBelowMarketRate" as "OLD_InterestBelowMarketRate",
                        "OLD"."LifecycleStatus" as "OLD_LifecycleStatus",
                        "OLD"."LifecycleStatusChangeDate" as "OLD_LifecycleStatusChangeDate",
                        "OLD"."LifecycleStatusReason" as "OLD_LifecycleStatusReason",
                        "OLD"."LongTermMeasure" as "OLD_LongTermMeasure",
                        "OLD"."MainMeasureReporting" as "OLD_MainMeasureReporting",
                        "OLD"."MainMeasureReportingDate" as "OLD_MainMeasureReportingDate",
                        "OLD"."MeasureGroupID" as "OLD_MeasureGroupID",
                        "OLD"."MeasureInForceEndDate" as "OLD_MeasureInForceEndDate",
                        "OLD"."MeasureIsSustainable" as "OLD_MeasureIsSustainable",
                        "OLD"."MeasureWorking" as "OLD_MeasureWorking",
                        "OLD"."MeasureWorkingDate" as "OLD_MeasureWorkingDate",
                        "OLD"."MeasureWorkingDetails" as "OLD_MeasureWorkingDetails",
                        "OLD"."PresentValueEffect" as "OLD_PresentValueEffect",
                        "OLD"."RegularPaymentStartDate" as "OLD_RegularPaymentStartDate",
                        "OLD"."SpecialTreatmentEndDate" as "OLD_SpecialTreatmentEndDate",
                        "OLD"."SpecialTreatmentReason" as "OLD_SpecialTreatmentReason",
                        "OLD"."SpecialTreatmentStartDate" as "OLD_SpecialTreatmentStartDate",
                        "OLD"."SubstantialContribution" as "OLD_SubstantialContribution",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::ForbearanceMeasure" as "OLD"
            on
                ifnull( "IN"."ForbearanceCategory", '' ) = "OLD"."ForbearanceCategory" and
                ifnull( "IN"."MeasureInForceStartDate", '0001-01-01' ) = "OLD"."MeasureInForceStartDate" and
                ifnull( "IN"."_ContractStatus.ContractStatusCategory", '' ) = "OLD"."_ContractStatus.ContractStatusCategory" and
                ifnull( "IN"."_ContractStatus.ContractStatusType", '' ) = "OLD"."_ContractStatus.ContractStatusType" and
                ifnull( "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID", '' ) = "OLD"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem", '' ) = "OLD"."_ContractStatus.ASSOC_FinancialContract.IDSystem" and
                ifnull( "IN"."_InstrumentStatus.InstrumentStatusCategory", '' ) = "OLD"."_InstrumentStatus.InstrumentStatusCategory" and
                ifnull( "IN"."_InstrumentStatus.InstrumentStatusType", '' ) = "OLD"."_InstrumentStatus.InstrumentStatusType" and
                ifnull( "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID", '' ) = "OLD"."_InstrumentStatus._Instrument.FinancialInstrumentID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::ForbearanceMeasure"
    where (
        "ForbearanceCategory",
        "MeasureInForceStartDate",
        "_ContractStatus.ContractStatusCategory",
        "_ContractStatus.ContractStatusType",
        "_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
        "_ContractStatus.ASSOC_FinancialContract.IDSystem",
        "_InstrumentStatus.InstrumentStatusCategory",
        "_InstrumentStatus.InstrumentStatusType",
        "_InstrumentStatus._Instrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ForbearanceCategory",
            "OLD"."MeasureInForceStartDate",
            "OLD"."_ContractStatus.ContractStatusCategory",
            "OLD"."_ContractStatus.ContractStatusType",
            "OLD"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
            "OLD"."_ContractStatus.ASSOC_FinancialContract.IDSystem",
            "OLD"."_InstrumentStatus.InstrumentStatusCategory",
            "OLD"."_InstrumentStatus.InstrumentStatusType",
            "OLD"."_InstrumentStatus._Instrument.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ForbearanceMeasure" as "OLD"
        on
           ifnull( "IN"."ForbearanceCategory", '' ) = "OLD"."ForbearanceCategory" and
           ifnull( "IN"."MeasureInForceStartDate", '0001-01-01' ) = "OLD"."MeasureInForceStartDate" and
           ifnull( "IN"."_ContractStatus.ContractStatusCategory", '' ) = "OLD"."_ContractStatus.ContractStatusCategory" and
           ifnull( "IN"."_ContractStatus.ContractStatusType", '' ) = "OLD"."_ContractStatus.ContractStatusType" and
           ifnull( "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID", '' ) = "OLD"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" and
           ifnull( "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem", '' ) = "OLD"."_ContractStatus.ASSOC_FinancialContract.IDSystem" and
           ifnull( "IN"."_InstrumentStatus.InstrumentStatusCategory", '' ) = "OLD"."_InstrumentStatus.InstrumentStatusCategory" and
           ifnull( "IN"."_InstrumentStatus.InstrumentStatusType", '' ) = "OLD"."_InstrumentStatus.InstrumentStatusType" and
           ifnull( "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID", '' ) = "OLD"."_InstrumentStatus._Instrument.FinancialInstrumentID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::ForbearanceMeasure" (
        "ForbearanceCategory",
        "MeasureInForceStartDate",
        "_ContractStatus.ContractStatusCategory",
        "_ContractStatus.ContractStatusType",
        "_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
        "_ContractStatus.ASSOC_FinancialContract.IDSystem",
        "_InstrumentStatus.InstrumentStatusCategory",
        "_InstrumentStatus.InstrumentStatusType",
        "_InstrumentStatus._Instrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "FinancialDifficulty",
        "ForbearanceType",
        "InterestBelowMarketRate",
        "LifecycleStatus",
        "LifecycleStatusChangeDate",
        "LifecycleStatusReason",
        "LongTermMeasure",
        "MainMeasureReporting",
        "MainMeasureReportingDate",
        "MeasureGroupID",
        "MeasureInForceEndDate",
        "MeasureIsSustainable",
        "MeasureWorking",
        "MeasureWorkingDate",
        "MeasureWorkingDetails",
        "PresentValueEffect",
        "RegularPaymentStartDate",
        "SpecialTreatmentEndDate",
        "SpecialTreatmentReason",
        "SpecialTreatmentStartDate",
        "SubstantialContribution",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "ForbearanceCategory", '' ) as "ForbearanceCategory",
            ifnull( "MeasureInForceStartDate", '0001-01-01' ) as "MeasureInForceStartDate",
            ifnull( "_ContractStatus.ContractStatusCategory", '' ) as "_ContractStatus.ContractStatusCategory",
            ifnull( "_ContractStatus.ContractStatusType", '' ) as "_ContractStatus.ContractStatusType",
            ifnull( "_ContractStatus.ASSOC_FinancialContract.FinancialContractID", '' ) as "_ContractStatus.ASSOC_FinancialContract.FinancialContractID",
            ifnull( "_ContractStatus.ASSOC_FinancialContract.IDSystem", '' ) as "_ContractStatus.ASSOC_FinancialContract.IDSystem",
            ifnull( "_InstrumentStatus.InstrumentStatusCategory", '' ) as "_InstrumentStatus.InstrumentStatusCategory",
            ifnull( "_InstrumentStatus.InstrumentStatusType", '' ) as "_InstrumentStatus.InstrumentStatusType",
            ifnull( "_InstrumentStatus._Instrument.FinancialInstrumentID", '' ) as "_InstrumentStatus._Instrument.FinancialInstrumentID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "FinancialDifficulty"  ,
            "ForbearanceType"  ,
            "InterestBelowMarketRate"  ,
            "LifecycleStatus"  ,
            "LifecycleStatusChangeDate"  ,
            "LifecycleStatusReason"  ,
            "LongTermMeasure"  ,
            "MainMeasureReporting"  ,
            "MainMeasureReportingDate"  ,
            "MeasureGroupID"  ,
            "MeasureInForceEndDate"  ,
            "MeasureIsSustainable"  ,
            "MeasureWorking"  ,
            "MeasureWorkingDate"  ,
            "MeasureWorkingDetails"  ,
            "PresentValueEffect"  ,
            "RegularPaymentStartDate"  ,
            "SpecialTreatmentEndDate"  ,
            "SpecialTreatmentReason"  ,
            "SpecialTreatmentStartDate"  ,
            "SubstantialContribution"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END