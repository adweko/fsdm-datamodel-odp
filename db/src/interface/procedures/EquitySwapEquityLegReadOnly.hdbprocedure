PROCEDURE "sap.fsdm.procedures::EquitySwapEquityLegReadOnly" (IN ROW "sap.fsdm.tabletypes::EquitySwapEquityLegTT", OUT CURR_DEL "sap.fsdm.tabletypes::EquitySwapEquityLegTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::EquitySwapEquityLegTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'RoleOfPayer=' || TO_VARCHAR("RoleOfPayer") || ' ' ||
                '_EquitySwap.FinancialContractID=' || TO_VARCHAR("_EquitySwap.FinancialContractID") || ' ' ||
                '_EquitySwap.IDSystem=' || TO_VARCHAR("_EquitySwap.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."RoleOfPayer",
                        "IN"."_EquitySwap.FinancialContractID",
                        "IN"."_EquitySwap.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."RoleOfPayer",
                        "IN"."_EquitySwap.FinancialContractID",
                        "IN"."_EquitySwap.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "RoleOfPayer",
                        "_EquitySwap.FinancialContractID",
                        "_EquitySwap.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."RoleOfPayer",
                                    "IN"."_EquitySwap.FinancialContractID",
                                    "IN"."_EquitySwap.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."RoleOfPayer",
                                    "IN"."_EquitySwap.FinancialContractID",
                                    "IN"."_EquitySwap.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "RoleOfPayer",
        "_EquitySwap.FinancialContractID",
        "_EquitySwap.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::EquitySwapEquityLeg" WHERE
        (            "RoleOfPayer" ,
            "_EquitySwap.FinancialContractID" ,
            "_EquitySwap.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."RoleOfPayer",
            "OLD"."_EquitySwap.FinancialContractID",
            "OLD"."_EquitySwap.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::EquitySwapEquityLeg" as "OLD"
            on
               ifnull( "IN"."RoleOfPayer",'' ) = "OLD"."RoleOfPayer" and
               ifnull( "IN"."_EquitySwap.FinancialContractID",'' ) = "OLD"."_EquitySwap.FinancialContractID" and
               ifnull( "IN"."_EquitySwap.IDSystem",'' ) = "OLD"."_EquitySwap.IDSystem" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "RoleOfPayer",
        "_EquitySwap.FinancialContractID",
        "_EquitySwap.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_UnderlyingIndex.IndexID",
        "_UnderlyingInstrument.FinancialInstrumentID",
        "EquityLegCategory",
        "IndexMultiplier",
        "IndexMultiplierCurrency",
        "UnderlyingQuantity",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "RoleOfPayer", '' ) as "RoleOfPayer",
                    ifnull( "_EquitySwap.FinancialContractID", '' ) as "_EquitySwap.FinancialContractID",
                    ifnull( "_EquitySwap.IDSystem", '' ) as "_EquitySwap.IDSystem",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "_UnderlyingIndex.IndexID"  ,
                    "_UnderlyingInstrument.FinancialInstrumentID"  ,
                    "EquityLegCategory"  ,
                    "IndexMultiplier"  ,
                    "IndexMultiplierCurrency"  ,
                    "UnderlyingQuantity"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_RoleOfPayer" as "RoleOfPayer" ,
                    "OLD__EquitySwap.FinancialContractID" as "_EquitySwap.FinancialContractID" ,
                    "OLD__EquitySwap.IDSystem" as "_EquitySwap.IDSystem" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD__UnderlyingIndex.IndexID" as "_UnderlyingIndex.IndexID" ,
                    "OLD__UnderlyingInstrument.FinancialInstrumentID" as "_UnderlyingInstrument.FinancialInstrumentID" ,
                    "OLD_EquityLegCategory" as "EquityLegCategory" ,
                    "OLD_IndexMultiplier" as "IndexMultiplier" ,
                    "OLD_IndexMultiplierCurrency" as "IndexMultiplierCurrency" ,
                    "OLD_UnderlyingQuantity" as "UnderlyingQuantity" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."RoleOfPayer",
                        "IN"."_EquitySwap.FinancialContractID",
                        "IN"."_EquitySwap.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."RoleOfPayer" as "OLD_RoleOfPayer",
                                "OLD"."_EquitySwap.FinancialContractID" as "OLD__EquitySwap.FinancialContractID",
                                "OLD"."_EquitySwap.IDSystem" as "OLD__EquitySwap.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."_UnderlyingIndex.IndexID" as "OLD__UnderlyingIndex.IndexID",
                                "OLD"."_UnderlyingInstrument.FinancialInstrumentID" as "OLD__UnderlyingInstrument.FinancialInstrumentID",
                                "OLD"."EquityLegCategory" as "OLD_EquityLegCategory",
                                "OLD"."IndexMultiplier" as "OLD_IndexMultiplier",
                                "OLD"."IndexMultiplierCurrency" as "OLD_IndexMultiplierCurrency",
                                "OLD"."UnderlyingQuantity" as "OLD_UnderlyingQuantity",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::EquitySwapEquityLeg" as "OLD"
            on
                ifnull( "IN"."RoleOfPayer", '') = "OLD"."RoleOfPayer" and
                ifnull( "IN"."_EquitySwap.FinancialContractID", '') = "OLD"."_EquitySwap.FinancialContractID" and
                ifnull( "IN"."_EquitySwap.IDSystem", '') = "OLD"."_EquitySwap.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_RoleOfPayer" as "RoleOfPayer",
            "OLD__EquitySwap.FinancialContractID" as "_EquitySwap.FinancialContractID",
            "OLD__EquitySwap.IDSystem" as "_EquitySwap.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__UnderlyingIndex.IndexID" as "_UnderlyingIndex.IndexID",
            "OLD__UnderlyingInstrument.FinancialInstrumentID" as "_UnderlyingInstrument.FinancialInstrumentID",
            "OLD_EquityLegCategory" as "EquityLegCategory",
            "OLD_IndexMultiplier" as "IndexMultiplier",
            "OLD_IndexMultiplierCurrency" as "IndexMultiplierCurrency",
            "OLD_UnderlyingQuantity" as "UnderlyingQuantity",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."RoleOfPayer",
                        "IN"."_EquitySwap.FinancialContractID",
                        "IN"."_EquitySwap.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."RoleOfPayer" as "OLD_RoleOfPayer",
                                "OLD"."_EquitySwap.FinancialContractID" as "OLD__EquitySwap.FinancialContractID",
                                "OLD"."_EquitySwap.IDSystem" as "OLD__EquitySwap.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."_UnderlyingIndex.IndexID" as "OLD__UnderlyingIndex.IndexID",
                                "OLD"."_UnderlyingInstrument.FinancialInstrumentID" as "OLD__UnderlyingInstrument.FinancialInstrumentID",
                                "OLD"."EquityLegCategory" as "OLD_EquityLegCategory",
                                "OLD"."IndexMultiplier" as "OLD_IndexMultiplier",
                                "OLD"."IndexMultiplierCurrency" as "OLD_IndexMultiplierCurrency",
                                "OLD"."UnderlyingQuantity" as "OLD_UnderlyingQuantity",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::EquitySwapEquityLeg" as "OLD"
            on
                ifnull("IN"."RoleOfPayer", '') = "OLD"."RoleOfPayer" and
                ifnull("IN"."_EquitySwap.FinancialContractID", '') = "OLD"."_EquitySwap.FinancialContractID" and
                ifnull("IN"."_EquitySwap.IDSystem", '') = "OLD"."_EquitySwap.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
