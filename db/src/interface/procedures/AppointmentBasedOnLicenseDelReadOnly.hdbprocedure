PROCEDURE "sap.fsdm.procedures::AppointmentBasedOnLicenseDelReadOnly" (IN ROW "sap.fsdm.tabletypes::AppointmentBasedOnLicenseTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::AppointmentBasedOnLicenseTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::AppointmentBasedOnLicenseTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Appointment.SequenceNumber=' || TO_VARCHAR("_Appointment.SequenceNumber") || ' ' ||
                '_Appointment._IndividualPerson.BusinessPartnerID=' || TO_VARCHAR("_Appointment._IndividualPerson.BusinessPartnerID") || ' ' ||
                '_License.Authority=' || TO_VARCHAR("_License.Authority") || ' ' ||
                '_License.IDSystem=' || TO_VARCHAR("_License.IDSystem") || ' ' ||
                '_License.SubjectOfLicenseIdentifier=' || TO_VARCHAR("_License.SubjectOfLicenseIdentifier") || ' ' ||
                '_License._BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_License._BusinessPartner.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Appointment.SequenceNumber",
                        "IN"."_Appointment._IndividualPerson.BusinessPartnerID",
                        "IN"."_License.Authority",
                        "IN"."_License.IDSystem",
                        "IN"."_License.SubjectOfLicenseIdentifier",
                        "IN"."_License._BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Appointment.SequenceNumber",
                        "IN"."_Appointment._IndividualPerson.BusinessPartnerID",
                        "IN"."_License.Authority",
                        "IN"."_License.IDSystem",
                        "IN"."_License.SubjectOfLicenseIdentifier",
                        "IN"."_License._BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Appointment.SequenceNumber",
                        "_Appointment._IndividualPerson.BusinessPartnerID",
                        "_License.Authority",
                        "_License.IDSystem",
                        "_License.SubjectOfLicenseIdentifier",
                        "_License._BusinessPartner.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Appointment.SequenceNumber",
                                    "IN"."_Appointment._IndividualPerson.BusinessPartnerID",
                                    "IN"."_License.Authority",
                                    "IN"."_License.IDSystem",
                                    "IN"."_License.SubjectOfLicenseIdentifier",
                                    "IN"."_License._BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Appointment.SequenceNumber",
                                    "IN"."_Appointment._IndividualPerson.BusinessPartnerID",
                                    "IN"."_License.Authority",
                                    "IN"."_License.IDSystem",
                                    "IN"."_License.SubjectOfLicenseIdentifier",
                                    "IN"."_License._BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Appointment.SequenceNumber" is null and
            "_Appointment._IndividualPerson.BusinessPartnerID" is null and
            "_License.Authority" is null and
            "_License.IDSystem" is null and
            "_License.SubjectOfLicenseIdentifier" is null and
            "_License._BusinessPartner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_Appointment.SequenceNumber",
            "_Appointment._IndividualPerson.BusinessPartnerID",
            "_License.Authority",
            "_License.IDSystem",
            "_License.SubjectOfLicenseIdentifier",
            "_License._BusinessPartner.BusinessPartnerID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::AppointmentBasedOnLicense" WHERE
            (
            "_Appointment.SequenceNumber" ,
            "_Appointment._IndividualPerson.BusinessPartnerID" ,
            "_License.Authority" ,
            "_License.IDSystem" ,
            "_License.SubjectOfLicenseIdentifier" ,
            "_License._BusinessPartner.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_Appointment.SequenceNumber",
            "OLD"."_Appointment._IndividualPerson.BusinessPartnerID",
            "OLD"."_License.Authority",
            "OLD"."_License.IDSystem",
            "OLD"."_License.SubjectOfLicenseIdentifier",
            "OLD"."_License._BusinessPartner.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::AppointmentBasedOnLicense" as "OLD"
        on
                              "IN"."_Appointment.SequenceNumber" = "OLD"."_Appointment.SequenceNumber" and
                              "IN"."_Appointment._IndividualPerson.BusinessPartnerID" = "OLD"."_Appointment._IndividualPerson.BusinessPartnerID" and
                              "IN"."_License.Authority" = "OLD"."_License.Authority" and
                              "IN"."_License.IDSystem" = "OLD"."_License.IDSystem" and
                              "IN"."_License.SubjectOfLicenseIdentifier" = "OLD"."_License.SubjectOfLicenseIdentifier" and
                              "IN"."_License._BusinessPartner.BusinessPartnerID" = "OLD"."_License._BusinessPartner.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_Appointment.SequenceNumber",
        "_Appointment._IndividualPerson.BusinessPartnerID",
        "_License.Authority",
        "_License.IDSystem",
        "_License.SubjectOfLicenseIdentifier",
        "_License._BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__Appointment.SequenceNumber" as "_Appointment.SequenceNumber" ,
            "OLD__Appointment._IndividualPerson.BusinessPartnerID" as "_Appointment._IndividualPerson.BusinessPartnerID" ,
            "OLD__License.Authority" as "_License.Authority" ,
            "OLD__License.IDSystem" as "_License.IDSystem" ,
            "OLD__License.SubjectOfLicenseIdentifier" as "_License.SubjectOfLicenseIdentifier" ,
            "OLD__License._BusinessPartner.BusinessPartnerID" as "_License._BusinessPartner.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_Appointment.SequenceNumber",
                        "OLD"."_Appointment._IndividualPerson.BusinessPartnerID",
                        "OLD"."_License.Authority",
                        "OLD"."_License.IDSystem",
                        "OLD"."_License.SubjectOfLicenseIdentifier",
                        "OLD"."_License._BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_Appointment.SequenceNumber" AS "OLD__Appointment.SequenceNumber" ,
                "OLD"."_Appointment._IndividualPerson.BusinessPartnerID" AS "OLD__Appointment._IndividualPerson.BusinessPartnerID" ,
                "OLD"."_License.Authority" AS "OLD__License.Authority" ,
                "OLD"."_License.IDSystem" AS "OLD__License.IDSystem" ,
                "OLD"."_License.SubjectOfLicenseIdentifier" AS "OLD__License.SubjectOfLicenseIdentifier" ,
                "OLD"."_License._BusinessPartner.BusinessPartnerID" AS "OLD__License._BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::AppointmentBasedOnLicense" as "OLD"
            on
                                      "IN"."_Appointment.SequenceNumber" = "OLD"."_Appointment.SequenceNumber" and
                                      "IN"."_Appointment._IndividualPerson.BusinessPartnerID" = "OLD"."_Appointment._IndividualPerson.BusinessPartnerID" and
                                      "IN"."_License.Authority" = "OLD"."_License.Authority" and
                                      "IN"."_License.IDSystem" = "OLD"."_License.IDSystem" and
                                      "IN"."_License.SubjectOfLicenseIdentifier" = "OLD"."_License.SubjectOfLicenseIdentifier" and
                                      "IN"."_License._BusinessPartner.BusinessPartnerID" = "OLD"."_License._BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__Appointment.SequenceNumber" as "_Appointment.SequenceNumber",
            "OLD__Appointment._IndividualPerson.BusinessPartnerID" as "_Appointment._IndividualPerson.BusinessPartnerID",
            "OLD__License.Authority" as "_License.Authority",
            "OLD__License.IDSystem" as "_License.IDSystem",
            "OLD__License.SubjectOfLicenseIdentifier" as "_License.SubjectOfLicenseIdentifier",
            "OLD__License._BusinessPartner.BusinessPartnerID" as "_License._BusinessPartner.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_Appointment.SequenceNumber",
                        "OLD"."_Appointment._IndividualPerson.BusinessPartnerID",
                        "OLD"."_License.Authority",
                        "OLD"."_License.IDSystem",
                        "OLD"."_License.SubjectOfLicenseIdentifier",
                        "OLD"."_License._BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_Appointment.SequenceNumber" AS "OLD__Appointment.SequenceNumber" ,
                "OLD"."_Appointment._IndividualPerson.BusinessPartnerID" AS "OLD__Appointment._IndividualPerson.BusinessPartnerID" ,
                "OLD"."_License.Authority" AS "OLD__License.Authority" ,
                "OLD"."_License.IDSystem" AS "OLD__License.IDSystem" ,
                "OLD"."_License.SubjectOfLicenseIdentifier" AS "OLD__License.SubjectOfLicenseIdentifier" ,
                "OLD"."_License._BusinessPartner.BusinessPartnerID" AS "OLD__License._BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::AppointmentBasedOnLicense" as "OLD"
            on
               "IN"."_Appointment.SequenceNumber" = "OLD"."_Appointment.SequenceNumber" and
               "IN"."_Appointment._IndividualPerson.BusinessPartnerID" = "OLD"."_Appointment._IndividualPerson.BusinessPartnerID" and
               "IN"."_License.Authority" = "OLD"."_License.Authority" and
               "IN"."_License.IDSystem" = "OLD"."_License.IDSystem" and
               "IN"."_License.SubjectOfLicenseIdentifier" = "OLD"."_License.SubjectOfLicenseIdentifier" and
               "IN"."_License._BusinessPartner.BusinessPartnerID" = "OLD"."_License._BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
