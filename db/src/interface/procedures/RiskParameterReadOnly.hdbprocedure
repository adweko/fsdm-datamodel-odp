PROCEDURE "sap.fsdm.procedures::RiskParameterReadOnly" (IN ROW "sap.fsdm.tabletypes::RiskParameterTT", OUT CURR_DEL "sap.fsdm.tabletypes::RiskParameterTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::RiskParameterTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'EstimationMethod=' || TO_VARCHAR("EstimationMethod") || ' ' ||
                'RiskParameterType=' || TO_VARCHAR("RiskParameterType") || ' ' ||
                'RiskProvisionScenario=' || TO_VARCHAR("RiskProvisionScenario") || ' ' ||
                'TimeHorizon=' || TO_VARCHAR("TimeHorizon") || ' ' ||
                'TimeHorizonUnit=' || TO_VARCHAR("TimeHorizonUnit") || ' ' ||
                '_Segment.SegmentID=' || TO_VARCHAR("_Segment.SegmentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."EstimationMethod",
                        "IN"."RiskParameterType",
                        "IN"."RiskProvisionScenario",
                        "IN"."TimeHorizon",
                        "IN"."TimeHorizonUnit",
                        "IN"."_Segment.SegmentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."EstimationMethod",
                        "IN"."RiskParameterType",
                        "IN"."RiskProvisionScenario",
                        "IN"."TimeHorizon",
                        "IN"."TimeHorizonUnit",
                        "IN"."_Segment.SegmentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "EstimationMethod",
                        "RiskParameterType",
                        "RiskProvisionScenario",
                        "TimeHorizon",
                        "TimeHorizonUnit",
                        "_Segment.SegmentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."EstimationMethod",
                                    "IN"."RiskParameterType",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."TimeHorizon",
                                    "IN"."TimeHorizonUnit",
                                    "IN"."_Segment.SegmentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."EstimationMethod",
                                    "IN"."RiskParameterType",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."TimeHorizon",
                                    "IN"."TimeHorizonUnit",
                                    "IN"."_Segment.SegmentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "EstimationMethod",
        "RiskParameterType",
        "RiskProvisionScenario",
        "TimeHorizon",
        "TimeHorizonUnit",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::RiskParameter" WHERE
        (            "EstimationMethod" ,
            "RiskParameterType" ,
            "RiskProvisionScenario" ,
            "TimeHorizon" ,
            "TimeHorizonUnit" ,
            "_Segment.SegmentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."EstimationMethod",
            "OLD"."RiskParameterType",
            "OLD"."RiskProvisionScenario",
            "OLD"."TimeHorizon",
            "OLD"."TimeHorizonUnit",
            "OLD"."_Segment.SegmentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::RiskParameter" as "OLD"
            on
               ifnull( "IN"."EstimationMethod",'' ) = "OLD"."EstimationMethod" and
               ifnull( "IN"."RiskParameterType",'' ) = "OLD"."RiskParameterType" and
               ifnull( "IN"."RiskProvisionScenario",'' ) = "OLD"."RiskProvisionScenario" and
               ifnull( "IN"."TimeHorizon",0 ) = "OLD"."TimeHorizon" and
               ifnull( "IN"."TimeHorizonUnit",'' ) = "OLD"."TimeHorizonUnit" and
               ifnull( "IN"."_Segment.SegmentID",'' ) = "OLD"."_Segment.SegmentID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "EstimationMethod",
        "RiskParameterType",
        "RiskProvisionScenario",
        "TimeHorizon",
        "TimeHorizonUnit",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "MarginOfConservatismCategoryA",
        "MarginOfConservatismCategoryB",
        "MarginOfConservatismCategoryC",
        "Rate",
        "RateBasedOnMarginsOfConservatism",
        "RiskParameterCategory",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "EstimationMethod", '' ) as "EstimationMethod",
                    ifnull( "RiskParameterType", '' ) as "RiskParameterType",
                    ifnull( "RiskProvisionScenario", '' ) as "RiskProvisionScenario",
                    ifnull( "TimeHorizon", 0 ) as "TimeHorizon",
                    ifnull( "TimeHorizonUnit", '' ) as "TimeHorizonUnit",
                    ifnull( "_Segment.SegmentID", '' ) as "_Segment.SegmentID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "MarginOfConservatismCategoryA"  ,
                    "MarginOfConservatismCategoryB"  ,
                    "MarginOfConservatismCategoryC"  ,
                    "Rate"  ,
                    "RateBasedOnMarginsOfConservatism"  ,
                    "RiskParameterCategory"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_EstimationMethod" as "EstimationMethod" ,
                    "OLD_RiskParameterType" as "RiskParameterType" ,
                    "OLD_RiskProvisionScenario" as "RiskProvisionScenario" ,
                    "OLD_TimeHorizon" as "TimeHorizon" ,
                    "OLD_TimeHorizonUnit" as "TimeHorizonUnit" ,
                    "OLD__Segment.SegmentID" as "_Segment.SegmentID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_MarginOfConservatismCategoryA" as "MarginOfConservatismCategoryA" ,
                    "OLD_MarginOfConservatismCategoryB" as "MarginOfConservatismCategoryB" ,
                    "OLD_MarginOfConservatismCategoryC" as "MarginOfConservatismCategoryC" ,
                    "OLD_Rate" as "Rate" ,
                    "OLD_RateBasedOnMarginsOfConservatism" as "RateBasedOnMarginsOfConservatism" ,
                    "OLD_RiskParameterCategory" as "RiskParameterCategory" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."EstimationMethod",
                        "IN"."RiskParameterType",
                        "IN"."RiskProvisionScenario",
                        "IN"."TimeHorizon",
                        "IN"."TimeHorizonUnit",
                        "IN"."_Segment.SegmentID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."EstimationMethod" as "OLD_EstimationMethod",
                                "OLD"."RiskParameterType" as "OLD_RiskParameterType",
                                "OLD"."RiskProvisionScenario" as "OLD_RiskProvisionScenario",
                                "OLD"."TimeHorizon" as "OLD_TimeHorizon",
                                "OLD"."TimeHorizonUnit" as "OLD_TimeHorizonUnit",
                                "OLD"."_Segment.SegmentID" as "OLD__Segment.SegmentID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."MarginOfConservatismCategoryA" as "OLD_MarginOfConservatismCategoryA",
                                "OLD"."MarginOfConservatismCategoryB" as "OLD_MarginOfConservatismCategoryB",
                                "OLD"."MarginOfConservatismCategoryC" as "OLD_MarginOfConservatismCategoryC",
                                "OLD"."Rate" as "OLD_Rate",
                                "OLD"."RateBasedOnMarginsOfConservatism" as "OLD_RateBasedOnMarginsOfConservatism",
                                "OLD"."RiskParameterCategory" as "OLD_RiskParameterCategory",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::RiskParameter" as "OLD"
            on
                ifnull( "IN"."EstimationMethod", '') = "OLD"."EstimationMethod" and
                ifnull( "IN"."RiskParameterType", '') = "OLD"."RiskParameterType" and
                ifnull( "IN"."RiskProvisionScenario", '') = "OLD"."RiskProvisionScenario" and
                ifnull( "IN"."TimeHorizon", 0) = "OLD"."TimeHorizon" and
                ifnull( "IN"."TimeHorizonUnit", '') = "OLD"."TimeHorizonUnit" and
                ifnull( "IN"."_Segment.SegmentID", '') = "OLD"."_Segment.SegmentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_EstimationMethod" as "EstimationMethod",
            "OLD_RiskParameterType" as "RiskParameterType",
            "OLD_RiskProvisionScenario" as "RiskProvisionScenario",
            "OLD_TimeHorizon" as "TimeHorizon",
            "OLD_TimeHorizonUnit" as "TimeHorizonUnit",
            "OLD__Segment.SegmentID" as "_Segment.SegmentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_MarginOfConservatismCategoryA" as "MarginOfConservatismCategoryA",
            "OLD_MarginOfConservatismCategoryB" as "MarginOfConservatismCategoryB",
            "OLD_MarginOfConservatismCategoryC" as "MarginOfConservatismCategoryC",
            "OLD_Rate" as "Rate",
            "OLD_RateBasedOnMarginsOfConservatism" as "RateBasedOnMarginsOfConservatism",
            "OLD_RiskParameterCategory" as "RiskParameterCategory",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."EstimationMethod",
                        "IN"."RiskParameterType",
                        "IN"."RiskProvisionScenario",
                        "IN"."TimeHorizon",
                        "IN"."TimeHorizonUnit",
                        "IN"."_Segment.SegmentID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                                "OLD"."EstimationMethod" as "OLD_EstimationMethod",
                                "OLD"."RiskParameterType" as "OLD_RiskParameterType",
                                "OLD"."RiskProvisionScenario" as "OLD_RiskProvisionScenario",
                                "OLD"."TimeHorizon" as "OLD_TimeHorizon",
                                "OLD"."TimeHorizonUnit" as "OLD_TimeHorizonUnit",
                                "OLD"."_Segment.SegmentID" as "OLD__Segment.SegmentID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."MarginOfConservatismCategoryA" as "OLD_MarginOfConservatismCategoryA",
                                "OLD"."MarginOfConservatismCategoryB" as "OLD_MarginOfConservatismCategoryB",
                                "OLD"."MarginOfConservatismCategoryC" as "OLD_MarginOfConservatismCategoryC",
                                "OLD"."Rate" as "OLD_Rate",
                                "OLD"."RateBasedOnMarginsOfConservatism" as "OLD_RateBasedOnMarginsOfConservatism",
                                "OLD"."RiskParameterCategory" as "OLD_RiskParameterCategory",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::RiskParameter" as "OLD"
            on
                ifnull("IN"."EstimationMethod", '') = "OLD"."EstimationMethod" and
                ifnull("IN"."RiskParameterType", '') = "OLD"."RiskParameterType" and
                ifnull("IN"."RiskProvisionScenario", '') = "OLD"."RiskProvisionScenario" and
                ifnull("IN"."TimeHorizon", 0) = "OLD"."TimeHorizon" and
                ifnull("IN"."TimeHorizonUnit", '') = "OLD"."TimeHorizonUnit" and
                ifnull("IN"."_Segment.SegmentID", '') = "OLD"."_Segment.SegmentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
