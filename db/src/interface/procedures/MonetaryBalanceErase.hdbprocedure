PROCEDURE "sap.fsdm.procedures::MonetaryBalanceErase" (IN ROW "sap.fsdm.tabletypes::MonetaryBalanceTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "PostingOrValueDateCutoff" is null and
            "RoleOfCurrency" is null and
            "ASSOC_BalanceForCcyOfMultiCcyAccount.PositionCurrency" is null and
            "ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.FinancialContractID" is null and
            "ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.IDSystem" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_CardIssue.CardIssueID" is null and
            "_CardIssue.ASSOC_CardAgreement.FinancialContractID" is null and
            "_CardIssue.ASSOC_CardAgreement.IDSystem" is null and
            "_CardIssue._BankAccount.FinancialContractID" is null and
            "_CardIssue._BankAccount.IDSystem" is null and
            "_CreditCardLoan.ID" is null and
            "_CreditCardLoan._CreditCardAgreement.FinancialContractID" is null and
            "_CreditCardLoan._CreditCardAgreement.IDSystem" is null and
            "_PlanBudgetForecast.ID" is null and
            "_PlanBudgetForecast.PlanBudgetForecastScenario" is null and
            "_PlanBudgetForecast.VersionID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::MonetaryBalance"
        WHERE
        (            "PostingOrValueDateCutoff" ,
            "RoleOfCurrency" ,
            "ASSOC_BalanceForCcyOfMultiCcyAccount.PositionCurrency" ,
            "ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.IDSystem" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_CardIssue.CardIssueID" ,
            "_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
            "_CardIssue.ASSOC_CardAgreement.IDSystem" ,
            "_CardIssue._BankAccount.FinancialContractID" ,
            "_CardIssue._BankAccount.IDSystem" ,
            "_CreditCardLoan.ID" ,
            "_CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
            "_CreditCardLoan._CreditCardAgreement.IDSystem" ,
            "_PlanBudgetForecast.ID" ,
            "_PlanBudgetForecast.PlanBudgetForecastScenario" ,
            "_PlanBudgetForecast.VersionID" 
        ) in
        (
            select                 "OLD"."PostingOrValueDateCutoff" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.PositionCurrency" ,
                "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_CardIssue.CardIssueID" ,
                "OLD"."_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
                "OLD"."_CardIssue.ASSOC_CardAgreement.IDSystem" ,
                "OLD"."_CardIssue._BankAccount.FinancialContractID" ,
                "OLD"."_CardIssue._BankAccount.IDSystem" ,
                "OLD"."_CreditCardLoan.ID" ,
                "OLD"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "OLD"."_CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "OLD"."_PlanBudgetForecast.ID" ,
                "OLD"."_PlanBudgetForecast.PlanBudgetForecastScenario" ,
                "OLD"."_PlanBudgetForecast.VersionID" 
            from :ROW "IN"
            inner join "sap.fsdm::MonetaryBalance" "OLD"
            on
            "IN"."PostingOrValueDateCutoff" = "OLD"."PostingOrValueDateCutoff" and
            "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
            "IN"."ASSOC_BalanceForCcyOfMultiCcyAccount.PositionCurrency" = "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.PositionCurrency" and
            "IN"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.FinancialContractID" and
            "IN"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.IDSystem" and
            "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
            "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
            "IN"."_CardIssue.CardIssueID" = "OLD"."_CardIssue.CardIssueID" and
            "IN"."_CardIssue.ASSOC_CardAgreement.FinancialContractID" = "OLD"."_CardIssue.ASSOC_CardAgreement.FinancialContractID" and
            "IN"."_CardIssue.ASSOC_CardAgreement.IDSystem" = "OLD"."_CardIssue.ASSOC_CardAgreement.IDSystem" and
            "IN"."_CardIssue._BankAccount.FinancialContractID" = "OLD"."_CardIssue._BankAccount.FinancialContractID" and
            "IN"."_CardIssue._BankAccount.IDSystem" = "OLD"."_CardIssue._BankAccount.IDSystem" and
            "IN"."_CreditCardLoan.ID" = "OLD"."_CreditCardLoan.ID" and
            "IN"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" and
            "IN"."_CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."_CreditCardLoan._CreditCardAgreement.IDSystem" and
            "IN"."_PlanBudgetForecast.ID" = "OLD"."_PlanBudgetForecast.ID" and
            "IN"."_PlanBudgetForecast.PlanBudgetForecastScenario" = "OLD"."_PlanBudgetForecast.PlanBudgetForecastScenario" and
            "IN"."_PlanBudgetForecast.VersionID" = "OLD"."_PlanBudgetForecast.VersionID" 
        );

        --delete data from history table
        delete from "sap.fsdm::MonetaryBalance_Historical"
        WHERE
        (
            "PostingOrValueDateCutoff" ,
            "RoleOfCurrency" ,
            "ASSOC_BalanceForCcyOfMultiCcyAccount.PositionCurrency" ,
            "ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.IDSystem" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_CardIssue.CardIssueID" ,
            "_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
            "_CardIssue.ASSOC_CardAgreement.IDSystem" ,
            "_CardIssue._BankAccount.FinancialContractID" ,
            "_CardIssue._BankAccount.IDSystem" ,
            "_CreditCardLoan.ID" ,
            "_CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
            "_CreditCardLoan._CreditCardAgreement.IDSystem" ,
            "_PlanBudgetForecast.ID" ,
            "_PlanBudgetForecast.PlanBudgetForecastScenario" ,
            "_PlanBudgetForecast.VersionID" 
        ) in
        (
            select
                "OLD"."PostingOrValueDateCutoff" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.PositionCurrency" ,
                "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_CardIssue.CardIssueID" ,
                "OLD"."_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
                "OLD"."_CardIssue.ASSOC_CardAgreement.IDSystem" ,
                "OLD"."_CardIssue._BankAccount.FinancialContractID" ,
                "OLD"."_CardIssue._BankAccount.IDSystem" ,
                "OLD"."_CreditCardLoan.ID" ,
                "OLD"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "OLD"."_CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "OLD"."_PlanBudgetForecast.ID" ,
                "OLD"."_PlanBudgetForecast.PlanBudgetForecastScenario" ,
                "OLD"."_PlanBudgetForecast.VersionID" 
            from :ROW "IN"
            inner join "sap.fsdm::MonetaryBalance_Historical" "OLD"
            on
                "IN"."PostingOrValueDateCutoff" = "OLD"."PostingOrValueDateCutoff" and
                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                "IN"."ASSOC_BalanceForCcyOfMultiCcyAccount.PositionCurrency" = "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.PositionCurrency" and
                "IN"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_BalanceForCcyOfMultiCcyAccount.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."_CardIssue.CardIssueID" = "OLD"."_CardIssue.CardIssueID" and
                "IN"."_CardIssue.ASSOC_CardAgreement.FinancialContractID" = "OLD"."_CardIssue.ASSOC_CardAgreement.FinancialContractID" and
                "IN"."_CardIssue.ASSOC_CardAgreement.IDSystem" = "OLD"."_CardIssue.ASSOC_CardAgreement.IDSystem" and
                "IN"."_CardIssue._BankAccount.FinancialContractID" = "OLD"."_CardIssue._BankAccount.FinancialContractID" and
                "IN"."_CardIssue._BankAccount.IDSystem" = "OLD"."_CardIssue._BankAccount.IDSystem" and
                "IN"."_CreditCardLoan.ID" = "OLD"."_CreditCardLoan.ID" and
                "IN"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" and
                "IN"."_CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."_CreditCardLoan._CreditCardAgreement.IDSystem" and
                "IN"."_PlanBudgetForecast.ID" = "OLD"."_PlanBudgetForecast.ID" and
                "IN"."_PlanBudgetForecast.PlanBudgetForecastScenario" = "OLD"."_PlanBudgetForecast.PlanBudgetForecastScenario" and
                "IN"."_PlanBudgetForecast.VersionID" = "OLD"."_PlanBudgetForecast.VersionID" 
        );

END
