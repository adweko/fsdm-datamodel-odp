PROCEDURE "sap.fsdm.procedures::FundKeyDateEvaluationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::FundKeyDateEvaluationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::FundKeyDateEvaluationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::FundKeyDateEvaluationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Fund.FundID" is null and
            "_Fund._InvestmentCorporation.BusinessPartnerID" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_Fund.FundID" ,
                "_Fund._InvestmentCorporation.BusinessPartnerID" ,
                "_ResultGroup.ResultDataProvider" ,
                "_ResultGroup.ResultGroupID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_Fund.FundID" ,
                "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FundKeyDateEvaluation" "OLD"
            on
                "IN"."_Fund.FundID" = "OLD"."_Fund.FundID" and
                "IN"."_Fund._InvestmentCorporation.BusinessPartnerID" = "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_Fund.FundID" ,
            "_Fund._InvestmentCorporation.BusinessPartnerID" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_Fund.FundID" ,
                "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FundKeyDateEvaluation_Historical" "OLD"
            on
                "IN"."_Fund.FundID" = "OLD"."_Fund.FundID" and
                "IN"."_Fund._InvestmentCorporation.BusinessPartnerID" = "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" 
        );

END
