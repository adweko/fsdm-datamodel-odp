PROCEDURE "sap.fsdm.procedures::FinancialCovenantListDelReadOnly" (IN ROW "sap.fsdm.tabletypes::FinancialCovenantTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::FinancialCovenantTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::FinancialCovenantTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                '_ContingentConvertibleInstrument.FinancialInstrumentID=' || TO_VARCHAR("_ContingentConvertibleInstrument.FinancialInstrumentID") || ' ' ||
                '_ContractBundle.ContractBundleID=' || TO_VARCHAR("_ContractBundle.ContractBundleID") || ' ' ||
                '_PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_PhysicalAsset.PhysicalAssetID") || ' ' ||
                '_TrancheInSyndication.TrancheSequenceNumber=' || TO_VARCHAR("_TrancheInSyndication.TrancheSequenceNumber") || ' ' ||
                '_TrancheInSyndication._SyndicationAgreement.FinancialContractID=' || TO_VARCHAR("_TrancheInSyndication._SyndicationAgreement.FinancialContractID") || ' ' ||
                '_TrancheInSyndication._SyndicationAgreement.IDSystem=' || TO_VARCHAR("_TrancheInSyndication._SyndicationAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_ContingentConvertibleInstrument.FinancialInstrumentID",
                        "IN"."_ContractBundle.ContractBundleID",
                        "IN"."_PhysicalAsset.PhysicalAssetID",
                        "IN"."_TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_ContingentConvertibleInstrument.FinancialInstrumentID",
                        "IN"."_ContractBundle.ContractBundleID",
                        "IN"."_PhysicalAsset.PhysicalAssetID",
                        "IN"."_TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "_BusinessPartner.BusinessPartnerID",
                        "_ContingentConvertibleInstrument.FinancialInstrumentID",
                        "_ContractBundle.ContractBundleID",
                        "_PhysicalAsset.PhysicalAssetID",
                        "_TrancheInSyndication.TrancheSequenceNumber",
                        "_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "_TrancheInSyndication._SyndicationAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_ContingentConvertibleInstrument.FinancialInstrumentID",
                                    "IN"."_ContractBundle.ContractBundleID",
                                    "IN"."_PhysicalAsset.PhysicalAssetID",
                                    "IN"."_TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_ContingentConvertibleInstrument.FinancialInstrumentID",
                                    "IN"."_ContractBundle.ContractBundleID",
                                    "IN"."_PhysicalAsset.PhysicalAssetID",
                                    "IN"."_TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "SequenceNumber",
            "ASSOC_FinancialContract.FinancialContractID",
            "ASSOC_FinancialContract.IDSystem",
            "_BusinessPartner.BusinessPartnerID",
            "_ContingentConvertibleInstrument.FinancialInstrumentID",
            "_ContractBundle.ContractBundleID",
            "_PhysicalAsset.PhysicalAssetID",
            "_TrancheInSyndication.TrancheSequenceNumber",
            "_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "_TrancheInSyndication._SyndicationAgreement.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::FinancialCovenant" WHERE
            (
            "SequenceNumber" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_ContingentConvertibleInstrument.FinancialInstrumentID" ,
            "_ContractBundle.ContractBundleID" ,
            "_PhysicalAsset.PhysicalAssetID" ,
            "_TrancheInSyndication.TrancheSequenceNumber" ,
            "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."_ContingentConvertibleInstrument.FinancialInstrumentID",
            "OLD"."_ContractBundle.ContractBundleID",
            "OLD"."_PhysicalAsset.PhysicalAssetID",
            "OLD"."_TrancheInSyndication.TrancheSequenceNumber",
            "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::FinancialCovenant" as "OLD"
        on
                              "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                              "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "SequenceNumber",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_BusinessPartner.BusinessPartnerID",
        "_ContingentConvertibleInstrument.FinancialInstrumentID",
        "_ContractBundle.ContractBundleID",
        "_PhysicalAsset.PhysicalAssetID",
        "_TrancheInSyndication.TrancheSequenceNumber",
        "_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
        "_TrancheInSyndication._SyndicationAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BoundaryUnit",
        "BusinessCalendar",
        "BusinessDayConvention",
        "CovenantBreachEffectType",
        "CovenantCategory",
        "CovenantCheckProcedure",
        "CovenantFulfilledEffectType",
        "CovenantType",
        "Criterion",
        "CriterionApplicableFrom",
        "CriterionApplicableTo",
        "CurePeriodLength",
        "CurePeriodTimeUnit",
        "Description",
        "EndOfContractualConsequences",
        "EndOfContractualConsequencesKeyDate",
        "EndOfContractualConsequencesSuccessfulTest",
        "FirstRegularMonitoringDate",
        "LowerBoundary",
        "LowerBoundaryCount",
        "LowerBoundaryPercentage",
        "LowerNonnumericalBoundary",
        "MonitoringPeriodLength",
        "MonitoringPeriodLengthTimeUnit",
        "MonitoringWithCurePhaseIndicator",
        "SubmissionDeadlinePeriodLength",
        "SubmissionDeadlineTimeUnit",
        "UpperBoundary",
        "UpperBoundaryCount",
        "UpperBoundaryPercentage",
        "UpperNonnumericalBoundary",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
            "OLD__ContingentConvertibleInstrument.FinancialInstrumentID" as "_ContingentConvertibleInstrument.FinancialInstrumentID" ,
            "OLD__ContractBundle.ContractBundleID" as "_ContractBundle.ContractBundleID" ,
            "OLD__PhysicalAsset.PhysicalAssetID" as "_PhysicalAsset.PhysicalAssetID" ,
            "OLD__TrancheInSyndication.TrancheSequenceNumber" as "_TrancheInSyndication.TrancheSequenceNumber" ,
            "OLD__TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "OLD__TrancheInSyndication._SyndicationAgreement.IDSystem" as "_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BoundaryUnit" as "BoundaryUnit" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_CovenantBreachEffectType" as "CovenantBreachEffectType" ,
            "OLD_CovenantCategory" as "CovenantCategory" ,
            "OLD_CovenantCheckProcedure" as "CovenantCheckProcedure" ,
            "OLD_CovenantFulfilledEffectType" as "CovenantFulfilledEffectType" ,
            "OLD_CovenantType" as "CovenantType" ,
            "OLD_Criterion" as "Criterion" ,
            "OLD_CriterionApplicableFrom" as "CriterionApplicableFrom" ,
            "OLD_CriterionApplicableTo" as "CriterionApplicableTo" ,
            "OLD_CurePeriodLength" as "CurePeriodLength" ,
            "OLD_CurePeriodTimeUnit" as "CurePeriodTimeUnit" ,
            "OLD_Description" as "Description" ,
            "OLD_EndOfContractualConsequences" as "EndOfContractualConsequences" ,
            "OLD_EndOfContractualConsequencesKeyDate" as "EndOfContractualConsequencesKeyDate" ,
            "OLD_EndOfContractualConsequencesSuccessfulTest" as "EndOfContractualConsequencesSuccessfulTest" ,
            "OLD_FirstRegularMonitoringDate" as "FirstRegularMonitoringDate" ,
            "OLD_LowerBoundary" as "LowerBoundary" ,
            "OLD_LowerBoundaryCount" as "LowerBoundaryCount" ,
            "OLD_LowerBoundaryPercentage" as "LowerBoundaryPercentage" ,
            "OLD_LowerNonnumericalBoundary" as "LowerNonnumericalBoundary" ,
            "OLD_MonitoringPeriodLength" as "MonitoringPeriodLength" ,
            "OLD_MonitoringPeriodLengthTimeUnit" as "MonitoringPeriodLengthTimeUnit" ,
            "OLD_MonitoringWithCurePhaseIndicator" as "MonitoringWithCurePhaseIndicator" ,
            "OLD_SubmissionDeadlinePeriodLength" as "SubmissionDeadlinePeriodLength" ,
            "OLD_SubmissionDeadlineTimeUnit" as "SubmissionDeadlineTimeUnit" ,
            "OLD_UpperBoundary" as "UpperBoundary" ,
            "OLD_UpperBoundaryCount" as "UpperBoundaryCount" ,
            "OLD_UpperBoundaryPercentage" as "UpperBoundaryPercentage" ,
            "OLD_UpperNonnumericalBoundary" as "UpperNonnumericalBoundary" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."_ContingentConvertibleInstrument.FinancialInstrumentID" AS "OLD__ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."_ContractBundle.ContractBundleID" AS "OLD__ContractBundle.ContractBundleID" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" AS "OLD__PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_TrancheInSyndication.TrancheSequenceNumber" AS "OLD__TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" AS "OLD__TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" AS "OLD__TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BoundaryUnit" AS "OLD_BoundaryUnit" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."CovenantBreachEffectType" AS "OLD_CovenantBreachEffectType" ,
                "OLD"."CovenantCategory" AS "OLD_CovenantCategory" ,
                "OLD"."CovenantCheckProcedure" AS "OLD_CovenantCheckProcedure" ,
                "OLD"."CovenantFulfilledEffectType" AS "OLD_CovenantFulfilledEffectType" ,
                "OLD"."CovenantType" AS "OLD_CovenantType" ,
                "OLD"."Criterion" AS "OLD_Criterion" ,
                "OLD"."CriterionApplicableFrom" AS "OLD_CriterionApplicableFrom" ,
                "OLD"."CriterionApplicableTo" AS "OLD_CriterionApplicableTo" ,
                "OLD"."CurePeriodLength" AS "OLD_CurePeriodLength" ,
                "OLD"."CurePeriodTimeUnit" AS "OLD_CurePeriodTimeUnit" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."EndOfContractualConsequences" AS "OLD_EndOfContractualConsequences" ,
                "OLD"."EndOfContractualConsequencesKeyDate" AS "OLD_EndOfContractualConsequencesKeyDate" ,
                "OLD"."EndOfContractualConsequencesSuccessfulTest" AS "OLD_EndOfContractualConsequencesSuccessfulTest" ,
                "OLD"."FirstRegularMonitoringDate" AS "OLD_FirstRegularMonitoringDate" ,
                "OLD"."LowerBoundary" AS "OLD_LowerBoundary" ,
                "OLD"."LowerBoundaryCount" AS "OLD_LowerBoundaryCount" ,
                "OLD"."LowerBoundaryPercentage" AS "OLD_LowerBoundaryPercentage" ,
                "OLD"."LowerNonnumericalBoundary" AS "OLD_LowerNonnumericalBoundary" ,
                "OLD"."MonitoringPeriodLength" AS "OLD_MonitoringPeriodLength" ,
                "OLD"."MonitoringPeriodLengthTimeUnit" AS "OLD_MonitoringPeriodLengthTimeUnit" ,
                "OLD"."MonitoringWithCurePhaseIndicator" AS "OLD_MonitoringWithCurePhaseIndicator" ,
                "OLD"."SubmissionDeadlinePeriodLength" AS "OLD_SubmissionDeadlinePeriodLength" ,
                "OLD"."SubmissionDeadlineTimeUnit" AS "OLD_SubmissionDeadlineTimeUnit" ,
                "OLD"."UpperBoundary" AS "OLD_UpperBoundary" ,
                "OLD"."UpperBoundaryCount" AS "OLD_UpperBoundaryCount" ,
                "OLD"."UpperBoundaryPercentage" AS "OLD_UpperBoundaryPercentage" ,
                "OLD"."UpperNonnumericalBoundary" AS "OLD_UpperNonnumericalBoundary" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::FinancialCovenant" as "OLD"
            on
                                      "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                      "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID",
            "OLD__ContingentConvertibleInstrument.FinancialInstrumentID" as "_ContingentConvertibleInstrument.FinancialInstrumentID",
            "OLD__ContractBundle.ContractBundleID" as "_ContractBundle.ContractBundleID",
            "OLD__PhysicalAsset.PhysicalAssetID" as "_PhysicalAsset.PhysicalAssetID",
            "OLD__TrancheInSyndication.TrancheSequenceNumber" as "_TrancheInSyndication.TrancheSequenceNumber",
            "OLD__TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD__TrancheInSyndication._SyndicationAgreement.IDSystem" as "_TrancheInSyndication._SyndicationAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_BoundaryUnit" as "BoundaryUnit",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_CovenantBreachEffectType" as "CovenantBreachEffectType",
            "OLD_CovenantCategory" as "CovenantCategory",
            "OLD_CovenantCheckProcedure" as "CovenantCheckProcedure",
            "OLD_CovenantFulfilledEffectType" as "CovenantFulfilledEffectType",
            "OLD_CovenantType" as "CovenantType",
            "OLD_Criterion" as "Criterion",
            "OLD_CriterionApplicableFrom" as "CriterionApplicableFrom",
            "OLD_CriterionApplicableTo" as "CriterionApplicableTo",
            "OLD_CurePeriodLength" as "CurePeriodLength",
            "OLD_CurePeriodTimeUnit" as "CurePeriodTimeUnit",
            "OLD_Description" as "Description",
            "OLD_EndOfContractualConsequences" as "EndOfContractualConsequences",
            "OLD_EndOfContractualConsequencesKeyDate" as "EndOfContractualConsequencesKeyDate",
            "OLD_EndOfContractualConsequencesSuccessfulTest" as "EndOfContractualConsequencesSuccessfulTest",
            "OLD_FirstRegularMonitoringDate" as "FirstRegularMonitoringDate",
            "OLD_LowerBoundary" as "LowerBoundary",
            "OLD_LowerBoundaryCount" as "LowerBoundaryCount",
            "OLD_LowerBoundaryPercentage" as "LowerBoundaryPercentage",
            "OLD_LowerNonnumericalBoundary" as "LowerNonnumericalBoundary",
            "OLD_MonitoringPeriodLength" as "MonitoringPeriodLength",
            "OLD_MonitoringPeriodLengthTimeUnit" as "MonitoringPeriodLengthTimeUnit",
            "OLD_MonitoringWithCurePhaseIndicator" as "MonitoringWithCurePhaseIndicator",
            "OLD_SubmissionDeadlinePeriodLength" as "SubmissionDeadlinePeriodLength",
            "OLD_SubmissionDeadlineTimeUnit" as "SubmissionDeadlineTimeUnit",
            "OLD_UpperBoundary" as "UpperBoundary",
            "OLD_UpperBoundaryCount" as "UpperBoundaryCount",
            "OLD_UpperBoundaryPercentage" as "UpperBoundaryPercentage",
            "OLD_UpperNonnumericalBoundary" as "UpperNonnumericalBoundary",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."_ContingentConvertibleInstrument.FinancialInstrumentID" AS "OLD__ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."_ContractBundle.ContractBundleID" AS "OLD__ContractBundle.ContractBundleID" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" AS "OLD__PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_TrancheInSyndication.TrancheSequenceNumber" AS "OLD__TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" AS "OLD__TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" AS "OLD__TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BoundaryUnit" AS "OLD_BoundaryUnit" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."CovenantBreachEffectType" AS "OLD_CovenantBreachEffectType" ,
                "OLD"."CovenantCategory" AS "OLD_CovenantCategory" ,
                "OLD"."CovenantCheckProcedure" AS "OLD_CovenantCheckProcedure" ,
                "OLD"."CovenantFulfilledEffectType" AS "OLD_CovenantFulfilledEffectType" ,
                "OLD"."CovenantType" AS "OLD_CovenantType" ,
                "OLD"."Criterion" AS "OLD_Criterion" ,
                "OLD"."CriterionApplicableFrom" AS "OLD_CriterionApplicableFrom" ,
                "OLD"."CriterionApplicableTo" AS "OLD_CriterionApplicableTo" ,
                "OLD"."CurePeriodLength" AS "OLD_CurePeriodLength" ,
                "OLD"."CurePeriodTimeUnit" AS "OLD_CurePeriodTimeUnit" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."EndOfContractualConsequences" AS "OLD_EndOfContractualConsequences" ,
                "OLD"."EndOfContractualConsequencesKeyDate" AS "OLD_EndOfContractualConsequencesKeyDate" ,
                "OLD"."EndOfContractualConsequencesSuccessfulTest" AS "OLD_EndOfContractualConsequencesSuccessfulTest" ,
                "OLD"."FirstRegularMonitoringDate" AS "OLD_FirstRegularMonitoringDate" ,
                "OLD"."LowerBoundary" AS "OLD_LowerBoundary" ,
                "OLD"."LowerBoundaryCount" AS "OLD_LowerBoundaryCount" ,
                "OLD"."LowerBoundaryPercentage" AS "OLD_LowerBoundaryPercentage" ,
                "OLD"."LowerNonnumericalBoundary" AS "OLD_LowerNonnumericalBoundary" ,
                "OLD"."MonitoringPeriodLength" AS "OLD_MonitoringPeriodLength" ,
                "OLD"."MonitoringPeriodLengthTimeUnit" AS "OLD_MonitoringPeriodLengthTimeUnit" ,
                "OLD"."MonitoringWithCurePhaseIndicator" AS "OLD_MonitoringWithCurePhaseIndicator" ,
                "OLD"."SubmissionDeadlinePeriodLength" AS "OLD_SubmissionDeadlinePeriodLength" ,
                "OLD"."SubmissionDeadlineTimeUnit" AS "OLD_SubmissionDeadlineTimeUnit" ,
                "OLD"."UpperBoundary" AS "OLD_UpperBoundary" ,
                "OLD"."UpperBoundaryCount" AS "OLD_UpperBoundaryCount" ,
                "OLD"."UpperBoundaryPercentage" AS "OLD_UpperBoundaryPercentage" ,
                "OLD"."UpperNonnumericalBoundary" AS "OLD_UpperNonnumericalBoundary" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::FinancialCovenant" as "OLD"
            on
               "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
               "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
