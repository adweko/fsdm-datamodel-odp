PROCEDURE "sap.fsdm.procedures::PhysicalAssetClassAssignmentErase" (IN ROW "sap.fsdm.tabletypes::PhysicalAssetClassAssignmentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_PhysicalAsset.PhysicalAssetID" is null and
            "_PhysicalAssetClass.PhysicalAssetClass" is null and
            "_PhysicalAssetClass.PhysicalAssetClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::PhysicalAssetClassAssignment"
        WHERE
        (            "_PhysicalAsset.PhysicalAssetID" ,
            "_PhysicalAssetClass.PhysicalAssetClass" ,
            "_PhysicalAssetClass.PhysicalAssetClassificationSystem" 
        ) in
        (
            select                 "OLD"."_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_PhysicalAssetClass.PhysicalAssetClass" ,
                "OLD"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::PhysicalAssetClassAssignment" "OLD"
            on
            "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
            "IN"."_PhysicalAssetClass.PhysicalAssetClass" = "OLD"."_PhysicalAssetClass.PhysicalAssetClass" and
            "IN"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" = "OLD"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::PhysicalAssetClassAssignment_Historical"
        WHERE
        (
            "_PhysicalAsset.PhysicalAssetID" ,
            "_PhysicalAssetClass.PhysicalAssetClass" ,
            "_PhysicalAssetClass.PhysicalAssetClassificationSystem" 
        ) in
        (
            select
                "OLD"."_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_PhysicalAssetClass.PhysicalAssetClass" ,
                "OLD"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::PhysicalAssetClassAssignment_Historical" "OLD"
            on
                "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                "IN"."_PhysicalAssetClass.PhysicalAssetClass" = "OLD"."_PhysicalAssetClass.PhysicalAssetClass" and
                "IN"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" = "OLD"."_PhysicalAssetClass.PhysicalAssetClassificationSystem" 
        );

END
