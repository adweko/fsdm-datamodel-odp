PROCEDURE "sap.fsdm.procedures::CancellationOptionListErase" (IN ROW "sap.fsdm.tabletypes::CancellationOptionTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_Trade.TradeID" is null and
            "_Trade.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::CancellationOption"
        WHERE
        (            "SequenceNumber" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_RentalContractInformation.RentalContractInformationReferenceID" ,
            "_Trade.IDSystem" ,
            "_Trade.TradeID" 
        ) in
        (
            select                 "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."_Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" 
            from :ROW "IN"
            inner join "sap.fsdm::CancellationOption" "OLD"
            on
            "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
            "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
            "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" and
            "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::CancellationOption_Historical"
        WHERE
        (
            "SequenceNumber" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_RentalContractInformation.RentalContractInformationReferenceID" ,
            "_Trade.IDSystem" ,
            "_Trade.TradeID" 
        ) in
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."_Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" 
            from :ROW "IN"
            inner join "sap.fsdm::CancellationOption_Historical" "OLD"
            on
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" and
                "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" 
        );

END
