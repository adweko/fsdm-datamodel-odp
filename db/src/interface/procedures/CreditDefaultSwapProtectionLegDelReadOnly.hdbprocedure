PROCEDURE "sap.fsdm.procedures::CreditDefaultSwapProtectionLegDelReadOnly" (IN ROW "sap.fsdm.tabletypes::CreditDefaultSwapProtectionLegTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::CreditDefaultSwapProtectionLegTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CreditDefaultSwapProtectionLegTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_CreditDefaultSwap.FinancialContractID=' || TO_VARCHAR("_CreditDefaultSwap.FinancialContractID") || ' ' ||
                '_CreditDefaultSwap.IDSystem=' || TO_VARCHAR("_CreditDefaultSwap.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_CreditDefaultSwap.FinancialContractID",
                        "IN"."_CreditDefaultSwap.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_CreditDefaultSwap.FinancialContractID",
                        "IN"."_CreditDefaultSwap.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_CreditDefaultSwap.FinancialContractID",
                        "_CreditDefaultSwap.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_CreditDefaultSwap.FinancialContractID",
                                    "IN"."_CreditDefaultSwap.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_CreditDefaultSwap.FinancialContractID",
                                    "IN"."_CreditDefaultSwap.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_CreditDefaultSwap.FinancialContractID" is null and
            "_CreditDefaultSwap.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_CreditDefaultSwap.FinancialContractID",
            "_CreditDefaultSwap.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::CreditDefaultSwapProtectionLeg" WHERE
            (
            "_CreditDefaultSwap.FinancialContractID" ,
            "_CreditDefaultSwap.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_CreditDefaultSwap.FinancialContractID",
            "OLD"."_CreditDefaultSwap.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CreditDefaultSwapProtectionLeg" as "OLD"
        on
                              "IN"."_CreditDefaultSwap.FinancialContractID" = "OLD"."_CreditDefaultSwap.FinancialContractID" and
                              "IN"."_CreditDefaultSwap.IDSystem" = "OLD"."_CreditDefaultSwap.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_CreditDefaultSwap.FinancialContractID",
        "_CreditDefaultSwap.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Basket.FinancialInstrumentID",
        "_CDSIndex.IndexID",
        "_DebtInstrument.FinancialInstrumentID",
        "_SecuritizationPool.CollectionID",
        "_SecuritizationPool.IDSystem",
        "BinaryPayOffAmount",
        "BinaryPayOffAmountCurrency",
        "CreditEventBackstopDate",
        "ProtectionAmount",
        "ProtectionAmountCurrency",
        "ProtectionCurrency",
        "ProtectionStartDate",
        "ReferenceEntityPairCode",
        "ReferencePrice",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__CreditDefaultSwap.FinancialContractID" as "_CreditDefaultSwap.FinancialContractID" ,
            "OLD__CreditDefaultSwap.IDSystem" as "_CreditDefaultSwap.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__Basket.FinancialInstrumentID" as "_Basket.FinancialInstrumentID" ,
            "OLD__CDSIndex.IndexID" as "_CDSIndex.IndexID" ,
            "OLD__DebtInstrument.FinancialInstrumentID" as "_DebtInstrument.FinancialInstrumentID" ,
            "OLD__SecuritizationPool.CollectionID" as "_SecuritizationPool.CollectionID" ,
            "OLD__SecuritizationPool.IDSystem" as "_SecuritizationPool.IDSystem" ,
            "OLD_BinaryPayOffAmount" as "BinaryPayOffAmount" ,
            "OLD_BinaryPayOffAmountCurrency" as "BinaryPayOffAmountCurrency" ,
            "OLD_CreditEventBackstopDate" as "CreditEventBackstopDate" ,
            "OLD_ProtectionAmount" as "ProtectionAmount" ,
            "OLD_ProtectionAmountCurrency" as "ProtectionAmountCurrency" ,
            "OLD_ProtectionCurrency" as "ProtectionCurrency" ,
            "OLD_ProtectionStartDate" as "ProtectionStartDate" ,
            "OLD_ReferenceEntityPairCode" as "ReferenceEntityPairCode" ,
            "OLD_ReferencePrice" as "ReferencePrice" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_CreditDefaultSwap.FinancialContractID",
                        "OLD"."_CreditDefaultSwap.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_CreditDefaultSwap.FinancialContractID" AS "OLD__CreditDefaultSwap.FinancialContractID" ,
                "OLD"."_CreditDefaultSwap.IDSystem" AS "OLD__CreditDefaultSwap.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_Basket.FinancialInstrumentID" AS "OLD__Basket.FinancialInstrumentID" ,
                "OLD"."_CDSIndex.IndexID" AS "OLD__CDSIndex.IndexID" ,
                "OLD"."_DebtInstrument.FinancialInstrumentID" AS "OLD__DebtInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritizationPool.CollectionID" AS "OLD__SecuritizationPool.CollectionID" ,
                "OLD"."_SecuritizationPool.IDSystem" AS "OLD__SecuritizationPool.IDSystem" ,
                "OLD"."BinaryPayOffAmount" AS "OLD_BinaryPayOffAmount" ,
                "OLD"."BinaryPayOffAmountCurrency" AS "OLD_BinaryPayOffAmountCurrency" ,
                "OLD"."CreditEventBackstopDate" AS "OLD_CreditEventBackstopDate" ,
                "OLD"."ProtectionAmount" AS "OLD_ProtectionAmount" ,
                "OLD"."ProtectionAmountCurrency" AS "OLD_ProtectionAmountCurrency" ,
                "OLD"."ProtectionCurrency" AS "OLD_ProtectionCurrency" ,
                "OLD"."ProtectionStartDate" AS "OLD_ProtectionStartDate" ,
                "OLD"."ReferenceEntityPairCode" AS "OLD_ReferenceEntityPairCode" ,
                "OLD"."ReferencePrice" AS "OLD_ReferencePrice" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CreditDefaultSwapProtectionLeg" as "OLD"
            on
                                      "IN"."_CreditDefaultSwap.FinancialContractID" = "OLD"."_CreditDefaultSwap.FinancialContractID" and
                                      "IN"."_CreditDefaultSwap.IDSystem" = "OLD"."_CreditDefaultSwap.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__CreditDefaultSwap.FinancialContractID" as "_CreditDefaultSwap.FinancialContractID",
            "OLD__CreditDefaultSwap.IDSystem" as "_CreditDefaultSwap.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__Basket.FinancialInstrumentID" as "_Basket.FinancialInstrumentID",
            "OLD__CDSIndex.IndexID" as "_CDSIndex.IndexID",
            "OLD__DebtInstrument.FinancialInstrumentID" as "_DebtInstrument.FinancialInstrumentID",
            "OLD__SecuritizationPool.CollectionID" as "_SecuritizationPool.CollectionID",
            "OLD__SecuritizationPool.IDSystem" as "_SecuritizationPool.IDSystem",
            "OLD_BinaryPayOffAmount" as "BinaryPayOffAmount",
            "OLD_BinaryPayOffAmountCurrency" as "BinaryPayOffAmountCurrency",
            "OLD_CreditEventBackstopDate" as "CreditEventBackstopDate",
            "OLD_ProtectionAmount" as "ProtectionAmount",
            "OLD_ProtectionAmountCurrency" as "ProtectionAmountCurrency",
            "OLD_ProtectionCurrency" as "ProtectionCurrency",
            "OLD_ProtectionStartDate" as "ProtectionStartDate",
            "OLD_ReferenceEntityPairCode" as "ReferenceEntityPairCode",
            "OLD_ReferencePrice" as "ReferencePrice",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_CreditDefaultSwap.FinancialContractID",
                        "OLD"."_CreditDefaultSwap.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_CreditDefaultSwap.FinancialContractID" AS "OLD__CreditDefaultSwap.FinancialContractID" ,
                "OLD"."_CreditDefaultSwap.IDSystem" AS "OLD__CreditDefaultSwap.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_Basket.FinancialInstrumentID" AS "OLD__Basket.FinancialInstrumentID" ,
                "OLD"."_CDSIndex.IndexID" AS "OLD__CDSIndex.IndexID" ,
                "OLD"."_DebtInstrument.FinancialInstrumentID" AS "OLD__DebtInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritizationPool.CollectionID" AS "OLD__SecuritizationPool.CollectionID" ,
                "OLD"."_SecuritizationPool.IDSystem" AS "OLD__SecuritizationPool.IDSystem" ,
                "OLD"."BinaryPayOffAmount" AS "OLD_BinaryPayOffAmount" ,
                "OLD"."BinaryPayOffAmountCurrency" AS "OLD_BinaryPayOffAmountCurrency" ,
                "OLD"."CreditEventBackstopDate" AS "OLD_CreditEventBackstopDate" ,
                "OLD"."ProtectionAmount" AS "OLD_ProtectionAmount" ,
                "OLD"."ProtectionAmountCurrency" AS "OLD_ProtectionAmountCurrency" ,
                "OLD"."ProtectionCurrency" AS "OLD_ProtectionCurrency" ,
                "OLD"."ProtectionStartDate" AS "OLD_ProtectionStartDate" ,
                "OLD"."ReferenceEntityPairCode" AS "OLD_ReferenceEntityPairCode" ,
                "OLD"."ReferencePrice" AS "OLD_ReferencePrice" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CreditDefaultSwapProtectionLeg" as "OLD"
            on
               "IN"."_CreditDefaultSwap.FinancialContractID" = "OLD"."_CreditDefaultSwap.FinancialContractID" and
               "IN"."_CreditDefaultSwap.IDSystem" = "OLD"."_CreditDefaultSwap.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
