PROCEDURE "sap.fsdm.procedures::PlannedEarlyRepaymentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::PlannedEarlyRepaymentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::PlannedEarlyRepaymentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::PlannedEarlyRepaymentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PlannedRepaymentDate=' || TO_VARCHAR("PlannedRepaymentDate") || ' ' ||
                'RepaymentDifferentiationCriterium=' || TO_VARCHAR("RepaymentDifferentiationCriterium") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PlannedRepaymentDate",
                        "IN"."RepaymentDifferentiationCriterium",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PlannedRepaymentDate",
                        "IN"."RepaymentDifferentiationCriterium",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PlannedRepaymentDate",
                        "RepaymentDifferentiationCriterium",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PlannedRepaymentDate",
                                    "IN"."RepaymentDifferentiationCriterium",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PlannedRepaymentDate",
                                    "IN"."RepaymentDifferentiationCriterium",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "PlannedRepaymentDate" is null and
            "RepaymentDifferentiationCriterium" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "PlannedRepaymentDate",
            "RepaymentDifferentiationCriterium",
            "_FinancialContract.FinancialContractID",
            "_FinancialContract.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::PlannedEarlyRepayment" WHERE
            (
            "PlannedRepaymentDate" ,
            "RepaymentDifferentiationCriterium" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."PlannedRepaymentDate",
            "OLD"."RepaymentDifferentiationCriterium",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PlannedEarlyRepayment" as "OLD"
        on
                              "IN"."PlannedRepaymentDate" = "OLD"."PlannedRepaymentDate" and
                              "IN"."RepaymentDifferentiationCriterium" = "OLD"."RepaymentDifferentiationCriterium" and
                              "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                              "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "PlannedRepaymentDate",
        "RepaymentDifferentiationCriterium",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PlannedRepaymentAmount",
        "PlannedRepaymentAmountCurrency",
        "PrepaymentPenaltyAmount",
        "PrepaymentPenaltyAmountCurrency",
        "RepaymentProbability",
        "Status",
        "StatusChangeDate",
        "StatusReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_PlannedRepaymentDate" as "PlannedRepaymentDate" ,
            "OLD_RepaymentDifferentiationCriterium" as "RepaymentDifferentiationCriterium" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_PlannedRepaymentAmount" as "PlannedRepaymentAmount" ,
            "OLD_PlannedRepaymentAmountCurrency" as "PlannedRepaymentAmountCurrency" ,
            "OLD_PrepaymentPenaltyAmount" as "PrepaymentPenaltyAmount" ,
            "OLD_PrepaymentPenaltyAmountCurrency" as "PrepaymentPenaltyAmountCurrency" ,
            "OLD_RepaymentProbability" as "RepaymentProbability" ,
            "OLD_Status" as "Status" ,
            "OLD_StatusChangeDate" as "StatusChangeDate" ,
            "OLD_StatusReason" as "StatusReason" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."PlannedRepaymentDate",
                        "OLD"."RepaymentDifferentiationCriterium",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."PlannedRepaymentDate" AS "OLD_PlannedRepaymentDate" ,
                "OLD"."RepaymentDifferentiationCriterium" AS "OLD_RepaymentDifferentiationCriterium" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PlannedRepaymentAmount" AS "OLD_PlannedRepaymentAmount" ,
                "OLD"."PlannedRepaymentAmountCurrency" AS "OLD_PlannedRepaymentAmountCurrency" ,
                "OLD"."PrepaymentPenaltyAmount" AS "OLD_PrepaymentPenaltyAmount" ,
                "OLD"."PrepaymentPenaltyAmountCurrency" AS "OLD_PrepaymentPenaltyAmountCurrency" ,
                "OLD"."RepaymentProbability" AS "OLD_RepaymentProbability" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeDate" AS "OLD_StatusChangeDate" ,
                "OLD"."StatusReason" AS "OLD_StatusReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PlannedEarlyRepayment" as "OLD"
            on
                                      "IN"."PlannedRepaymentDate" = "OLD"."PlannedRepaymentDate" and
                                      "IN"."RepaymentDifferentiationCriterium" = "OLD"."RepaymentDifferentiationCriterium" and
                                      "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                      "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_PlannedRepaymentDate" as "PlannedRepaymentDate",
            "OLD_RepaymentDifferentiationCriterium" as "RepaymentDifferentiationCriterium",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_PlannedRepaymentAmount" as "PlannedRepaymentAmount",
            "OLD_PlannedRepaymentAmountCurrency" as "PlannedRepaymentAmountCurrency",
            "OLD_PrepaymentPenaltyAmount" as "PrepaymentPenaltyAmount",
            "OLD_PrepaymentPenaltyAmountCurrency" as "PrepaymentPenaltyAmountCurrency",
            "OLD_RepaymentProbability" as "RepaymentProbability",
            "OLD_Status" as "Status",
            "OLD_StatusChangeDate" as "StatusChangeDate",
            "OLD_StatusReason" as "StatusReason",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."PlannedRepaymentDate",
                        "OLD"."RepaymentDifferentiationCriterium",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."PlannedRepaymentDate" AS "OLD_PlannedRepaymentDate" ,
                "OLD"."RepaymentDifferentiationCriterium" AS "OLD_RepaymentDifferentiationCriterium" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PlannedRepaymentAmount" AS "OLD_PlannedRepaymentAmount" ,
                "OLD"."PlannedRepaymentAmountCurrency" AS "OLD_PlannedRepaymentAmountCurrency" ,
                "OLD"."PrepaymentPenaltyAmount" AS "OLD_PrepaymentPenaltyAmount" ,
                "OLD"."PrepaymentPenaltyAmountCurrency" AS "OLD_PrepaymentPenaltyAmountCurrency" ,
                "OLD"."RepaymentProbability" AS "OLD_RepaymentProbability" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeDate" AS "OLD_StatusChangeDate" ,
                "OLD"."StatusReason" AS "OLD_StatusReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PlannedEarlyRepayment" as "OLD"
            on
               "IN"."PlannedRepaymentDate" = "OLD"."PlannedRepaymentDate" and
               "IN"."RepaymentDifferentiationCriterium" = "OLD"."RepaymentDifferentiationCriterium" and
               "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
               "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
