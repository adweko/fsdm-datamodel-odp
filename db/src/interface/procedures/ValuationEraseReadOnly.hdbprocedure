PROCEDURE "sap.fsdm.procedures::ValuationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ValuationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ValuationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ValuationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Appraiser" is null and
            "ComponentNumber" is null and
            "FXSpotOrForward" is null and
            "LotID" is null and
            "RoleOfPayer" is null and
            "Scenario" is null and
            "ValuationMethod" is null and
            "ASSOC_BusinessInterestInOrganization.BusinessPartnerRelationType" is null and
            "ASSOC_BusinessInterestInOrganization.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" is null and
            "ASSOC_BusinessInterestInOrganization.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" is null and
            "ASSOC_CollateralPortion.PortionNumber" is null and
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" is null and
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_Organization.BusinessPartnerID" is null and
            "ASSOC_PhysicalAsset.PhysicalAssetID" is null and
            "ASSOC_PositionCurrencyForAccount.PositionCurrency" is null and
            "ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.FinancialContractID" is null and
            "ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.IDSystem" is null and
            "ASSOC_Receivable.ReceivableID" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_Fund.FundID" is null and
            "_Fund._InvestmentCorporation.BusinessPartnerID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Appraiser" ,
                "ComponentNumber" ,
                "FXSpotOrForward" ,
                "LotID" ,
                "RoleOfPayer" ,
                "Scenario" ,
                "ValuationMethod" ,
                "ASSOC_BusinessInterestInOrganization.BusinessPartnerRelationType" ,
                "ASSOC_BusinessInterestInOrganization.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
                "ASSOC_BusinessInterestInOrganization.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
                "ASSOC_CollateralPortion.PortionNumber" ,
                "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_FinancialContract.IDSystem" ,
                "ASSOC_Organization.BusinessPartnerID" ,
                "ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "ASSOC_PositionCurrencyForAccount.PositionCurrency" ,
                "ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.IDSystem" ,
                "ASSOC_Receivable.ReceivableID" ,
                "_AccountingSystem.AccountingSystemID" ,
                "_Collection.CollectionID" ,
                "_Collection.IDSystem" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "_Fund.FundID" ,
                "_Fund._InvestmentCorporation.BusinessPartnerID" ,
                "_SecuritiesAccount.FinancialContractID" ,
                "_SecuritiesAccount.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Appraiser" ,
                "OLD"."ComponentNumber" ,
                "OLD"."FXSpotOrForward" ,
                "OLD"."LotID" ,
                "OLD"."RoleOfPayer" ,
                "OLD"."Scenario" ,
                "OLD"."ValuationMethod" ,
                "OLD"."ASSOC_BusinessInterestInOrganization.BusinessPartnerRelationType" ,
                "OLD"."ASSOC_BusinessInterestInOrganization.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."ASSOC_BusinessInterestInOrganization.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."ASSOC_CollateralPortion.PortionNumber" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_Organization.BusinessPartnerID" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."ASSOC_PositionCurrencyForAccount.PositionCurrency" ,
                "OLD"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."ASSOC_Receivable.ReceivableID" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_Fund.FundID" ,
                "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Valuation" "OLD"
            on
                "IN"."Appraiser" = "OLD"."Appraiser" and
                "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
                "IN"."FXSpotOrForward" = "OLD"."FXSpotOrForward" and
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                "IN"."Scenario" = "OLD"."Scenario" and
                "IN"."ValuationMethod" = "OLD"."ValuationMethod" and
                "IN"."ASSOC_BusinessInterestInOrganization.BusinessPartnerRelationType" = "OLD"."ASSOC_BusinessInterestInOrganization.BusinessPartnerRelationType" and
                "IN"."ASSOC_BusinessInterestInOrganization.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" = "OLD"."ASSOC_BusinessInterestInOrganization.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" and
                "IN"."ASSOC_BusinessInterestInOrganization.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" = "OLD"."ASSOC_BusinessInterestInOrganization.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" and
                "IN"."ASSOC_CollateralPortion.PortionNumber" = "OLD"."ASSOC_CollateralPortion.PortionNumber" and
                "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_Organization.BusinessPartnerID" = "OLD"."ASSOC_Organization.BusinessPartnerID" and
                "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" and
                "IN"."ASSOC_PositionCurrencyForAccount.PositionCurrency" = "OLD"."ASSOC_PositionCurrencyForAccount.PositionCurrency" and
                "IN"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."ASSOC_Receivable.ReceivableID" = "OLD"."ASSOC_Receivable.ReceivableID" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_Fund.FundID" = "OLD"."_Fund.FundID" and
                "IN"."_Fund._InvestmentCorporation.BusinessPartnerID" = "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Appraiser" ,
            "ComponentNumber" ,
            "FXSpotOrForward" ,
            "LotID" ,
            "RoleOfPayer" ,
            "Scenario" ,
            "ValuationMethod" ,
            "ASSOC_BusinessInterestInOrganization.BusinessPartnerRelationType" ,
            "ASSOC_BusinessInterestInOrganization.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
            "ASSOC_BusinessInterestInOrganization.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
            "ASSOC_CollateralPortion.PortionNumber" ,
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_Organization.BusinessPartnerID" ,
            "ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "ASSOC_PositionCurrencyForAccount.PositionCurrency" ,
            "ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.IDSystem" ,
            "ASSOC_Receivable.ReceivableID" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_Fund.FundID" ,
            "_Fund._InvestmentCorporation.BusinessPartnerID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Appraiser" ,
                "OLD"."ComponentNumber" ,
                "OLD"."FXSpotOrForward" ,
                "OLD"."LotID" ,
                "OLD"."RoleOfPayer" ,
                "OLD"."Scenario" ,
                "OLD"."ValuationMethod" ,
                "OLD"."ASSOC_BusinessInterestInOrganization.BusinessPartnerRelationType" ,
                "OLD"."ASSOC_BusinessInterestInOrganization.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."ASSOC_BusinessInterestInOrganization.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."ASSOC_CollateralPortion.PortionNumber" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_Organization.BusinessPartnerID" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."ASSOC_PositionCurrencyForAccount.PositionCurrency" ,
                "OLD"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."ASSOC_Receivable.ReceivableID" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_Fund.FundID" ,
                "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Valuation_Historical" "OLD"
            on
                "IN"."Appraiser" = "OLD"."Appraiser" and
                "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
                "IN"."FXSpotOrForward" = "OLD"."FXSpotOrForward" and
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                "IN"."Scenario" = "OLD"."Scenario" and
                "IN"."ValuationMethod" = "OLD"."ValuationMethod" and
                "IN"."ASSOC_BusinessInterestInOrganization.BusinessPartnerRelationType" = "OLD"."ASSOC_BusinessInterestInOrganization.BusinessPartnerRelationType" and
                "IN"."ASSOC_BusinessInterestInOrganization.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" = "OLD"."ASSOC_BusinessInterestInOrganization.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" and
                "IN"."ASSOC_BusinessInterestInOrganization.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" = "OLD"."ASSOC_BusinessInterestInOrganization.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" and
                "IN"."ASSOC_CollateralPortion.PortionNumber" = "OLD"."ASSOC_CollateralPortion.PortionNumber" and
                "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_Organization.BusinessPartnerID" = "OLD"."ASSOC_Organization.BusinessPartnerID" and
                "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" and
                "IN"."ASSOC_PositionCurrencyForAccount.PositionCurrency" = "OLD"."ASSOC_PositionCurrencyForAccount.PositionCurrency" and
                "IN"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_PositionCurrencyForAccount.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."ASSOC_Receivable.ReceivableID" = "OLD"."ASSOC_Receivable.ReceivableID" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_Fund.FundID" = "OLD"."_Fund.FundID" and
                "IN"."_Fund._InvestmentCorporation.BusinessPartnerID" = "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        );

END
