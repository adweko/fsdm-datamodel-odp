PROCEDURE "sap.fsdm.procedures::PositionErase" (IN ROW "sap.fsdm.tabletypes::PositionTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Currency" is null and
            "ExpirationDate" is null and
            "LotID" is null and
            "PutOrCall" is null and
            "StrikePrice" is null and
            "ValuationMethod" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_DerivativesAccount.FinancialContractID" is null and
            "_DerivativesAccount.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::Position"
        WHERE
        (            "Currency" ,
            "ExpirationDate" ,
            "LotID" ,
            "PutOrCall" ,
            "StrikePrice" ,
            "ValuationMethod" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_DerivativesAccount.FinancialContractID" ,
            "_DerivativesAccount.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" 
        ) in
        (
            select                 "OLD"."Currency" ,
                "OLD"."ExpirationDate" ,
                "OLD"."LotID" ,
                "OLD"."PutOrCall" ,
                "OLD"."StrikePrice" ,
                "OLD"."ValuationMethod" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_DerivativesAccount.FinancialContractID" ,
                "OLD"."_DerivativesAccount.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::Position" "OLD"
            on
            "IN"."Currency" = "OLD"."Currency" and
            "IN"."ExpirationDate" = "OLD"."ExpirationDate" and
            "IN"."LotID" = "OLD"."LotID" and
            "IN"."PutOrCall" = "OLD"."PutOrCall" and
            "IN"."StrikePrice" = "OLD"."StrikePrice" and
            "IN"."ValuationMethod" = "OLD"."ValuationMethod" and
            "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
            "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
            "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
            "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
            "IN"."_DerivativesAccount.FinancialContractID" = "OLD"."_DerivativesAccount.FinancialContractID" and
            "IN"."_DerivativesAccount.IDSystem" = "OLD"."_DerivativesAccount.IDSystem" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
            "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
            "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::Position_Historical"
        WHERE
        (
            "Currency" ,
            "ExpirationDate" ,
            "LotID" ,
            "PutOrCall" ,
            "StrikePrice" ,
            "ValuationMethod" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_DerivativesAccount.FinancialContractID" ,
            "_DerivativesAccount.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" 
        ) in
        (
            select
                "OLD"."Currency" ,
                "OLD"."ExpirationDate" ,
                "OLD"."LotID" ,
                "OLD"."PutOrCall" ,
                "OLD"."StrikePrice" ,
                "OLD"."ValuationMethod" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_DerivativesAccount.FinancialContractID" ,
                "OLD"."_DerivativesAccount.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::Position_Historical" "OLD"
            on
                "IN"."Currency" = "OLD"."Currency" and
                "IN"."ExpirationDate" = "OLD"."ExpirationDate" and
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."PutOrCall" = "OLD"."PutOrCall" and
                "IN"."StrikePrice" = "OLD"."StrikePrice" and
                "IN"."ValuationMethod" = "OLD"."ValuationMethod" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_DerivativesAccount.FinancialContractID" = "OLD"."_DerivativesAccount.FinancialContractID" and
                "IN"."_DerivativesAccount.IDSystem" = "OLD"."_DerivativesAccount.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        );

END
