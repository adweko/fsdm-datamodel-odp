PROCEDURE "sap.fsdm.procedures::RegulatoryIndicatorForCollectionEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::RegulatoryIndicatorForCollectionTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::RegulatoryIndicatorForCollectionTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::RegulatoryIndicatorForCollectionTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Criterion" is null and
            "Regulation" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Criterion" ,
                "Regulation" ,
                "_Collection.CollectionID" ,
                "_Collection.IDSystem" ,
                "_Collection._Client.BusinessPartnerID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Criterion" ,
                "OLD"."Regulation" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RegulatoryIndicatorForCollection" "OLD"
            on
                "IN"."Criterion" = "OLD"."Criterion" and
                "IN"."Regulation" = "OLD"."Regulation" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Criterion" ,
            "Regulation" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Criterion" ,
                "OLD"."Regulation" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RegulatoryIndicatorForCollection_Historical" "OLD"
            on
                "IN"."Criterion" = "OLD"."Criterion" and
                "IN"."Regulation" = "OLD"."Regulation" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" 
        );

END
