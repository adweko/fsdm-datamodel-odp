PROCEDURE "sap.fsdm.procedures::InterestRateOptionComponentEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::InterestRateOptionComponentTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::InterestRateOptionComponentTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::InterestRateOptionComponentTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ComponentNumber" is null and
            "_InterestRateOption.FinancialContractID" is null and
            "_InterestRateOption.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "ComponentNumber" ,
                "_InterestRateOption.FinancialContractID" ,
                "_InterestRateOption.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."ComponentNumber" ,
                "OLD"."_InterestRateOption.FinancialContractID" ,
                "OLD"."_InterestRateOption.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InterestRateOptionComponent" "OLD"
            on
                "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
                "IN"."_InterestRateOption.FinancialContractID" = "OLD"."_InterestRateOption.FinancialContractID" and
                "IN"."_InterestRateOption.IDSystem" = "OLD"."_InterestRateOption.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "ComponentNumber" ,
            "_InterestRateOption.FinancialContractID" ,
            "_InterestRateOption.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."ComponentNumber" ,
                "OLD"."_InterestRateOption.FinancialContractID" ,
                "OLD"."_InterestRateOption.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InterestRateOptionComponent_Historical" "OLD"
            on
                "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
                "IN"."_InterestRateOption.FinancialContractID" = "OLD"."_InterestRateOption.FinancialContractID" and
                "IN"."_InterestRateOption.IDSystem" = "OLD"."_InterestRateOption.IDSystem" 
        );

END
