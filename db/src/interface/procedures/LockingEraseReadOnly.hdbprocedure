PROCEDURE "sap.fsdm.procedures::LockingEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::LockingTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::LockingTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::LockingTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "LockingType" is null and
            "_AccountingEntityGLAccount.CompanyCode" is null and
            "_AccountingEntityGLAccount._GLAccount.GLAccount" is null and
            "_AccountingEntityGLAccount._GLAccount._ChartOfAccounts.ChartOfAccountId" is null and
            "_ContractBundle.ContractBundleID" is null and
            "_LockingOfFee.SequenceNumber" is null and
            "_LockingOfFee.ASSOC_FinancialContract.FinancialContractID" is null and
            "_LockingOfFee.ASSOC_FinancialContract.IDSystem" is null and
            "_LockingOfFee.ASSOC_TransferOrder.IDSystem" is null and
            "_LockingOfFee.ASSOC_TransferOrder.TransferOrderID" is null and
            "_LockingOfFee._LeaseService.ID" is null and
            "_LockingOfFee._LeaseService._LeaseServiceAgreement.FinancialContractID" is null and
            "_LockingOfFee._LeaseService._LeaseServiceAgreement.IDSystem" is null and
            "_LockingOfFee._Trade.IDSystem" is null and
            "_LockingOfFee._Trade.TradeID" is null and
            "_LockingOfFee._TrancheInSyndication.TrancheSequenceNumber" is null and
            "_LockingOfFee._TrancheInSyndication._SyndicationAgreement.FinancialContractID" is null and
            "_LockingOfFee._TrancheInSyndication._SyndicationAgreement.IDSystem" is null and
            "_LockingOfFinancialContract.FinancialContractID" is null and
            "_LockingOfFinancialContract.IDSystem" is null and
            "_LockingOfGLAccount.GLAccount" is null and
            "_LockingOfGLAccount._ChartOfAccounts.ChartOfAccountId" is null and
            "_LockingOfInterest.SequenceNumber" is null and
            "_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" is null and
            "_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" is null and
            "_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" is null and
            "_LockingOfInterest.ASSOC_FinancialContract.FinancialContractID" is null and
            "_LockingOfInterest.ASSOC_FinancialContract.IDSystem" is null and
            "_LockingOfInterest._DebtInstrument.FinancialInstrumentID" is null and
            "_LockingOfInterest._OptionOfReferenceRateSpecification.ComponentNumber" is null and
            "_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" is null and
            "_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" is null and
            "_LockingOfInterest._OptionOfStrikeSpecification.ComponentNumber" is null and
            "_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" is null and
            "_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" is null and
            "_LockingOfInterest._Trade.IDSystem" is null and
            "_LockingOfInterest._Trade.TradeID" is null and
            "_LockingOfPaymentSchedule.SequenceNumber" is null and
            "_LockingOfPaymentSchedule.ASSOC_FinancialContract.FinancialContractID" is null and
            "_LockingOfPaymentSchedule.ASSOC_FinancialContract.IDSystem" is null and
            "_LockingOfPaymentSchedule.ASSOC_Receivable.ReceivableID" is null and
            "_LockingOfPaymentSchedule._CreditCardLoan.ID" is null and
            "_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" is null and
            "_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" is null and
            "_LockingOfPaymentSchedule._DebtInstrument.FinancialInstrumentID" is null and
            "_LockingOfPaymentSchedule._InflationOptionComponent.ComponentNumber" is null and
            "_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID" is null and
            "_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.IDSystem" is null and
            "_LockingOfPaymentSchedule._InterestRateOptionComponent.ComponentNumber" is null and
            "_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" is null and
            "_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" is null and
            "_TradingLimit.LimitID" is null and
            "_TradingLimit._TimeBucket.MaturityBandID" is null and
            "_TradingLimit._TimeBucket.TimeBucketID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "LockingType" ,
                "_AccountingEntityGLAccount.CompanyCode" ,
                "_AccountingEntityGLAccount._GLAccount.GLAccount" ,
                "_AccountingEntityGLAccount._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "_ContractBundle.ContractBundleID" ,
                "_LockingOfFee.SequenceNumber" ,
                "_LockingOfFee.ASSOC_FinancialContract.FinancialContractID" ,
                "_LockingOfFee.ASSOC_FinancialContract.IDSystem" ,
                "_LockingOfFee.ASSOC_TransferOrder.IDSystem" ,
                "_LockingOfFee.ASSOC_TransferOrder.TransferOrderID" ,
                "_LockingOfFee._LeaseService.ID" ,
                "_LockingOfFee._LeaseService._LeaseServiceAgreement.FinancialContractID" ,
                "_LockingOfFee._LeaseService._LeaseServiceAgreement.IDSystem" ,
                "_LockingOfFee._Trade.IDSystem" ,
                "_LockingOfFee._Trade.TradeID" ,
                "_LockingOfFee._TrancheInSyndication.TrancheSequenceNumber" ,
                "_LockingOfFee._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "_LockingOfFee._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "_LockingOfFinancialContract.FinancialContractID" ,
                "_LockingOfFinancialContract.IDSystem" ,
                "_LockingOfGLAccount.GLAccount" ,
                "_LockingOfGLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "_LockingOfInterest.SequenceNumber" ,
                "_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" ,
                "_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" ,
                "_LockingOfInterest.ASSOC_FinancialContract.FinancialContractID" ,
                "_LockingOfInterest.ASSOC_FinancialContract.IDSystem" ,
                "_LockingOfInterest._DebtInstrument.FinancialInstrumentID" ,
                "_LockingOfInterest._OptionOfReferenceRateSpecification.ComponentNumber" ,
                "_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" ,
                "_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" ,
                "_LockingOfInterest._OptionOfStrikeSpecification.ComponentNumber" ,
                "_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" ,
                "_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" ,
                "_LockingOfInterest._Trade.IDSystem" ,
                "_LockingOfInterest._Trade.TradeID" ,
                "_LockingOfPaymentSchedule.SequenceNumber" ,
                "_LockingOfPaymentSchedule.ASSOC_FinancialContract.FinancialContractID" ,
                "_LockingOfPaymentSchedule.ASSOC_FinancialContract.IDSystem" ,
                "_LockingOfPaymentSchedule.ASSOC_Receivable.ReceivableID" ,
                "_LockingOfPaymentSchedule._CreditCardLoan.ID" ,
                "_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "_LockingOfPaymentSchedule._DebtInstrument.FinancialInstrumentID" ,
                "_LockingOfPaymentSchedule._InflationOptionComponent.ComponentNumber" ,
                "_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID" ,
                "_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.IDSystem" ,
                "_LockingOfPaymentSchedule._InterestRateOptionComponent.ComponentNumber" ,
                "_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "_TradingLimit.LimitID" ,
                "_TradingLimit._TimeBucket.MaturityBandID" ,
                "_TradingLimit._TimeBucket.TimeBucketID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."LockingType" ,
                "OLD"."_AccountingEntityGLAccount.CompanyCode" ,
                "OLD"."_AccountingEntityGLAccount._GLAccount.GLAccount" ,
                "OLD"."_AccountingEntityGLAccount._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_ContractBundle.ContractBundleID" ,
                "OLD"."_LockingOfFee.SequenceNumber" ,
                "OLD"."_LockingOfFee.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_LockingOfFee.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_LockingOfFee.ASSOC_TransferOrder.IDSystem" ,
                "OLD"."_LockingOfFee.ASSOC_TransferOrder.TransferOrderID" ,
                "OLD"."_LockingOfFee._LeaseService.ID" ,
                "OLD"."_LockingOfFee._LeaseService._LeaseServiceAgreement.FinancialContractID" ,
                "OLD"."_LockingOfFee._LeaseService._LeaseServiceAgreement.IDSystem" ,
                "OLD"."_LockingOfFee._Trade.IDSystem" ,
                "OLD"."_LockingOfFee._Trade.TradeID" ,
                "OLD"."_LockingOfFee._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."_LockingOfFinancialContract.FinancialContractID" ,
                "OLD"."_LockingOfFinancialContract.IDSystem" ,
                "OLD"."_LockingOfGLAccount.GLAccount" ,
                "OLD"."_LockingOfGLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_LockingOfInterest.SequenceNumber" ,
                "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" ,
                "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_LockingOfInterest.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_LockingOfInterest.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_LockingOfInterest._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification.ComponentNumber" ,
                "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" ,
                "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" ,
                "OLD"."_LockingOfInterest._OptionOfStrikeSpecification.ComponentNumber" ,
                "OLD"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" ,
                "OLD"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" ,
                "OLD"."_LockingOfInterest._Trade.IDSystem" ,
                "OLD"."_LockingOfInterest._Trade.TradeID" ,
                "OLD"."_LockingOfPaymentSchedule.SequenceNumber" ,
                "OLD"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_LockingOfPaymentSchedule.ASSOC_Receivable.ReceivableID" ,
                "OLD"."_LockingOfPaymentSchedule._CreditCardLoan.ID" ,
                "OLD"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "OLD"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "OLD"."_LockingOfPaymentSchedule._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent.ComponentNumber" ,
                "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID" ,
                "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.IDSystem" ,
                "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent.ComponentNumber" ,
                "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "OLD"."_TradingLimit.LimitID" ,
                "OLD"."_TradingLimit._TimeBucket.MaturityBandID" ,
                "OLD"."_TradingLimit._TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Locking" "OLD"
            on
                "IN"."LockingType" = "OLD"."LockingType" and
                "IN"."_AccountingEntityGLAccount.CompanyCode" = "OLD"."_AccountingEntityGLAccount.CompanyCode" and
                "IN"."_AccountingEntityGLAccount._GLAccount.GLAccount" = "OLD"."_AccountingEntityGLAccount._GLAccount.GLAccount" and
                "IN"."_AccountingEntityGLAccount._GLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_AccountingEntityGLAccount._GLAccount._ChartOfAccounts.ChartOfAccountId" and
                "IN"."_ContractBundle.ContractBundleID" = "OLD"."_ContractBundle.ContractBundleID" and
                "IN"."_LockingOfFee.SequenceNumber" = "OLD"."_LockingOfFee.SequenceNumber" and
                "IN"."_LockingOfFee.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_LockingOfFee.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."_LockingOfFee.ASSOC_FinancialContract.IDSystem" = "OLD"."_LockingOfFee.ASSOC_FinancialContract.IDSystem" and
                "IN"."_LockingOfFee.ASSOC_TransferOrder.IDSystem" = "OLD"."_LockingOfFee.ASSOC_TransferOrder.IDSystem" and
                "IN"."_LockingOfFee.ASSOC_TransferOrder.TransferOrderID" = "OLD"."_LockingOfFee.ASSOC_TransferOrder.TransferOrderID" and
                "IN"."_LockingOfFee._LeaseService.ID" = "OLD"."_LockingOfFee._LeaseService.ID" and
                "IN"."_LockingOfFee._LeaseService._LeaseServiceAgreement.FinancialContractID" = "OLD"."_LockingOfFee._LeaseService._LeaseServiceAgreement.FinancialContractID" and
                "IN"."_LockingOfFee._LeaseService._LeaseServiceAgreement.IDSystem" = "OLD"."_LockingOfFee._LeaseService._LeaseServiceAgreement.IDSystem" and
                "IN"."_LockingOfFee._Trade.IDSystem" = "OLD"."_LockingOfFee._Trade.IDSystem" and
                "IN"."_LockingOfFee._Trade.TradeID" = "OLD"."_LockingOfFee._Trade.TradeID" and
                "IN"."_LockingOfFee._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_LockingOfFee._TrancheInSyndication.TrancheSequenceNumber" and
                "IN"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                "IN"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.IDSystem" and
                "IN"."_LockingOfFinancialContract.FinancialContractID" = "OLD"."_LockingOfFinancialContract.FinancialContractID" and
                "IN"."_LockingOfFinancialContract.IDSystem" = "OLD"."_LockingOfFinancialContract.IDSystem" and
                "IN"."_LockingOfGLAccount.GLAccount" = "OLD"."_LockingOfGLAccount.GLAccount" and
                "IN"."_LockingOfGLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_LockingOfGLAccount._ChartOfAccounts.ChartOfAccountId" and
                "IN"."_LockingOfInterest.SequenceNumber" = "OLD"."_LockingOfInterest.SequenceNumber" and
                "IN"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" and
                "IN"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."_LockingOfInterest.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_LockingOfInterest.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."_LockingOfInterest.ASSOC_FinancialContract.IDSystem" = "OLD"."_LockingOfInterest.ASSOC_FinancialContract.IDSystem" and
                "IN"."_LockingOfInterest._DebtInstrument.FinancialInstrumentID" = "OLD"."_LockingOfInterest._DebtInstrument.FinancialInstrumentID" and
                "IN"."_LockingOfInterest._OptionOfReferenceRateSpecification.ComponentNumber" = "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification.ComponentNumber" and
                "IN"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" and
                "IN"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" and
                "IN"."_LockingOfInterest._OptionOfStrikeSpecification.ComponentNumber" = "OLD"."_LockingOfInterest._OptionOfStrikeSpecification.ComponentNumber" and
                "IN"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "OLD"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" and
                "IN"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "OLD"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" and
                "IN"."_LockingOfInterest._Trade.IDSystem" = "OLD"."_LockingOfInterest._Trade.IDSystem" and
                "IN"."_LockingOfInterest._Trade.TradeID" = "OLD"."_LockingOfInterest._Trade.TradeID" and
                "IN"."_LockingOfPaymentSchedule.SequenceNumber" = "OLD"."_LockingOfPaymentSchedule.SequenceNumber" and
                "IN"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.IDSystem" = "OLD"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.IDSystem" and
                "IN"."_LockingOfPaymentSchedule.ASSOC_Receivable.ReceivableID" = "OLD"."_LockingOfPaymentSchedule.ASSOC_Receivable.ReceivableID" and
                "IN"."_LockingOfPaymentSchedule._CreditCardLoan.ID" = "OLD"."_LockingOfPaymentSchedule._CreditCardLoan.ID" and
                "IN"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" and
                "IN"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" and
                "IN"."_LockingOfPaymentSchedule._DebtInstrument.FinancialInstrumentID" = "OLD"."_LockingOfPaymentSchedule._DebtInstrument.FinancialInstrumentID" and
                "IN"."_LockingOfPaymentSchedule._InflationOptionComponent.ComponentNumber" = "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent.ComponentNumber" and
                "IN"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID" = "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID" and
                "IN"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.IDSystem" = "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.IDSystem" and
                "IN"."_LockingOfPaymentSchedule._InterestRateOptionComponent.ComponentNumber" = "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent.ComponentNumber" and
                "IN"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" and
                "IN"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" = "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" and
                "IN"."_TradingLimit.LimitID" = "OLD"."_TradingLimit.LimitID" and
                "IN"."_TradingLimit._TimeBucket.MaturityBandID" = "OLD"."_TradingLimit._TimeBucket.MaturityBandID" and
                "IN"."_TradingLimit._TimeBucket.TimeBucketID" = "OLD"."_TradingLimit._TimeBucket.TimeBucketID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "LockingType" ,
            "_AccountingEntityGLAccount.CompanyCode" ,
            "_AccountingEntityGLAccount._GLAccount.GLAccount" ,
            "_AccountingEntityGLAccount._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
            "_ContractBundle.ContractBundleID" ,
            "_LockingOfFee.SequenceNumber" ,
            "_LockingOfFee.ASSOC_FinancialContract.FinancialContractID" ,
            "_LockingOfFee.ASSOC_FinancialContract.IDSystem" ,
            "_LockingOfFee.ASSOC_TransferOrder.IDSystem" ,
            "_LockingOfFee.ASSOC_TransferOrder.TransferOrderID" ,
            "_LockingOfFee._LeaseService.ID" ,
            "_LockingOfFee._LeaseService._LeaseServiceAgreement.FinancialContractID" ,
            "_LockingOfFee._LeaseService._LeaseServiceAgreement.IDSystem" ,
            "_LockingOfFee._Trade.IDSystem" ,
            "_LockingOfFee._Trade.TradeID" ,
            "_LockingOfFee._TrancheInSyndication.TrancheSequenceNumber" ,
            "_LockingOfFee._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_LockingOfFee._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "_LockingOfFinancialContract.FinancialContractID" ,
            "_LockingOfFinancialContract.IDSystem" ,
            "_LockingOfGLAccount.GLAccount" ,
            "_LockingOfGLAccount._ChartOfAccounts.ChartOfAccountId" ,
            "_LockingOfInterest.SequenceNumber" ,
            "_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" ,
            "_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_LockingOfInterest.ASSOC_FinancialContract.FinancialContractID" ,
            "_LockingOfInterest.ASSOC_FinancialContract.IDSystem" ,
            "_LockingOfInterest._DebtInstrument.FinancialInstrumentID" ,
            "_LockingOfInterest._OptionOfReferenceRateSpecification.ComponentNumber" ,
            "_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" ,
            "_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" ,
            "_LockingOfInterest._OptionOfStrikeSpecification.ComponentNumber" ,
            "_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" ,
            "_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" ,
            "_LockingOfInterest._Trade.IDSystem" ,
            "_LockingOfInterest._Trade.TradeID" ,
            "_LockingOfPaymentSchedule.SequenceNumber" ,
            "_LockingOfPaymentSchedule.ASSOC_FinancialContract.FinancialContractID" ,
            "_LockingOfPaymentSchedule.ASSOC_FinancialContract.IDSystem" ,
            "_LockingOfPaymentSchedule.ASSOC_Receivable.ReceivableID" ,
            "_LockingOfPaymentSchedule._CreditCardLoan.ID" ,
            "_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
            "_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" ,
            "_LockingOfPaymentSchedule._DebtInstrument.FinancialInstrumentID" ,
            "_LockingOfPaymentSchedule._InflationOptionComponent.ComponentNumber" ,
            "_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID" ,
            "_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.IDSystem" ,
            "_LockingOfPaymentSchedule._InterestRateOptionComponent.ComponentNumber" ,
            "_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
            "_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
            "_TradingLimit.LimitID" ,
            "_TradingLimit._TimeBucket.MaturityBandID" ,
            "_TradingLimit._TimeBucket.TimeBucketID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."LockingType" ,
                "OLD"."_AccountingEntityGLAccount.CompanyCode" ,
                "OLD"."_AccountingEntityGLAccount._GLAccount.GLAccount" ,
                "OLD"."_AccountingEntityGLAccount._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_ContractBundle.ContractBundleID" ,
                "OLD"."_LockingOfFee.SequenceNumber" ,
                "OLD"."_LockingOfFee.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_LockingOfFee.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_LockingOfFee.ASSOC_TransferOrder.IDSystem" ,
                "OLD"."_LockingOfFee.ASSOC_TransferOrder.TransferOrderID" ,
                "OLD"."_LockingOfFee._LeaseService.ID" ,
                "OLD"."_LockingOfFee._LeaseService._LeaseServiceAgreement.FinancialContractID" ,
                "OLD"."_LockingOfFee._LeaseService._LeaseServiceAgreement.IDSystem" ,
                "OLD"."_LockingOfFee._Trade.IDSystem" ,
                "OLD"."_LockingOfFee._Trade.TradeID" ,
                "OLD"."_LockingOfFee._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."_LockingOfFinancialContract.FinancialContractID" ,
                "OLD"."_LockingOfFinancialContract.IDSystem" ,
                "OLD"."_LockingOfGLAccount.GLAccount" ,
                "OLD"."_LockingOfGLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_LockingOfInterest.SequenceNumber" ,
                "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" ,
                "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_LockingOfInterest.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_LockingOfInterest.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_LockingOfInterest._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification.ComponentNumber" ,
                "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" ,
                "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" ,
                "OLD"."_LockingOfInterest._OptionOfStrikeSpecification.ComponentNumber" ,
                "OLD"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" ,
                "OLD"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" ,
                "OLD"."_LockingOfInterest._Trade.IDSystem" ,
                "OLD"."_LockingOfInterest._Trade.TradeID" ,
                "OLD"."_LockingOfPaymentSchedule.SequenceNumber" ,
                "OLD"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_LockingOfPaymentSchedule.ASSOC_Receivable.ReceivableID" ,
                "OLD"."_LockingOfPaymentSchedule._CreditCardLoan.ID" ,
                "OLD"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "OLD"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "OLD"."_LockingOfPaymentSchedule._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent.ComponentNumber" ,
                "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID" ,
                "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.IDSystem" ,
                "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent.ComponentNumber" ,
                "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "OLD"."_TradingLimit.LimitID" ,
                "OLD"."_TradingLimit._TimeBucket.MaturityBandID" ,
                "OLD"."_TradingLimit._TimeBucket.TimeBucketID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Locking_Historical" "OLD"
            on
                "IN"."LockingType" = "OLD"."LockingType" and
                "IN"."_AccountingEntityGLAccount.CompanyCode" = "OLD"."_AccountingEntityGLAccount.CompanyCode" and
                "IN"."_AccountingEntityGLAccount._GLAccount.GLAccount" = "OLD"."_AccountingEntityGLAccount._GLAccount.GLAccount" and
                "IN"."_AccountingEntityGLAccount._GLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_AccountingEntityGLAccount._GLAccount._ChartOfAccounts.ChartOfAccountId" and
                "IN"."_ContractBundle.ContractBundleID" = "OLD"."_ContractBundle.ContractBundleID" and
                "IN"."_LockingOfFee.SequenceNumber" = "OLD"."_LockingOfFee.SequenceNumber" and
                "IN"."_LockingOfFee.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_LockingOfFee.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."_LockingOfFee.ASSOC_FinancialContract.IDSystem" = "OLD"."_LockingOfFee.ASSOC_FinancialContract.IDSystem" and
                "IN"."_LockingOfFee.ASSOC_TransferOrder.IDSystem" = "OLD"."_LockingOfFee.ASSOC_TransferOrder.IDSystem" and
                "IN"."_LockingOfFee.ASSOC_TransferOrder.TransferOrderID" = "OLD"."_LockingOfFee.ASSOC_TransferOrder.TransferOrderID" and
                "IN"."_LockingOfFee._LeaseService.ID" = "OLD"."_LockingOfFee._LeaseService.ID" and
                "IN"."_LockingOfFee._LeaseService._LeaseServiceAgreement.FinancialContractID" = "OLD"."_LockingOfFee._LeaseService._LeaseServiceAgreement.FinancialContractID" and
                "IN"."_LockingOfFee._LeaseService._LeaseServiceAgreement.IDSystem" = "OLD"."_LockingOfFee._LeaseService._LeaseServiceAgreement.IDSystem" and
                "IN"."_LockingOfFee._Trade.IDSystem" = "OLD"."_LockingOfFee._Trade.IDSystem" and
                "IN"."_LockingOfFee._Trade.TradeID" = "OLD"."_LockingOfFee._Trade.TradeID" and
                "IN"."_LockingOfFee._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_LockingOfFee._TrancheInSyndication.TrancheSequenceNumber" and
                "IN"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                "IN"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_LockingOfFee._TrancheInSyndication._SyndicationAgreement.IDSystem" and
                "IN"."_LockingOfFinancialContract.FinancialContractID" = "OLD"."_LockingOfFinancialContract.FinancialContractID" and
                "IN"."_LockingOfFinancialContract.IDSystem" = "OLD"."_LockingOfFinancialContract.IDSystem" and
                "IN"."_LockingOfGLAccount.GLAccount" = "OLD"."_LockingOfGLAccount.GLAccount" and
                "IN"."_LockingOfGLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_LockingOfGLAccount._ChartOfAccounts.ChartOfAccountId" and
                "IN"."_LockingOfInterest.SequenceNumber" = "OLD"."_LockingOfInterest.SequenceNumber" and
                "IN"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" and
                "IN"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."_LockingOfInterest.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_LockingOfInterest.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."_LockingOfInterest.ASSOC_FinancialContract.IDSystem" = "OLD"."_LockingOfInterest.ASSOC_FinancialContract.IDSystem" and
                "IN"."_LockingOfInterest._DebtInstrument.FinancialInstrumentID" = "OLD"."_LockingOfInterest._DebtInstrument.FinancialInstrumentID" and
                "IN"."_LockingOfInterest._OptionOfReferenceRateSpecification.ComponentNumber" = "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification.ComponentNumber" and
                "IN"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" and
                "IN"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "OLD"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" and
                "IN"."_LockingOfInterest._OptionOfStrikeSpecification.ComponentNumber" = "OLD"."_LockingOfInterest._OptionOfStrikeSpecification.ComponentNumber" and
                "IN"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "OLD"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" and
                "IN"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "OLD"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" and
                "IN"."_LockingOfInterest._Trade.IDSystem" = "OLD"."_LockingOfInterest._Trade.IDSystem" and
                "IN"."_LockingOfInterest._Trade.TradeID" = "OLD"."_LockingOfInterest._Trade.TradeID" and
                "IN"."_LockingOfPaymentSchedule.SequenceNumber" = "OLD"."_LockingOfPaymentSchedule.SequenceNumber" and
                "IN"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.IDSystem" = "OLD"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.IDSystem" and
                "IN"."_LockingOfPaymentSchedule.ASSOC_Receivable.ReceivableID" = "OLD"."_LockingOfPaymentSchedule.ASSOC_Receivable.ReceivableID" and
                "IN"."_LockingOfPaymentSchedule._CreditCardLoan.ID" = "OLD"."_LockingOfPaymentSchedule._CreditCardLoan.ID" and
                "IN"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" and
                "IN"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" and
                "IN"."_LockingOfPaymentSchedule._DebtInstrument.FinancialInstrumentID" = "OLD"."_LockingOfPaymentSchedule._DebtInstrument.FinancialInstrumentID" and
                "IN"."_LockingOfPaymentSchedule._InflationOptionComponent.ComponentNumber" = "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent.ComponentNumber" and
                "IN"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID" = "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID" and
                "IN"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.IDSystem" = "OLD"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.IDSystem" and
                "IN"."_LockingOfPaymentSchedule._InterestRateOptionComponent.ComponentNumber" = "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent.ComponentNumber" and
                "IN"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" and
                "IN"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" = "OLD"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" and
                "IN"."_TradingLimit.LimitID" = "OLD"."_TradingLimit.LimitID" and
                "IN"."_TradingLimit._TimeBucket.MaturityBandID" = "OLD"."_TradingLimit._TimeBucket.MaturityBandID" and
                "IN"."_TradingLimit._TimeBucket.TimeBucketID" = "OLD"."_TradingLimit._TimeBucket.TimeBucketID" 
        );

END
