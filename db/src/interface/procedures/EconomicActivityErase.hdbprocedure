PROCEDURE "sap.fsdm.procedures::EconomicActivityErase" (IN ROW "sap.fsdm.tabletypes::EconomicActivityTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "EconomicActivityID" is null and
            "TaxonomyID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::EconomicActivity"
        WHERE
        (            "EconomicActivityID" ,
            "TaxonomyID" 
        ) in
        (
            select                 "OLD"."EconomicActivityID" ,
                "OLD"."TaxonomyID" 
            from :ROW "IN"
            inner join "sap.fsdm::EconomicActivity" "OLD"
            on
            "IN"."EconomicActivityID" = "OLD"."EconomicActivityID" and
            "IN"."TaxonomyID" = "OLD"."TaxonomyID" 
        );

        --delete data from history table
        delete from "sap.fsdm::EconomicActivity_Historical"
        WHERE
        (
            "EconomicActivityID" ,
            "TaxonomyID" 
        ) in
        (
            select
                "OLD"."EconomicActivityID" ,
                "OLD"."TaxonomyID" 
            from :ROW "IN"
            inner join "sap.fsdm::EconomicActivity_Historical" "OLD"
            on
                "IN"."EconomicActivityID" = "OLD"."EconomicActivityID" and
                "IN"."TaxonomyID" = "OLD"."TaxonomyID" 
        );

END
