view "sap.fsdm.SQLViews::SubledgerAccountbalance_View" 
as select
      "AccountingBalanceType" ,
      "AccountingChangeReason" ,
      "AssetLiabilityStatus" ,
      "FairValueLevel" ,
      "FiscalYear" ,
      "HoldingCategory" ,
      "ImpairmentStatus" ,
      "MovementType" ,
      "PostingDate" ,
      "PostingDirection" ,
      "TransactionCurrency" ,
      "_AccountingSystem.AccountingSystemID" ,
      "_BusinessPartner.BusinessPartnerID" ,
      "_BusinessSegment.IDSystem" ,
      "_BusinessSegment.OrganizationalUnitID" ,
      "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_BusinessSegmentAtCounterparty.IDSystem" ,
      "_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
      "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_CompanyCode.CompanyCode" ,
      "_CompanyCode.ASSOC_Company.BusinessPartnerID" ,
      "_CostCenter.IDSystem" ,
      "_CostCenter.OrganizationalUnitID" ,
      "_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_CostCenterAtCounterparty.IDSystem" ,
      "_CostCenterAtCounterparty.OrganizationalUnitID" ,
      "_CostCenterAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_FinancialContract.FinancialContractID" ,
      "_FinancialContract.IDSystem" ,
      "_FinancialInstrument.FinancialInstrumentID" ,
      "_GLAccount.GLAccount" ,
      "_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
      "_PlanBudgetForecast.ID" ,
      "_PlanBudgetForecast.PlanBudgetForecastScenario" ,
      "_PlanBudgetForecast.VersionID" ,
      "_ProductCatalogItem.ProductCatalogItem" ,
      "_ProductCatalogItem._ProductCatalog.CatalogID" ,
      "_ProductClass.ProductClass" ,
      "_ProductClass.ProductClassificationSystem" ,
      "_ProfitCenter.IDSystem" ,
      "_ProfitCenter.OrganizationalUnitID" ,
      "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_ProftCenterAtCouterparty.IDSystem" ,
      "_ProftCenterAtCouterparty.OrganizationalUnitID" ,
      "_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_SecuritiesAccount.FinancialContractID" ,
      "_SecuritiesAccount.IDSystem" ,
      "_SubledgerAccount.SubledgerAccount" ,
      "BusinessValidFrom" ,
      "BusinessValidTo" ,
      "SystemValidFrom" ,
      "SystemValidTo" ,
      "AmountInFunctionalCurrency" ,
      "AmountInGroupCurrency" ,
      "AmountInHardCurrency" ,
      "AmountInIndexCurrency" ,
      "AmountInLocalCurrency" ,
      "AmountInTransactionCurrency" ,
      "FiscalPeriodEnd" ,
      "FiscalPeriodStart" ,
      "FunctionalCurrency" ,
      "GroupCurrency" ,
      "HardCurrency" ,
      "IndexCurrency" ,
      "LocalCurrency" ,
      "SourceSystemID" ,
      "ChangeTimestampInSourceSystem" ,
      "ChangingUserInSourceSystem" ,
      "ChangingProcessType" ,
      "ChangingProcessID" 
  
from "sap.fsdm::SubledgerAccountbalance"
with associations
(
	join "sap.fsdm.SQLViews::AccountingSystem_View" as "_AccountingSystem"
	  on "_AccountingSystem"."AccountingSystemID" = "_AccountingSystem.AccountingSystemID"
	     ,
	join "sap.fsdm.SQLViews::BusinessPartner_View" as "_BusinessPartner"
	  on "_BusinessPartner"."BusinessPartnerID" = "_BusinessPartner.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_BusinessSegment"
	  on "_BusinessSegment"."IDSystem" = "_BusinessSegment.IDSystem" AND 
	     "_BusinessSegment"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AND 
	     "_BusinessSegment"."OrganizationalUnitID" = "_BusinessSegment.OrganizationalUnitID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_BusinessSegmentAtCounterparty"
	  on "_BusinessSegmentAtCounterparty"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AND 
	     "_BusinessSegmentAtCounterparty"."OrganizationalUnitID" = "_BusinessSegmentAtCounterparty.OrganizationalUnitID" AND 
	     "_BusinessSegmentAtCounterparty"."IDSystem" = "_BusinessSegmentAtCounterparty.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::CompanyCode_View" as "_CompanyCode"
	  on "_CompanyCode"."CompanyCode" = "_CompanyCode.CompanyCode" AND 
	     "_CompanyCode"."ASSOC_Company.BusinessPartnerID" = "_CompanyCode.ASSOC_Company.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_CostCenter"
	  on "_CostCenter"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AND 
	     "_CostCenter"."IDSystem" = "_CostCenter.IDSystem" AND 
	     "_CostCenter"."OrganizationalUnitID" = "_CostCenter.OrganizationalUnitID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_CostCenterAtCounterparty"
	  on "_CostCenterAtCounterparty"."OrganizationalUnitID" = "_CostCenterAtCounterparty.OrganizationalUnitID" AND 
	     "_CostCenterAtCounterparty"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_CostCenterAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AND 
	     "_CostCenterAtCounterparty"."IDSystem" = "_CostCenterAtCounterparty.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "_FinancialContract"
	  on "_FinancialContract"."FinancialContractID" = "_FinancialContract.FinancialContractID" AND 
	     "_FinancialContract"."IDSystem" = "_FinancialContract.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::FinancialInstrument_View" as "_FinancialInstrument"
	  on "_FinancialInstrument"."FinancialInstrumentID" = "_FinancialInstrument.FinancialInstrumentID"
	     ,
	join "sap.fsdm.SQLViews::GLAccount_View" as "_GLAccount"
	  on "_GLAccount"."_ChartOfAccounts.ChartOfAccountId" = "_GLAccount._ChartOfAccounts.ChartOfAccountId" AND 
	     "_GLAccount"."GLAccount" = "_GLAccount.GLAccount"
	     ,
	join "sap.fsdm.SQLViews::PlanBudgetForecast_View" as "_PlanBudgetForecast"
	  on "_PlanBudgetForecast"."VersionID" = "_PlanBudgetForecast.VersionID" AND 
	     "_PlanBudgetForecast"."ID" = "_PlanBudgetForecast.ID" AND 
	     "_PlanBudgetForecast"."PlanBudgetForecastScenario" = "_PlanBudgetForecast.PlanBudgetForecastScenario"
	     ,
	join "sap.fsdm.SQLViews::ProductCatalogItem_View" as "_ProductCatalogItem"
	  on "_ProductCatalogItem"."ProductCatalogItem" = "_ProductCatalogItem.ProductCatalogItem" AND 
	     "_ProductCatalogItem"."_ProductCatalog.CatalogID" = "_ProductCatalogItem._ProductCatalog.CatalogID"
	     ,
	join "sap.fsdm.SQLViews::ProductClass_View" as "_ProductClass"
	  on "_ProductClass"."ProductClass" = "_ProductClass.ProductClass" AND 
	     "_ProductClass"."ProductClassificationSystem" = "_ProductClass.ProductClassificationSystem"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_ProfitCenter"
	  on "_ProfitCenter"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AND 
	     "_ProfitCenter"."OrganizationalUnitID" = "_ProfitCenter.OrganizationalUnitID" AND 
	     "_ProfitCenter"."IDSystem" = "_ProfitCenter.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_ProftCenterAtCouterparty"
	  on "_ProftCenterAtCouterparty"."IDSystem" = "_ProftCenterAtCouterparty.IDSystem" AND 
	     "_ProftCenterAtCouterparty"."OrganizationalUnitID" = "_ProftCenterAtCouterparty.OrganizationalUnitID" AND 
	     "_ProftCenterAtCouterparty"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "_SecuritiesAccount"
	  on "_SecuritiesAccount"."IDSystem" = "_SecuritiesAccount.IDSystem" AND 
	     "_SecuritiesAccount"."FinancialContractID" = "_SecuritiesAccount.FinancialContractID"
	     ,
	join "sap.fsdm.SQLViews::SubledgerAccount_View" as "_SubledgerAccount"
	  on "_SubledgerAccount"."SubledgerAccount" = "_SubledgerAccount.SubledgerAccount"
	     
);