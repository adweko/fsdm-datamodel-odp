view "sap.fsdm.SQLViews::GeneralLedgerAccountBalance_View" 
as select
      "AccountingBalanceType" ,
      "FiscalYear" ,
      "MovementType" ,
      "PostingDate" ,
      "PostingDirection" ,
      "TransactionCurrency" ,
      "_AccountingSystem.AccountingSystemID" ,
      "_BusinessSegment.IDSystem" ,
      "_BusinessSegment.OrganizationalUnitID" ,
      "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_BusinessSegmentAtCounterparty.IDSystem" ,
      "_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
      "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_CompanyCode.CompanyCode" ,
      "_CompanyCode.ASSOC_Company.BusinessPartnerID" ,
      "_CostCenter.IDSystem" ,
      "_CostCenter.OrganizationalUnitID" ,
      "_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_CostCenterAtCounterparty.IDSystem" ,
      "_CostCenterAtCounterparty.OrganizationalUnitID" ,
      "_CostCenterAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_GLAccount.GLAccount" ,
      "_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
      "_PlanBudgetForecast.ID" ,
      "_PlanBudgetForecast.PlanBudgetForecastScenario" ,
      "_PlanBudgetForecast.VersionID" ,
      "_ProductCatalogItem.ProductCatalogItem" ,
      "_ProductCatalogItem._ProductCatalog.CatalogID" ,
      "_ProductClass.ProductClass" ,
      "_ProductClass.ProductClassificationSystem" ,
      "_ProfitCenter.IDSystem" ,
      "_ProfitCenter.OrganizationalUnitID" ,
      "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_ProftCenterAtCouterparty.IDSystem" ,
      "_ProftCenterAtCouterparty.OrganizationalUnitID" ,
      "_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "BusinessValidFrom" ,
      "BusinessValidTo" ,
      "SystemValidFrom" ,
      "SystemValidTo" ,
      "AmountInFunctionalCurrency" ,
      "AmountInGroupCurrency" ,
      "AmountInHardCurrency" ,
      "AmountInIndexCurrency" ,
      "AmountInLocalCurrency" ,
      "AmountInTransactionCurrency" ,
      "FiscalPeriodEnd" ,
      "FiscalPeriodStart" ,
      "FunctionalCurrency" ,
      "GroupCurrency" ,
      "HardCurrency" ,
      "IndexCurrency" ,
      "LocalCurrency" ,
      "SourceSystemID" ,
      "ChangeTimestampInSourceSystem" ,
      "ChangingUserInSourceSystem" ,
      "ChangingProcessType" ,
      "ChangingProcessID" 
  
from "sap.fsdm::GeneralLedgerAccountBalance"
with associations
(
	join "sap.fsdm.SQLViews::AccountingSystem_View" as "_AccountingSystem"
	  on "_AccountingSystem"."AccountingSystemID" = "_AccountingSystem.AccountingSystemID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_BusinessSegment"
	  on "_BusinessSegment"."OrganizationalUnitID" = "_BusinessSegment.OrganizationalUnitID" AND 
	     "_BusinessSegment"."IDSystem" = "_BusinessSegment.IDSystem" AND 
	     "_BusinessSegment"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_BusinessSegmentAtCounterparty"
	  on "_BusinessSegmentAtCounterparty"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AND 
	     "_BusinessSegmentAtCounterparty"."OrganizationalUnitID" = "_BusinessSegmentAtCounterparty.OrganizationalUnitID" AND 
	     "_BusinessSegmentAtCounterparty"."IDSystem" = "_BusinessSegmentAtCounterparty.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::CompanyCode_View" as "_CompanyCode"
	  on "_CompanyCode"."ASSOC_Company.BusinessPartnerID" = "_CompanyCode.ASSOC_Company.BusinessPartnerID" AND 
	     "_CompanyCode"."CompanyCode" = "_CompanyCode.CompanyCode"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_CostCenter"
	  on "_CostCenter"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_CostCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AND 
	     "_CostCenter"."IDSystem" = "_CostCenter.IDSystem" AND 
	     "_CostCenter"."OrganizationalUnitID" = "_CostCenter.OrganizationalUnitID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_CostCenterAtCounterparty"
	  on "_CostCenterAtCounterparty"."IDSystem" = "_CostCenterAtCounterparty.IDSystem" AND 
	     "_CostCenterAtCounterparty"."OrganizationalUnitID" = "_CostCenterAtCounterparty.OrganizationalUnitID" AND 
	     "_CostCenterAtCounterparty"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_CostCenterAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::GLAccount_View" as "_GLAccount"
	  on "_GLAccount"."GLAccount" = "_GLAccount.GLAccount" AND 
	     "_GLAccount"."_ChartOfAccounts.ChartOfAccountId" = "_GLAccount._ChartOfAccounts.ChartOfAccountId"
	     ,
	join "sap.fsdm.SQLViews::PlanBudgetForecast_View" as "_PlanBudgetForecast"
	  on "_PlanBudgetForecast"."ID" = "_PlanBudgetForecast.ID" AND 
	     "_PlanBudgetForecast"."VersionID" = "_PlanBudgetForecast.VersionID" AND 
	     "_PlanBudgetForecast"."PlanBudgetForecastScenario" = "_PlanBudgetForecast.PlanBudgetForecastScenario"
	     ,
	join "sap.fsdm.SQLViews::ProductCatalogItem_View" as "_ProductCatalogItem"
	  on "_ProductCatalogItem"."_ProductCatalog.CatalogID" = "_ProductCatalogItem._ProductCatalog.CatalogID" AND 
	     "_ProductCatalogItem"."ProductCatalogItem" = "_ProductCatalogItem.ProductCatalogItem"
	     ,
	join "sap.fsdm.SQLViews::ProductClass_View" as "_ProductClass"
	  on "_ProductClass"."ProductClassificationSystem" = "_ProductClass.ProductClassificationSystem" AND 
	     "_ProductClass"."ProductClass" = "_ProductClass.ProductClass"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_ProfitCenter"
	  on "_ProfitCenter"."IDSystem" = "_ProfitCenter.IDSystem" AND 
	     "_ProfitCenter"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AND 
	     "_ProfitCenter"."OrganizationalUnitID" = "_ProfitCenter.OrganizationalUnitID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_ProftCenterAtCouterparty"
	  on "_ProftCenterAtCouterparty"."OrganizationalUnitID" = "_ProftCenterAtCouterparty.OrganizationalUnitID" AND 
	     "_ProftCenterAtCouterparty"."IDSystem" = "_ProftCenterAtCouterparty.IDSystem" AND 
	     "_ProftCenterAtCouterparty"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
	     
);