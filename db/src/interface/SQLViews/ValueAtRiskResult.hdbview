view "sap.fsdm.SQLViews::ValueAtRiskResult_View" 
as select
      "ConfidenceLevel" ,
      "RiskGroup" ,
      "RiskProvisionScenario" ,
      "RoleOfCurrency" ,
      "_ResultGroup.ResultDataProvider" ,
      "_ResultGroup.ResultGroupID" ,
      "_RiskReportingNode.RiskReportingNodeID" ,
      "BusinessValidFrom" ,
      "BusinessValidTo" ,
      "SystemValidFrom" ,
      "SystemValidTo" ,
      "Currency" ,
      "DecayFactor" ,
      "DeltaVariance" ,
      "ExpectedShortfall" ,
      "GammaVariance" ,
      "HistoricalPeriodEndDate" ,
      "HistoricalPeriodStartDate" ,
      "HoldingPeriod" ,
      "HoldingPeriodTimeUnit" ,
      "LowerBoundaryValue" ,
      "MeanNetPresentValue" ,
      "NumberOfSimulationRuns" ,
      "OverlapReturnHorizonPeriod" ,
      "OverlapReturnHorizonPeriodTimeUnit" ,
      "ReturnHorizonPeriod" ,
      "ReturnHorizonTimeUnit" ,
      "SimulatedMarketDataCreationMethod" ,
      "ThirdMomentDelta" ,
      "ThirdMomentGamma" ,
      "UpperBoundaryValue" ,
      "ValueAtRisk" ,
      "ValueAtRiskCalculationMethod" ,
      "ValueAtRiskResultCategory" ,
      "SourceSystemID" ,
      "ChangeTimestampInSourceSystem" ,
      "ChangingUserInSourceSystem" ,
      "ChangingProcessType" ,
      "ChangingProcessID" ,
      "P_0001210074_FinancialContract.FinancialContractID" ,
      "P_0001210074_FinancialContract.IDSystem" ,
      "P_0001210074_FinancialInstrument.FinancialInstrumentID" ,
      "P_0001210074_SecuritiesAccount.FinancialContractID" ,
      "P_0001210074_SecuritiesAccount.IDSystem" ,
      "P_0001210074_RiskGroup" ,
      "P_0001210074_HistoricalPeriodEndDate" ,
      "P_0001210074_HistoricalPeriodStartDate" ,
      "P_0001210074_ConfidenceLevel" ,
      "ASSOC_PositionCurrency.PositionCurrency" ,
      "P_0001210074_HypoNPV" ,
      "P_0001210074_MeanNPVUP" ,
      "P_0001210074_MeanNPVDown" ,
      "P_0001210074_NetPresentValueInternalTreasuryView" ,
      "P_0001210074_NetPresentValueInternalPureView" ,
      "P_0001210074_NetPresentValue" ,
      "P_0001210074_OptionNetPresentValue" ,
      "P_0001210074_NetExposure" ,
      "P_0001210074_MeanLoss" 
  
from "sap.fsdm::ValueAtRiskResult"
with associations
(
	join "sap.fsdm.SQLViews::ValueAtRiskBacktestingInterimResult_View" as "_ValueAtRiskBacktestingInterimResult__ValueAtRiskResult"
	  on "ConfidenceLevel" = "_ValueAtRiskBacktestingInterimResult__ValueAtRiskResult"."_ValueAtRiskResult.ConfidenceLevel" AND 
	     "RiskProvisionScenario" = "_ValueAtRiskBacktestingInterimResult__ValueAtRiskResult"."_ValueAtRiskResult.RiskProvisionScenario" AND 
	     "RiskGroup" = "_ValueAtRiskBacktestingInterimResult__ValueAtRiskResult"."_ValueAtRiskResult.RiskGroup" AND 
	     "RoleOfCurrency" = "_ValueAtRiskBacktestingInterimResult__ValueAtRiskResult"."_ValueAtRiskResult.RoleOfCurrency" AND 
	     "_ResultGroup.ResultGroupID" = "_ValueAtRiskBacktestingInterimResult__ValueAtRiskResult"."_ValueAtRiskResult._ResultGroup.ResultGroupID" AND 
	     "_RiskReportingNode.RiskReportingNodeID" = "_ValueAtRiskBacktestingInterimResult__ValueAtRiskResult"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" AND 
	     "_ResultGroup.ResultDataProvider" = "_ValueAtRiskBacktestingInterimResult__ValueAtRiskResult"."_ValueAtRiskResult._ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::ValueAtRiskBacktestingResult_View" as "_ValueAtRiskBacktestingResult__ValueAtRiskResult"
	  on "RiskProvisionScenario" = "_ValueAtRiskBacktestingResult__ValueAtRiskResult"."_ValueAtRiskResult.RiskProvisionScenario" AND 
	     "_ResultGroup.ResultDataProvider" = "_ValueAtRiskBacktestingResult__ValueAtRiskResult"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" AND 
	     "RiskGroup" = "_ValueAtRiskBacktestingResult__ValueAtRiskResult"."_ValueAtRiskResult.RiskGroup" AND 
	     "_RiskReportingNode.RiskReportingNodeID" = "_ValueAtRiskBacktestingResult__ValueAtRiskResult"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" AND 
	     "_ResultGroup.ResultGroupID" = "_ValueAtRiskBacktestingResult__ValueAtRiskResult"."_ValueAtRiskResult._ResultGroup.ResultGroupID" AND 
	     "RoleOfCurrency" = "_ValueAtRiskBacktestingResult__ValueAtRiskResult"."_ValueAtRiskResult.RoleOfCurrency" AND 
	     "ConfidenceLevel" = "_ValueAtRiskBacktestingResult__ValueAtRiskResult"."_ValueAtRiskResult.ConfidenceLevel"
	     ,
	join "sap.fsdm.SQLViews::PositionCurrencyOfMultiCurrencyContract_View" as "ASSOC_PositionCurrency"
	  on "ASSOC_PositionCurrency"."PositionCurrency" = "ASSOC_PositionCurrency.PositionCurrency"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "P_0001210074_FinancialContract"
	  on "P_0001210074_FinancialContract"."IDSystem" = "P_0001210074_FinancialContract.IDSystem" AND 
	     "P_0001210074_FinancialContract"."FinancialContractID" = "P_0001210074_FinancialContract.FinancialContractID"
	     ,
	join "sap.fsdm.SQLViews::FinancialInstrument_View" as "P_0001210074_FinancialInstrument"
	  on "P_0001210074_FinancialInstrument"."FinancialInstrumentID" = "P_0001210074_FinancialInstrument.FinancialInstrumentID"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "P_0001210074_SecuritiesAccount"
	  on "P_0001210074_SecuritiesAccount"."FinancialContractID" = "P_0001210074_SecuritiesAccount.FinancialContractID" AND 
	     "P_0001210074_SecuritiesAccount"."IDSystem" = "P_0001210074_SecuritiesAccount.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::ResultGroup_View" as "_ResultGroup"
	  on "_ResultGroup"."ResultGroupID" = "_ResultGroup.ResultGroupID" AND 
	     "_ResultGroup"."ResultDataProvider" = "_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::RiskReportingNode_View" as "_RiskReportingNode"
	  on "_RiskReportingNode"."RiskReportingNodeID" = "_RiskReportingNode.RiskReportingNodeID"
	     
);