view "sap.fsdm.SQLViews::ContributionMargin_View" 
as select
      "Category" ,
      "PeriodFrequency" ,
      "PeriodStartDate" ,
      "RoleOfCurrency" ,
      "_BusinessPartner.BusinessPartnerID" ,
      "_FinancialContract.FinancialContractID" ,
      "_FinancialContract.IDSystem" ,
      "_FinancialInstrument.FinancialInstrumentID" ,
      "_OrganizationalUnit.IDSystem" ,
      "_OrganizationalUnit.OrganizationalUnitID" ,
      "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "_SecuritiesAccount.FinancialContractID" ,
      "_SecuritiesAccount.IDSystem" ,
      "_SettlementItem.IDSystem" ,
      "_SettlementItem.ItemNumber" ,
      "_SettlementItem.SettlementID" ,
      "BusinessValidFrom" ,
      "BusinessValidTo" ,
      "SystemValidFrom" ,
      "SystemValidTo" ,
      "AmortizedCostRealizedResults" ,
      "BankGuaranteeStandardRiskCosts" ,
      "BankLevy" ,
      "CapitalCosts" ,
      "ChangeInEquityResults" ,
      "CommissionExpense" ,
      "CommissionIncome" ,
      "CoveredLiquidityCosts" ,
      "CreditCommitmentFeeIncome" ,
      "CreditCommitmentLiquidityCosts" ,
      "CreditCommitmentStandardRiskCosts" ,
      "CurrencyCode" ,
      "DebtInstrumentProductClearingResult" ,
      "DelayedCoverageCosts" ,
      "DerivativesSalesMargin" ,
      "DiscountedFundingLoss" ,
      "EarlyRepaymentChargesFromUnscheduledRepayment" ,
      "FairValueRealizedResults" ,
      "FairValueUnrealizedResults" ,
      "FeeExpense" ,
      "FeeIncome" ,
      "FundingAdvantage" ,
      "FundingLossFromUnscheduledRepayment" ,
      "InterestExpense" ,
      "InterestIncome" ,
      "InterestLikeIncome" ,
      "LiabilityLiquidityCosts" ,
      "LoanFairValueThroughProfitLossChange" ,
      "LoanStandardRiskCosts" ,
      "OpportunityExpense" ,
      "OpportunityIncome" ,
      "OtherStandardRiskCosts" ,
      "ParticipationInvestmentLiquidityCosts" ,
      "ParticipationInvestmentMarketInterest" ,
      "PeriodEndDate" ,
      "ProfitDistributionIncome" ,
      "ProvisionOfCollateralCosts" ,
      "RealizedFinancialAssetResults" ,
      "ReallocatedBankLevy" ,
      "ReallocatedCapitalCosts" ,
      "ReallocatedChangeInEquityResults" ,
      "ReallocatedComissionSurplusResults" ,
      "ReallocatedDisposalOrReclassificationOfAssets" ,
      "ReallocatedFairValueRealizedResults" ,
      "ReallocatedFairValueUnrealizedResults" ,
      "ReallocatedFinancialAssetResults" ,
      "ReallocatedFinancialInstrumentsAndFinancialContractsFairValueThroughProfitAndLossResults" ,
      "ReallocatedIFRSStage3LoanLossProvisionResults" ,
      "ReallocatedInterestSurplusCoreBusinessResults" ,
      "ReallocatedInterestSurplusNonCoreBusinessResults" ,
      "ReallocatedLoansFairValueChangeFromFairValueThroughProfitLoss" ,
      "ReallocatedPurchasedOrOriginatedCreditImpairedLoanLossProvisionResults" ,
      "ReallocatedSecuritiesFairValueProfitLoss" ,
      "ReallocatedSecuritiesRiskProvisioning" ,
      "ReallocatedStandardProcessingCosts" ,
      "ReallocatedStandardRiskCosts" ,
      "ReallocatedTax" ,
      "ReclassificationFromAmortizedCostResults" ,
      "ReclassificationFromFairValueOtherComprehensiveIncomeResults" ,
      "SalesMarginDerivatives" ,
      "SecuritiesLiquidityCosts" ,
      "SecuritiesMarketRateContribution" ,
      "SecuritiesRiskProvisioning" ,
      "StandardProcessingCosts" ,
      "Tax" ,
      "UncoveredLiquidityCosts" ,
      "UnrealizedFinancialAssetResults" ,
      "SourceSystemID" ,
      "ChangeTimestampInSourceSystem" ,
      "ChangingUserInSourceSystem" ,
      "ChangingProcessType" ,
      "ChangingProcessID" 
  
from "sap.fsdm::ContributionMargin"
with associations
(
	join "sap.fsdm.SQLViews::BusinessPartner_View" as "_BusinessPartner"
	  on "_BusinessPartner"."BusinessPartnerID" = "_BusinessPartner.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "_FinancialContract"
	  on "_FinancialContract"."FinancialContractID" = "_FinancialContract.FinancialContractID" AND 
	     "_FinancialContract"."IDSystem" = "_FinancialContract.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::FinancialInstrument_View" as "_FinancialInstrument"
	  on "_FinancialInstrument"."FinancialInstrumentID" = "_FinancialInstrument.FinancialInstrumentID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_OrganizationalUnit"
	  on "_OrganizationalUnit"."OrganizationalUnitID" = "_OrganizationalUnit.OrganizationalUnitID" AND 
	     "_OrganizationalUnit"."IDSystem" = "_OrganizationalUnit.IDSystem" AND 
	     "_OrganizationalUnit"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "_SecuritiesAccount"
	  on "_SecuritiesAccount"."FinancialContractID" = "_SecuritiesAccount.FinancialContractID" AND 
	     "_SecuritiesAccount"."IDSystem" = "_SecuritiesAccount.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::Settlement_View" as "_SettlementItem"
	  on "_SettlementItem"."SettlementID" = "_SettlementItem.SettlementID" AND 
	     "_SettlementItem"."ItemNumber" = "_SettlementItem.ItemNumber" AND 
	     "_SettlementItem"."IDSystem" = "_SettlementItem.IDSystem"
	     
);