namespace "sap"."fsdm";

using "sap"."fsdm"::"AccountingSystem";
using "sap"."fsdm"::"MaturityBand";
using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"FinancialInstrument";
using "sap"."fsdm"::"PositionCurrencyOfMultiCurrencyContract";
using "sap"."fsdm"::"ResultGroup";
using "sap"."fsdm"::"RiskReportingNode";

entity "BookValueAnalysisAndSimulationResult" {
    key "BookValueAnalysisSplitPartType"           : String(100)                                                  default '';
    key "BookValueAnalysisType"                    : String(100)                                                  default '';
    key "RiskProvisionScenario"                    : String(100)                                                  default '';
    key "RoleOfCurrency"                           : String(40)                                                   default '';
    key "_AccountingSystem"                        : association to AccountingSystem { AccountingSystemID }       not null;
    key "_DynamicTimeBucket"                       : association to MaturityBand {
                                                                                   MaturityBandID,
                                                                                   TimeBucketID
                                                                                 }                                not null;
    key "_FinancialContract"                       : association to FinancialContract {
                                                                                        FinancialContractID,
                                                                                        IDSystem
                                                                                      }                           not null;
    key "_FinancialInstrument"                     : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_PositionCurrencyOfMultiCurrencyContract" : association to PositionCurrencyOfMultiCurrencyContract {
                                                                                                              PositionCurrency,
                                                                                                              ASSOC_MultiCcyAccnt
                                                                                                            }     not null;
    key "_ResultGroup"                             : association to ResultGroup {
                                                                                  ResultDataProvider,
                                                                                  ResultGroupID
                                                                                }                                 not null;
    key "_RiskReportingNode"                       : association to RiskReportingNode { RiskReportingNodeID }     not null;
    key "_SecuritiesAccount"                       : association to FinancialContract {
                                                                                        FinancialContractID,
                                                                                        IDSystem
                                                                                      }                           not null;
    key "_TimeBucket"                              : association to MaturityBand {
                                                                                   MaturityBandID,
                                                                                   TimeBucketID
                                                                                 }                                not null;
    key "BusinessValidFrom"                        : LocalDate;
    key "BusinessValidTo"                          : LocalDate;
        "SystemValidFrom"                          : UTCTimestamp                                                 not null;
        "SystemValidTo"                            : UTCTimestamp                                                 not null;
        "BookIncomeAmount"                         : Decimal(34, 6);
        "BookValue"                                : Decimal(34, 6);
        "Currency"                                 : String(3);
        "DynamicTimeBucketEndDate"                 : LocalDate;
        "DynamicTimeBucketStartDate"               : LocalDate;
        "EffectiveInterestRate"                    : Decimal(15, 11);
        "NominalIncomeAmount"                      : Decimal(34, 6);
        "NominalValue"                             : Decimal(34, 6);
        "TimeBucketEndDate"                        : LocalDate;
        "TimeBucketStartDate"                      : LocalDate;
        "SourceSystemID"                           : String(128);
        "ChangeTimestampInSourceSystem"            : UTCTimestamp;
        "ChangingUserInSourceSystem"               : String(128);
        "ChangingProcessType"                      : String(40);
        "ChangingProcessID"                        : String(128);
}
technical configuration {
    column store;
};

entity "BookValueAnalysisAndSimulationResult_Historical" {
    "BookValueAnalysisSplitPartType"           : String(100)                                                  default '' not null;
    "BookValueAnalysisType"                    : String(100)                                                  default '' not null;
    "RiskProvisionScenario"                    : String(100)                                                  default '' not null;
    "RoleOfCurrency"                           : String(40)                                                   default '' not null;
    "_AccountingSystem"                        : association to AccountingSystem { AccountingSystemID }       not null;
    "_DynamicTimeBucket"                       : association to MaturityBand {
                                                                               MaturityBandID,
                                                                               TimeBucketID
                                                                             }                                not null;
    "_FinancialContract"                       : association to FinancialContract {
                                                                                    FinancialContractID,
                                                                                    IDSystem
                                                                                  }                           not null;
    "_FinancialInstrument"                     : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_PositionCurrencyOfMultiCurrencyContract" : association to PositionCurrencyOfMultiCurrencyContract {
                                                                                                          PositionCurrency,
                                                                                                          ASSOC_MultiCcyAccnt
                                                                                                        }     not null;
    "_ResultGroup"                             : association to ResultGroup {
                                                                              ResultDataProvider,
                                                                              ResultGroupID
                                                                            }                                 not null;
    "_RiskReportingNode"                       : association to RiskReportingNode { RiskReportingNodeID }     not null;
    "_SecuritiesAccount"                       : association to FinancialContract {
                                                                                    FinancialContractID,
                                                                                    IDSystem
                                                                                  }                           not null;
    "_TimeBucket"                              : association to MaturityBand {
                                                                               MaturityBandID,
                                                                               TimeBucketID
                                                                             }                                not null;
    "BusinessValidFrom"                        : LocalDate                                                    not null;
    "BusinessValidTo"                          : LocalDate                                                    not null;
    "SystemValidFrom"                          : UTCTimestamp                                                 not null;
    "SystemValidTo"                            : UTCTimestamp                                                 not null;
    "BookIncomeAmount"                         : Decimal(34, 6);
    "BookValue"                                : Decimal(34, 6);
    "Currency"                                 : String(3);
    "DynamicTimeBucketEndDate"                 : LocalDate;
    "DynamicTimeBucketStartDate"               : LocalDate;
    "EffectiveInterestRate"                    : Decimal(15, 11);
    "NominalIncomeAmount"                      : Decimal(34, 6);
    "NominalValue"                             : Decimal(34, 6);
    "TimeBucketEndDate"                        : LocalDate;
    "TimeBucketStartDate"                      : LocalDate;
    "SourceSystemID"                           : String(128);
    "ChangeTimestampInSourceSystem"            : UTCTimestamp;
    "ChangingUserInSourceSystem"               : String(128);
    "ChangingProcessType"                      : String(40);
    "ChangingProcessID"                        : String(128);
}
technical configuration {
    column store;
};