namespace "sap"."fsdm";

using "sap"."fsdm"::"AccountingSystem";
using "sap"."fsdm"::"FinancialContract";

entity "StatementAboutContractModification" {
    key "BookValueType"                       : String(40)                                             default '';
    key "ModificationCategory"                : String(40);
    key "ModificationDate"                    : LocalDate;
    key "_AccountingSystem"                   : association to AccountingSystem { AccountingSystemID } not null;
    key "_FinancialContract"                  : association to FinancialContract {
                                                                                   FinancialContractID,
                                                                                   IDSystem
                                                                                 }                     not null;
    key "BusinessValidFrom"                   : LocalDate;
    key "BusinessValidTo"                     : LocalDate;
        "SystemValidFrom"                     : UTCTimestamp                                           not null;
        "SystemValidTo"                       : UTCTimestamp                                           not null;
        "_Reverses"                           : association to StatementAboutContractModification {
                                                                                                    BookValueType,
                                                                                                    ModificationCategory,
                                                                                                    ModificationDate,
                                                                                                    _AccountingSystem,
                                                                                                    _FinancialContract
                                                                                                  };
        "BookValueModificationAmount"         : Decimal(34, 6);
        "BookValueModificationAmountCurrency" : String(3);
        "Reversal"                            : Boolean;
        "SourceSystemID"                      : String(128);
        "ChangeTimestampInSourceSystem"       : UTCTimestamp;
        "ChangingUserInSourceSystem"          : String(128);
        "ChangingProcessType"                 : String(40);
        "ChangingProcessID"                   : String(128);
}
technical configuration {
    column store;
};

entity "StatementAboutContractModification_Historical" {
    "BookValueType"                       : String(40)                                             default '' not null;
    "ModificationCategory"                : String(40)                                             not null;
    "ModificationDate"                    : LocalDate                                              not null;
    "_AccountingSystem"                   : association to AccountingSystem { AccountingSystemID } not null;
    "_FinancialContract"                  : association to FinancialContract {
                                                                               FinancialContractID,
                                                                               IDSystem
                                                                             }                     not null;
    "BusinessValidFrom"                   : LocalDate                                              not null;
    "BusinessValidTo"                     : LocalDate                                              not null;
    "SystemValidFrom"                     : UTCTimestamp                                           not null;
    "SystemValidTo"                       : UTCTimestamp                                           not null;
    "_Reverses"                           : association to StatementAboutContractModification {
                                                                                                BookValueType,
                                                                                                ModificationCategory,
                                                                                                ModificationDate,
                                                                                                _AccountingSystem,
                                                                                                _FinancialContract
                                                                                              };
    "BookValueModificationAmount"         : Decimal(34, 6);
    "BookValueModificationAmountCurrency" : String(3);
    "Reversal"                            : Boolean;
    "SourceSystemID"                      : String(128);
    "ChangeTimestampInSourceSystem"       : UTCTimestamp;
    "ChangingUserInSourceSystem"          : String(128);
    "ChangingProcessType"                 : String(40);
    "ChangingProcessID"                   : String(128);
}
technical configuration {
    column store;
};