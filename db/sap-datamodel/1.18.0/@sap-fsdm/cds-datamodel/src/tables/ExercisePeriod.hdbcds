namespace "sap"."fsdm";

using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"OptionRight";
using "sap"."fsdm"::"FinancialInstrument";

entity "ExercisePeriod" {
    key "StartDate"                     : LocalDate                                                    default '0001-01-01';
    key "_Option"                       : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           }                           not null;
    key "_OptionRight"                  : association to OptionRight {
                                                                       SequenceNumber,
                                                                       _FinancialContract,
                                                                       _FinancialInstrument,
                                                                       _TrancheInSyndication
                                                                     }                                 not null;
    key "_Swaption"                     : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           }                           not null;
    key "_Warrant"                      : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "BusinessValidFrom"             : LocalDate;
    key "BusinessValidTo"               : LocalDate;
        "SystemValidFrom"               : UTCTimestamp                                                 not null;
        "SystemValidTo"                 : UTCTimestamp                                                 not null;
        "BusinessCalendar"              : String(200);
        "BusinessDayConvention"         : String(40);
        "EndDate"                       : LocalDate;
        "ExercisePeriodLength"          : Decimal(34, 6);
        "ExercisePeriodTimeUnit"        : String(128);
        "SourceSystemID"                : String(128);
        "ChangeTimestampInSourceSystem" : UTCTimestamp;
        "ChangingUserInSourceSystem"    : String(128);
        "ChangingProcessType"           : String(40);
        "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};

entity "ExercisePeriod_Historical" {
    "StartDate"                     : LocalDate                                                    default '0001-01-01' not null;
    "_Option"                       : association to FinancialContract {
                                                                         FinancialContractID,
                                                                         IDSystem
                                                                       }                           not null;
    "_OptionRight"                  : association to OptionRight {
                                                                   SequenceNumber,
                                                                   _FinancialContract,
                                                                   _FinancialInstrument,
                                                                   _TrancheInSyndication
                                                                 }                                 not null;
    "_Swaption"                     : association to FinancialContract {
                                                                         FinancialContractID,
                                                                         IDSystem
                                                                       }                           not null;
    "_Warrant"                      : association to FinancialInstrument { FinancialInstrumentID } not null;
    "BusinessValidFrom"             : LocalDate                                                    not null;
    "BusinessValidTo"               : LocalDate                                                    not null;
    "SystemValidFrom"               : UTCTimestamp                                                 not null;
    "SystemValidTo"                 : UTCTimestamp                                                 not null;
    "BusinessCalendar"              : String(200);
    "BusinessDayConvention"         : String(40);
    "EndDate"                       : LocalDate;
    "ExercisePeriodLength"          : Decimal(34, 6);
    "ExercisePeriodTimeUnit"        : String(128);
    "SourceSystemID"                : String(128);
    "ChangeTimestampInSourceSystem" : UTCTimestamp;
    "ChangingUserInSourceSystem"    : String(128);
    "ChangingProcessType"           : String(40);
    "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};