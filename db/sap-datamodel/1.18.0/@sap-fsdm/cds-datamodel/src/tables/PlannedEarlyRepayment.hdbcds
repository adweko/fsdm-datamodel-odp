namespace "sap"."fsdm";

using "sap"."fsdm"::"FinancialContract";

entity "PlannedEarlyRepayment" {
    key "PlannedRepaymentDate"              : LocalDate;
    key "RepaymentDifferentiationCriterium" : String(40);
    key "_FinancialContract"                : association to FinancialContract {
                                                                                 FinancialContractID,
                                                                                 IDSystem
                                                                               } not null;
    key "BusinessValidFrom"                 : LocalDate;
    key "BusinessValidTo"                   : LocalDate;
        "SystemValidFrom"                   : UTCTimestamp                       not null;
        "SystemValidTo"                     : UTCTimestamp                       not null;
        "PlannedRepaymentAmount"            : Decimal(34, 6);
        "PlannedRepaymentAmountCurrency"    : String(3);
        "PrepaymentPenaltyAmount"           : Decimal(34, 6);
        "PrepaymentPenaltyAmountCurrency"   : String(3);
        "RepaymentProbability"              : String(40);
        "Status"                            : String(40);
        "StatusChangeDate"                  : LocalDate;
        "StatusReason"                      : String(256);
        "SourceSystemID"                    : String(128);
        "ChangeTimestampInSourceSystem"     : UTCTimestamp;
        "ChangingUserInSourceSystem"        : String(128);
        "ChangingProcessType"               : String(40);
        "ChangingProcessID"                 : String(128);
}
technical configuration {
    column store;
};

entity "PlannedEarlyRepayment_Historical" {
    "PlannedRepaymentDate"              : LocalDate                          not null;
    "RepaymentDifferentiationCriterium" : String(40)                         not null;
    "_FinancialContract"                : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           } not null;
    "BusinessValidFrom"                 : LocalDate                          not null;
    "BusinessValidTo"                   : LocalDate                          not null;
    "SystemValidFrom"                   : UTCTimestamp                       not null;
    "SystemValidTo"                     : UTCTimestamp                       not null;
    "PlannedRepaymentAmount"            : Decimal(34, 6);
    "PlannedRepaymentAmountCurrency"    : String(3);
    "PrepaymentPenaltyAmount"           : Decimal(34, 6);
    "PrepaymentPenaltyAmountCurrency"   : String(3);
    "RepaymentProbability"              : String(40);
    "Status"                            : String(40);
    "StatusChangeDate"                  : LocalDate;
    "StatusReason"                      : String(256);
    "SourceSystemID"                    : String(128);
    "ChangeTimestampInSourceSystem"     : UTCTimestamp;
    "ChangingUserInSourceSystem"        : String(128);
    "ChangingProcessType"               : String(40);
    "ChangingProcessID"                 : String(128);
}
technical configuration {
    column store;
};