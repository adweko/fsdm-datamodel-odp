namespace "sap"."fsdm";

using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"BusinessPartner";
using "sap"."fsdm"::"FinancialInstrument";
using "sap"."fsdm"::"ContractBundle";
using "sap"."fsdm"::"PhysicalAsset";
using "sap"."fsdm"::"TrancheInSyndication";

entity "FinancialCovenant" {
    key "SequenceNumber"                             : Integer                                                      default -1;
    key "ASSOC_FinancialContract"                    : association to FinancialContract {
                                                                                          FinancialContractID,
                                                                                          IDSystem
                                                                                        }                           not null;
    key "_BusinessPartner"                           : association to BusinessPartner { BusinessPartnerID }         not null;
    key "_ContingentConvertibleInstrument"           : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_ContractBundle"                            : association to ContractBundle { ContractBundleID }           not null;
    key "_PhysicalAsset"                             : association to PhysicalAsset { PhysicalAssetID }             not null;
    key "_TrancheInSyndication"                      : association to TrancheInSyndication {
                                                                                             TrancheSequenceNumber,
                                                                                             _SyndicationAgreement
                                                                                           }                        not null;
    key "BusinessValidFrom"                          : LocalDate;
    key "BusinessValidTo"                            : LocalDate;
        "SystemValidFrom"                            : UTCTimestamp                                                 not null;
        "SystemValidTo"                              : UTCTimestamp                                                 not null;
        "BoundaryUnit"                               : String(10);
        "BusinessCalendar"                           : String(200);
        "BusinessDayConvention"                      : String(40);
        "CovenantBreachEffectType"                   : String(40);
        "CovenantCategory"                           : String(40);
        "CovenantCheckProcedure"                     : String(40);
        "CovenantFulfilledEffectType"                : String(40);
        "CovenantType"                               : String(40);
        "Criterion"                                  : String(40);
        "CriterionApplicableFrom"                    : LocalDate;
        "CriterionApplicableTo"                      : LocalDate;
        "CurePeriodLength"                           : Decimal(34, 6);
        "CurePeriodTimeUnit"                         : String(128);
        "Description"                                : String(256);
        "EndOfContractualConsequences"               : String(40);
        "EndOfContractualConsequencesKeyDate"        : LocalDate;
        "EndOfContractualConsequencesSuccessfulTest" : Integer;
        "FirstRegularMonitoringDate"                 : LocalDate;
        "LowerBoundary"                              : Decimal(34, 6);
        "LowerBoundaryCount"                         : Integer;
        "LowerBoundaryPercentage"                    : Decimal(21, 11);
        "LowerNonnumericalBoundary"                  : String(40);
        "MonitoringPeriodLength"                     : Decimal(34, 6);
        "MonitoringPeriodLengthTimeUnit"             : String(128);
        "MonitoringWithCurePhaseIndicator"           : Boolean;
        "SubmissionDeadlinePeriodLength"             : Decimal(34, 6);
        "SubmissionDeadlineTimeUnit"                 : String(128);
        "UpperBoundary"                              : Decimal(34, 6);
        "UpperBoundaryCount"                         : Integer;
        "UpperBoundaryPercentage"                    : Decimal(21, 11);
        "UpperNonnumericalBoundary"                  : String(40);
        "SourceSystemID"                             : String(128);
        "ChangeTimestampInSourceSystem"              : UTCTimestamp;
        "ChangingUserInSourceSystem"                 : String(128);
        "ChangingProcessType"                        : String(40);
        "ChangingProcessID"                          : String(128);
}
technical configuration {
    column store;
};

entity "FinancialCovenant_Historical" {
    "SequenceNumber"                             : Integer                                                      default -1 not null;
    "ASSOC_FinancialContract"                    : association to FinancialContract {
                                                                                      FinancialContractID,
                                                                                      IDSystem
                                                                                    }                           not null;
    "_BusinessPartner"                           : association to BusinessPartner { BusinessPartnerID }         not null;
    "_ContingentConvertibleInstrument"           : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_ContractBundle"                            : association to ContractBundle { ContractBundleID }           not null;
    "_PhysicalAsset"                             : association to PhysicalAsset { PhysicalAssetID }             not null;
    "_TrancheInSyndication"                      : association to TrancheInSyndication {
                                                                                         TrancheSequenceNumber,
                                                                                         _SyndicationAgreement
                                                                                       }                        not null;
    "BusinessValidFrom"                          : LocalDate                                                    not null;
    "BusinessValidTo"                            : LocalDate                                                    not null;
    "SystemValidFrom"                            : UTCTimestamp                                                 not null;
    "SystemValidTo"                              : UTCTimestamp                                                 not null;
    "BoundaryUnit"                               : String(10);
    "BusinessCalendar"                           : String(200);
    "BusinessDayConvention"                      : String(40);
    "CovenantBreachEffectType"                   : String(40);
    "CovenantCategory"                           : String(40);
    "CovenantCheckProcedure"                     : String(40);
    "CovenantFulfilledEffectType"                : String(40);
    "CovenantType"                               : String(40);
    "Criterion"                                  : String(40);
    "CriterionApplicableFrom"                    : LocalDate;
    "CriterionApplicableTo"                      : LocalDate;
    "CurePeriodLength"                           : Decimal(34, 6);
    "CurePeriodTimeUnit"                         : String(128);
    "Description"                                : String(256);
    "EndOfContractualConsequences"               : String(40);
    "EndOfContractualConsequencesKeyDate"        : LocalDate;
    "EndOfContractualConsequencesSuccessfulTest" : Integer;
    "FirstRegularMonitoringDate"                 : LocalDate;
    "LowerBoundary"                              : Decimal(34, 6);
    "LowerBoundaryCount"                         : Integer;
    "LowerBoundaryPercentage"                    : Decimal(21, 11);
    "LowerNonnumericalBoundary"                  : String(40);
    "MonitoringPeriodLength"                     : Decimal(34, 6);
    "MonitoringPeriodLengthTimeUnit"             : String(128);
    "MonitoringWithCurePhaseIndicator"           : Boolean;
    "SubmissionDeadlinePeriodLength"             : Decimal(34, 6);
    "SubmissionDeadlineTimeUnit"                 : String(128);
    "UpperBoundary"                              : Decimal(34, 6);
    "UpperBoundaryCount"                         : Integer;
    "UpperBoundaryPercentage"                    : Decimal(21, 11);
    "UpperNonnumericalBoundary"                  : String(40);
    "SourceSystemID"                             : String(128);
    "ChangeTimestampInSourceSystem"              : UTCTimestamp;
    "ChangingUserInSourceSystem"                 : String(128);
    "ChangingProcessType"                        : String(40);
    "ChangingProcessID"                          : String(128);
}
technical configuration {
    column store;
};