namespace "sap"."fsdm";

using "sap"."fsdm"::"FinancialContract";

entity "InflationSwapCalculationPeriod" {
    key "IntervalEndDate"               : LocalDate                          default '0001-01-01';
    key "IntervalStartDate"             : LocalDate                          default '0001-01-01';
    key "RoleOfPayer"                   : String(50)                         default '';
    key "_Swap"                         : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           } not null;
    key "BusinessValidFrom"             : LocalDate;
    key "BusinessValidTo"               : LocalDate;
        "SystemValidFrom"               : UTCTimestamp                       not null;
        "SystemValidTo"                 : UTCTimestamp                       not null;
        "BusinessCalendar"              : String(200);
        "BusinessDayConvention"         : String(40);
        "CappedPercentage"              : Decimal(15, 11);
        "DayCountConvention"            : String(40);
        "FlooredPercentage"             : Decimal(15, 11);
        "NotionalAmount"                : Decimal(34, 6);
        "NotionalAmountCurrency"        : String(3);
        "Spread"                        : Decimal(15, 11);
        "SourceSystemID"                : String(128);
        "ChangeTimestampInSourceSystem" : UTCTimestamp;
        "ChangingUserInSourceSystem"    : String(128);
        "ChangingProcessType"           : String(40);
        "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};

entity "InflationSwapCalculationPeriod_Historical" {
    "IntervalEndDate"               : LocalDate                          default '0001-01-01' not null;
    "IntervalStartDate"             : LocalDate                          default '0001-01-01' not null;
    "RoleOfPayer"                   : String(50)                         default '' not null;
    "_Swap"                         : association to FinancialContract {
                                                                         FinancialContractID,
                                                                         IDSystem
                                                                       } not null;
    "BusinessValidFrom"             : LocalDate                          not null;
    "BusinessValidTo"               : LocalDate                          not null;
    "SystemValidFrom"               : UTCTimestamp                       not null;
    "SystemValidTo"                 : UTCTimestamp                       not null;
    "BusinessCalendar"              : String(200);
    "BusinessDayConvention"         : String(40);
    "CappedPercentage"              : Decimal(15, 11);
    "DayCountConvention"            : String(40);
    "FlooredPercentage"             : Decimal(15, 11);
    "NotionalAmount"                : Decimal(34, 6);
    "NotionalAmountCurrency"        : String(3);
    "Spread"                        : Decimal(15, 11);
    "SourceSystemID"                : String(128);
    "ChangeTimestampInSourceSystem" : UTCTimestamp;
    "ChangingUserInSourceSystem"    : String(128);
    "ChangingProcessType"           : String(40);
    "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};