namespace "sap"."fsdm";

using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"BankingChannel";
using "sap"."fsdm"::"BusinessPartner";

entity "LoanSaleOrPurchaseTransaction" {
    key "TransactionID"                 : String(128);
    key "_Loan"                         : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           }                    not null;
    key "BusinessValidFrom"             : LocalDate;
    key "BusinessValidTo"               : LocalDate;
        "SystemValidFrom"               : UTCTimestamp                                          not null;
        "SystemValidTo"                 : UTCTimestamp                                          not null;
        "_BankingChannel"               : association to BankingChannel { BankingChannelID };
        "_BusinessEventDataOwner"       : association to BusinessPartner { BusinessPartnerID };
        "_LoanPurchaser"                : association to BusinessPartner { BusinessPartnerID };
        "_LoanSeller"                   : association to BusinessPartner { BusinessPartnerID };
        "CancellationDate"              : LocalDate;
        "CarryingAmount"                : Decimal(34, 6);
        "CarryingAmountCurrency"        : String(3);
        "ValueDate"                     : LocalDate;
        "SourceSystemID"                : String(128);
        "ChangeTimestampInSourceSystem" : UTCTimestamp;
        "ChangingUserInSourceSystem"    : String(128);
        "ChangingProcessType"           : String(40);
        "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};

entity "LoanSaleOrPurchaseTransaction_Historical" {
    "TransactionID"                 : String(128)                                           not null;
    "_Loan"                         : association to FinancialContract {
                                                                         FinancialContractID,
                                                                         IDSystem
                                                                       }                    not null;
    "BusinessValidFrom"             : LocalDate                                             not null;
    "BusinessValidTo"               : LocalDate                                             not null;
    "SystemValidFrom"               : UTCTimestamp                                          not null;
    "SystemValidTo"                 : UTCTimestamp                                          not null;
    "_BankingChannel"               : association to BankingChannel { BankingChannelID };
    "_BusinessEventDataOwner"       : association to BusinessPartner { BusinessPartnerID };
    "_LoanPurchaser"                : association to BusinessPartner { BusinessPartnerID };
    "_LoanSeller"                   : association to BusinessPartner { BusinessPartnerID };
    "CancellationDate"              : LocalDate;
    "CarryingAmount"                : Decimal(34, 6);
    "CarryingAmountCurrency"        : String(3);
    "ValueDate"                     : LocalDate;
    "SourceSystemID"                : String(128);
    "ChangeTimestampInSourceSystem" : UTCTimestamp;
    "ChangingUserInSourceSystem"    : String(128);
    "ChangingProcessType"           : String(40);
    "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};