namespace "sap"."fsdm";

using "sap"."fsdm"::"BusinessPartner";
using "sap"."fsdm"::"AccountingSystem";
using "sap"."fsdm"::"OrganizationalUnit";

entity "Collection" {
    key "CollectionID"                             : String(128)                                             default '';
    key "IDSystem"                                 : String(40)                                              default '';
    key "_Client"                                  : association to BusinessPartner { BusinessPartnerID }    not null;
    key "BusinessValidFrom"                        : LocalDate;
    key "BusinessValidTo"                          : LocalDate;
        "SystemValidFrom"                          : UTCTimestamp                                            not null;
        "SystemValidTo"                            : UTCTimestamp                                            not null;
        "_AccountingSystem"                        : association to AccountingSystem { AccountingSystemID };
        "_CostCenter"                              : association to OrganizationalUnit {
                                                                                         IDSystem,
                                                                                         OrganizationalUnitID,
                                                                                         ASSOC_OrganizationHostingOrganizationalUnit
                                                                                       };
        "_EmployeeResponsibleForPortfolio"         : association to BusinessPartner { BusinessPartnerID };
        "_OrganizationalUnit"                      : association to OrganizationalUnit {
                                                                                         IDSystem,
                                                                                         OrganizationalUnitID,
                                                                                         ASSOC_OrganizationHostingOrganizationalUnit
                                                                                       };
        "_ProfitCenter"                            : association to OrganizationalUnit {
                                                                                         IDSystem,
                                                                                         OrganizationalUnitID,
                                                                                         ASSOC_OrganizationHostingOrganizationalUnit
                                                                                       };
        "CollateralPoolCategory"                   : String(100);
        "CollateralPoolCoveringType"               : String(100);
        "CollateralPoolType"                       : String(100);
        "CollectionCategory"                       : String(40);
        "CollectionName"                           : String(256);
        "CountryOfCollateralPoolCovering"          : String(2);
        "DerivativeStrategyType"                   : String(100);
        "Distributed"                              : Boolean;
        "FactorLagLength"                          : Decimal(34, 6);
        "FactorLagTimeUnit"                        : String(128);
        "FinancialInstrumentPortfolioType"         : String(100);
        "HedgeRelationshipStatus"                  : String(100);
        "HedgeStrategyType"                        : String(100);
        "InvestmentObjective"                      : String(40);
        "ManagingUnit"                             : String(256);
        "OriginalExposureAtDefault"                : Decimal(34, 6);
        "OriginalExposureAtDefaultCurrency"        : String(3);
        "OriginalRegulatoryCapitalRequirementRate" : Decimal(15, 11);
        "PoolAdministrationType"                   : String(100);
        "PortfolioCategory"                        : String(80);
        "SizeOfTradingBucket"                      : Decimal(15, 11);
        "TradeBookType"                            : String(100);
        "TradingDeskType"                          : String(100);
        "UnderlyingExposureType"                   : String(100);
        "SourceSystemID"                           : String(128);
        "ChangeTimestampInSourceSystem"            : UTCTimestamp;
        "ChangingUserInSourceSystem"               : String(128);
        "ChangingProcessType"                      : String(40);
        "ChangingProcessID"                        : String(128);
}
technical configuration {
    column store;
};

entity "Collection_Historical" {
    "CollectionID"                             : String(128)                                             default '' not null;
    "IDSystem"                                 : String(40)                                              default '' not null;
    "_Client"                                  : association to BusinessPartner { BusinessPartnerID }    not null;
    "BusinessValidFrom"                        : LocalDate                                               not null;
    "BusinessValidTo"                          : LocalDate                                               not null;
    "SystemValidFrom"                          : UTCTimestamp                                            not null;
    "SystemValidTo"                            : UTCTimestamp                                            not null;
    "_AccountingSystem"                        : association to AccountingSystem { AccountingSystemID };
    "_CostCenter"                              : association to OrganizationalUnit {
                                                                                     IDSystem,
                                                                                     OrganizationalUnitID,
                                                                                     ASSOC_OrganizationHostingOrganizationalUnit
                                                                                   };
    "_EmployeeResponsibleForPortfolio"         : association to BusinessPartner { BusinessPartnerID };
    "_OrganizationalUnit"                      : association to OrganizationalUnit {
                                                                                     IDSystem,
                                                                                     OrganizationalUnitID,
                                                                                     ASSOC_OrganizationHostingOrganizationalUnit
                                                                                   };
    "_ProfitCenter"                            : association to OrganizationalUnit {
                                                                                     IDSystem,
                                                                                     OrganizationalUnitID,
                                                                                     ASSOC_OrganizationHostingOrganizationalUnit
                                                                                   };
    "CollateralPoolCategory"                   : String(100);
    "CollateralPoolCoveringType"               : String(100);
    "CollateralPoolType"                       : String(100);
    "CollectionCategory"                       : String(40);
    "CollectionName"                           : String(256);
    "CountryOfCollateralPoolCovering"          : String(2);
    "DerivativeStrategyType"                   : String(100);
    "Distributed"                              : Boolean;
    "FactorLagLength"                          : Decimal(34, 6);
    "FactorLagTimeUnit"                        : String(128);
    "FinancialInstrumentPortfolioType"         : String(100);
    "HedgeRelationshipStatus"                  : String(100);
    "HedgeStrategyType"                        : String(100);
    "InvestmentObjective"                      : String(40);
    "ManagingUnit"                             : String(256);
    "OriginalExposureAtDefault"                : Decimal(34, 6);
    "OriginalExposureAtDefaultCurrency"        : String(3);
    "OriginalRegulatoryCapitalRequirementRate" : Decimal(15, 11);
    "PoolAdministrationType"                   : String(100);
    "PortfolioCategory"                        : String(80);
    "SizeOfTradingBucket"                      : Decimal(15, 11);
    "TradeBookType"                            : String(100);
    "TradingDeskType"                          : String(100);
    "UnderlyingExposureType"                   : String(100);
    "SourceSystemID"                           : String(128);
    "ChangeTimestampInSourceSystem"            : UTCTimestamp;
    "ChangingUserInSourceSystem"               : String(128);
    "ChangingProcessType"                      : String(40);
    "ChangingProcessID"                        : String(128);
}
technical configuration {
    column store;
};