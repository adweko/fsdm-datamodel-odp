namespace "sap"."fsdm";

using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"FinancialInstrument";
using "sap"."fsdm"::"PositionCurrencyOfMultiCurrencyContract";
using "sap"."fsdm"::"ResultGroup";
using "sap"."fsdm"::"RiskReportingNode";
using "sap"."fsdm"::"MaturityBand";

entity "RegulatoryGeneralInterestRateExposure" {
    key "InterestRateOptionComponentNumber"        : Integer                                                      default -1;
    key "RegulatoryMarketRiskSplitPartType"        : String(100)                                                  default '';
    key "RiskProvisionScenario"                    : String(100)                                                  default '';
    key "RoleOfCurrency"                           : String(40)                                                   default '';
    key "RoleOfPayer"                              : String(50)                                                   default '';
    key "_FinancialContract"                       : association to FinancialContract {
                                                                                        FinancialContractID,
                                                                                        IDSystem
                                                                                      }                           not null;
    key "_FinancialInstrument"                     : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_PositionCurrencyOfMultiCurrencyContract" : association to PositionCurrencyOfMultiCurrencyContract {
                                                                                                              PositionCurrency,
                                                                                                              ASSOC_MultiCcyAccnt
                                                                                                            }     not null;
    key "_ResultGroup"                             : association to ResultGroup {
                                                                                  ResultDataProvider,
                                                                                  ResultGroupID
                                                                                }                                 not null;
    key "_RiskReportingNode"                       : association to RiskReportingNode { RiskReportingNodeID }     not null;
    key "_SecuritiesAccount"                       : association to FinancialContract {
                                                                                        FinancialContractID,
                                                                                        IDSystem
                                                                                      }                           not null;
    key "_TimeBucket"                              : association to MaturityBand {
                                                                                   MaturityBandID,
                                                                                   TimeBucketID
                                                                                 }                                not null;
    key "BusinessValidFrom"                        : LocalDate;
    key "BusinessValidTo"                          : LocalDate;
        "SystemValidFrom"                          : UTCTimestamp                                                 not null;
        "SystemValidTo"                            : UTCTimestamp                                                 not null;
        "Currency"                                 : String(3);
        "DollarDurationContributionAmount"         : Decimal(34, 6);
        "ExposureAmountLong"                       : Decimal(34, 6);
        "ExposureAmountShort"                      : Decimal(34, 6);
        "GeneralInterestRateRiskCalculationMethod" : String(100);
        "SourceSystemID"                           : String(128);
        "ChangeTimestampInSourceSystem"            : UTCTimestamp;
        "ChangingUserInSourceSystem"               : String(128);
        "ChangingProcessType"                      : String(40);
        "ChangingProcessID"                        : String(128);
}
technical configuration {
    column store;
};

entity "RegulatoryGeneralInterestRateExposure_Historical" {
    "InterestRateOptionComponentNumber"        : Integer                                                      default -1 not null;
    "RegulatoryMarketRiskSplitPartType"        : String(100)                                                  default '' not null;
    "RiskProvisionScenario"                    : String(100)                                                  default '' not null;
    "RoleOfCurrency"                           : String(40)                                                   default '' not null;
    "RoleOfPayer"                              : String(50)                                                   default '' not null;
    "_FinancialContract"                       : association to FinancialContract {
                                                                                    FinancialContractID,
                                                                                    IDSystem
                                                                                  }                           not null;
    "_FinancialInstrument"                     : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_PositionCurrencyOfMultiCurrencyContract" : association to PositionCurrencyOfMultiCurrencyContract {
                                                                                                          PositionCurrency,
                                                                                                          ASSOC_MultiCcyAccnt
                                                                                                        }     not null;
    "_ResultGroup"                             : association to ResultGroup {
                                                                              ResultDataProvider,
                                                                              ResultGroupID
                                                                            }                                 not null;
    "_RiskReportingNode"                       : association to RiskReportingNode { RiskReportingNodeID }     not null;
    "_SecuritiesAccount"                       : association to FinancialContract {
                                                                                    FinancialContractID,
                                                                                    IDSystem
                                                                                  }                           not null;
    "_TimeBucket"                              : association to MaturityBand {
                                                                               MaturityBandID,
                                                                               TimeBucketID
                                                                             }                                not null;
    "BusinessValidFrom"                        : LocalDate                                                    not null;
    "BusinessValidTo"                          : LocalDate                                                    not null;
    "SystemValidFrom"                          : UTCTimestamp                                                 not null;
    "SystemValidTo"                            : UTCTimestamp                                                 not null;
    "Currency"                                 : String(3);
    "DollarDurationContributionAmount"         : Decimal(34, 6);
    "ExposureAmountLong"                       : Decimal(34, 6);
    "ExposureAmountShort"                      : Decimal(34, 6);
    "GeneralInterestRateRiskCalculationMethod" : String(100);
    "SourceSystemID"                           : String(128);
    "ChangeTimestampInSourceSystem"            : UTCTimestamp;
    "ChangingUserInSourceSystem"               : String(128);
    "ChangingProcessType"                      : String(40);
    "ChangingProcessID"                        : String(128);
}
technical configuration {
    column store;
};