namespace "sap"."fsdm";

using "sap"."fsdm"::"OrganizationalUnit";
using "sap"."fsdm"::"PlanBudgetForecast";

entity "OrganizationalUnitFiscalYearInformation" {
    key "FiscalPeriod"                        : String(128)                         default '';
    key "FiscalYear"                          : String(10)                          default '';
    key "_OrganizationalUnit"                 : association to OrganizationalUnit {
                                                                                    IDSystem,
                                                                                    OrganizationalUnitID,
                                                                                    ASSOC_OrganizationHostingOrganizationalUnit
                                                                                  } not null;
    key "_PlanBudgetForecast"                 : association to PlanBudgetForecast {
                                                                                    ID,
                                                                                    PlanBudgetForecastScenario,
                                                                                    VersionID
                                                                                  } not null;
    key "BusinessValidFrom"                   : LocalDate;
    key "BusinessValidTo"                     : LocalDate;
        "SystemValidFrom"                     : UTCTimestamp                        not null;
        "SystemValidTo"                       : UTCTimestamp                        not null;
        "NumberOfFulltimeEquivalentEmployees" : Decimal(15, 6);
        "SourceSystemID"                      : String(128);
        "ChangeTimestampInSourceSystem"       : UTCTimestamp;
        "ChangingUserInSourceSystem"          : String(128);
        "ChangingProcessType"                 : String(40);
        "ChangingProcessID"                   : String(128);
}
technical configuration {
    column store;
};

entity "OrganizationalUnitFiscalYearInformation_Historical" {
    "FiscalPeriod"                        : String(128)                         default '' not null;
    "FiscalYear"                          : String(10)                          default '' not null;
    "_OrganizationalUnit"                 : association to OrganizationalUnit {
                                                                                IDSystem,
                                                                                OrganizationalUnitID,
                                                                                ASSOC_OrganizationHostingOrganizationalUnit
                                                                              } not null;
    "_PlanBudgetForecast"                 : association to PlanBudgetForecast {
                                                                                ID,
                                                                                PlanBudgetForecastScenario,
                                                                                VersionID
                                                                              } not null;
    "BusinessValidFrom"                   : LocalDate                           not null;
    "BusinessValidTo"                     : LocalDate                           not null;
    "SystemValidFrom"                     : UTCTimestamp                        not null;
    "SystemValidTo"                       : UTCTimestamp                        not null;
    "NumberOfFulltimeEquivalentEmployees" : Decimal(15, 6);
    "SourceSystemID"                      : String(128);
    "ChangeTimestampInSourceSystem"       : UTCTimestamp;
    "ChangingUserInSourceSystem"          : String(128);
    "ChangingProcessType"                 : String(40);
    "ChangingProcessID"                   : String(128);
}
technical configuration {
    column store;
};