#!groovy
pipeline {
    agent any
    options {
        timeout(time: 1, unit: 'HOURS')
    }
    // edit the environment variable to change the hana space to deploy to
    environment {
    	hanaSpace = "OSX_Risk_Dev"
		projectName = "fsdm-datamodel"
		credentialIdForHana = "728ae367-5e9b-4731-b750-63def955323a"
	}
    stages {
        stage('Build') {
        	steps {
        		script {
					echo "extracting the version from mta.yaml"
					env.WORKSPACE = pwd()
					def mta = readFile "${env.WORKSPACE}/mta.yaml"
					/* expecting the following format in the mta.yaml
						_schema-version: x.x.x.x
						version: 1.12.321
						modules:
					*/
					if (mta.contains("_schema-version")){
						def mtaSubString = mta.substring(mta.indexOf("_schema-version") + 15)
						if (mta.contains("version")){
							echo "version found"
							mtaSubString = mtaSubString.substring(mtaSubString.indexOf("version") + 8)
							env.VERSIONMAJOR = mtaSubString.substring(0, mtaSubString.indexOf(".")).trim()
							mtaSubString = mtaSubString.substring(mtaSubString.indexOf(".") + 1)
							env.VERSIONMINOR = mtaSubString.substring(0, mtaSubString.indexOf("."))
							mtaSubString = mtaSubString.substring(mtaSubString.indexOf(".") + 1)
							env.VERSIONPATCH = mtaSubString.substring(0, mtaSubString.indexOf("modules:")).trim()
							echo "Major version number: ${env.VERSIONMAJOR}"
							echo "Minor version number: ${env.VERSIONMINOR}"
							echo "Patch version number: ${env.VERSIONPATCH}"
							env.BUILDNAME = "${projectName}_${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONPATCH}"
						}else{
							error("Version number not found")
						}
					}else{
						error("Schema version number not found")
					}
					echo "Building: ${BUILDNAME}"
					sh 'java -version'
                    sh 'mbt build --platform=xsa --mtar="${BUILDNAME}.mtar"'
				}
            }
            
        }
        
		stage('Deploy'){
			steps{
				script {
					withCredentials([usernamePassword(credentialsId: "${credentialIdForHana}", passwordVariable: 'passwordCredential', usernameVariable: 'userCredential')]) {
					    sh '/opt/xs-client/bin/xs login -a "https://10.1.5.17:30030/" -u "${userCredential}" -p "${passwordCredential}" -s "${hanaSpace}" --skip-ssl-validation' 
					}

					sh '/opt/xs-client/bin/xs deploy "${WORKSPACE}/mta_archives/${BUILDNAME}.mtar" -f'
				}
			}
		}
		stage('Archive') {
            steps {
                script {
                    nexusArtifactUploader artifacts: [[artifactId: "${projectName}", classifier: '', file: "${WORKSPACE}/mta_archives/${BUILDNAME}.mtar", type: 'mtar']], credentialsId: 'nexusCredentials', groupId: 'com.adweko.fsdm', nexusUrl: '10.1.1.10:8081/', nexusVersion: 'nexus3', protocol: 'http', repository: 'hana-repo', version: "${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONPATCH}"
                }
            }
        }
    }

    post {
        failure {
            emailext (
                    subject: "Jenkins FAILED job \'${env.JOB_NAME} [${env.BUILD_NUMBER}]\'",
                    mimeType: 'text/html',
                    body: "<p>FAILED Job ${env.JOB_NAME}  ${env.BUILD_NUMBER}:</p><p>Check console output at &QUOT;<a href=\"${env.BUILD_URL}\">${env.BUILD_URL}</a>&QUOT;</p>",
                    attachLog: true,
                    recipientProviders: [culprits(), requestor(), upstreamDevelopers(), developers()]
            )
        }
    }
}